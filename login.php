<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2012 osCommerce

  Released under the GNU General Public License
*/
// Function to get the client ip address
function get_client_ip_env() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
  require('includes/application_top.php');

// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
  if ($session_started == false) {
    if ( !isset($HTTP_GET_VARS['cookie_test']) ) {
      $all_get = tep_get_all_get_params();

      tep_redirect(tep_href_link(FILENAME_LOGIN, $all_get . (empty($all_get) ? '' : '&') . 'cookie_test=1', 'SSL'));
    }

    tep_redirect(tep_href_link(FILENAME_COOKIE_USAGE));
  }
if (isset($HTTP_GET_VARS['action']) && ($HTTP_GET_VARS['action'] == 'process')) {
    $username = tep_db_prepare_input($HTTP_POST_VARS['email_address']);
    $password = tep_db_prepare_input($HTTP_POST_VARS['password']);
    $check_query = tep_db_query("
      select
          id,
          user_name,
          full_name,
          user_password,
          role_id,
          link_to,
          block_ip,
          status
      from
        " . TABLE_ADMINISTRATORS . "
      where
        user_name = '" . tep_db_input($username) . "'
            and
        status = 1
    ");
    if (tep_db_num_rows($check_query) == 1) {
        $check = tep_db_fetch_array($check_query);
        if (tep_validate_password($password, $check['user_password'])) {
            $user_name = $check['user_name'];
            $full_name = $check['full_name'];
            $role_id = $check['role_id'];
            $id = $check['id'];
            $link_to = $check['link_to'];
            $block_ip = $check['block_ip'];
            // check if has set block ip address
            if(!empty($block_ip)){
                $ip = get_client_ip_env();
                if($ip == $block_ip){
                    tep_session_register('user_name');
                    tep_session_register('full_name');
                    tep_session_register('id');
                    tep_session_register('role_id');
                    tep_session_register('link_to');
                    tep_session_register('invoiceNo');
                    tep_session_register('customerCode');
                    tep_session_register('receiptNo');
                    tep_session_register('quotationNo');
                    tep_redirect(tep_href_link('#/dashboard'));
                }else {
                    $messageStack->add('login', 'Error: Invalid IP Address.');
                }
            } else {
                tep_session_register('user_name');
                tep_session_register('full_name');
                tep_session_register('id');
                tep_session_register('receiptNo');
                tep_session_register('quotationNo');
                tep_session_register('customerCode');
                tep_session_register('invoiceNo');
                tep_session_register('role_id');
                tep_session_register('link_to');
                tep_redirect(tep_href_link('#/dashboard'));
            }
        }else {
            $messageStack->add('login', 'Error: Invalid administrator login attempt.');
        }
    }else {
        $messageStack->add('login', 'Error: Invalid administrator login attempt.');
    }
}
  require(DIR_WS_LANGUAGES . $language . '/' . FILENAME_LOGIN);

  $breadcrumb->add(NAVBAR_TITLE, tep_href_link(FILENAME_LOGIN, '', 'SSL'));

//  require(DIR_WS_INCLUDES . 'template_top.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/login/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/login/assets/css/form-elements.css">
    <link rel="stylesheet" href="css/login/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="<?php echo $company['logo'];?>">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="css/login/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="css/login/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="css/login/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="css/login/assets/ico/apple-touch-icon-57-precomposed.png">

</head>

<body>
<!-- Top content -->
<div class="top-content">

    <div class="inner-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>
                                <?php
                                    echo $company['company_name'];
                                ?>
                            </h3>
                            <p>Enter your username and password to log on:</p>
                        </div>
                        <div class="form-top-right">
                            <img src="<?php echo $company['logo'];?>" width="100px"/>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                            if ($messageStack->size('login') > 0) {
                                echo $messageStack->output('login');
                            }
                        ?>
                    </div>
                    <div class="form-bottom">

                            <?php echo tep_draw_form('login', tep_href_link(FILENAME_LOGIN, 'action=process', 'SSL'), 'post', 'autocomplete="off" class="login-form"', true); ?>
                                <div class="form-group">
                                    <label class="sr-only" for="form-username">Email Address</label>
                                    <input type="text"
                                        required
                                        name="email_address"
                                        placeholder="User Name..."
                                        class="form-username form-control"
                                        id="inputEmail"/>
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="form-password">Password</label>
                                    <input type="password"
                                        name="password"
                                        placeholder="Password..."
                                        class="form-password form-control"
                                        id="inputPassword"
                                        required/>
                                </div>
                                <button type="submit" class="btn">Sign in!</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>


<!-- Javascript -->
<script src="css/login/assets/js/jquery-1.11.1.min.js"></script>
<script src="css/login/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="css/login/assets/js/jquery.backstretch.min.js"></script>
<script src="css/login/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="css/login/assets/js/placeholder.js"></script>
<![endif]-->

</body>

</html>
<?php
  require(DIR_WS_INCLUDES . 'application_bottom.php');
?>
