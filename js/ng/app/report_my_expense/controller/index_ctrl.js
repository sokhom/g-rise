app.controller(
    'my_expense_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.headers = ['Expense Date','Invoice No', 'Doctor Name', 
                'Customer Name', 'Note', 'Total', 'Payment Type', 'Payment Amount'];
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.loading = true;
            vm.init = function(params){
                if (!vm.from_date || !vm.to_date) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    filter_by_owner: true
                };
                vm.loading = false;
                vm.csv = [];
                Restful.get('api/DoctorExpense', data).success(function(data){
                    vm.expense = data;console.log(data);
                    vm.loading = true;
                    vm.count = data.count;
                    vm.total_payment_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_payment_amount = vm.total_payment_amount + obj.grand_total;
                    }
                    // angular.forEach(data.elements, function(value, key) {
                    //     vm.csv.push({
                    //         expense_date: vm.service.dateFormat( value.expense_date),
                    //         invoice_no: value.invoice_no,
                    //         doctor_name: value.doctor_name,
                    //         customer_name: value.customer_name,
                    //         note: value.note,
                    //         total: value.total,
                    //         payment_type: value.payment_type,
                    //         payment_amount: value.grand_total
                    //     });
                    // });
                });

            };

        }
    ]);
