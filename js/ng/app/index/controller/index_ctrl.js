app.controller(
	'index_ctrl', [
	'$scope'
	, 'Restful'
	, function ($scope, Restful){
		$scope.$on('$viewContentLoaded', function() {   
			//TODO logic for after view is loaded
		});
		var vm = this;
		vm.title = 'Dashboard';
		vm.saleBestByItemFromDate = moment().startOf('month').format("YYYY-MM-DD");
		vm.saleBestByItemToDate = moment().endOf('month').format("YYYY-MM-DD");
		vm.saleBestByServiceFromDate = moment().startOf('month').format("YYYY-MM-DD");
		vm.saleBestByServiceToDate = moment().endOf('month').format("YYYY-MM-DD");
		vm.expenseFromDate = moment().startOf('month').format("YYYY-MM-DD");
		vm.expenseToDate = moment().endOf('month').format("YYYY-MM-DD");
		vm.options =
		{
			scales:
			{
				xAxes: [{
					display: false
				}],
			},
		}

		function init(){
			Restful.get('api/Index').success(function(data){
				vm.index = data;
			});				
		};
		init();

		vm.getSaleBestByService = function(){
			vm.saleBestLabelByService = [];	
			vm.saleBestDataByService = [];
			var params = {from_date: vm.saleBestByServiceFromDate, to_date: vm.saleBestByServiceToDate};
			Restful.get('api/SaleBestByService', params).success(function(result){
				result.elements.map(function(item, index){
					vm.saleBestLabelByService.push(item.product_name);
					vm.saleBestDataByService.push(item.total_sale);
				});
			});		
		};
		// vm.getSaleBestByService();
		vm.getSaleBestByItem = function(){
			vm.saleBestLabelByItem = [];
			vm.saleBestDataByItem = [];
			var params = {from_date: vm.saleBestByItemFromDate, to_date: vm.saleBestByItemToDate};
			Restful.get('api/SaleBestByItem', params).success(function(result){				
				result.elements.map(function(item, index){
					vm.saleBestLabelByItem.push(item.product_name);
					vm.saleBestDataByItem.push(item.total_sale);
				});
			});	
		}
		// vm.getSaleBestByItem();
		vm.year = new Date().getFullYear();
		vm.incomeChartOptions = { legend: { display: true } };
		vm.incomeStatement = function(){
			vm.labels = [];
			vm.series = ['Income', 'Expense'];
			vm.data = [[],[]];
			
			Restful.get('api/IncomeGrahpic', {year: vm.year}).success(function(result){
				result.map(function(item, index){
					vm.labels.push(item.month);
					vm.data[0].push(parseFloat(item.income).toFixed(2));
					vm.data[1].push(parseFloat(item.expense).toFixed(2));
					//console.log(item);
				});
			});
		};
		// get Expense grah
		vm.expenseChartOptions = { legend: { display: true } };
		vm.getExpenseGrap = function(){
			vm.expenseLabel = [];
			vm.expenseData = [];
			var params = {from_date: vm.expenseFromDate, to_date: vm.expenseToDate};
			Restful.get('api/ExpenseGrahpic', params).success(function(result){				
				result.elements.map(function(item, index){
					vm.expenseLabel.push(item.name);
					vm.expenseData.push(item.amount);
				});
			});	
		}
		
		// vm.getExpenseGrap();

		vm.increase = function(){
			vm.year++;
			vm.incomeStatement();
		};
		vm.decrease = function(){
			vm.year--;
			vm.incomeStatement();
		};


	}
]);
