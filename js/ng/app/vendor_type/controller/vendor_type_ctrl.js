app.controller(
    'vendor_type_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/VendorType/';
            vm.loading = false;
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.vendorType = data;console.log(data);
                    vm.totalItems = data.count;
                    vm.loading = false;
                });
            };
            vm.init();
            /**
             * start functionality pagination
             */
            var params = {};
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    console.log(data);
                    vm.service.alertMessageSuccess();
                });
            };

            vm.search = function(id){
                params.name = vm.search_name;
                params.id = vm.search_id;
                vm.init(params);
                console.log(params);
            };

        }
    ]);