app.controller(
    'report_customer_balance_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = false;
            vm.headers = ['InvoiceNo', 'Invoice Date', 'Customer Name', 'Customer Code',
                'Telephone', 'Balance'];
            // vm.from_date = moment().format('YYYY-MM-DD');
            // vm.to_date = moment().format('YYYY-MM-DD');
            vm.init = function(){
                // if($scope.report.$invalid){
                //     return;
                // }
                vm.csv = [];
                vm.loading = true;
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    customer_id: vm.customerObj != null  ? vm.customerObj.id : '',
                    balance: 'yes',
                    status: 1
                };
                Restful.get('api/Invoice/', data).success(function(data){
                    vm.invoice = data;
                    vm.totalItems = data.count;
                    vm.total_balance = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_balance = vm.total_balance + obj.balance;
                    }

                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            Name: value.invoice_no,
                            stock_out_date: value.invoice_date,
                            customer_name: value.customer_name,
                            customer_code: value.customer_code,
                            customer_telephone: value.customer_tel,
                            balance: value.balance,
                        });
                    });
                }).finally(function(){
                    vm.loading = false;
                });
            };
            vm.init();
        }
    ]);