app.controller(
    'staff_type_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorType/';
            vm.model = {};
            if($state.current.name == 'staff_type.edit') {
                Restful.get(url + $stateParams.id).success(function (data) {
                    vm.model = data.elements[0];
                });
            }

            vm.disable = true;
            vm.save = function(){
                if (!$scope.doctorForm.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        $state.go('staff_type');
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                        $state.go('staff_type');
                    });
                }
            };
    }]);