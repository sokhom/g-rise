app.controller(
    'staff_type_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$translate'
        , 'SweetAlert'
        , function ($scope, Restful, Services, $translate, SweetAlert){
            'use strict';
            var vm = this;
            vm.service = new Services();
            vm.params = {pagination: 'yes'};
            var url = 'api/DoctorType/';
            vm.loading = false;
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.loading = false;
                    vm.doctorType = data;
                    vm.totalItems = data.count;
                });
            };
            vm.init(vm.params);
            /**
             * start functionality pagination
             */

            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init(vm.params);
            };

            vm.search = function(){
                vm.params.name = vm.search_name;
                vm.init(vm.params);
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    vm.service.alertMessageSuccess();
                });
            };

            vm.delete = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.name }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete(url + params.id, params ).success(function(data) {
                            console.log(data);
                            if(data === 'success'){
                                vm.init();
                            }
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: data === 'success' ? $translate.instant('SuccessDelete') : $translate.instant('RecordUsing'), 
                                type: data === 'success' ? "success" : 'error',
                                timer: 1000
                            });
                        });
                    }
                }); 
            };
        }
    ]);