app.controller(
    'complaint_resolve_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/Complaint/';
            vm.init = function(params) {
                Restful.get(url + $stateParams.id).success(function (data) {
                    vm.model = data.elements[0];
                    vm.model.resolve_date = moment().format("YYYY-MM-DD");
                    console.log(vm.model);
                });
            };
            vm.init();

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.model.resolve_by = $scope.appSession.id;
                vm.model.resolve_by_name = $scope.appSession.user_name;
                console.log(vm.model);
                vm.disable = false;
                Restful.put( url + vm.model.id, vm.model).success(function (data) {
                    console.log(data);
                    $state.go('complaint.list');
                    vm.service.alertMessageSuccess();
                    vm.disable = true;
                });
            };

        }
    ]);