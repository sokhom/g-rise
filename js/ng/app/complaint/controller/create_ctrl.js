app.controller(
    'complaint_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/Complaint/';
            vm.model = {
                complaint_date: moment().format("YYYY-MM-DD")
            };

            vm.init = function(params) {
                if ($state.current.name == 'complaint.edit'){
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.customer = {
                            id: vm.model.customer_id,
                            customer_name: vm.model.customer_name
                        };
                        vm.invoice = {invoice_no: vm.model.invoice_no};
                    });
                }
            };
            vm.init();

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.model.customer_id = vm.customer.id;
                vm.model.customer_name = vm.customer.customer_name ? vm.customer.customer_name : vm.customer.customers_first_name + (vm.customer.customers_last_name ? ' & ' + vm.customer.customers_last_name : '');
                vm.model.invoice_no = vm.invoice.invoice_no;
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        console.log(data);
                        $state.go('complaint.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        $state.go('complaint.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };

        }
    ]);