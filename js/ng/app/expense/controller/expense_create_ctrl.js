app.controller(
    'expense_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            vm.model = {
                expense_date: moment().format("YYYY-MM-DD")
            };
            var url = 'api/Expense/';
            vm.init = function(params) {
                if ($state.current.name == 'expense.edit'){
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        console.log(vm.model);
                    });
                }
                Restful.get('api/ExpenseType', {status: 1}).success(function (data) {
                    vm.expenseList = data.elements;
                    console.log(vm.expenseList);
                });
            };
            vm.init();

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        console.log(data);
                        $state.go('expense.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        $state.go('expense.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };

        }
    ]);