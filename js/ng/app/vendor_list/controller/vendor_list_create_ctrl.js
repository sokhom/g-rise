app.controller(
    'vendor_list_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            vm.model = {country: 'Cambodia'};
            var url = 'api/VendorList/';
            var params = {paginate: 'yes'};
            vm.init = function(params){
                if($state.current.name == 'vendor_list.edit') {
                    params.id = $stateParams.id;
                    Restful.get(url, params).success(function (data) {
                        vm.model = data.elements[0];
                    });
                }
                Restful.get('api/VendorType').success(function(data){
                    vm.supplierType = data.elements;
                });
            };
            vm.init(params);
            vm.disable = true;
            vm.save = function(){
                if (!$scope.vendorForm.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                        $state.go('vendor_list.list');
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                        $state.go('vendor_list.list');
                    });
                }
            };

        }
    ]);