app.controller(
    'vendor_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/VendorList/';
            vm.params = {paginate: 'yes'};
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.vendorList = data;
                    vm.totalItems = data.count;
                    vm.loading = false;
                });
            };
            vm.init(vm.params);
            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init(vm.params);
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {console.log(data);
                    vm.service.alertMessageSuccess();
                });
            };

            vm.search = function(){
                vm.params.name = vm.search_name;
                vm.init(vm.params);
                console.log(vm.params);
            };

        }
    ]);