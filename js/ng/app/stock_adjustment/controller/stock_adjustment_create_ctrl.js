app.controller(
    'stock_adjustment_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $translate){
            var vm = this;
            vm.service = new Services();
            vm.model = {
                adjustment_date: moment().format('YYYY-MM-DD'),
                total_adjustment: 0,
            };
            
            //init get um type
            Restful.get('api/UMType').success(function(data){
                vm.um_types = data.elements;
            });

            vm.getDefaultItem = function(){
                $timeout(function() {
                    console.log(vm.product_filter);
                    vm.costOfItem = angular.copy(vm.product_filter.products_price_in);
                    vm.add();
                }, 50);
            };

            var initGetBranch = function(){
                Restful.get('api/Branch', {status: 1}).success(function (data) {
                    console.log(data);
                    vm.branchs = data.elements;
                });
            };
            initGetBranch();

            /** Functionality for update model change when UM update **/
            vm.updateModel = function(params){
                params.um_type_id = params.um_type_collection.id;
                params.um_type_name = params.um_type_collection.name;
                params.um_type_amount = params.um_type_collection.amount;
                console.log(params);
            };

            
            vm.listData = [];
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.total_adjustment = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    obj.balance_qty = obj.adjust_qty ? Math.abs(obj.adjust_qty) : 0;
                    vm.model.total_adjustment = vm.model.total_adjustment + obj.balance_qty;
                }
            };

            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.listData.length == 0){
                    return vm.service.alertMessageError($translate.instant('RecordNotFound'));
                }
                vm.loading = true;
                var data = {
                    model: vm.model,
                    model_detail: vm.listData
                };
                vm.loading = true;
                console.log(data);
                Restful.save('api/AdjustmentStock', data).success(function (data) {
                    console.log(data);                    
                    vm.service.alertMessageSuccess();
                    clear();
                    clearProduct();
                }).finally(function(){
                    vm.loading = false;
                });
            };

            function clearProduct(){
                vm.model = {
                    adjustment_date: moment().format('YYYY-MM-DD'),
                    total_adjustment: 0,
                };
                vm.listData = [];
                $scope.form.$submitted = false;
            };

            // Functionality for scan barcode input
            vm.filterProduct = function(){
                var params = {barcode: vm.product_barcode, status: '1'};
                Restful.get('api/Products', params).success(function(data) {
                    vm.product_barcode = '';
                    if(data.count > 0){
                        vm.product_filter = data.elements[0];
                        vm.add();
                    } else{
                        vm.service.alertMessageError($translate.instant('RecordNotFound'));
                    }
                });
            };

            vm.add = function(){
                // check if exist in list
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    if (obj.product_id === vm.product_filter.id) {
                        // obj.unit_price = vm.product_filter.products_price_in;
                        // obj.description = vm.product_filter.products_description;
                        // obj.barcode = vm.product_filter.barcode;
                        // obj.total = obj.qty * obj.unit_price;
                        vm.getTotal();
                        clear();
                        return;
                    }
                }
                vm.qty = 0;
                var priceIn = vm.product_filter.products_price_in ? vm.product_filter.products_price_in : 0;
                var priceOut = vm.product_filter.products_price_out ? vm.product_filter.products_price_out : 0;
                vm.listData.push({
                    product_id: vm.product_filter.id,
                    product_name: vm.product_filter.products_name,
                    barcode: vm.product_filter.barcode,
                    product_quantity: vm.product_filter.products_quantity,
                    product_image: vm.product_filter.products_image,
                    adjust_qty: vm.qty,
                    product_price_in: priceIn,
                    product_price_out: priceOut,
                    product_type_name: vm.product_filter.products_type_fields[0].name,
                    product_type_id: vm.product_filter.products_type_fields[0].id,
                    product_kind_of: vm.product_filter.products_kind_of,
                    um_type_collection: {
                        id: vm.product_filter.um_type_id,
                        amount:  vm.product_filter.um_detail[0].amount,
                    },
                    product_um_type_id: vm.product_filter.um_detail[0].id,
                    product_um_type_name: vm.product_filter.um_detail[0].name,
                    product_um_type_amount: vm.product_filter.um_detail[0].amount,
                    um_type_id: vm.product_filter.um_detail[0].id,
                    um_type_name: vm.product_filter.um_detail[0].name,
                    um_type_amount: vm.product_filter.um_detail[0].amount,
                    //get price in from item to store in data to calculate average cost
                });
                console.log(vm.listData);
                
                vm.getTotal();
                clear();
            };

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                vm.getTotal();
            };

            function clear(){
                vm.product_filter = '';
                vm.product_barcode = '';
            };

        }
]);