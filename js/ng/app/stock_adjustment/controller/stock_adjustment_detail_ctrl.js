app.controller(
    'stock_adjustment_detail_ctrl', [
        '$scope'
        , 'Restful'
        , '$stateParams'
        , 'Services'
        , function ($scope, Restful, $stateParams, Service) {
            var vm  = this;
            vm.service = new Service();
            var params = {id: $stateParams.id};
            function initDetail(params){
                Restful.get('api/AdjustmentStock', params).success(function(data){
                    console.log(data);
                    vm.model = data.elements[0];
                });
            };
            initDetail(params);
        }
    ]);