app.controller(
    'customer_list_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , 'Upload'
        , '$translate'
        , '$timeout'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, Upload, $translate, $timeout){
            'use strict';
            $anchorScroll();
            var vm = this;
            vm.service = new Services();
            vm.model = {
                title1: 'Miss',
                customers_country: 'Cambodia',
                customers_documents: [],
                customers_photo: 'images/Custom-Icon.png',
                title: 'Mr'
            };
            var url = 'api/CustomerList/';

            vm.generateCode = function(){
                vm.disabled = true;
                Restful.get('api/GenerateCustomerCode').success(function(data){
                    vm.model.customers_code = data.code;
                    vm.disabled = false;
                });
            };

            vm.init = function(){
                if($state.current.name == 'customer_list_edit') {
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.loadingDocument = true;
                        vm.model.staff_responsible = {
                            id: vm.model.staff_responsible,
                            name: vm.model.staff_responsible_detail.length > 0 ? vm.model.staff_responsible_detail[0].name: ''
                        };
                        
                        vm.location = {
                            id: vm.model.location_id,
                            name: vm.model.location_name
                        };
                        vm.locationType = {
                            id: vm.model.location_type_id,
                            name: vm.model.location_type_name
                        };
                        console.log(data);
                        // Restful.get("api/CustomerDocument",{customer_id: $stateParams.id}).success(function (data) {
                        //     vm.model.customers_documents = data.elements;
                        //     vm.loadingDocument = false;
                        // });
                    });
                }
                // start init package type
                Restful.get('api/PackageType', {status : 1}).success(function(data){
                    vm.packageList = data.elements;
                });
                // start init customer type
                Restful.get('api/CustomerType', {status : 1}).success(function(data){
                    vm.customerType = data;
                });
                // start init member ship
                Restful.get('api/MemberShip', {status : 1}).success(function(data){
                    vm.memberShipList = data;
                });
                // start init customer type
                Restful.get('api/Branch', {status : 1}).success(function(data){
                    vm.branchs = data;
                });
                // start init location
                vm.getLocation();
                
            };
            
            vm.getLocation = function(){
                $timeout(function(){
                    Restful.get('api/Location', {status : 1, type: vm.locationType ? vm.locationType.id : ''}).success(function(data){
                        vm.locationList = data;
                    });
                }, 100);                
            };
            
            vm.init();

            vm.disable = true;
            vm.save = function(){
                if (!$scope.customerListCreate.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.disable = false;
                console.log(vm.model);
                vm.model.staff_responsible = vm.model.staff_responsible ? vm.model.staff_responsible.id : '';//reassign id
                // assign location
                vm.model.location_id = vm.location ? vm.location.id : '';
                vm.model.location_name = vm.location ? vm.location.name: '';
                vm.model.location_type_id = vm.locationType ? vm.locationType.id: '';
                vm.model.location_type_name = vm.locationType ? vm.locationType.name : '';

                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        console.log(data);
                        if(data === 'error'){
                            vm.disable = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            // $state.go('customer_list');
                        }
                    }).finally(function(){
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        // $state.go('customer_list');
                        // vm.service.alertMessageSuccess();       
                        if(data.error){
                            vm.disable = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            // $state.go('customer_list');
                        }                
                    }).finally(function(){
                        vm.disable = true;
                    });
                }
            };

            vm.showUploadPhoto = function(){
                $('#crop-image-popup').modal('show');
            };

            vm.showSnapPhoto = function(){
                vm.hide = true;
                $('#snap-photo-popup').modal('show');
            };

            vm.showDocumentSnapPhoto = function(){
                vm.hideDocument = true;
                $('#document-popup').modal('hide');
                $('#upload-document-popup').modal('show');
            };
            vm.removeFile = function($index){
                vm.model.customers_documents.splice($index, 1);
            };

            vm.upload = function (file) {
                vm.loadingUpload = true;
                vm.progressPercentage = 0;
                Upload.upload({
                    url: 'api/ImageUpload',
                    data: {file: file, 'username': $scope.username}
                }).then(function (resp) {
                    console.log(resp);
                    vm.loadingUpload = false;
                    vm.customer_document.image = resp.data.image;
                    console.log('Success ' + resp.data.image + 'uploaded. Response: ' + resp.data);
                }, function (resp) {
                    console.log('Error status: ' + resp.status);
                }, function (evt) {
                    vm.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    console.log('progress: ' + vm.progressPercentage + '% ' + evt.config.data.file.name);
                });
            };

            // vm.initStaffInfo = function(){
            //     Restful.get("api/DoctorList").success(function (data) {
            //         console.log(data);
            //         vm.doctors = data.elements;
            //     });
            // };
            // vm.initStaffInfo();
            vm.customer_document = {date: moment().format("YYYY-MM-DD")};
            vm.saveCustomerDocument = function(){
                if (!$scope.customerDocument.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.customer_document.check_by_staff_id = vm.staff.id;
                vm.customer_document.check_by_staff_name = vm.staff.name;
                if(vm.editCustomerDocumentIndex != null ){
                    vm.model.customers_documents[vm.editCustomerDocumentIndex] = vm.customer_document;
                }else{
                    vm.model.customers_documents.push(vm.customer_document);
                }
                console.log(vm.customer_document);
                console.log(vm.model.customers_documents);
                // clear after add 
                vm.customer_document = {date: moment().format("YYYY-MM-DD")};
                vm.staff = null;
                $("#document-popup").modal("hide");
                vm.editCustomerDocumentIndex = null;
            };

            vm.addDoc = function(){
                vm.staff = null;
            };

            vm.editCustomerDocument = function(row, index){
                vm.editCustomerDocumentIndex = index;
                $("#document-popup").modal("show");
                vm.staff = {id: row.check_by_staff_id, name: row.check_by_staff_name};
                vm.customer_document = row;
                console.log(row);
            }
        }
    ]);