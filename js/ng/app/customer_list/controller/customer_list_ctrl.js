app.controller(
    'customer_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$translate'
        , 'SweetAlert'
        , '$sce'
        , function ($scope, Restful, Services, $anchorScroll, $translate, SweetAlert, $sce){
            'use strict';
            $anchorScroll();
            var vm = this;
            vm.loading = false;
            vm.service = new Services();
            var url = 'api/CustomerList/';
            vm.params = {};
            vm.typeCode = "has code";
            vm.init = function(){
                vm.params.type_code = vm.typeCode;
                vm.loading = true;
                Restful.get(url, vm.params).success(function(data){
                    vm.totalItems = data.count;
                    vm.loading = false;
                    console.log(data);
                    vm.customerList = data;
                });
            };
            vm.init();

            vm.search = function(){
                vm.params.name = vm.name;
                vm.params.id = vm.id;
                vm.params.telephone = vm.telephone;
                vm.init();
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    vm.service.alertMessageSuccess();
                });
            };

            vm.delete = function(params){
                var name = (params.customers_last_name ? " And " + params.customers_last_name : '');
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.customers_first_name + name }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete(url + params.id, params ).success(function(data) {
                            vm.init();
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: $translate.instant('SuccessDelete'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };


            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init();
            };

            vm.displayImage = function(params){
                var img = '<img src="'+params.customers_photo+'" class="img-customer"/>';
                return img;
            };

            //  Start Functionality for follow up 
            vm.getIdFollowUp = function(params){
                vm.currentCustomer = params;
                vm.id = params.id;
                vm.description = "";
                for(var i =0;i < vm.customerList.elements.length;i++){
                    var item = vm.customerList.elements[i];
                    if(item.id === params.id){
                        item.isOpen = true;
                    }else{
                        item.isOpen = false;
                    }
                }
            };
            vm.html = 'myPopoverTemplate.html';
            vm.saveFollowUp = function(){
                if(!vm.id && (!vm.description || vm.description != '')) return;
                vm.disable = true;
                vm.input = {customer_id: vm.id, description: vm.description};
                Restful.save('api/CustomerFollowUp/', vm.input ).success(function(data) {
                    vm.service.alertMessageSuccess();
                    vm.description = "";
                    vm.id = "";
                    vm.currentCustomer.isOpen = false;
                    vm.disable = false;
                    console.log(data);
                });
            }
        }
    ]);