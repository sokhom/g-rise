app.controller(
    'customer_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$stateParams'
        , '$translate'
        , 'SweetAlert'
        , function ($scope, Restful, Services, $anchorScroll, $stateParams, $translate, SweetAlert){
            'use strict';
            $anchorScroll();
            var vm = this;
            vm.loading = false;
            vm.service = new Services();
            var url = 'api/CustomerList/';
            vm.init = function(){
                vm.loading = true;
                Restful.get(url, {id: $stateParams.id}).success(function(data){
                    vm.totalItems = data.count;
                    vm.loading = false;
                    console.log(data);
                    vm.model = data.elements[0];
                    // Restful.get("api/CustomerDocument",{customer_id: $stateParams.id}).success(function (data) {
                    //     vm.model.customers_documents = data.elements;
                    //     console.log(data);
                    // });
                });
            };
            vm.init();

            vm.delete = function(params, index){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.description }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete('api/CustomerFollowUp/' + params.id, params).success(function(data) {
                            vm.model.customer_follow_up.splice(index, 1);
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: $translate.instant('SuccessDelete'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };
        }
    ]);