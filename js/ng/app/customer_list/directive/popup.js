app.directive('uploadDocument', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                docs: '=',
                isHide: '=',
            },
            require: ['?ngModel'],
            templateUrl: 'js/ng/app/customer_list/views/upload-document.html',
            controller: 'uploadDocument as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('uploadDocument', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        $scope.isHide = false;
        $scope.$watch('isHide', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.isHide != newValue) {
                $scope.isHide = newValue;
            }
        });
        $scope.$watch('docs', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.docs != newValue) {
                $scope.docs = newValue;
            }
        });
        var _video = null, patData = null;

        $scope.patOpts = { x: 0, y: 0, w: 25, h: 25 };

        // Setup a channel to receive a video property
        // with a reference to the video element
        // See the HTML binding in main.html
        $scope.channel = {};

        $scope.webcamError = false;
        $scope.onError = function (err) {
            console.log(err);
            $scope.$apply(
                function () {
                    $scope.webcamError = err;
                }
            );
        };

        $scope.onSuccess = function () {
            // The video element contains the captured camera data
            _video = $scope.channel.video;
            $scope.$apply(function () {
                $scope.patOpts.w = _video.width;
                $scope.patOpts.h = _video.height;
                //$scope.showDemos = true;
            });
        };

        $scope.onStream = function (stream) {
            // You could do something manually with the stream.
        };

        $scope.makeSnapshot = function () {
            if (_video) {
                var patCanvas = document.querySelector('#snapshot');
                if (!patCanvas) return;
                patCanvas.width = _video.width;
                patCanvas.height = _video.height;
                var ctxPat = patCanvas.getContext('2d');

                var idata = getVideoData($scope.patOpts.x, $scope.patOpts.y, $scope.patOpts.w, $scope.patOpts.h);
                ctxPat.putImageData(idata, 0, 0);

                $scope.resultImage = patCanvas.toDataURL();
                patData = idata;

                vm.loading = true;
                Restful.save( 'api/ImageUploadBase64' , {fileName: $scope.resultImage}).success(function (data) {
                    // console.log($scope.docs);
                    // console.log(data);
                    // if(!$scope.docs){
                    //     $scope.docs = [];
                    // }
                    // $scope.docs.push({name:data.fileName});
                    
                    $('#upload-document-popup').modal('hide');
                    $('#document-popup').modal('show');
                    $scope.docs = data.fileName;
                }).finally(function(){
                    vm.loading = false;
                    $scope.isHide = false;
                });
                
            }
        };

        vm.cancel = function(){
            $scope.isHide = false;
            $('#document-popup').modal('show');
        };

        var getVideoData = function getVideoData(x, y, w, h) {
            var hiddenCanvas = document.createElement('canvas');
            hiddenCanvas.width = _video.width;
            hiddenCanvas.height = _video.height;
            var ctx = hiddenCanvas.getContext('2d');
            ctx.drawImage(_video, 0, 0, _video.width, _video.height);
            return ctx.getImageData(x, y, w, h);
        };

    }
]);
