app.controller(
    'stock_adjustment_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Adjustment No', 'Title', 'Adjustment Date', 'Reason',
                'Remark', 'Total Amount'];
            vm.init = function(){
                vm.csv = [];
                vm.loading = false;
                vm.total_amount = 0;
                var params = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    filter: vm.filters
                };
                Restful.get('api/AdjustmentStock', params).success(function(data){
                    vm.adjustStocks = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.total_amount += value.total_adjustment;
                        vm.csv.push({
                            adjustment_no: value.adjustment_no,
                            title: value.title,
                            adjustment_date: value.adjustment_date,
                            reason: value.reason,
                            remark: value.remark,
                            total_adjustment: value.total_adjustment,
                        });
                    });
                }).finally(function(response){
                    vm.loading = true;
                });
            };

        }
    ]);