app.controller(
    'transfer_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['TransferNO', 'Transfer To', 'Transfer Type', 'Transfer Date', 'Description',
                'Total'];
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            // get transfer Type
            Restful.get('api/TransferType').success(function(data){
                vm.transferTypes = data.elements;
                console.log(vm.transferTypes);
            });
            // get branch
            Restful.get('api/Branch').success(function(data){
                vm.branchs = data.elements;
                console.log(data);
            });

            vm.init = function(){
                vm.csv = [];
                vm.loading = false;
                vm.total_amount = 0;
                var params = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    reference_no: vm.filters,
                    transfer_type_id: vm.transferTypeId,
                    branch_id: vm.branchId,
                    status: 1
                };
                Restful.get('api/Transfer', params).success(function(data){
                    vm.transfers = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.total_amount += value.total;
                        vm.csv.push({
                            transferNo: value.transfer_no,
                            transferTo: value.transfer_branch_name,
                            transferType: value.transfer_type_name,
                            transferDate: value.transfer_date,
                            description: value.description,
                            total: value.total,
                        });
                    });
                }).finally(function(response){
                    vm.loading = true;
                });
            };

        }
    ]);