app.controller(
    'staff_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$translate'
        , 'SweetAlert'
        , function ($scope, Restful, Services, $translate, SweetAlert){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorList/';
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.doctorList = data;
                    vm.loading = false;
                    vm.totalItems = data.count;
                });
                Restful.get('api/DoctorType', {status: 1}).success(function(data){
                    vm.doctorType = data;
                });
            };
            var params = {pagination: 'yes'};
            vm.init(params);
            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.search = function(id){
                params.name = vm.search_name;
                params.id = vm.search_id;
                vm.init(params);
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    console.log(data);
                    vm.service.alertMessageSuccess();
                });
            };

            vm.delete = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.name }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete(url + params.id, params ).success(function(data) {
                            console.log(data);
                            vm.init();
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: $translate.instant('SuccessDelete'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };

        }
    ]);