app.controller(
    'staff_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            vm.model = {
                sex: 'Male',
                use_filter: true
            };
            var url = 'api/DoctorList/';
            vm.init = function(params) {
                if ($state.current.name == 'staff_list.edit'){
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.use_filter = vm.model.use_filter ? true : false;
                        console.log(vm.model);
                    });
                }
                Restful.get('api/DoctorType', {status: 1}).success(function(data){
                    vm.doctorType = data;
                });
            };
            vm.init();


            vm.disable = true;
            vm.save = function(){
                if (!$scope.doctorForm.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.model.use_filter = vm.model.use_filter ? 1 : 0;
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        console.log(data);
                        $state.go('staff_list.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        $state.go('staff_list.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };

        }
    ]);