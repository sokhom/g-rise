app.controller(
	'add_on_type_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/AddOnType/';
		vm.model = {};
		
		vm.init = function(params){
			vm.loading = true;
			Restful.get(url, params).success(function(data){
                vm.collections = data;
                vm.totalItems = data.count;
			}).finally(function(res){
				vm.loading = false;
			});
		};
		var params = {paginate:'yes'};
		vm.init(params);

		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				vm.service.alertMessageSuccess();
				console.log(data);
			});
		};

		vm.search = function(id){
			params.filter = vm.search_name;
			vm.init(params);
		};

		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
            vm.init(params);
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#add-on-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.addOnTypeForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
                    vm.init();
					vm.service.alertMessageSuccess();
					$('#add-on-popup').modal('hide');
                    vm.clear();
					vm.disable = true;
					console.log(data);
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
                    vm.init();
					$('#add-on-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
					vm.disable = true;
					console.log(data);
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.addOnTypeForm.$submitted = false;
            vm.model = {};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.name }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(url + params.id, params ).success(function(data) {
						console.log(data);
						if(data === 'success'){
							vm.init();
						}
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: data === 'success' ? $translate.instant('SuccessDelete') : $translate.instant('RecordUsing'), 
							type: data === 'success' ? "success" : 'error',
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);