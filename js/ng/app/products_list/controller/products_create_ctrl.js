app.controller(
    'products_list_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, $translate, $rootScope){
            'use strict';
            var vm = this;
            $anchorScroll();
            vm.model = {
                products_kind_of: 'item', 
                is_sale: true,
                products_image: "images/item-icon.png"
            };
            vm.service = new Services();
            var url = 'api/Products/';
            vm.init = function(params){
                if($state.current.name == 'product_list.edit') {
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        console.log(data);
                        vm.model.um_sale_detail = vm.model.um_sale_detail[0];
                        vm.model.um_detail =  vm.model.um_detail[0];
                        vm.model.is_sale = vm.model.is_sale == 1 ? true: false;
                    });
                }
                Restful.get('api/ProductType').success(function(data){
                    vm.ProductType = data;
                });
                Restful.get('api/UMType').success(function(data){
                    vm.um_types = data.elements;
                });
            };
            var params = {pagination: 'yes'};
            vm.init(params);

            vm.disable = true;
            vm.save = function(){
                if (!$scope.productListCreate.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.model.um_type_sale_id = vm.model.um_sale_detail ? vm.model.um_sale_detail.id : '';
                vm.model.um_type_id =  vm.model.um_detail ? vm.model.um_detail.id : '';
                vm.model.is_sale = vm.model.is_sale ? 1 : 0;
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        // call from parent scope function in main.js
						$rootScope.$emit("InitNotificationMethod", {});
                        console.log(data);
                        if(data == 'Duplicate'){
                            vm.disable = true;
                            vm.duplicate = 'Duplicate Barcode.';
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('product_list.list');
                        }
                    }).finally(function(){
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        // call from parent scope function in main.js
						$rootScope.$emit("InitNotificationMethod", {});
                        console.log(data);
                        if(data.error){
                            vm.duplicate = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('product_list.list');
                        }
                    }).finally(function () {
                        vm.disable = true;
                    });
                }
            };

            vm.showUploadPhoto = function(){
                $('#crop-image-popup').modal('show');
            };

            vm.showSnapPhoto = function(){
                vm.hide = true;
                $('#snap-photo-popup').modal('show');
            };

        }
    ]);