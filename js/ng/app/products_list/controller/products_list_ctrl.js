app.controller(
    'products_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$translate'
        , function ($scope, Restful, Services, $translate){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/Products/';
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.productsList = data;
                    vm.totalItems = data.count;
                }).finally(function(){
                    vm.loading = false;
                });
            };
            var params = {pagination: 'yes',kind_of: 'item'};
            
            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    vm.service.alertMessageSuccess();
                });
            };
            vm.translateKindOf = function(text){
                return $translate.instant(text);
            };
            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.search = function(){
                params.name = vm.filterText;
                vm.init(params);
            };

            vm.checkPermission = function(string){
                var found = 0;
                if($scope.featureRole){
                    angular.forEach($scope.featureRole.permission, function(value, index){
                        //console.log(value.feature_name);
                        //console.log(index);
                        if (value.feature_name == string){
                            found = value.is_selected;
                            return;
                        }
                    });
                }
                return found;
            };
            vm.init(params);
        }
    ]);