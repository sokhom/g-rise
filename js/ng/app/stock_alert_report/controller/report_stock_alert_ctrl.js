app.controller(
    'report_stock_alert_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Item', 'Type', 'KindOf', 'Barcode',
                'Description', 'Qty On Hand', 'Order Alert', 'Cost', 'Price'];
            vm.init = function(){
                vm.csv = [];
                var data = {
                    status: 1,
                    stock_alert: "yes",
                    name: vm.filters
                };
                vm.loading = false;
                vm.products = [];
                Restful.get('api/Products', data).success(function(data){
                    vm.products = data;
                    vm.totalItems = data.elements.length;
                    console.log(vm.totalItems);
                    vm.loading = true;
                    vm.total_qty = 0;
                    vm.total_price_in = 0;
                    vm.total_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_qty = vm.total_qty + (obj.qty_on_hand + obj.stock_in - obj.stock_out);
                        vm.total_price_in = vm.total_price_in + obj.cost;
                        vm.total_amount = vm.total_amount + ( (obj.qty_on_hand + obj.stock_in - obj.stock_out) * obj.cost);
                    }
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            item: value.products_name,
                            type: value.products_type_fields[0].name,
                            kind_of: value.products_kind_of,
                            barcode: value.barcode,
                            description: value.products_description,
                            qty_on_hand: value.products_quantity,
                            order_alert: value.order_alert,
                            cost: value.products_price_in,
							price: value.products_price_out,
                        });
                    });
                });
            };

            vm.init();
        }
]);