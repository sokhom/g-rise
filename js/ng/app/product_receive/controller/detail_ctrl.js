app.controller(
    'product_receive_detail_ctrl', [
        '$scope'
        , 'Restful'
        , '$stateParams'
        , 'Services'
        , function ($scope, Restful, $stateParams, Service) {
            var vm  = this;
            vm.service = new Service();
            var params = {id: $stateParams.id};
            function initDetail(params){
                Restful.get('api/ReceiveProduct/', params).success(function(data){
                    console.log(data);
                    vm.model = data.elements[0];
                });
            };
            initDetail(params);

            vm.print = function(){
                vm.invoice = vm.model.transfer_no;
                vm.copy = {
                    transfer: {
                        transfer_branch_name: vm.model.transfer_branch_name,
                        total: vm.model.total
                    },
                    transfer_detail: vm.model.transfer_detail
                };angular.copy(vm.model);
                $('#invoice-popup-transfer').modal('show');
            };
        }
    ]);