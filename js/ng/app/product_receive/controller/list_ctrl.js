app.controller(
    'product_receive_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , function ($scope, Restful, Services, SweetAlert, $translate){
            var vm = this;
            vm.service = new Services();
            var params = {pagination: 'yes'};
            vm.search = function(){
                params.reference_no = vm.reference_no;
                init(params);
            };

            function init(params){
                vm.loading = true;
                Restful.get('api/ReceiveProduct', params).success(function(data){
                    vm.model = data;
                    console.log(data);
                    vm.loading = false;
                    vm.totalItems = data.count;
                });
            };
            init(params);
            vm.currentPage = 1;
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                init(params);
            };

            vm.updateStatus = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.receive_product_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        params.status === 1 ? params.status = 0 : params.status = 1;
                        Restful.patch('api/ReceiveProduct/' + params.id, params ).success(function(data) {
                            console.log(data);
                            SweetAlert.swal({
                                title: $translate.instant('Disable'), 
                                text: $translate.instant('SuccessDisable'), 
                                type: "success",
                                timer: 1000
                            });
                        }); 
                    }
                });  
               
            };

        }
]);