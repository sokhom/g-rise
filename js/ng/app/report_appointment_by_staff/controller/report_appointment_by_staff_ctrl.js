app.controller(
    'report_appointment_by_staff_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['#', 'Customer Name', 'Gender', 'DOB', 'Telephone', 'Appointment Doctor',
                'Appointment Date', 'Description'];
            var params = {};
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.init = function(){
                vm.csv = [];
                params.appointment_alert = $stateParams.appointment_alert;
                params.from_date = vm.from_date;
                params.to_date = vm.to_date;
                params.status = 1;
                vm.loading = false;
                vm.appointmentList = [];
                Restful.get('api/AppointmentByStaff', params).success(function(data){
                    vm.appointmentList = data.elements.map(function(item){
                        item.appointment_period = new Date(item.appointment_period.replace(/-/g, "/"));
                        return item;
                    });
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.error = data.success;
                    vm.loading = true;
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            Id: key + 1,
                            Name: value.customer_detail[0].customers_name,
                            Gender: value.customer_detail[0].customers_gender,
                            Dob: value.customer_detail[0].customers_dob,
                            Telephone: value.customer_detail[0].customers_telephone,
                            DoctorName: value.doctor_detail[0].name,
                            AppointmentDate: value.appointment_date,
                            Description: value.description,
                        });
                    });
                });
            };
            console.log($stateParams);
            if($stateParams.data){
                vm.from_date = "";
                vm.to_date = "";
                vm.init();
            }
        }
    ]);