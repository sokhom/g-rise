app.controller(
    'service_list_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, $translate, $rootScope){
            'use strict';
            var vm = this;
            $anchorScroll();
            vm.model = {
                image: "images/item-icon.png",
                is_sale: true
            };
            vm.service = new Services();
            var url = 'api/Service/';
            vm.init = function(params){
                if($state.current.name == 'service_list.edit') {
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.is_sale = vm.model.is_sale == 1 ? true: false;
                    });
                }
                Restful.get('api/ServiceType').success(function(data){
                    vm.ProductType = data;
                });
            };
            var params = {pagination: 'yes'};
            vm.init(params);

            vm.disable = true;
            vm.save = function(){
                if (!$scope.productListCreate.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.model.is_sale = vm.model.is_sale ? 1: false;
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        // call from parent scope function in main.js
						//$rootScope.$emit("InitNotificationMethod", {});
                        console.log(data);
                        if(data == 'Duplicate'){
                            vm.disable = true;
                            vm.duplicate = 'Duplicate Barcode.';
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('service_list.list');
                        }
                    }).finally(function(){
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        // call from parent scope function in main.js
						//$rootScope.$emit("InitNotificationMethod", {});
                        console.log(data);
                        if(data.error){
                            vm.duplicate = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('service_list.list');
                        }
                    }).finally(function () {
                        vm.disable = true;
                    });
                }
            };

            vm.showUploadPhoto = function(){
                $('#crop-image-popup').modal('show');
            };

            vm.showSnapPhoto = function(){
                vm.hide = true;
                $('#snap-photo-popup').modal('show');
            };

        }
    ]);