app.controller(
    'transfer_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $translate){
            var vm = this;
            vm.service = new Services();
            //vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 'BIDC', 'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank'];
            vm.model = {
                transfer_date: moment().format('YYYY-MM-DD'),
                //payment_method: 'Cash',
                total: 0,
                //payment: '',
               // remain: 0
            };
            vm.getDefaultItem = function(){
                $timeout(function() {
                    console.log(vm.product_filter);
                    vm.costOfItem = angular.copy(vm.product_filter.products_price_in);
                    //vm.add();
                    Restful.get('api/Products', {id: vm.product_filter.id}).success(function(data) {
                    if(data.count > 0){
                        vm.product_filter = data.elements[0];
                        vm.add();
                    } else{
                        vm.service.alertMessageError($translate.instant('RecordNotFound'));
                    }
                });
                }, 50);
            };
            var initSetting = function(){
                Restful.get('api/Branch', {status: 1}).success(function (data) {
                    console.log(data);
                    vm.branchs = data.elements;
                });
                //init get um type
                Restful.get('api/UMType').success(function(data){
                    vm.um_types = data.elements;
                });
                // get transfer type
                Restful.get("api/TransferType").success(function(data){
                    vm.transferType = data.elements;
                });
            };
            initSetting();

            vm.listData = [];
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.total = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    var price = obj.unit_price ? obj.unit_price : 0;
                    var qty = (obj.qty * obj.um_type_collection.amount / obj.product_um_type_amount);
                    vm.model.total = vm.model.total + (qty * price);
                }
                vm.model.total.toFixed(2);
                //vm.model.remain = vm.total;
                //vm.input();
            };

            vm.save = function(){
                if (!$scope.form.$valid) {
                    //$anchorScroll();
                    return;
                }
                if(vm.listData.length == 0){
                    return vm.service.alertMessageError($translate.instant('RecordNotFound'));
                }
                vm.loading = true;
                vm.model.transfer_branch_id = vm.selectBranch.id;
                vm.model.transfer_branch_name = vm.selectBranch.name;
                vm.model.transfer_type_id = vm.selectTransferType.id;
                vm.model.transfer_type_name = vm.selectTransferType.name;
                var data = {
                    transfer: vm.model,
                    transfer_detail: vm.listData
                };
                //copy object to print
                vm.copy = angular.copy(data);
                vm.loading = true;
                console.log(data);
                Restful.save('api/Transfer', data).success(function (data) {
                    console.log(data);
                    vm.invoice = data.transfer_no;
                    vm.loading = false;
                    $('#invoice-popup-transfer').modal('show');
                    vm.service.alertMessageSuccess();
                    clear();
                    clearProduct();
                });
            };

            function clearProduct(){
                vm.model = {
                    transfer_date: moment().format('YYYY-MM-DD'),
                    description: '',
                    total: 0,
                    // payment: '',
                    // remain: 0
                };
                vm.listData = [];
                vm.selectBranch = "";
                $scope.form.$submitted = false;
            };

            // Functionality for scan barcode input
            vm.filterProduct = function(){
                var params = {barcode: vm.product_barcode, status: '1'};
                Restful.get('api/Products', params).success(function(data) {
                    vm.product_barcode = '';
                    if(data.count > 0){
                        vm.product_filter = data.elements[0];
                        vm.add();
                    } else{
                        vm.service.alertMessageError($translate.instant('RecordNotFound'));
                    }
                });
            };

            vm.add = function(){
                vm.qty_in_hand = vm.product_filter.products_quantity;
                // check if qty has in stock add
                if(vm.qty_in_hand > 0) {
                    // check if exist in list
                    for (var i = 0, l = vm.listData.length; i < l; i++) {
                        var obj = vm.listData[i];
                        if (obj.product_id === vm.product_filter.id) {
                            obj.qty = obj.qty + 1;
                            obj.unit_price = vm.product_filter.products_price_in;
                            obj.description = vm.product_filter.products_description;
                            obj.barcode = vm.product_filter.barcode;
                            obj.total = obj.qty * obj.unit_price;
                            vm.getTotal();
                            clear();
                            return;
                        }
                    }
                    vm.qty = 1;
                    var price = vm.product_filter.products_price_in ? vm.product_filter.products_price_in : 0
                    vm.listData.push({
                        product_id: vm.product_filter.id,
                        product_name: vm.product_filter.products_name,
                        description: vm.product_filter.products_description,
                        barcode: vm.product_filter.barcode,
                        qty: vm.qty,
                        unit_price: price,
                        price_out: vm.product_filter.products_price_out_whole_sale ? vm.product_filter.products_price_out_whole_sale : 0,
                        product_type: vm.product_filter.products_type_fields[0].name,
                        product_type_id: vm.product_filter.products_type_fields[0].id,
                        qty_on_hand: vm.product_filter.products_quantity,
                        //get price in from item to store in data to calculate average cost
                        price_in: vm.costOfItem,
                        total: vm.qty * parseInt(price),
                        um_type_collection: {
                            id: vm.product_filter.um_type_id,
                            amount:  vm.product_filter.um_detail[0].amount,
                        },
                        product_um_type_id: vm.product_filter.um_detail[0].id,
                        product_um_type_name: vm.product_filter.um_detail[0].name,
                        product_um_type_amount: vm.product_filter.um_detail[0].amount,
                        um_type_id: vm.product_filter.um_detail[0].id,
                        um_type_name: vm.product_filter.um_detail[0].name,
                        um_type_amount: vm.product_filter.um_detail[0].amount,
                    });
                    console.log(vm.listData);
                    console.log(vm.qty);
                    console.log(vm.product_filter.products_price_in);
                    vm.getTotal();
                    clear();
                }else{
                    return vm.service.alertMessage(
                        'warning:','OPP! Out Off Stock. You Have Only ' +
                        vm.qty_in_hand +' Unit In Stock.','warning'
                    );
                }
            };

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                vm.getTotal();
            };

            function clear(){
                vm.product_filter = '';
                vm.product_barcode = '';
            };

            /** Functionality for update model change when UM update **/
            vm.updateModel = function(params){
                params.um_type_id = params.um_type_collection.id;
                params.um_type_name = params.um_type_collection.name;
                params.um_type_amount = params.um_type_collection.amount;
                vm.getTotal();
            };
        }
]);