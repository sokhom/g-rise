app.controller(
    'report_sale_summary_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Invoice No', 'Invoice Date',
                'Discount Type', 'Discount Amount', 'Total Discount Amount',
                'Grand Total'];
            vm.init = function(){
                if (!$scope.report.$valid) {
                    return;
                }
                vm.csv = [];
                var data = {
                    from_date: vm.from_date,
                    status: 1,
                    to_date: vm.to_date,
                    customer_id: vm.customer ? vm.customer.id : ''
                };
                vm.loading = false;
                vm.stockOut = [];
                Restful.get('api/StockOut', data).success(function(data){
                    vm.stockOut = data;console.log(data);
                    vm.loading = true;
                    vm.grand_total = 0;
                    vm.sub_total = 0;
                    vm.discount_total_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.grand_total = vm.grand_total + obj.grand_total;
                        vm.discount_total_amount = vm.discount_total_amount + obj.discount_total_amount;
                    }
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            invoice_no: value.stock_out_no,
                            invoice_date: value.stock_out_date,
                            discount_type: value.discount_type,
                            discount_amount: value.discount_amount,
                            total_discount_amount: value.discount_total_amount,
                            grand_total: value.grand_total,
                        });
                    });console.log(vm.csv);
                });
            };


        }
]);