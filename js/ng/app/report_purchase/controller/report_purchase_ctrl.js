app.controller(
    'report_purchase_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.service = new Services();
            console.log($stateParams);
            vm.loading = true;
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.vendor = '';
            vm.init = function(){
                if (!$scope.report.$valid) {
                    return;
                }
                filter();
            };
            
            if($stateParams.date && $stateParams.s_id){
                vm.from_date = $stateParams.date;
                vm.to_date = $stateParams.date;
                vm.vendor = {id: $stateParams.s_id, name: $stateParams.s_name};
                filter();
            }

            function filter(){                
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    vendor_id: vm.vendor.id,
                    status: 1,
                    reference_no: $stateParams.invoice_no
                };
                console.log(data);
                vm.loading = false;
                vm.purchase = [];
                Restful.get('api/Purchase', data).success(function(data){
                    vm.purchase = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                });
            };
        }
        
]);