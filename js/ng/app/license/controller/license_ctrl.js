app.controller(
	'license_ctrl', [
	'$scope'
	, 'Restful'
	, function ($scope, Restful){
		'use strict';
		var url = 'api/Setting/';
		$scope.init = function(params){
			Restful.get(url, params).success(function(data){
				$scope.setting = data;
			});
		};
		$scope.init();
	}
]);