app.directive('reportLayout', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                fromDate: '=',
                toDate: '=',
                filter: '=',
                print: '=',
                export: '=',
                exportFileName: '=',
                onClick: '&'
            },
            require: ['?ngModel'],
            templateUrl: 'js/ng/app/core/directive/report-layout.html',
            controller: 'reportLayoutCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('reportLayoutCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.fromDate = $scope.fromDate ? $scope.fromDate : '';
        $scope.$watch('vm.fromDate', function (newValue, oldValue) {
            if ($scope.fromDate != newValue) {
                $scope.fromDate = newValue;
            }
        });
        $scope.$watch('fromDate', function (newValue, oldValue) {
            if (newValue != vm.fromDate) {
                vm.fromDate = newValue;
            }
        });

        vm.toDate = $scope.toDate ? $scope.toDate : '';
        $scope.$watch('vm.toDate', function (newValue, oldValue) {
            if ($scope.toDate != newValue) {
                $scope.toDate = newValue;
            }
        });
        $scope.$watch('toDate', function (newValue, oldValue) {
            if (newValue != vm.toDate) {
                vm.toDate = newValue;
            }
        });

    }
]);
