app.directive('onlyNumbers', function() {
    return function(scope, element, attrs) {
        var keyCode =
            [
                8,9,13,37,39,46,48,49,50,51,52,53,54,55,56,57,96,97,
                98,99,100,101,102,103,104,105,110,190
            ];
        element.bind("keydown", function(event) {
            if($.inArray(event.which,keyCode) == -1) {
                scope.$apply(function(){
                    scope.$eval(attrs.onlyNum);
                    event.preventDefault();
                });
                event.preventDefault();
            }
        });
    };
});

app.directive('emptyData', function() {
    return {
        restrict: 'AE',
        template: "<div class='alert alert-warning alert-dismissible'><strong>{{'Warning' | translate }}</strong> {{'RecordNotFound' | translate }}</div>",
        link: function ($scope, element, att) {

        }
    }
});
/**
 * location list directive
 */
app.directive('locationListDropDown', ['Restful',
    function (Restful) {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                required: '@required',
                ngChange: '&',
                name: '@name'
            },
            require: ['?ngModel'],
            controller: 'locationListCtrl as vm',
            template: '\
                <select class="form-control" data-ng-required="{{required}}" data-ng-name="{{name}}" \
                    data-ng-model="vm.localSelected" \
                    data-ng-options="x as x.name for x in vm.locationLists track by x.id">\
                    <option value="">{{"Select" | translate}}</option>\
                </select> \
            ',
            link: function ($scope, element, attrs) {}
        };
    }
]);
app.controller('locationListCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function($scope, Restful, $translate){    
        var vm = this;
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });
        vm.locationLists = [];
        vm.init = function (filterText) {
            var params = {status: 1};
            Restful.get('api/Location/', params).then(function(response) {
                vm.locationLists = response.data.elements;
            });
        };
        vm.init();    
    }
]);
/**
 * location type directive
 */
app.directive('locationTypeDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                required: '@required',
                ngChange: '&',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            controller: 'locationTypeCtrl as vm',
            template: '\
                <select class="form-control" data-ng-required="{{required}}" data-ng-name="{{name}}" \
                    data-ng-model="vm.localSelected" data-ng-change="ngChange()"\
                    data-ng-options="x as x.name for x in vm.locationTypeLists track by x.id">\
                    <option value="">{{"Select" | translate}}</option>\
                </select> \
            ',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('locationTypeCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function($scope, Restful, $translate){    
        var vm = this;
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.locationLists = [];
        vm.init = function (filterText) {
            var params = {status: 1};
            Restful.get('api/LocationType/', params).then(function(response) {
                vm.locationTypeLists = response.data.elements;
            });
        };
        vm.init();    
    }
]);
/**
 * customer list directive
 */
app.directive('customerListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                required: '@required',
                ngChange: '&',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            // templateUrl: 'js/ng/app/core/directive/customerListDropDown.html',
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.searchCustomer | translate}}" allow-clear="true">\
                        {{$select.selected.customer_name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.customerLists"\
                                       refresh="vm.bindCustomerList($select.search)"\
                                       refresh-delay="400">\
                        <span data-ng-bind-html="u.customer_name | highlight: $select.search"></span>\
                        <div>\
                            <small>\
                                {{"CustomerID" | translate}}:\
                                <span data-ng-bind-html="u.customers_code | highlight: $select.search"></span>,\
                                {{"Telephone" | translate}}:\
                                <span data-ng-bind-html="u.customers_telephone | tel | highlight: $select.search"></span>\
                            </small>\
                        </div>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'customerListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
/**
 * controller for customer list drop down
 */
app.controller('customerListDropDownCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function ($scope, Restful, $translate) {
        var vm = this;

        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        vm.searchCustomer =  'SearchCustomer';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.customerLists = [];

        vm.bindCustomerList = function (filterText) {
            var params = {filter: filterText, status: 1};
            return Restful.get('api/CustomerList/', params).then(function(response) {
                vm.customerLists = response.data.elements.map(function(item){
                    item.customer_name = item.customers_first_name + " " + (item.customers_last_name ? "& " + item.customers_last_name : '');
                    return item;
                });
            });
        }
    }
]);


/**
 * staff list directive
 */
app.directive('staffListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=',
                isActive: '=',
                useFilter: '=',
                required: '@required',
                filterType: '=',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '<ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        name="{{name}}"\
                        required="{{required}}">\
                <ui-select-match placeholder="{{ vm.searchText | translate}}"  allow-clear="true">\
                    {{$select.selected.name}}\
                </ui-select-match>\
                <ui-select-choices repeat="u as u in vm.staffLists"\
                                    refresh="vm.bindList($select.search)"\
                                    refresh-delay="400">\
                    <span data-ng-bind-html="u.name | highlight: $select.search"></span>\
                    <<small data-ng-bind-html="u.doctor_type[0].name | highlight: $select.search"></small>>\
                    <div>\
                        <small>\
                            {{"Gender" | translate}}:\
                            <span data-ng-bind-html="u.sex | highlight: $select.search"></span>,\
                            {{"Telephone" | translate}}:\
                            <span data-ng-bind-html="u.phone | tel | highlight: $select.search"></span>\
                        </small>\
                    </div>\
                </ui-select-choices>\
            </ui-select>',
            controller: 'staffListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
/**
 * staff list directive with multiple select tag
 */
app.directive('staffListMultipleDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=',
                isActive: '=',
                useFilter: '=',
                required: '@required',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '<ui-select multiple data-ng-model="vm.localSelected"\
                        theme="select2" \
                        name="{{name}}"\
                        required="{{required}}"\
                        ng-disabled="ctrl.disabled"\
                        sortable="true" close-on-select="false" style="width: 100%;">\
                    <ui-select-match placeholder="{{ vm.searchText | translate}}">\
                        {{$item.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.staffLists track by u.id"\
                        refresh="vm.bindList($select.search)"\
                        refresh-delay="400">\
                    <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                    <div>\
                        <small>\
                            {{"Gender" | translate}}:\
                            <span data-ng-bind-html="u.sex | highlight: $select.search"></span>,\
                            {{"Telephone" | translate}}:\
                            <span data-ng-bind-html="u.phone | tel | highlight: $select.search"></span>\
                        </small>\
                    </div>\
                </ui-select-choices>\
            </ui-select>',
            // template: '<ui-select theme="select2"\
            //             style="width: 100%;"\ 
            //             multiple \
            //             reset-search-input="true"\
            //             data-ng-model="vm.localSelected"\
            //             name="{{name}}"\
            //             required="{{required}}">\
            //     <ui-select-match placeholder="{{ vm.searchText | translate}}">\
            //         {{$select.selected.name}}\
            //     </ui-select-match>\
            //     <ui-select-choices repeat="u as u in vm.doctorLists track by u.id"\
            //                         refresh="vm.bindDoctorList($select.search)"\
            //                         refresh-delay="1000">\
            //         <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
            //         <div>\
            //             <small>\
            //                 {{"Gender" | translate}}:\
            //                 <span data-ng-bind-html="u.sex | highlight: $select.search"></span>,\
            //                 {{"Telephone" | translate}}:\
            //                 <span data-ng-bind-html="u.phone | tel | highlight: $select.search"></span>\
            //             </small>\
            //         </div>\
            //     </ui-select-choices>\
            // </ui-select>',
            controller: 'staffListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
/**
 * controller for staff list drop down
 */
app.controller('staffListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;

        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        vm.searchText = 'SearchStaff';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.staffLists = [];

        vm.bindList = function (filterText) {
            var params = {
                name: filterText, 
                status: $scope.isActive, 
                use_filter: $scope.useFilter,
                type: $scope.filterType
            };
            return Restful.get('api/DoctorList', params).then(function(response) {
                vm.staffLists = response.data.elements;
            });
        }
    }
]);




/********************************************
 * product list drop down directive
 *******************************************/
app.directive('productListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                productModel: '=',
                onChange: '&',
                filterByItem: '=',
                required: '@required',
                name: '@name',
                isSale: '=',
                id: '@id'
            },
            require: ['?ngModel'],
            // templateUrl: 'js/ng/app/core/directive/productListDropDown.html',
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="onChange();"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{ vm.searchProductName | translate}}">\
                        {{$select.selected.products_name}}\
                    </ui-select-match>\
                    <ui-select-null-choice>No person</ui-select-null-choice>\
                    <ui-select-choices repeat="u as u in vm.productLists track by $index"\
                                        refresh="vm.bindProductList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.products_name | highlight: $select.search"></div>\
                        <small>\
                            <i>\
                                <span>{{u.barcode}}</span>,\
                                <span>{{u.products_type_fields[0].name}}</span>\
                            </i>\
                        </small>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            //replace: true,
            controller: 'productListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);

/********************************************
 * controller for product list drop down
 *******************************************/
app.controller('productListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.searchProductName = 'SearchProductName';
        vm.localSelected = $scope.productModel ? $scope.productModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.productModel != newValue) {
                $scope.productModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('productModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.productLists = [];

        vm.bindProductList = function (filterText) {
            var params = {
                name: filterText, 
                is_sale: $scope.isSale,
                pagination: 'yes', 
                kind_of: $scope.filterByItem, 
                status: 1
            };
            return Restful.get('api/Products', params).then(function(response) {
                vm.productLists = response.data.elements;
            });
        }
    }
]);

/** ==========================================================
 * ====================== End ================================
 * ==========================================================*/

/********************************************
 * vendor list drop down directive
 *******************************************/
app.directive('vendorListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                vendorModel: '=',
                required: '@required',
                name: '@name',
                onChange: '&',
                id: '@id'
            },
            require: ['?ngModel'],
            templateUrl: 'js/ng/app/core/directive/vendorListDropDown.html',
            controller: 'vendorListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);

/********************************************
 * controller for vendorListDropDownCtrl drop down
 *******************************************/
app.controller('vendorListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;

        vm.localSelected = $scope.vendorModel ? $scope.vendorModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.vendorModel != newValue) {
                $scope.vendorModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('vendorModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.vendorLists = [];

        vm.bindVendorList = function (filterText) {
            var params = {name: filterText, paginate: 'yes', status: 1};
            return Restful.get('api/VendorList', params).then(function(response) {
                vm.vendorLists = response.data.elements;
            });
        }
    }
]);

/********************************************
 * invoice list drop down directive
 *******************************************/
app.directive('invoiceListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                invoiceModel: '=',
                required: '@required',
                name: '@name',
                onChange: '&',
                id: '@id'
            },
            require: ['?ngModel'],
            // templateUrl: 'js/ng/app/core/directive/invoiceListDropDown.html',
            template: '\
                <ui-select theme="select2"\
                    style="width: 100%;"\
                    reset-search-input="true"\
                    data-ng-model="vm.localSelected"\
                    data-ng-change="onChange()"\
                    name="{{name}}"\
                    required="{{required}}">\
                    <ui-select-match placeholder="{{ vm.search | translate}}">\
                        {{$select.selected.invoice_no}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.invoiceLists"\
                                        refresh="vm.bindInvoiceList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.invoice_no | highlight: $select.search"></div>\
                        <div>\
                            <small>\
                                {{"CustomerName" | translate}}:\
                                <span data-ng-bind-html="u.customer_name | highlight: $select.search"></span>\
                                (<span data-ng-bind-html="u.customer_code | highlight: $select.search"></span>),\
                                {{"Telephone" | translate}}:\
                                <span data-ng-bind-html="u.customer_tel | tel | highlight: $select.search"></span>\
                            </small>\
                        </div>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'invoiceListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);

/********************************************
 * controller for invoiceListDropDownCtrl drop down
 *******************************************/
app.controller('invoiceListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.search = "Search";
        vm.localSelected = $scope.invoiceModel ? $scope.invoiceModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.invoiceModel != newValue) {
                $scope.invoiceModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('invoiceModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.invoiceLists = [];

        vm.bindInvoiceList = function (filterText) {
            var params = {invoice_no: filterText, paginate: 'yes', status: 1};
            return Restful.get('api/Invoice', params).then(function(response) {
                vm.invoiceLists = response.data.elements;
                //console.log(response);
            });
        }
    }
]);


/********************************************
 * invoice Receive list drop down directive
 *******************************************/
app.directive('invoiceReceiveListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                invoiceModel: '=',
                required: '@required',
                name: '@name',
                onChange: '&',
                id: '@id'
            },
            require: ['?ngModel'],
            // templateUrl: 'js/ng/app/core/directive/invoiceReceiveListDropDown.html',
            template: '<ui-select theme="select2"\
                            style="width: 100%;"\
                            reset-search-input="false"\
                            data-ng-change="onChange()"\
                            data-ng-model="vm.localSelected"\
                            name="{{name}}"\
                            required="{{required}}">\
                    <ui-select-match placeholder="{{ vm.search | translate}}">\
                        {{$select.selected.receive_payment_no}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.invoiceReceiveLists"\
                                        refresh="vm.bindInvoiceReceiveList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.receive_payment_no | highlight: $select.search"></div>\
                        <div>\
                            <small>\
                                {{"CustomerName" | translate}}:\
                                <span data-ng-bind-html="u.customer_name | highlight: $select.search"></span>,\
                                {{"Telephone" | translate}}:\
                                <span data-ng-bind-html="u.customer_telephone | tel | highlight: $select.search"></span>\
                            </small>\
                        </div>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'invoiceReceiveListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);

/********************************************
 * controller for invoiceListDropDownCtrl drop down
 *******************************************/
app.controller('invoiceReceiveListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.search = 'Search';
        vm.localSelected = $scope.invoiceModel ? $scope.invoiceModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.invoiceModel != newValue) {
                $scope.invoiceModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('invoiceModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.invoiceReceiveLists = [];

        vm.bindInvoiceReceiveList = function (filterText) {
            var params = {payment_no: filterText, pagination: 'yes', status: 1};
            return Restful.get('api/ReceivePayment', params).then(function(response) {
                vm.invoiceReceiveLists = response.data.elements;
            });
        }
    }
]);
/***************************************
 * Directive for permission user role **
 ***************************************/
app.directive('rolePermission', ['Restful',
    function(Restful) {
        return {
            restrict: 'A',
            scope: {
                permission: '=',
                feature: '='
            },
            link: function ($scope, elem, attrs) {
                var found = false;
                var keepGoing = true;
                if($scope.feature){
                    angular.forEach($scope.feature.permission, function(value, index){
                        if(keepGoing) {
                            if (value.feature_name === $scope.permission){
                                found = value.is_selected;
                                keepGoing = false;
                                return;
                            }
                        }
                    });
                }
                if(found){
                    elem.show();
                }else{
                    elem.remove();
                }
            }
        }
    }
]);


/*****************************************
 * Directive for Service List Drop down **
 *****************************************/
app.directive('serviceListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=',
                onChange: '&',
                filterByItem: '=',
                required: '@required',
                name: '@name',
                isSale: '=',
                id: '@id'
            },
            require: ['?ngModel'],
            // templateUrl: 'js/ng/app/core/directive/productListDropDown.html',
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="onChange();"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{ vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-null-choice>No person</ui-select-null-choice>\
                    <ui-select-choices repeat="u as u in vm.serviceLists track by $index"\
                                        refresh="vm.bindServiceList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                        <small>\
                            <i>\
                                <span>{{u.code}}</span>,\
                                <span>{{u.type_detail[0].name}}</span>\
                            </i>\
                        </small>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            //replace: true,
            controller: 'serviceListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('serviceListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.search = 'SearchService';
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.serviceLists = [];

        vm.bindServiceList = function (filterText) {
            var params = {
                name: filterText, 
                is_sale: $scope.isSale,
                pagination: 'yes', 
                kind_of: $scope.filterByItem, 
                status: 1
            };
            return Restful.get('api/Service', params).then(function(response) {
                vm.serviceLists = response.data.elements;

            });
        }
    }
]);

/********************************
 * End Service List DropDown ****
 ********************************/

//******************************/
//#region **** Option Directive
app.directive('optionListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                isMultiObj: '=',
                required: '@required',
                ngChange: '&',
                filterType: '=',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        data-ng-if="!isMultiObj"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u.id as u in vm.collections"\
                                        refresh="vm.bindList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                    </ui-select-choices>\
                </ui-select>\
                <ui-select theme="select2"\
                        data-ng-if="isMultiObj"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.collections"\
                                        refresh="vm.bindList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'optionListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('optionListDropDownCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function ($scope, Restful, $translate) {
        var vm = this;
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        vm.search =  'SearchOption';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });
        vm.bindList = function (filterText) {
            var params = {filter: filterText, status: 1, type: $scope.filterType};
            return Restful.get('api/Options/', params).then(function(response) {
                vm.collections = response.data.elements;
            });
        }
    }
]);

//#endregion **** Option Directive


//******************************/
//#region **** Addon Directive
app.directive('addOnListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                required: '@required',
                isMultiObj: '=',
                ngChange: '&',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '\
                <ui-select theme="select2"\
                        data-ng-if="isMultiObj"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.collections"\
                                        refresh="vm.bindList($select.search)"\
                                        refresh-delay="500">\
                        <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                    </ui-select-choices>\
                </ui-select>\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        data-ng-if="!isMultiObj"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u.id as u in vm.collections"\
                                        refresh="vm.bindList($select.search)"\
                                        refresh-delay="500">\
                        <div data-ng-bind-html="u.name | highlight: $select.search"></div>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'addOnListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
app.controller('addOnListDropDownCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function ($scope, Restful, $translate) {
        var vm = this;
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        vm.search =  'SearchAddOn';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });
        vm.bindList = function (filterText) {
            var params = {filter: filterText, status: 1};
            return Restful.get('api/AddOn/', params).then(function(response) {
                vm.collections = response.data.elements;
            });
        }
    }
]);

//#endregion **** Addon Directive



//#region Package select
app.directive('packageListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=ngModel',
                required: '@required',
                ngChange: '&',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="ngChange()"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{vm.search | translate}}">\
                        {{$select.selected.name}}\
                    </ui-select-match>\
                    <ui-select-choices repeat="u as u in vm.collections"\
                                        refresh="vm.bindList($select.search)"\
                                        refresh-delay="800">\
                        <span data-ng-bind-html="u.name | highlight: $select.search"></span>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            controller: 'packageListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);
/**
 * controller for customer list drop down
 */
app.controller('packageListDropDownCtrl', [
    '$scope',
    'Restful',
    '$translate',
    function ($scope, Restful, $translate) {
        var vm = this;

        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';
        vm.search =  'SearchPackage';
        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });

        vm.bindList = function (filterText) {
            var params = {filter: filterText, status: 1};
            return Restful.get('api/Package/', params).then(function(response) {
                vm.collections = response.data.elements;
            });
        }
    }
]);



/********************************************
 * quotation drop down directive
 *******************************************/
app.directive('quotationListDropDown', [
    function () {
        return {
            restrict: 'AE',
            scope: {
                ngModel: '=',
                onChange: '&',
                required: '@required',
                name: '@name',
                id: '@id'
            },
            require: ['?ngModel'],
            template: '\
                <ui-select theme="select2"\
                        style="width: 100%;"\
                        reset-search-input="true"\
                        data-ng-model="vm.localSelected"\
                        data-ng-change="onChange();"\
                        name="{{name}}"\
                        required="{{required}}">\
                    <ui-select-match placeholder="{{ vm.searchName | translate}}">\
                        {{$select.selected.invoice_no}}\
                    </ui-select-match>\
                    <ui-select-null-choice>No person</ui-select-null-choice>\
                    <ui-select-choices repeat="u as u in vm.quotationLists track by $index"\
                                        refresh="vm.bindQuotationList($select.search)"\
                                        refresh-delay="400">\
                        <div data-ng-bind-html="u.invoice_no | highlight: $select.search"></div>\
                        <small>\
                            <i>\
                                <span>{{u.customer_name}}</span>,\
                                <span>({{u.customer_code}})</span>\
                            </i>\
                        </small>\
                    </ui-select-choices>\
                </ui-select>\
            ',
            //replace: true,
            controller: 'productListDropDownCtrl as vm',
            link: function ($scope, element, attrs) {
                //@todo
            }
        };
    }
]);

/********************************************
 * controller for product list drop down
 *******************************************/
app.controller('productListDropDownCtrl', [
    '$scope',
    'Restful',
    function ($scope, Restful) {
        var vm = this;
        vm.searchName = 'SelectQuotation';
        vm.localSelected = $scope.ngModel ? $scope.ngModel : '';

        //when local data is changed, update model from outside
        $scope.$watch('vm.localSelected', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if ($scope.ngModel != newValue) {
                $scope.ngModel = newValue;
            }
        });

        //when model from outside is changed, update local data
        $scope.$watch('ngModel', function (newValue, oldValue) {
            //check to make sure no loop cycle
            if (newValue != vm.localSelected) {
                vm.localSelected = newValue;
            }
        });


        vm.quotationLists = [];
        vm.bindQuotationList = function (filterText) {
            var params = {
                name: filterText, 
                paginate: 'yes', 
                status: 1
            };
            return Restful.get('api/Quotation', params).then(function(response) {
                vm.quotationLists = response.data.elements;
            });
        }
    }
]);
