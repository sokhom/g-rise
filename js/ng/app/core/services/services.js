app.service("Services", [
    'Restful',
    '$translate'
	, function(Restful, $translate) {
        var services = function(){
            var self = this;
        };

        services.prototype.dateFormat = function(date){
            var d = moment(date, 'DD-MM-YYYY');
            if(d.isValid()){
                return moment(date).format('DD-MM-YYYY');
            } else{
                return '';
            }
        };

        services.prototype.getDate = function(){
            return new Date();
        };

        // Functionality convert date string from api 
        // return new date of string 
        services.prototype.dateConvert = function(date){
            if(!date) return;
            var res = new Date(date.replace(/-/g, "/"));
            return res;
        };

        services.prototype.getExchangeRate = function(id) {
             return Restful.get('api/ExchangeRate', {get_last_record: 'get_last', id: id});
        };

        services.prototype.alertMessage = function(title, message, type){
            $.notify({
                title: '<i class="fa fa-check-circle"></i> <b>' + title + '</b>',
                message: message
            },{
                type: type
            });
        };

        services.prototype.alertMessageSuccess = function(){
            $.notify({
                title: '<i class="fa fa-check-circle"></i>',
                message: $translate.instant('SaveSuccess')
            },{
                type: 'success'
            });
        };

        services.prototype.alertMessageError = function(text){
            $.notify({
                title: '<i class="fa fa-warning"></i> <b> ' + $translate.instant("Warning") + ': </b>',
                message: text
            },{
                type: 'warning'
            });
        };
        services.prototype.alertMessageInfo = function(){
            $.notify({
                title: '<i class="fa fa-info-circle"></i> <b>Info: </b>',
                message: $translate.instant('SaveSuccess')
            },{
                type: 'info'
            });
        };

        services.prototype.getAge = function(dob){
            var year = moment().diff(dob, 'years');
            return year + ' ' +  $translate.instant('YearOld');
        };

        services.prototype.convertDateToUTC = function(date){
            var date = new Date(Date.parse(date.replace(/-/g, "/")));
            date = new Date(date.getTime() -(60000 * date.getTimezoneOffset()));
            return date;
        };

        services.prototype.getAgeWithNoString = function(dob){
            var year = moment().diff(dob, 'years');
            return parseInt(year);
        };
        return services;
        
 	}
]);
/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
app.filter('propsFilter', function() {
    return function(items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    }
});

/**
 * Angular Cache factory for cache storage or memory
 */
app.factory('stateCache', ['CacheFactory',
    function (CacheFactory) {
        var controllerCache = {
            memCache : {}
        };
        // Check to make sure the cache doesn't already exist
        if (!CacheFactory.get('memCache')) {
            controllerCache.memCache = CacheFactory('memCache', {
                deleteOnExpire: 'aggressive',
                //storageMode: 'localStorage'
            });
        }
        controllerCache.memCache = CacheFactory.get('memCache');
        return controllerCache;
    }
]);

/*
Automatic phone number formatting in input field

Credits:
    - Wade Tandy via
    http://stackoverflow.com/questions/19094150/using-angularjs-directive-to-format-input-field-while-leaving-scope-variable-unc
    
    - kstep via
    http://stackoverflow.com/questions/12700145/how-to-format-a-telephone-number-in-angularjs
    
    - hans via
    https://codepen.io/hans/details/uDmzf/
*/

app.directive('phoneInput', function($filter, $browser) {
    return {
        require: 'ngModel',
        link: function($scope, $element, $attrs, ngModelCtrl) {
            var listener = function() {
                var value = $element.val().replace(/[^0-9]/g, '');
                $element.val($filter('tel')(value, false));
            };

            // This runs when we update the text field
            ngModelCtrl.$parsers.push(function(viewValue) {
                return viewValue.replace(/[^0-9]/g, '').slice(0,10);
            });

            // This runs when the model gets updated on the scope directly and keeps our view in sync
            ngModelCtrl.$render = function() {
                $element.val($filter('tel')(ngModelCtrl.$viewValue, false));
            };

            $element.bind('change', listener);
            $element.bind('keydown', function(event) {
                var key = event.keyCode;
                // If the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
                // This lets us support copy and paste too
                if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)){
                    return;
                }
                $browser.defer(listener); // Have to do this or changes don't get picked up properly
            });

            $element.bind('paste cut', function() {
                $browser.defer(listener);
            });
        }

    };
});
app.filter('tel', function () {
    return function (tel) {        
        if (!tel) { return ''; }
        var value = tel.toString().trim().replace(/^\+/, '');
        if (value.match(/[^0-9]/)) {
            return tel;
        }
        var country, city, number;
        switch (value.length) {
            case 1:
            case 2:
            case 3:
                city = value;
                break;
            default:
                city = value.slice(0, 3);
                number = value.slice(3);
        }

        if(number){
            if(number.length>3){
                number = number.slice(0, 3) + '-' + number.slice(3,7);
            }
            else{
                number = number;
            }

            return ("(" + city + ") " + number).trim();
        }
        else{
            return "(" + city;
        }

    };
});