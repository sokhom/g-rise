app.service('dbService',[
	'$http',
	function($http) {
		return {
			getData: function($q, $http) {
				var defer = $q.defer();
				$http.get('api/GetUserInfo').success(function(data) {
					defer.resolve(data);
				});
				return defer.promise;
			}
		};
	}
]);


/**
 * Directive Define title of the page
 */
app.directive('updateTitle', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function(scope, element) {

        var listener = function(event, toState) {

          var title = 'Default Title';
          if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

          $timeout(function() {
            element.text(title);
          }, 0, false);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);
app.config([ '$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		var name = ' - SKWeb Solution';
		
		$stateProvider.state('license', {
			url: '/license',
			templateUrl: 'js/ng/app/license/views/index.html',
			controller: 'license_ctrl',
			data: { pageTitle: 'License' + name}
		});
	
		$stateProvider.
			state('dashboard', {
				url: '/dashboard',
				templateUrl: 'js/ng/app/index/partials/index.html',
				controller: 'index_ctrl as vm',
				data: { pageTitle: 'Dashboard' + name},
			})
			.state('journal_entry', {
				url: '/journal_entry',
				templateUrl: 'js/ng/app/journal/views/index.html',
				controller: 'journal_ctrl as vm',
				data: { pageTitle: 'Journal' + name},
			})
			.state('ledger_report', {
				url: '/ledger_report',
				templateUrl: 'js/ng/app/ledger_report/views/index.html',
				controller: 'ledger_report_ctrl as vm',
				data: { pageTitle: 'Ledger Report' + name},
			})
			.state('account_type', {
				url: '/account_type',
				templateUrl: 'js/ng/app/account_type/views/index.html',
				controller: 'account_type_ctrl as vm',
				data: { pageTitle: 'Account Type' + name},
			})
			// customer route
			.state('customer_list', {
				url: '/customer_list',
				templateUrl: 'js/ng/app/customer_list/views/index.html',
				controller: 'customer_list_ctrl as vm',
				data: { pageTitle: 'Customer List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('customer_list_create', {
                url: '/customer_list/create',
                templateUrl: 'js/ng/app/customer_list/views/create.html',
                controller: 'customer_list_create_ctrl as vm',
				data: { pageTitle: 'Customer List Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('customer_list_edit', {
                url: '/customer_list/edit/:id',
                templateUrl: 'js/ng/app/customer_list/views/create.html',
                controller: 'customer_list_create_ctrl as vm',
				data: { pageTitle: 'Customer List Edit' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('customer_detail', {
                url: '/customer/detail/:id',
                templateUrl: 'js/ng/app/customer_list/views/detail.html',
                controller: 'customer_detail_ctrl as vm',
                data: { pageTitle: 'Customer Detail Edit' + name},
            })
			.state('customer_type', {
				url: '/customer_type',
				templateUrl: 'js/ng/app/customer_type/views/index.html',
				controller: 'customer_type_ctrl as vm',
				data: { pageTitle: 'Customer Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful) {
						return Restful.get('api/GetUserInfo').success(function(data){
							return data;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements[0].role_detail[0].permission;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Type_View'){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('branch', {
				url: '/branch',
				templateUrl: 'js/ng/app/branch/views/index.html',
				controller: 'branch_ctrl as vm',
				data: { pageTitle: 'Branch' + name},
				resolve:{
					getDetail: ['Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Branch_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Branch_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('create_invoice', {
				url: '/create_invoice',
				templateUrl: 'js/ng/app/create_invoice/views/index.html',
				controller: 'create_invoice_ctrl as vm',
				data: { pageTitle: 'Create Invoice' + name},
			})
			/** customer received payment route **/
			.state('received_payment', {
				url: '/received_payment',
				template: '<div ui-view/>',
				redirectTo: 'received_payment.search',
				data: { pageTitle: 'Receive Payment' + name}
			})
			.state('received_payment.search', {
				url: '/',
				templateUrl: 'js/ng/app/received_payment/views/index.html',
				controller: 'received_payment_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Payment_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Payment_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('received_payment.create', {
				url: '/create',
				templateUrl: 'js/ng/app/received_payment/views/create.html',
				controller: 'received_payment_create_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Payment_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Payment_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('received_payment.detail', {
				url: '/detail/:id?invoice_no',
				templateUrl: 'js/ng/app/received_payment/views/detail.html',
				controller: 'received_payment_detail_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
			})
			/** end customer received payment route **/
			.state('service', {
				url: '/service',
				templateUrl: 'js/ng/app/service/views/index.html',
				controller: 'service_ctrl as vm',
				data: { pageTitle: 'Service' + name}
			})
			// user route
			.state('user', {
				url: '/user',
				templateUrl: 'js/ng/app/user/views/index.html',
				controller: 'user_ctrl as vm',
				data: { pageTitle: 'User' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["User_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'User_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			// staff route
			.state('staff', {
				url: '/staff',
				template: '<div ui-view/>',
				redirectTo: 'staff.list',
				data: { pageTitle: 'Staff List' + name}
			})
			.state('staff.list', {
				url: '/list',
				templateUrl: 'js/ng/app/staff_list/views/index.html',
				controller: 'staff_list_ctrl as vm',
				data: { pageTitle: 'Staff List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('staff.create', {
				url: '/create',
				templateUrl: 'js/ng/app/staff_list/views/create.html',
				controller: 'staff_create_ctrl as vm',
				data: { pageTitle: 'Create Staff List' + name}
			})
			.state('staff.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/staff_list/views/create.html',
				controller: 'staff_create_ctrl as vm',
				data: { pageTitle: 'Edit Staff List' + name}
			})
			// customer_birthday
			.state('customer-birthday', {
				url: '/customer-birthday',
				templateUrl: 'js/ng/app/customer_birthday/views/index.html',
				controller: 'customer_birthday_ctrl as vm',
				data: { pageTitle: 'Customer Birthday' + name}
			})
			// doctor type route
			.state('doctor_type', {
				url: '/staff_type',
				template: '<div ui-view/>',
				redirectTo: 'doctor_type.list'
			})
			.state('doctor_type.list', {
				url: '/',
				templateUrl: 'js/ng/app/doctor_type/views/index.html',
				controller: 'doctor_type_ctrl as vm',
				data: { pageTitle: 'Doctor Type' + name}
			})
			.state('doctor_type.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/doctor_type/views/create.html',
				controller: 'doctor_type_create_ctrl as vm',
				data: { pageTitle: 'Edit Doctor Type' + name}
			})
			.state('doctor_type.create', {
				url: '/create',
				templateUrl: 'js/ng/app/doctor_type/views/create.html',
				controller: 'doctor_type_create_ctrl as vm',
				data: { pageTitle: 'Create Doctor Type' + name}
			})
			/***********************************
			*****  Doctor list route ***********
		  	************************************/
			.state('doctor_list', {
				url: '/staff_list',
				template: '<div ui-view/>',
				redirectTo: 'doctor_list.list'
			})
			.state('doctor_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/doctor_list/views/index.html',
				controller: 'doctor_list_ctrl as vm',
				data: { pageTitle: 'Doctor List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_list.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/doctor_list/views/create.html',
				controller: 'doctor_create_ctrl as vm',
				data: { pageTitle: 'Edit Doctor List' + name}
			})
			.state('doctor_list.create', {
				url: '/create',
				templateUrl: 'js/ng/app/doctor_list/views/create.html',
				controller: 'doctor_create_ctrl as vm',
				data: { pageTitle: 'Create Doctor List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_List_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_list.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/doctor_list/views/detail.html',
				controller: 'doctor_detail_ctrl as vm',
				data: { pageTitle: 'Doctor Detail' + name}
			})
			/**************************
			 * Doctor expense router **
			 **************************/
			.state('doctor_expense', {
				url: '/staff_expense',
				redirectTo: 'doctor_expense.list',
				template: '<div ui-view=""/>'
			})
			.state('doctor_expense.list', {
				url: '/list',
				templateUrl: 'js/ng/app/doctor_expense/views/index.html',
				controller: 'doctor_expense_ctrl as vm',
				data: { pageTitle: 'Create Doctor Expense' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_Expense_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_Expense_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_expense.create', {
				url: '/create',
				templateUrl: 'js/ng/app/doctor_expense/views/create.html',
				controller: 'doctor_expense_create_ctrl as vm',
				data: { pageTitle: 'Create Doctor Expense' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_Expense_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_Expense_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_expense.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/doctor_expense/views/detail.html',
				controller: 'doctor_expense_detail_ctrl as vm',
				data: { pageTitle: 'View Detail Doctor Expense' + name}
			})
			.state('doctor_expense_report', {
				url: '/staff_expense_report',
				templateUrl: 'js/ng/app/doctor_expense_report/views/index.html',
				controller: 'doctor_expense_report_ctrl as vm',
				data: { pageTitle: 'Report Doctor Expense' + name}
			})
			/****************************
			 * Stock Adjustment router **
			 ****************************/
			.state('stock_adjustment', {
				url: '/stock_adjustment',
				redirectTo: 'stock_adjustment.list',
				template: '<div ui-view=""/>'
			})
			.state('stock_adjustment.list', {
				url: '/list',
				templateUrl: 'js/ng/app/stock_adjustment/views/index.html',
				controller: 'stock_adjustment_list_ctrl as vm',
				data: { pageTitle: 'Create stock adjustment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Stock_Adjustment_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Stock_Adjustment_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('stock_adjustment.create', {
				url: '/create',
				templateUrl: 'js/ng/app/stock_adjustment/views/create.html',
				controller: 'stock_adjustment_create_ctrl as vm',
				data: { pageTitle: 'Create stock adjustment' + name}
			})
			.state('stock_adjustment.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/stock_adjustment/views/create.html',
				controller: 'stock_adjustment_create_ctrl as vm',
				data: { pageTitle: 'Edit stock adjustment' + name}
			})
			.state('stock_adjustment.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/stock_adjustment/views/detail.html',
				controller: 'stock_adjustment_detail_ctrl as vm',
				data: { pageTitle: 'View Detail stock adjustment' + name}
			})
			.state('stock_adjustment.report', {
				url: '/report',
				templateUrl: 'js/ng/app/stock_adjustment_report/views/index.html',
				controller: 'stock_adjustment_report_ctrl as vm',
				data: { pageTitle: 'Report stock adjustment' + name}
			})
			/**************************
			 * Other Expense Type router **
			 **************************/
			.state('expense_type', {
				url: '/expense-type',
				redirectTo: 'expense_type.list',
				template: '<div ui-view=""/>'
			})
			.state('expense_type.list', {
				url: '/list',
				templateUrl: 'js/ng/app/expense_type/views/index.html',
				controller: 'expense_type_list_ctrl as vm',
				data: { pageTitle: 'Expense Type List' + name}
			})
			.state('expense_type.create', {
				url: '/create',
				templateUrl: 'js/ng/app/expense_type/views/create.html',
				controller: 'expense_type_create_ctrl as vm',
				data: { pageTitle: 'Create Expense Type' + name}
			})
			.state('expense_type.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/expense_type/views/create.html',
				controller: 'expense_type_create_ctrl as vm',
				data: { pageTitle: 'Edit Expense Type' + name}
			})
			.state('expense_type.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/expense_type/views/detail.html',
				controller: 'expense_type_detail_ctrl as vm',
				data: { pageTitle: 'View Detail Expense Type' + name}
			})
			/**************************
			 * Other Expense router **
			 **************************/
			.state('expense', {
				url: '/expense',
				redirectTo: 'expense.list',
				template: '<div ui-view=""/>'
			})
			.state('expense.list', {
				url: '/list',
				templateUrl: 'js/ng/app/expense/views/index.html',
				controller: 'expense_list_ctrl as vm',
				data: { pageTitle: 'Expense List' + name}
			})
			.state('expense.create', {
				url: '/create',
				templateUrl: 'js/ng/app/expense/views/create.html',
				controller: 'expense_create_ctrl as vm',
				data: { pageTitle: 'Create Expense' + name}
			})
			.state('expense.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/expense/views/create.html',
				controller: 'expense_create_ctrl as vm',
				data: { pageTitle: 'Edit Expense' + name}
			})
			.state('expense.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/expense/views/detail.html',
				controller: 'expense_detail_ctrl as vm',
				data: { pageTitle: 'View Detail Expense' + name}
			})
			.state('expense_report', {
				url: '/expense_report',
				templateUrl: 'js/ng/app/expense_report/views/index.html',
				controller: 'expense_report_ctrl as vm',
				data: { pageTitle: 'Report Expense' + name}
			})
			/*******************************
			 * Allowance Deduction router **
			 *******************************/
			.state('allowance_deduction', {
				url: '/allowance_deduction',
				redirectTo: 'allowance_deduction.list',
				template: '<div ui-view=""/>'
			})
			.state('allowance_deduction.list', {
				url: '/list',
				templateUrl: 'js/ng/app/allowance_deduction/views/index.html',
				controller: 'allowance_deduction_ctrl as vm',
				data: { pageTitle: 'Create allowance_deduction' + name}
			})
			.state('allowance_deduction.create', {
				url: '/create',
				templateUrl: 'js/ng/app/allowance_deduction/views/create.html',
				controller: 'allowance_deduction_create_ctrl as vm',
				data: { pageTitle: 'Create allowance_deduction' + name}
			})
			.state('allowance_deduction.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/allowance_deduction/views/create.html',
				controller: 'allowance_deduction_create_ctrl as vm',
				data: { pageTitle: 'Edit allowance_deduction' + name}
			})
			.state('allowance_deduction.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/allowance_deduction/views/detail.html',
				controller: 'allowance_deduction_detail_ctrl as vm',
				data: { pageTitle: 'View Detail allowance_deduction' + name}
			})
			/**************************
			 * Staff Birthday router **
			 **************************/
			.state('staff_birthday', {
				url: '/staff-bithday',
				templateUrl: 'js/ng/app/report_staff_birthday/views/index.html',
				controller: 'report_staff_birthday_ctrl as vm',
				data: { pageTitle: 'staff birthday' + name}
			})
			/**************************
			 * Doctor Payroll router **
			 **************************/
			.state('doctor_payslip', {
				url: '/staff_payroll',
				redirectTo: 'doctor_payslip.list',
				template: '<div ui-view=""/>'
			})
			.state('doctor_payslip.list', {
				url: '/list',
				templateUrl: 'js/ng/app/doctor_payslip/views/index.html',
				controller: 'doctor_payslip_list_ctrl as vm',
				data: { pageTitle: 'Create doctor_payslip' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_Payslip_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_Payslip_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_payslip.create', {
				url: '/create',
				templateUrl: 'js/ng/app/doctor_payslip/views/create.html',
				controller: 'doctor_payslip_create_ctrl as vm',
				data: { pageTitle: 'Create doctor_payslip' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_Payslip_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_Payslip_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_payslip.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/doctor_payslip/views/create.html',
				controller: 'doctor_payslip_create_ctrl as vm',
				data: { pageTitle: 'Edit doctor_payslip' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Doctor_Payslip_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Doctor_Payslip_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('doctor_payslip.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/doctor_payslip/views/detail.html',
				controller: 'doctor_payslip_detail_ctrl as vm',
				data: { pageTitle: 'View Detail doctor_payslip' + name}
			})
			.state('doctor_payslip_report', {
				url: '/staff_payroll_report',
				templateUrl: 'js/ng/app/doctor_payslip_report/views/index.html',
				controller: 'doctor_payslip_report_ctrl as vm',
				data: { pageTitle: 'Report doctor payslip' + name}
			})
			
			// service type route
			.state('service_type', {
				url: '/service_type',
				templateUrl: 'js/ng/app/service_type/views/index.html',
				controller: 'service_type_ctrl as vm',
				data: { pageTitle: 'Service Type' + name}
			})
			// product type route
			.state('product_type', {
				url: '/product_type',
				templateUrl: 'js/ng/app/products_type/views/index.html',
				controller: 'products_type_ctrl as vm',
				data: { pageTitle: 'Product Type' + name}
			})
			// UMTYPE Route setup
			.state('um_type', {
				url: '/um_type',
				templateUrl: 'js/ng/app/um_type/views/index.html',
				controller: 'um_type_ctrl as vm',
				data: { pageTitle: 'UM Type' + name},
			})
			/**
			 * Product List Route
			 */
			.state('product_list', {
				url: '/product_list',
				template: '<div ui-view/>',
				redirectTo: 'product_list.list'
			})
			.state('product_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/products_list/views/index.html',
				controller: 'products_list_ctrl as vm',
				data: { pageTitle: 'Product List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_List_View' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('product_list.create', {
                url: '/create',
                templateUrl: 'js/ng/app/products_list/views/create.html',
                controller: 'products_list_create_ctrl as vm',
				data: { pageTitle: 'Create Product List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {console.warn(getDetail.data);
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_List_Create' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('product_list.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/products_list/views/detail.html',
                controller: 'products_detail_ctrl as vm',
                data: { pageTitle: 'Create Product Detail' + name}
            })
            .state('product_list.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/products_list/views/create.html',
                controller: 'products_list_create_ctrl as vm',
                data: { pageTitle: 'Edit Product List' + name}
            })
			.state('product_report', {
				url: '/product-report',
				templateUrl: 'js/ng/app/product_report/views/index.html',
				controller: 'product_report_ctrl as vm',
				data: { pageTitle: 'Product Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			/**
			 * Route For Service List 
			 */
			.state('service_list', {
				url: '/service_list',
				template: '<div ui-view/>',
				redirectTo: 'service_list.list'
			})
			.state('service_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/service_list/views/index.html',
				controller: 'service_list_ctrl as vm',
				data: { pageTitle: 'Service List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Service_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Service_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('service_list.create', {
                url: '/create',
                templateUrl: 'js/ng/app/service_list/views/create.html',
                controller: 'service_list_create_ctrl as vm',
				data: { pageTitle: 'Create Service List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Service_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Service_List_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('service_list.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/service_list/views/detail.html',
                controller: 'service_detail_ctrl as vm',
                data: { pageTitle: 'Service Service Detail' + name}
            })
            .state('service_list.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/service_list/views/create.html',
                controller: 'service_list_create_ctrl as vm',
				data: { pageTitle: 'Edit Service List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Service_List_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Service_List_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('service_report', {
				url: '/service-report',
				templateUrl: 'js/ng/app/service_report/views/index.html',
				controller: 'service_report_ctrl as vm',
				data: { pageTitle: 'Service Report' + name},
			})
			/**
			 * Product List Sale Route
			 */
			.state('product_sale_list', {
				url: '/product_sale',
				template: '<div ui-view/>',
				redirectTo: 'product_sale_list.list'
			})
			.state('product_sale_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/sale_service/views/index.html',
				controller: 'sale_service_ctrl as vm',
				data: { pageTitle: 'Product Sale List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful) {
						return Restful.get('api/GetUserInfo').success(function(data){
							return data.elements[0].role_detail[0].permission;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements[0].role_detail[0].permission;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Sale_View'){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('product_sale_list.create', {
                url: '/create',
                templateUrl: 'js/ng/app/sale_service/views/create.html',
                controller: 'sale_service_create_ctrl as vm',
                data: { pageTitle: 'Create Product List' + name}
            })
			.state('product_sale_list.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/sale_service/views/create.html',
                controller: 'sale_service_create_ctrl as vm',
                data: { pageTitle: 'Edit Product List' + name}
            })
			.state('product_sale_list.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/sale_service/views/detail.html',
                controller: 'sale_service_detail_ctrl as vm',
                data: { pageTitle: 'View Product Sale List' + name}
            })
			/**** product transfter report ***/
			.state('product_transfer_report', {
				url: '/product-transfer-report',
				templateUrl: 'js/ng/app/transfer_report/views/index.html',
				controller: 'transfer_report_ctrl as vm',
				data: { pageTitle: 'Product Transfer Report' + name},
				
			})
			// transfer stock product list
			.state('product_transfer', {
				url: '/product_transfer',
				template: '<div ui-view/>',
				redirectTo: 'product_transfer.list',
				resolve:{
					getDetail: ['Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Transfer_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Transfer_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('product_transfer.list', {
				url: '/',
				templateUrl: 'js/ng/app/transfer/views/index.html',
				controller: 'transfer_list_ctrl as vm',
				data: { pageTitle: 'Product Transfer List' + name},
			})
			.state('product_transfer.create', {
                url: '/create',
                templateUrl: 'js/ng/app/transfer/views/create.html',
                controller: 'transfer_create_ctrl as vm',
                data: { pageTitle: 'Create Product Transfer' + name}
            })
			 .state('product_transfer.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/transfer/views/create.html',
                controller: 'transfer_create_ctrl as vm',
                data: { pageTitle: 'Edit Product Transfer' + name}
            })
			.state('product_transfer.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/transfer/views/detail.html',
                controller: 'transfer_detail_ctrl as vm',
                data: { pageTitle: 'View Product Transfer' + name}
            })
			// end route transfer


			/**** product receive report ***/
			.state('product_receive_report', {
				url: '/product-receive-report',
				templateUrl: 'js/ng/app/product_receive_report/views/index.html',
				controller: 'product_receive_report_ctrl as vm',
				data: { pageTitle: 'Product receive Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Receive_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Receive_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			// product receive route
			.state('product_receive', {
				url: '/product_receive',
				template: '<div ui-view/>',
				redirectTo: 'product_receive.list'
			})
			.state('product_receive.list', {
				url: '/',
				templateUrl: 'js/ng/app/product_receive/views/index.html',
				controller: 'product_receive_list_ctrl as vm',
				data: { pageTitle: 'Product Receive List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Receive_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Receive_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('product_receive.create', {
                url: '/create',
                templateUrl: 'js/ng/app/product_receive/views/create.html',
                controller: 'product_receive_create_ctrl as vm',
                data: { pageTitle: 'Create Product Receive' + name}
            })
			 .state('product_receive.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/product_receive/views/create.html',
                controller: 'product_receive_create_ctrl as vm',
                data: { pageTitle: 'Edit Product receive' + name}
            })
			.state('product_receive.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/product_receive/views/detail.html',
                controller: 'product_receive_detail_ctrl as vm',
                data: { pageTitle: 'View Product receive' + name}
            })
			// end route transfer

			
			/**
			 * START NEW ROUTE FOR TRNSFER TYPE
			 */
			.state('transfer_type', {
				url: '/transfer_type',
				template: '<div ui-view/>',
				redirectTo: 'transfer_type.list',
			})
			.state('transfer_type.list', {
				url: '/',
				templateUrl: 'js/ng/app/transfer_type/views/index.html',
				controller: 'transfer_type_ctrl as vm',
				data: { pageTitle: 'Transfer Type List' + name},
				resolve:{
					getDetail: ['Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Transfer_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Transfer_Type_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			/**
			 * END Transfer Route
			 */
			
			.state('import_product', {
				url: '/import_product',
				templateUrl: 'js/ng/app/import_product/views/index.html',
				controller: 'import_product_ctrl as vm',
				data: { pageTitle: 'Import Product List' + name}
			})
			.state('import_staff', {
				url: '/import_staff',
				templateUrl: 'js/ng/app/import_staff/views/index.html',
				controller: 'import_staff_ctrl as vm',
				data: { pageTitle: 'Import Staff List' + name}
			})
			.state('import_vendor', {
				url: '/import_vendor',
				templateUrl: 'js/ng/app/import_vendor/views/index.html',
				controller: 'import_vendor_ctrl as vm',
				data: { pageTitle: 'Import Vendor List' + name}
			})
			.state('import_customer', {
				url: '/import_customer',
				templateUrl: 'js/ng/app/import_customer/views/index.html',
				controller: 'import_customer_ctrl as vm',
				data: { pageTitle: 'Import Customer' + name}
			})
			.state('stock_alert_report', {
				url: '/stock_alert_report',
				templateUrl: 'js/ng/app/stock_alert_report/views/index.html',
				controller: 'report_stock_alert_ctrl as vm',
				data: { pageTitle: 'Stock Alert' + name}
			})
			/**
			 * Stock out route
			 */
			.state('stock_out', {
				url: '/stock_out',
				template: '<div ui-view/>',
				redirectTo: 'stock_out.list'
			})
			.state('stock_out.list', {
				url: '/',
				templateUrl: 'js/ng/app/stock_out/views/index.html',
				controller: 'stock_out_ctrl as vm',
				data: { pageTitle: 'Stock Out' + name}
			})
			.state('stock_out.create', {
				url: '/create',
				templateUrl: 'js/ng/app/stock_out/views/create.html',
				controller: 'stock_out_create_ctrl as vm',
				data: { pageTitle: 'Stock Out' + name}
			})
			.state('stock_out.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/stock_out/views/detail.html',
				controller: 'stock_out_detail_ctrl as vm',
				data: { pageTitle: 'Stock Out Detail' + name}
			})
			/**
			 * sale return route
			 */
			.state('sale_return', {
				url: '/sale_return',
				template: '<div ui-view/>',
				redirectTo: 'sale_return.list'
			})
			.state('sale_return.list', {
				url: '/',
				templateUrl: 'js/ng/app/sale_return/views/index.html',
				controller: 'sale_return_ctrl as vm',
				data: { pageTitle: 'Sale Return' + name}
			})
			.state('sale_return.create', {
				url: '/create',
				templateUrl: 'js/ng/app/sale_return/views/create.html',
				controller: 'sale_return_create_ctrl as vm',
				data: { pageTitle: 'Create Sale Return' + name}
			})
			.state('sale_return.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/sale_return/views/detail.html',
				controller: 'sale_return_detail_ctrl as vm',
				data: { pageTitle: 'Sale Return Detail' + name}
			})
			// Sale Return report
			.state('sale_return_report', {
				url: '/sale_return_report',
				templateUrl: 'js/ng/app/sale_return_report/views/index.html',
				controller: 'sale_return_report_ctrl as vm',
				data: { pageTitle: 'Sale Return Report' + name}
			})
			/**
			 * Setting route
			 */
			.state('exchange-rate', {
				url: '/exchange-rate',
				templateUrl: 'js/ng/app/exchange_rate/views/index.html',
				controller: 'exchange_rate_ctrl as vm',
				data: { pageTitle: 'Exchange Rate' + name}
			})
			/**
			 * vendor type route
			 */
			.state('vendor_type', {
				url: '/vendor_type',
				template: '<div ui-view/>',
				redirectTo: 'vendor_type.list'
			})
			.state('vendor_type.list', {
				url: '/',
				templateUrl: 'js/ng/app/vendor_type/views/index.html',
				controller: 'vendor_type_ctrl as vm',
				data: { pageTitle: 'vendor type List' + name}
			})
			.state('vendor_type.create', {
				url: '/create',
				templateUrl: 'js/ng/app/vendor_type/views/create.html',
				controller: 'vendor_type_create_ctrl as vm',
				data: { pageTitle: 'create vendor type' + name}
			})
			.state('vendor_type.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/vendor_type/views/create.html',
				controller: 'vendor_type_create_ctrl as vm',
				data: { pageTitle: 'edit vendor type' + name}
			})
			.state('vendor_type.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/vendor_type/views/detail.html',
				controller: 'vendor_type_detail_ctrl as vm',
				data: { pageTitle: 'View vendor type' + name}
			})
			/**
			 * Vendor List Route
			 */
			.state('vendor_list', {
				url: '/vendor_list',
				template: '<div ui-view/>',
				redirectTo: 'vendor_list.list'
			})
			.state('vendor_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/vendor_list/views/index.html',
				controller: 'vendor_list_ctrl as vm',
				data: { pageTitle: 'Vendor List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Vendor_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Vendor_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('vendor_list.create', {
				url: '/create',
				templateUrl: 'js/ng/app/vendor_list/views/create.html',
				controller: 'vendor_list_create_ctrl as vm',
				data: { pageTitle: 'Create Vendor List' + name}
			})
			.state('vendor_list.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/vendor_list/views/create.html',
				controller: 'vendor_list_create_ctrl as vm',
				data: { pageTitle: 'Edit Vendor List' + name}
			})
			.state('vendor_list.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/vendor_list/views/detail.html',
				controller: 'vendor_list_detail_ctrl as vm',
				data: { pageTitle: 'View vendor List' + name}
			})
			.state('vendor_report', {
				url: '/vendor_report',
				templateUrl: 'js/ng/app/report_vendor/views/index.html',
				controller: 'report_vendor_ctrl as vm',
				data: { pageTitle: 'Vendor Report' + name}
			})
			.state('vendor_balance', {
				url: '/vendor_balance',
				templateUrl: 'js/ng/app/report_vendor_balance/views/index.html',
				controller: 'report_vendor_balance_ctrl as vm',
				data: { pageTitle: 'Vendor Balance' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Vendor_Balance_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Vendor_Balance_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
				
			})
			// purchase route
			.state('purchase', {
				url: '/purchase',
				template: '<div ui-view/>',
				redirectTo: 'purchase.list'
			})
			.state('purchase.list', {
				url: '/',
				templateUrl: 'js/ng/app/purchase/views/index.html',
				controller: 'purchase_ctrl as vm',
				data: { pageTitle: 'Purchase List' + name}
			})
			.state('purchase.create', {
				url: '/create',
				templateUrl: 'js/ng/app/purchase/views/create.html',
				controller: 'purchase_create_ctrl as vm',
				data: { pageTitle: 'Create Purchase' + name}
			})
			.state('purchase.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/purchase/views/create.html',
				controller: 'purchase_create_ctrl as vm',
				data: { pageTitle: 'Edit Purchase' + name}
			})
			.state('purchase.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/purchase/views/detail.html',
				controller: 'purchase_detail_ctrl as vm',
				data: { pageTitle: 'Purchase Detail' + name}
			})

			.state('pay_bill', {
				url: '/pay_bill',
				template: '<div ui-view/>',
				redirectTo: 'pay_bill.list'
			})
			.state('pay_bill.list', {
				url: '/list',
				templateUrl: 'js/ng/app/pay_bill/views/index.html',
				controller: 'pay_bill_list_ctrl as vm',
				data: { pageTitle: 'Pay Bill List' + name}
			})
			.state('pay_bill.create', {
				url: '/create',
				templateUrl: 'js/ng/app/pay_bill/views/create.html',
				controller: 'pay_bill_create_ctrl as vm',
				data: { pageTitle: 'Create Pay Bill' + name}
			})
			.state('pay_bill.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/pay_bill/views/detail.html',
				controller: 'pay_bill_detail_ctrl as vm',
				data: { pageTitle: 'Pay Bill Detail' + name}
			})

			// report purchase route
			.state('report_purchase', {
				url: '/report_purchase?invoice_no?date?s_name?s_id',
				templateUrl: 'js/ng/app/report_purchase/views/index.html',
				controller: 'report_purchase_ctrl as vm',
				data: { pageTitle: 'Report Purchase Detail' + name}
			})
			// report_purchase_summary route
			.state('report_purchase_summary', {
				url: '/report_purchase_summary',
				templateUrl: 'js/ng/app/report_purchase_summary/views/index.html',
				controller: 'report_purchase_summary_ctrl as vm',
				data: { pageTitle: 'Report Purchase Summary' + name}
			})
			.state('account_payable', {
				url: '/account_payable',
				templateUrl: 'js/ng/app/report_pay_bill/views/index.html',
				controller: 'report_pay_bill_ctrl as vm',
				data: { pageTitle: 'Account Payable' + name}
			})
			.state('account_payable_summary', {
				url: '/account_payable_summary',
				templateUrl: 'js/ng/app/report_payment_summary/views/index.html',
				controller: 'report_pay_bill_summary_ctrl as vm',
				data: { pageTitle: 'Account Payable Summary' + name}
			})
			// report invoice rote
			.state('/report_invoice', {
				url: '/report_invoice',
				templateUrl: 'js/ng/app/report_invoice/views/index.html',
				controller: 'report_invoice_ctrl'
			})
			// staff payroll rote
			// .state('staff_payroll', {
			// 	url: '/staff_payroll',
			// 	templateUrl: 'js/ng/app/pay_roll/views/index.html',
			// 	controller: 'pay_roll_ctrl as vm',
			// 	data: { pageTitle: 'Staff Payroll' + name}
			// })
			// report_staff rote
			// .state('staff_report', {
			// 	url: '/staff_report',
			// 	templateUrl: 'js/ng/app/report_staff/views/index.html',
			// 	controller: 'report_staff_ctrl as vm',
			// 	data: { pageTitle: 'Staff Report' + name}
			// })
			// report_receive_payment rote
			.state('account_receivable', {
				url: '/account_receivable',
				templateUrl: 'js/ng/app/report_receive_payment/views/index.html',
				controller: 'report_receive_payment_ctrl as vm',
				data: { pageTitle: 'Account Receivable' + name}
			})
			// report_receive_payment rote
			.state('account_receivable_summary', {
				url: '/account_receivable_summary',
				templateUrl: 'js/ng/app/report_account_receivable_summary/views/index.html',
				controller: 'report_account_receivable_summary_ctrl as vm',
				data: { pageTitle: 'Account Receivable Summary' + name}
			})
			// report stock rote
			.state('stock_report', {
				url: '/stock_report',
				templateUrl: 'js/ng/app/report_stock/views/index.html',
				controller: 'report_stock_ctrl as vm',
				data: { pageTitle: 'Stock Report' + name}
			})			
			// report Service transaction rote
			.state('service_transaction_report', {
				url: '/service_transaction_report',
				templateUrl: 'js/ng/app/report_service_transaction/views/index.html',
				controller: 'report_service_transaction_ctrl as vm',
				data: { pageTitle: 'Service Transaction Report' + name}
			})
			// report_sale summary rote
			.state('report_sale_summary', {
				url: '/report_sale_summary',
				templateUrl: 'js/ng/app/report_sale_summary/views/index.html',
				controller: 'report_sale_summary_ctrl as vm',
				data: { pageTitle: 'Report Sale Summary' + name},
				params: { data: {} }
			})
			// report sale service rote
			.state('report_sale', {
				url: '/report_sale?s_date?c_id?c_name',
				templateUrl: 'js/ng/app/report_sale/views/index.html',
				controller: 'report_sale_ctrl as vm',
				data: { pageTitle: 'Report Sale Detail' + name},
				params: { data: {} }
			})
			// setting company profile rote
			.state('company-profile', {
				url: '/company-profile',
				templateUrl: 'js/ng/app/setting/views/index.html',
				controller: 'setting_ctrl as vm',
				data: { pageTitle: 'Company Profile' + name}
			})
			// report customer rote
			.state('customer_report', {
				url: '/customer_report',
				templateUrl: 'js/ng/app/report_customer/views/index.html',
				controller: 'report_customer_ctrl as vm',
				data: { pageTitle: 'Customer Report' + name},
				params: {data: {}}
			})
			// report customer treatment rote
			.state('customer_treatment_report', {
				url: '/customer_treatment_report',
				templateUrl: 'js/ng/app/report_customer_treatment/views/index.html',
				controller: 'report_customer_treatment_ctrl as vm',
				data: { pageTitle: 'Customer Treatment Report' + name},
				params: {data: null}
			})
			// report doctor rote
			.state('doctor_report', {
				url: '/staff_report',
				templateUrl: 'js/ng/app/report_doctor/views/index.html',
				controller: 'report_doctor_ctrl as vm',
				data: { pageTitle: 'Doctor Report' + name}
			})
			/**
			 * start appointment route
			 */
			.state('appointment', {
				url: '/appointment',
				template: '<div ui-view></div>',
				redirectTo: 'appointment.list'
			})
			.state('appointment.list', {
				url: '',
				templateUrl: 'js/ng/app/appointment/views/index.html',
				controller: 'appointment_ctrl as vm',
				data: { pageTitle: 'Appointment' + name}
			})
			.state('appointment.create', {
				url: '/create',
				templateUrl: 'js/ng/app/appointment/views/create.html',
				controller: 'appointment_create_ctrl as vm',
				data: { pageTitle: 'Create Appointment' + name}
			})
			.state('appointment.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/appointment/views/create.html',
				controller: 'appointment_create_ctrl as vm',
				data: { pageTitle: 'Edit Appointment' + name}
			})
			.state('appointment_report', {
				url: '/appointment_report',
				templateUrl: 'js/ng/app/appointment_report/views/index.html',
				controller: 'appointment_report_ctrl as vm',
				data: { pageTitle: 'Appointment Report' + name},
				params: {appointment_alert: null}
			})
			// report payroll route
			.state('payroll_report', {
				url: '/payroll_report',
				templateUrl: 'js/ng/app/report_payroll/views/index.html',
				controller: 'report_payroll_ctrl as vm',
				data: { pageTitle: 'Payroll Report' + name}
			})
			// chart account route
			.state('balance_sheet', {
				url: '/balance_sheet',
				templateUrl: 'js/ng/app/report_balance_sheet/views/index.html',
				controller: 'report_balance_sheet_ctrl as vm',
				data: { pageTitle: 'Report Balance Sheet' + name}
			})
			// chart account route
			.state('chart_account', {
				url: '/chart_account',
				templateUrl: 'js/ng/app/chart_account/views/index.html',
				controller: 'chart_account_ctrl as vm',
				data: { pageTitle: 'Chart Account' + name}
			})
			.state('journal_report', {
				url: '/journal_report',
				templateUrl: 'js/ng/app/report_journal/views/index.html',
				controller: 'journal_report_ctrl as vm',
				data: { pageTitle: 'Journal Report' + name}
			})
			.state('cash_flow', {
				url: '/cash_flow',
				templateUrl: 'js/ng/app/report_cash_flow/views/index.html',
				controller: 'cash_flow_report_ctrl as vm',
				data: { pageTitle: 'Cash Flow' + name}
			})
			.state('income_statement', {
				url: '/income_statement',
				templateUrl: 'js/ng/app/report_income_statement/views/index.html',
				controller: 'income_statement_report_ctrl as vm',
				data: { pageTitle: 'Income Statement' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Income_Statement_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Income_Statement_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('customer_balance', {
				url: '/customer_balance',
				templateUrl: 'js/ng/app/report_customer_balance/views/index.html',
				controller: 'report_customer_balance_ctrl as vm',
				data: { pageTitle: 'Customer Balance' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Balance_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Balance_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('customer_overdue_report', {
				url: '/customer_overdue_report',
				templateUrl: 'js/ng/app/report_customer_overdue/views/index.html',
				controller: 'report_customer_overdue_ctrl as vm',
				data: { pageTitle: 'Customer Overdud Report' + name },
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Overdue_Report_View", "Customer_Overdue_Report_Export"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {console.warn(getDetail.data);
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Overdue_Report_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('daily_case_report', {
				url: '/daily_case_report',
				templateUrl: 'js/ng/app/report_daily_case/views/index.html',
				controller: 'report_daily_case_ctrl as vm',
				data: { pageTitle: 'Daily Cash Report' + name}
			})
			/**
			 * Report staff by doing
			 */
			.state('report-appointment-by-staff', {
				url: '/report-appointment-by-staff',
				templateUrl: 'js/ng/app/report_appointment_by_staff/views/index.html',
				controller: 'report_appointment_by_staff_ctrl as vm',
				data: { pageTitle: 'Appointment Report' + name},
				params: {
					data: null
				}
			})
			.state('report-payslip-staff', {
				url: '/report-payslip-staff',
				templateUrl: 'js/ng/app/report_payslip_by_staff/views/index.html',
				controller: 'report_payslip_by_staff_ctrl as vm',
				data: { pageTitle: 'Payslip Staff Report' + name}
			})
			.state('report-expense-staff', {
				url: '/report-my-expense',
				templateUrl: 'js/ng/app/report_my_expense/views/index.html',
				controller: 'my_expense_report_ctrl as vm',
				data: { pageTitle: 'My Expense Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["My_Expense_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'My_Expense_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			
			/**
			 * End
			 */
			.state('role', {
				url: '/role',
				templateUrl: 'js/ng/app/role/views/index.html',
				controller: 'role_ctrl as vm',
				data: { pageTitle: 'Role' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Role_View", "Role_Edit", "Role_Delete"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Role_View' && value.is_selected ){
										found = true;//value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
		;
		$urlRouterProvider.otherwise('dashboard');
		// use the HTML5 History API
		//$locationProvider.html5Mode(true);
	}
]);

// for redirect to child from parent
app.run(['$rootScope', '$state', function($rootScope, $state) {
	$rootScope.$on('$stateChangeStart', function(evt, to, params) {
		if (to.redirectTo) {
			evt.preventDefault();
			$state.go(to.redirectTo, params, {location: 'replace'})
		}
	});
}]);
