app.service('dbService',[
	'$http',
	function($http) {
		return {
			getData: function($q, $http) {
				var defer = $q.defer();
				$http.get('api/GetUserInfo').success(function(data) {
					defer.resolve(data);
				});
				return defer.promise;
			}
		};
	}
]);


/**
 * Directive Define title of the page
 */
app.directive('updateTitle', ['$rootScope', '$timeout',
  function($rootScope, $timeout) {
    return {
      link: function(scope, element) {

        var listener = function(event, toState) {

          var title = 'Default Title';
          if (toState.data && toState.data.pageTitle) title = toState.data.pageTitle;

          $timeout(function() {
            element.text(title);
          }, 0, false);
        };

        $rootScope.$on('$stateChangeSuccess', listener);
      }
    };
  }
]);
app.config([ '$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		var name = ' - SKWeb Solution';
		
		$stateProvider.state('license', {
			url: '/license',
			templateUrl: 'js/ng/app/license/views/index.html',
			controller: 'license_ctrl',
			data: { pageTitle: 'License' + name}
		});
	
		$stateProvider.
			state('dashboard', {
				url: '/dashboard',
				templateUrl: 'js/ng/app/index/partials/index.html',
				controller: 'index_ctrl as vm',
				data: { pageTitle: 'Dashboard' + name},
			})
			//#region Start Customer Route
			.state('customer_type', {
				url: '/customer_type',
				templateUrl: 'js/ng/app/customer_type/views/index.html',
				controller: 'customer_type_ctrl as vm',
				data: { pageTitle: 'Customer Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful) {
						return Restful.get('api/GetUserInfo').success(function(data){
							return data;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements[0].role_detail[0].permission;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Type_View'){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('member_ship', {
				url: '/member-ship',
				templateUrl: 'js/ng/app/member_ship/views/index.html',
				controller: 'member_ship_ctrl as vm',
				data: { pageTitle: 'member ship' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful) {
						return Restful.get('api/GetUserInfo').success(function(data){
							return data;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements[0].role_detail[0].permission;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Member_Ship_View'){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('customer_list', {
				url: '/customer_list',
				templateUrl: 'js/ng/app/customer_list/views/index.html',
				controller: 'customer_list_ctrl as vm',
				data: { pageTitle: 'Customer List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('customer_list_create', {
                url: '/customer_list/create',
                templateUrl: 'js/ng/app/customer_list/views/create.html',
                controller: 'customer_list_create_ctrl as vm',
				data: { pageTitle: 'Customer List Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('customer_list_edit', {
                url: '/customer_list/edit/:id',
                templateUrl: 'js/ng/app/customer_list/views/create.html',
                controller: 'customer_list_create_ctrl as vm',
				data: { pageTitle: 'Customer List Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_List_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_List_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('customer_detail', {
                url: '/customer/detail/:id',
                templateUrl: 'js/ng/app/customer_list/views/detail.html',
                controller: 'customer_detail_ctrl as vm',
				data: { pageTitle: 'Customer Detail Edit' + name},
				params: { params: null},
            })
			.state('customer-birthday', {
				url: '/customer-birthday',
				templateUrl: 'js/ng/app/customer_birthday/views/index.html',
				controller: 'customer_birthday_ctrl as vm',
				data: { pageTitle: 'Customer Birthday' + name}
			})

			.state('feed_back', {
				url: '/feed_back',
				template: '<div ui-view/>',
				redirectTo: 'feed_back.list'
			})
			.state('feed_back.list', {
				url: '/',
				templateUrl: 'js/ng/app/feed_back/views/index.html',
				controller: 'feed_back_index_ctrl as vm',
				data: { pageTitle: 'Feed Back' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Feed_Back_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Feed_Back_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('feed_back.create', {
                url: '/create',
                templateUrl: 'js/ng/app/feed_back/views/create.html',
                controller: 'feed_back_create_ctrl as vm',
				data: { pageTitle: 'Feed Back Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Feed_Back_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Feed_Back_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('feed_back.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/feed_back/views/create.html',
                controller: 'feed_back_create_ctrl as vm',
				data: { pageTitle: 'Feed_Back Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Feed_Back_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Feed_Back_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('feed_back.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/feed_back/views/detail.html',
				controller: 'feed_back_detail_ctrl as vm',
				params: { params: null},
                data: { pageTitle: 'feed back detail' + name},
			})
			
			.state('complaint', {
				url: '/complaint',
				template: '<div ui-view/>',
				redirectTo: 'complaint.list'
			})
			.state('complaint.list', {
				url: '/',
				templateUrl: 'js/ng/app/complaint/views/index.html',
				controller: 'complaint_index_ctrl as vm',
				data: { pageTitle: 'complaint' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Complaint_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Complaint_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('complaint.create', {
                url: '/create',
                templateUrl: 'js/ng/app/complaint/views/create.html',
                controller: 'complaint_create_ctrl as vm',
				data: { pageTitle: 'complaint Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Complaint_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Complaint_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('complaint.resolve', {
                url: '/resolve/:id',
                templateUrl: 'js/ng/app/complaint/views/resolve.html',
                controller: 'complaint_resolve_ctrl as vm',
				data: { pageTitle: 'complaint resolve' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Complaint_Resolve"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Complaint_Resolve' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('complaint.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/complaint/views/create.html',
                controller: 'complaint_create_ctrl as vm',
				data: { pageTitle: 'Complaint Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Complaint_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Complaint_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('complaint.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/complaint/views/detail.html',
                controller: 'complaint_detail_ctrl as vm',
				data: { pageTitle: 'complaint detail' + name},
				params: { params: null},
			})
			
			.state('sale_form', {
				url: '/sale_form',
				template: '<div ui-view/>',
				redirectTo: 'sale_form.list'
			})
			.state('sale_form.list', {
				url: '/',
				templateUrl: 'js/ng/app/sale_form/views/index.html',
				controller: 'sale_form_index_ctrl as vm',
				data: { pageTitle: 'Sale Form List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('sale_form.create', {
                url: '/create',
                templateUrl: 'js/ng/app/sale_form/views/create.html',
                controller: 'sale_form_create_ctrl as vm',
				data: { pageTitle: 'Sale Form Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('sale_form.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/sale_form/views/create.html',
                controller: 'sale_form_create_ctrl as vm',
				data: { pageTitle: 'Sale Form Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Edit' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
            .state('sale_form.mission', {
                url: '/mission/:id',
                templateUrl: 'js/ng/app/sale_form/views/mission.html',
                controller: 'sale_form_mission_ctrl as vm',
				data: { pageTitle: 'Sale Form Mission' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Mission"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Mission' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('sale_form.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/sale_form/views/detail.html',
                controller: 'sale_form_detail_ctrl as vm',
				data: { pageTitle: 'Sale Form detail' + name},
				params: { params: null},
			})
			.state('sale_form.photo_selection', {
                url: '/photo-selection/:id',
                templateUrl: 'js/ng/app/sale_form/views/photo_selection.html',
                controller: 'sale_form_photo_selection_ctrl as vm',
				data: { pageTitle: 'Sale Form Photo Selection' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Photo_Selection"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Photo_Selection' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('sale_form.photo_process', {
                url: '/photo-process/:id',
                templateUrl: 'js/ng/app/sale_form/views/photo_process.html',
                controller: 'sale_form_photo_process_ctrl as vm',
				data: { pageTitle: 'Sale Form Photo Process' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Photo_Process"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Photo_Process' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('sale_form.delevery', {
                url: '/delevery/:id',
                templateUrl: 'js/ng/app/sale_form/views/delevery.html',
                controller: 'sale_form_delevery_ctrl as vm',
				data: { pageTitle: 'Sale Form Delevery' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Delevery"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Delevery' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			//#endregion Customer Route

			//#region Start Staff list route
			.state('staff_type', {
				url: '/staff_type',
				template: '<div ui-view/>',
				redirectTo: 'staff_type.list'
			})
			.state('staff_type.list', {
				url: '/',
				templateUrl: 'js/ng/app/staff_type/views/index.html',
				controller: 'staff_type_ctrl as vm',
				data: { pageTitle: 'Staff Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Staff_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Staff_Type_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('staff_type.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/staff_type/views/create.html',
				controller: 'staff_type_create_ctrl as vm',
				data: { pageTitle: 'Edit Staff Type' + name}
			})
			.state('staff_type.create', {
				url: '/create',
				templateUrl: 'js/ng/app/staff_type/views/create.html',
				controller: 'staff_type_create_ctrl as vm',
				data: { pageTitle: 'Create Staff Type' + name}
			})
			.state('staff_list', {
				url: '/staff_list',
				template: '<div ui-view/>',
				redirectTo: 'staff_list.list'
			})
			.state('staff_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/staff_list/views/index.html',
				controller: 'staff_list_ctrl as vm',
				data: { pageTitle: 'Staff List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Staff_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Staff_List_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('staff_list.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/staff_list/views/create.html',
				controller: 'staff_create_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Edit Staff List' + name}
			})
			.state('staff_list.create', {
				url: '/create',
				templateUrl: 'js/ng/app/staff_list/views/create.html',
				controller: 'staff_create_ctrl as vm',
				data: { pageTitle: 'Create Staff List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Staff_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Staff_List_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('staff_list.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/staff_list/views/detail.html',
				controller: 'staff_detail_ctrl as vm',
				data: { pageTitle: 'Staff Detail' + name},
				params: { params: null},
			})	
			.state('staff_birthday', {
				url: '/staff-bithday',
				templateUrl: 'js/ng/app/report_staff_birthday/views/index.html',
				controller: 'report_staff_birthday_ctrl as vm',
				data: { pageTitle: 'staff birthday' + name}
			})
			//#endregion end staff route
			  
			//#region Start Item Route
			.state('um_type', {
				url: '/um_type',
				templateUrl: 'js/ng/app/um_type/views/index.html',
				controller: 'um_type_ctrl as vm',
				data: { pageTitle: 'UM Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["UM_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'UM_Type_View' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('product_type', {
				url: '/product_type',
				templateUrl: 'js/ng/app/products_type/views/index.html',
				controller: 'products_type_ctrl as vm',
				data: { pageTitle: 'Product Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Type_View' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('product_list', {
				url: '/product_list',
				template: '<div ui-view/>',
				redirectTo: 'product_list.list'
			})
			.state('product_list.list', {
				url: '/',
				templateUrl: 'js/ng/app/products_list/views/index.html',
				controller: 'products_list_ctrl as vm',
				data: { pageTitle: 'Product List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_List_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_List_View' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
            .state('product_list.create', {
                url: '/create',
                templateUrl: 'js/ng/app/products_list/views/create.html',
                controller: 'products_list_create_ctrl as vm',
				data: { pageTitle: 'Create Product List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_List_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {console.warn(getDetail.data);
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_List_Create' && value.is_selected){
										found = value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
            })
			.state('product_list.detail', {
                url: '/detail/:id',
                templateUrl: 'js/ng/app/products_list/views/detail.html',
				controller: 'products_detail_ctrl as vm',
				params: { params: null},
                data: { pageTitle: 'Create Product Detail' + name}
            })
            .state('product_list.edit', {
                url: '/edit/:id',
                templateUrl: 'js/ng/app/products_list/views/create.html',
				controller: 'products_list_create_ctrl as vm',
				params: { params: null},
                data: { pageTitle: 'Edit Product List' + name}
            })
			.state('option_type', {
				url: '/option-type',
				templateUrl: 'js/ng/app/option_type/views/index.html',
				controller: 'option_type_ctrl as vm',
				data: { pageTitle: 'Option Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Option_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Option_Type_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('option', {
				url: '/options',
				templateUrl: 'js/ng/app/options/views/index.html',
				controller: 'options_ctrl as vm',
				data: { pageTitle: 'Options' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Option_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Option_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('add_on_type', {
				url: '/add-on-type',
				templateUrl: 'js/ng/app/add_on_type/views/index.html',
				controller: 'add_on_type_ctrl as vm',
				data: { pageTitle: 'Add On Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Add_On_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							console.warn(result);
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Add_On_Type_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                               return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('add_on', {
				url: '/add-on',
				templateUrl: 'js/ng/app/add_on/views/index.html',
				controller: 'add_on_ctrl as vm',
				data: { pageTitle: 'Add On' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Add_On_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Add_On_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package', {
				url: '/package',
				template: '<div ui-view/>',
				redirectTo: 'package.list'
			})
			.state('package.list', {
				url: '/list',
				templateUrl: 'js/ng/app/packages/views/index.html',
				controller: 'package_list_ctrl as vm',
				data: { pageTitle: 'Package' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package.create', {
				url: '/create',
				templateUrl: 'js/ng/app/packages/views/create.html',
				controller: 'package_create_ctrl as vm',
				data: { pageTitle: 'Package Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Create' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/packages/views/create.html',
				controller: 'package_create_ctrl as vm',
				data: { pageTitle: 'Package Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Edit' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/packages/views/detail.html',
				controller: 'package_detail_ctrl as vm',
				data: { pageTitle: 'Package Detail' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Detail"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Detail' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			//package type
			.state('package_type', {
				url: '/package_type',
				template: '<div ui-view/>',
				redirectTo: 'package_type.list'
			})
			.state('package_type.list', {
				url: '/list',
				templateUrl: 'js/ng/app/package_type/views/index.html',
				controller: 'package_type_list_ctrl as vm',
				data: { pageTitle: 'package type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Type_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package_type.create', {
				url: '/create',
				templateUrl: 'js/ng/app/package_type/views/create.html',
				controller: 'package_type_create_ctrl as vm',
				data: { pageTitle: 'Package Type Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Type_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Type_Create' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package_type.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/package_type/views/create.html',
				controller: 'package_type_create_ctrl as vm',
				data: { pageTitle: 'Package Type Edit' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Type_Edit"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Type_Edit' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('package_type.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/package_type/views/detail.html',
				controller: 'package_type_detail_ctrl as vm',
				data: { pageTitle: 'Package Type Detail' + name},
				params: { params: null},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Type_Detail"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Type_Detail' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			// location
			.state('location_type', {
				url: '/location-type',
				templateUrl: 'js/ng/app/location_type/views/index.html',
				controller: 'location_type_ctrl as vm',
				data: { pageTitle: 'Locatin Type' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Location_Type_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							console.warn(result);
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Location_Type_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                               return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('location', {
				url: '/location',
				templateUrl: 'js/ng/app/location/views/index.html',
				controller: 'location_ctrl as vm',
				data: { pageTitle: 'Location' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Location_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							return result;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {
                            if (getDetail.data.count && getDetail.data.count > 0) {
								var found = false;
								var result = getDetail.data.elements;
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Location_View' && value.is_selected){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			//#endregion
			

			//#region Start Invoice list route
			.state('invoice', {
				url: '/invoice',
				template: '<div ui-view/>',
				redirectTo: 'invoice.list'
			})
			.state('invoice.list', {
				url: '/',
				templateUrl: 'js/ng/app/invoice/views/index.html',
				controller: 'invoice_ctrl as vm',
				data: { pageTitle: 'Invoice List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Invoice_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Invoice_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('invoice.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/invoice/views/create.html',
				controller: 'invoice_create_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Invoice Edit' + name}
			})
			.state('invoice.create', {
				url: '/create',
				templateUrl: 'js/ng/app/invoice/views/create.html',
				controller: 'invoice_create_ctrl as vm',
				data: { pageTitle: 'Invoice Create' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Invoice_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Invoice_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('invoice.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/invoice/views/detail.html',
				controller: 'invoice_detail_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Invoice Detail' + name}
			})	
			// invoice term			
			.state('invoice_term', {
				url: '/invoice-term',
				templateUrl: 'js/ng/app/invoice_term/views/index.html',
				controller: 'invoice_term_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Invoice Term' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Invoice_Term_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Invoice_Term_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('quotation', {
				url: '/quotation',
				template: '<div ui-view/>',
				redirectTo: 'quotation.list'
			})
			.state('quotation.list', {
				url: '/',
				templateUrl: 'js/ng/app/quotation/views/index.html',
				controller: 'quotation_ctrl as vm',
				data: { pageTitle: 'Quotation List' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Quotation_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Quotation_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('quotation.edit', {
				url: '/edit/:id',
				templateUrl: 'js/ng/app/quotation/views/create.html',
				controller: 'quotation_create_ctrl as vm',
				data: { pageTitle: 'quotation Edit' + name}
			})
			.state('quotation.create', {
				url: '/create',
				templateUrl: 'js/ng/app/quotation/views/create.html',
				controller: 'quotation_create_ctrl as vm',
				data: { pageTitle: 'Create Quotation' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Quotation_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Quotation_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('quotation.detail', {
				url: '/detail/:id',
				templateUrl: 'js/ng/app/quotation/views/detail.html',
				controller: 'quotation_detail_ctrl as vm',
				data: { pageTitle: 'Quotation Detail' + name}
			})	
			// quotation term			
			.state('quotation_term', {
				url: '/quotation-term',
				templateUrl: 'js/ng/app/quotation_term/views/index.html',
				controller: 'quotation_term_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Quotation Term' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Quotation_Term_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
					detailResult: [
						'getDetail', '$q', '$timeout', '$state',
						function (getDetail, $q, $timeout, $state) {							
							if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Quotation_Term_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
							} else {
								return $q.resolve(getDetail);
							}
						}
					]
				}
			})
			.state('received_payment', {
				url: '/received_payment',
				template: '<div ui-view/>',
				redirectTo: 'received_payment.search',
				data: { pageTitle: 'Receive Payment' + name}
			})
			.state('received_payment.search', {
				url: '/',
				templateUrl: 'js/ng/app/received_payment/views/index.html',
				controller: 'received_payment_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Receive_Payment_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Receive_Payment_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('received_payment.create', {
				url: '/create',
				templateUrl: 'js/ng/app/received_payment/views/create.html',
				controller: 'received_payment_create_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Receive_Payment_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Receive_Payment_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('received_payment.detail', {
				url: '/detail/:id?invoice_no',
				templateUrl: 'js/ng/app/received_payment/views/detail.html',
				controller: 'received_payment_detail_ctrl as vm',
				data: { pageTitle: 'Receive Payment' + name},
			})
			// received_payment term			
			.state('received_payment_term', {
				url: '/received-payment-term',
				templateUrl: 'js/ng/app/received_payment_term/views/index.html',
				controller: 'received_payment_term_ctrl as vm',
				params: { params: null},
				data: { pageTitle: 'Received Payment Term' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Received_Payment_Term_Create"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Received_Payment_Term_Create' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			//#endregion end invoice route


			//#region Report route map
			.state('customer_report', {
				url: '/customer_report',
				templateUrl: 'js/ng/app/report_customer/views/index.html',
				controller: 'report_customer_ctrl as vm',
				data: { pageTitle: 'Customer Report' + name},
				params: {data: {}},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('customer_balance_report', {
				url: '/customer_balance',
				templateUrl: 'js/ng/app/report_customer_balance/views/index.html',
				controller: 'report_customer_balance_ctrl as vm',
				data: { pageTitle: 'Customer Balance' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Customer_Balance_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Customer_Balance_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('product_report', {
				url: '/product-report',
				templateUrl: 'js/ng/app/product_report/views/index.html',
				controller: 'product_report_ctrl as vm',
				data: { pageTitle: 'Product Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Product_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Product_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_option', {
				url: '/option-report',
				templateUrl: 'js/ng/app/report_option/views/index.html',
				controller: 'report_option_ctrl as vm',
				data: { pageTitle: 'Option Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Option_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Option_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('staff_report', {
				url: '/staff_report',
				templateUrl: 'js/ng/app/report_staff/views/index.html',
				controller: 'report_staff_ctrl as vm',
				data: { pageTitle: 'Staff Report' + name}
			})
			.state('report_add_on', {
				url: '/add-on-report',
				templateUrl: 'js/ng/app/report_add_on/views/index.html',
				controller: 'report_add_on_ctrl as vm',
				data: { pageTitle: 'Add On Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Add_On_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Add_On_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_package', {
				url: '/package-report',
				templateUrl: 'js/ng/app/report_package/views/index.html',
				controller: 'report_package_ctrl as vm',
				data: { pageTitle: 'Package Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Package_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Package_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_invoice', {
				url: '/invoice-report',
				templateUrl: 'js/ng/app/report_invoice/views/index.html',
				controller: 'report_invoice_ctrl as vm',
				data: { pageTitle: 'Invoice Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Invoice_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Invoice_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_invoice_summary', {
				url: '/invoice-summary-report',
				templateUrl: 'js/ng/app/report_invoice_summary/views/index.html',
				controller: 'report_invoice_summary_ctrl as vm',
				data: { pageTitle: 'Invoice Summary Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Invoice_Summary_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Invoice_Summary_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			// report_receive_payment rote
			.state('report_receive_payment', {
				url: '/report_receive_payment',
				templateUrl: 'js/ng/app/report_received_payment/views/index.html',
				controller: 'report_receive_payment_ctrl as vm',
				data: { pageTitle: 'Receivable Payment' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Receivable_Payment_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Receivable_Payment_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('cash_flow', {
				url: '/cash_flow',
				templateUrl: 'js/ng/app/report_daily_cash/views/index.html',
				controller: 'report_daily_cash_ctrl as vm',
				data: { pageTitle: 'Cash Flow' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Cash_Flow_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Cash_Flow_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_quotation', {
				url: '/quotation-report',
				templateUrl: 'js/ng/app/report_quotation/views/index.html',
				controller: 'report_quotation_ctrl as vm',
				data: { pageTitle: 'quotation Report' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Quotation_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Quotation_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('report_sale_form', {
				url: '/sale-form-report',
				templateUrl: 'js/ng/app/report_sale_form/views/index.html',
				controller: 'report_sale_form_ctrl as vm',
				data: { pageTitle: 'Sale Form Report' + name},
				params: { data: {}},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Sale_Form_Report_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Sale_Form_Report_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			//#endregion


			//testing
			.state('calendar', {
				url: '/calendar',
				templateUrl: 'js/ng/app/calendar/views/index.html',
				controller: 'calendar_ctrl as vm',
				data: { pageTitle: 'calendar list' + name}
			})

			//#region Import route data
			.state('import_product', {
				url: '/import_product',
				templateUrl: 'js/ng/app/import_product/views/index.html',
				controller: 'import_product_ctrl as vm',
				data: { pageTitle: 'Import Product List' + name}
			})
			.state('import_staff', {
				url: '/import_staff',
				templateUrl: 'js/ng/app/import_staff/views/index.html',
				controller: 'import_staff_ctrl as vm',
				data: { pageTitle: 'Import Staff List' + name}
			})
			.state('import_vendor', {
				url: '/import_vendor',
				templateUrl: 'js/ng/app/import_vendor/views/index.html',
				controller: 'import_vendor_ctrl as vm',
				data: { pageTitle: 'Import Vendor List' + name}
			})
			//#endregion

			//#region Setting Route
			.state('exchange-rate', {
				url: '/exchange-rate',
				templateUrl: 'js/ng/app/exchange_rate/views/index.html',
				controller: 'exchange_rate_ctrl as vm',
				data: { pageTitle: 'Exchange Rate' + name}
			})
			.state('company-profile', {
				url: '/company-profile',
				templateUrl: 'js/ng/app/setting/views/index.html',
				controller: 'setting_ctrl as vm',
				data: { pageTitle: 'Company Profile' + name}
			})
			//#endregion

			//#region Start Administrator Route
			.state('role', {
				url: '/role',
				templateUrl: 'js/ng/app/role/views/index.html',
				controller: 'role_ctrl as vm',
				data: { pageTitle: 'Role' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["Role_View", "Role_Edit", "Role_Delete"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){	
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'Role_View' && value.is_selected ){
										found = true;//value.is_selected == 1 ? true: false;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			.state('user', {
				url: '/user',
				templateUrl: 'js/ng/app/user/views/index.html',
				controller: 'user_ctrl as vm',
				data: { pageTitle: 'User' + name},
				resolve:{
					getDetail: [ 'Restful', function (Restful, stateCache) {
						var vm = this;
						vm.input = {
							data: JSON.stringify(["User_View"])
						};
						return Restful.get('api/GetPermission', vm.input).success(function(result){				
							
							//return result.elements;
						});
					}],
                    detailResult: [
                        'getDetail', '$q', '$timeout', '$state',
                        function (getDetail, $q, $timeout, $state) {							
                            if (getDetail.data && getDetail.data.count) {
								var found = false;
								var result = getDetail.data.elements;
								
								angular.forEach(result, function(value, key){
									if (value.feature_name === 'User_View' && value.is_selected ){
										found = true;
										return;
									}
								});	
								if(!found){
									// Prevent migration to default state
									event.preventDefault();
									$state.go('dashboard');
								}
                            } else {
                                return $q.resolve(getDetail);
                            }
                        }
                    ]
				}
			})
			//#endregion
		;
		$urlRouterProvider.otherwise('dashboard');
		// use the HTML5 History API
		//$locationProvider.html5Mode(true);
	}
]);

// for redirect to child from parent
app.run(['$rootScope', '$state', function($rootScope, $state) {
	$rootScope.$on('$stateChangeStart', function(evt, to, params) {
		if (to.redirectTo) {
			evt.preventDefault();
			$state.go(to.redirectTo, params, {location: 'replace'})
		}
	});
}]);
