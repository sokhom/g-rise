app.controller(
    'doctor_payslip_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $anchorScroll){
            var vm = this;
            vm.doctor = {};
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.loading = true;
            vm.init = function(){
                 if(!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.model = angular.copy(vm.staff);
                var input = {
                    staff_id: vm.model.id,
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1
                }
                vm.loading = false;
                Restful.get('api/DoctorPayslipDetail', input).success(function(data){
                    vm.doctor_payslips = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                });
            };

        }
    ]);