app.controller(
	'calendar_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'calendarConfig'
	, function ($scope, Restful, Services,  calendarConfig){
		'use strict';
			var vm = this;
			vm.service = new Services();
		
		  //These variables MUST be set as a minimum for the calendar to work
		  vm.calendarView = 'month';
			vm.viewDate = new Date();
			
		  vm.actions = [{
				label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
				onClick: function(args) {
					console.log('Edited', args.calendarEvent);
				}
		  }, {
				label: '<i class=\'glyphicon glyphicon-remove\'></i>',
				onClick: function(args) {
					console.log('Deleted', args.calendarEvent);
				}
			}];
			
			vm.init = function(){
				var input = {
					start: moment(vm.viewDate).startOf('month').format('YYYY-MM-DD'), 
					end: moment(vm.viewDate).endOf('month').format('YYYY-MM-DD')
				};
				Restful.get('api/RealProcess', input).success(function(res){
					console.log(res);
					vm.events = res.elements.map(function(item){
						item.title = item.invoice_no;
						item.color = calendarConfig.colorTypes.warning;
						item.startsAt = moment(item.shoot_date).toDate();
						item.endsAt = moment(item.shoot_date).toDate();
						item.draggable = true;
						item.resizable = true;
						item.actions = vm.actions;						
						return item;
					});
					console.log(vm.events);

				});
			}
			vm.init();
			
		  // vm.events = [
			// 	{
			// 		title: 'An event',
			// 		color: calendarConfig.colorTypes.warning,
			// 		startsAt: moment().startOf('week').subtract(2, 'days').add(8, 'hours').toDate(),
			// 		endsAt: moment().startOf('week').add(1, 'week').add(9, 'hours').toDate(),
			// 		draggable: true,
			// 		resizable: true,
			// 		actions: vm.actions
			// 	}, {
			// 		title: '<i class="glyphicon glyphicon-asterisk"></i> <span class="text-primary">Another event</span>, with a <i>html</i> title',
			// 		color: calendarConfig.colorTypes.info,
			// 		startsAt: moment().subtract(1, 'day').toDate(),
			// 		endsAt: moment().add(5, 'days').toDate(),
			// 		draggable: true,
			// 		resizable: true,
			// 		actions: vm.actions
			// 	}, {
			// 		title: 'This is a really long event title that occurs on every year',
			// 		color: calendarConfig.colorTypes.important,
			// 		startsAt: moment().startOf('day').add(7, 'hours').toDate(),
			// 		endsAt: moment().startOf('day').add(19, 'hours').toDate(),
			// 		recursOn: 'year',
			// 		draggable: true,
			// 		resizable: true,
			// 		actions: vm.actions
			// 	}
		  // ];
	  
		  vm.cellIsOpen = true;
	  
		  vm.addEvent = function() {
				vm.events.push({
					title: 'New event',
					startsAt: moment().startOf('day').toDate(),
					endsAt: moment().endOf('day').toDate(),
					color: calendarConfig.colorTypes.important,
					draggable: true,
					resizable: true
				});
		  };
	  
		  vm.eventClicked = function(event) {
				console.log('Clicked', event);
		  };
	  
		  vm.eventEdited = function(event) {
				console.log('Edited', event);
		  };
	  
		  vm.eventDeleted = function(event) {
				console.log('Deleted', event);
		  };
	  
		  vm.eventTimesChanged = function(event) {
				console.log('Dropped or resized', event);
		  };
	  
		  vm.toggle = function($event, field, event) {
				$event.preventDefault();
				$event.stopPropagation();
				event[field] = !event[field];
		  };
	  
		  vm.timespanClicked = function(date, cell) {
	  
			if (vm.calendarView === 'month') {
			  if ((vm.cellIsOpen && moment(date).startOf('day').isSame(moment(vm.viewDate).startOf('day'))) || cell.events.length === 0 || !cell.inMonth) {
				vm.cellIsOpen = false;
			  } else {
				vm.cellIsOpen = true;
				vm.viewDate = date;
			  }
			} else if (vm.calendarView === 'year') {
			  if ((vm.cellIsOpen && moment(date).startOf('month').isSame(moment(vm.viewDate).startOf('month'))) || cell.events.length === 0) {
				vm.cellIsOpen = false;
			  } else {
				vm.cellIsOpen = true;
				vm.viewDate = date;
			  }
			}
	  
		  };
	  
	}
]);