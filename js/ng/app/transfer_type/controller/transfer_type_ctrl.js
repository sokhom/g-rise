app.controller(
    'transfer_type_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $anchorScroll){
            'use strict';
            $anchorScroll();
            var vm = this;
            vm.service = new Services();
            var url = 'api/TransferType/';
            vm.init = function(params){
                Restful.get(url, params).success(function(data){
                    vm.transferType = data;
                    vm.totalItems = data.count;
                });
            };
            var params = {pagination: 'yes'};
            vm.init(params);
            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.search = function(id){
                params.name = vm.search_name;
                params.id = vm.search_id;
                vm.init(params);
            };

            vm.edit = function(params){
                vm.model = angular.copy(params);
                $('#transfer-type-popup').modal('show');
            };
            vm.disable = true;
            vm.save = function(){
                if (!$scope.transferForm.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function (data) {
                        vm.init();
                        vm.service.alertMessageSuccess();
                        $('#transfer-type-popup').modal('hide');
                        vm.clear();
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        vm.init();
                        console.log(data);
                        $('#transfer-type-popup').modal('hide');
                        vm.clear();
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    console.log(data);
                    vm.service.alertMessageSuccess();
                });
            };

            vm.clear = function(){
                vm.disable = true;
                vm.model = {};
            };
        }
    ]);