app.controller(
    'report_stock_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Item', 'Type', 'KindOf', 'Barcode',
                'Description', 'Qty On Hand', 'Stock In',
                'Stock Out', 'Balance Qty'];
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.init = function(params){
                vm.csv = [];

                var data = {
                    status: vm.status,
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    filter: vm.filters,
                    kind_of: vm.kind_of
                };
                vm.loading = false;
                vm.products = [];
                Restful.get('stockTransaction.php', data).success(function(data){
                    vm.products = data;
                    vm.totalItems = data.elements.length;
                    console.log(vm.totalItems);
                    vm.loading = true;
                    vm.total_qty = 0;
                    vm.total_price_in = 0;
                    vm.total_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_qty = vm.total_qty + (obj.qty_on_hand + obj.stock_in - obj.stock_out);
                        vm.total_price_in = vm.total_price_in + obj.cost;
                        vm.total_amount = vm.total_amount + ( (obj.qty_on_hand + obj.stock_in - obj.stock_out) * obj.cost);
                    }
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            item: value.product_name,
                            type: value.product_type,
                            kind_of: value.product_kind_of,
                            barcode: value.barcode,
                            description: value.product_description,
                            qty_on_hand: value.qty_on_hand,
                            stock_in: value.stock_in,
                            stock_out: value.stock_out,
                            balance_qty: value.qty_on_hand + value.stock_in - value.stock_out,
       //                      cost: value.cost,
							// price: value.price,
       //                      amount: value.cost * (value.qty_on_hand + value.stock_in - value.stock_out),
                        });
                    });
                });
            };

        }
]);