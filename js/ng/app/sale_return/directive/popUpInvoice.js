app.directive('popUpInvoice',
    function(){
        return {
            restrict: 'EA',
            templateUrl: 'js/ng/app/stock_out/views/popup-invoice.html'
        };
    }
);