app.controller(
	'sale_return_create_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$anchorScroll'
	, '$translate'
	, '$timeout'
	, '$rootScope'
	, function ($scope, Restful, Services, $anchorScroll, $translate, $timeout, $rootScope){
		var vm = this;
		vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 'BIDC', 
			'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank', 'Credit Card', 'Other'];
		$anchorScroll();
		vm.service = new Services();
		vm.stock_out_return_date = moment().format("YYYY-MM-DD");
		/** get currency rate **/
		vm.service.getExchangeRate().success(function (data) {
			vm.exchange_rate_out =  data.elements[0].exchange_rate_out;
			vm.exchange_rate_in =  data.elements[0].exchange_rate_in;
			vm.exchange_id =  data.elements[0].id;
		});

		//init get um type
		Restful.get('api/UMType').success(function(data){
			vm.um_types = data.elements;
		});
	
		vm.getInvoiceDetail = function(){
			$timeout(function() {
				vm.model = vm.invoice;
				vm.model.detail = vm.model.detail.map(function(item){
					item.product_name_before = item.product_name;
					item.discount_cash_before = item.discount_cash;
					item.discount_percent_before = item.discount_percent;
					item.total_before = item.total;
					item.price_before = item.price;
					return item;
				});
			}, 50);
		};

		/** Functionality for update model change when UM update **/
		vm.updateModel = function(params){
			if(params.type=="whole_sale"){
				params.unit_price = params.price_whole_sale;
			}else{
				params.unit_price = params.price_retail;
			}
			vm.getTotal();
		};

		vm.qty_in_hand = '';
		// Event for calculate qty with price when input
		vm.calculate = function() {
			vm.total = (vm.qty * vm.product_filter.products_price_out).toFixed(2) ;
		};

		// functional get total of all products
		vm.getTotal = function(){
			vm.model.sub_total = 0;
			for (var i = 0, l = vm.model.detail.length; i < l; i++) {
				var obj = vm.model.detail[i];
				//console.log(obj);
				//var sub_total = obj.qty * obj.price;
				// var b = ( (obj.unit_price + obj.add_more_price) * obj.qty) - obj.discount_cash - 
				// (( (obj.unit_price + obj.add_more_price) * obj.qty) * obj.discount_percent / 100);
				obj.add_qty = obj.add_qty ? obj.add_qty : 0;
				var totalAmount = (obj.qty + obj.add_qty) * (obj.price + obj.add_more_price);
				var disAmount = totalAmount * (obj.discount_percent / 100);
				var totalDisAmount = (totalAmount - disAmount) - obj.discount_cash;
				vm.model.sub_total = vm.model.sub_total + totalDisAmount;
			}
			vm.model.sub_total.toFixed(2);
			vm.inputDiscount();
			// vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
			// vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
		};

		// calculate money
		vm.inputPayment = function(){
			if(vm.model.payment > 0){
				vm.model.remain = (vm.model.grand_total - vm.model.payment).toFixed(2);
			}else{
				vm.model.remain = vm.model.grand_total;
			}
		};

		vm.save = function(){
			if (!$scope.saleForm.$valid) {
				//$anchorScroll();
				return;
			}
			vm.today = new Date();
			if(vm.model.detail.length == 0){
				return vm.service.alertMessageError($translate.instant('RecordNotFound'));
			}
			vm.model.detail = vm.model.detail.map(function(item){
				delete item.edit;
				return item;
			});
			// copy data to print out
			// vm.printData = angular.copy(vm.model);
			// vm.printData.remain = vm.remain > 0 ? vm.remain : 0;
			console.log(vm.model);
			vm.disable = true;
			Restful.save('api/StockOutReturn', vm.model).success(function (data) {
				 // call from parent scope function in main.js
				// $rootScope.$emit("InitNotificationMethod", {});
				console.log(data);
				// vm.invoiceNo = data.invoice_no;
				vm.service.alertMessageSuccess();
				// $('#invoice-popup').modal('show');
				clearList();
				clearAll();
			});
		};

		function clearAll(){
			vm.disable = false;
			vm.model = null;
			vm.invoice = null;
			$scope.saleForm.$submitted = false;
		};
		// Functionality for scan barcode input
		vm.filterProduct = function(filter){
			//console.log(filter);
			var params = {barcode: filter, status: 1, is_sale: 1};
			Restful.get('api/Products', params).success(function(data) {
				vm.product_filter.selected = data.elements[0];
				//console.log(data);
				if(data.count > 0 ){
					vm.add();
				} else{
					vm.service.alertMessageError($translate.instant('RecordNotFound'));
				}
			});
			
		};
		// Functionality for scan service barcode input
		vm.filterService = function(){
			var params = {code: vm.service_text, status: 1, is_sale: 1};
			Restful.get('api/Service', params).success(function(data) {
				vm.product_filter.selected = data.elements[0];
				//console.log(data);
				if(data.count > 0 ){
					vm.add();
				} else{
					vm.service.alertMessageError($translate.instant('RecordNotFound'));
				}
			});
			
		};
		
		vm.product_filter = {};
		vm.addToList = function(){
			$timeout(function() {
				//vm.add();
				if(vm.product_filter.selected.products_kind_of){
					Restful.get('api/Products', {id: vm.product_filter.selected.id, is_sale: 1}).success(function(data) {
						vm.product_filter.selected = data.elements[0];
						// console.log(data);
						if(data.count > 0 ){
							vm.add();
						} else{
							vm.service.alertMessageError($translate.instant('RecordNotFound'));
						}
					});
				}else{
					vm.add();
				}
			}, 50);
		};

		vm.add = function(){
			vm.qty = 1;
			// console.log(vm.model);
			// console.log(vm.product_filter);
			if(vm.product_filter.selected.products_kind_of){
				vm.qty_in_hand = vm.product_filter.selected.products_quantity;
				// check if qty has in stock add
				if(vm.qty <= vm.qty_in_hand || vm.product_filter.selected.products_kind_of == 'service') {
					// check if exist in list
					for (var i = 0, l = vm.model.detail.length; i < l; i++) {
						var obj = vm.model.detail[i];
						if (obj.product_id === vm.product_filter.selected.id) {
							var old_qty = parseInt(obj.qty) + parseInt(vm.qty);
							// check again in existing object
							if( old_qty <= vm.qty_in_hand || vm.product_filter.selected.products_kind_of == 'service'){
								obj.qty = old_qty;
								//obj.unit_price = vm.product_filter.selected.products_price_out;
								obj.description = vm.product_filter.selected.products_description,
								obj.total = obj.qty * obj.unit_price;
								vm.getTotal();
								clearList();
								return;
							}else{
								return  vm.service.alertMessage(
									'warning:','OPP! Out Off Stock. You Have Only ' +
									vm.qty_in_hand +' Unit In Stock.','warning'
								);
							}
						}
					}
					vm.model.detail.push({
						product_id: vm.product_filter.selected.id,
						product_name: vm.product_filter.selected.products_name,
						description: vm.product_filter.selected.products_description,
						product_type: vm.product_filter.selected.products_type_fields[0].name,
						product_type_id: vm.product_filter.selected.products_type_fields[0].id,
						barcode: vm.product_filter.selected.barcode,
						qty_on_hand: vm.product_filter.selected.products_quantity,
						cost: vm.product_filter.selected.products_price_in,
						product_kind_of: vm.product_filter.selected.products_kind_of,
						qty: vm.qty,
						remark: '',
						add_more_price: 0,
						discount_cash: 0,
						discount_percent: 0,
						price: vm.product_filter.selected.products_price_out_whole_sale,
						price_whole_sale: vm.product_filter.selected.products_price_out_whole_sale,
						price_retail: vm.product_filter.selected.products_price_out_retail,
						um_type_whole_sale_id: vm.product_filter.selected.um_detail.length > 0 ? vm.product_filter.selected.um_detail[0].id : '',
						um_type_whole_sale_name: vm.product_filter.selected.um_detail.length > 0 ? vm.product_filter.selected.um_detail[0].name : '',
						um_type_whole_sale_amount: vm.product_filter.selected.um_detail.length > 0 ? vm.product_filter.selected.um_detail[0].amount : '',
						type: 'whole_sale',
						add_qty: 1,
						um_type_retail_id: vm.product_filter.selected.um_sale_detail.length > 0 ? vm.product_filter.selected.um_sale_detail[0].id : '',
						um_type_retail_name: vm.product_filter.selected.um_sale_detail.length > 0 ? vm.product_filter.selected.um_sale_detail[0].name : '',
						um_type_retail_amount: vm.product_filter.selected.um_sale_detail.length > 0 ? vm.product_filter.selected.um_sale_detail[0].amount : '',
						total: vm.qty * parseInt(vm.product_filter.selected.products_price_out)
					});

					vm.getTotal();
					clearList();
				}else{
					return vm.service.alertMessage(
						'warning:','OPP! Out Off Stock. You Have Only ' +
						vm.qty_in_hand +' Unit In Stock.','warning'
					);
				}
			}else{
				// check if exist in list
				for (var i = 0, l = vm.model.detail.length; i < l; i++) {
					var obj = vm.model.detail[i];
					if (obj.product_id === vm.product_filter.selected.id) {
						var old_qty = parseInt(obj.qty) + parseInt(vm.qty);
						obj.qty = old_qty;
						//obj.unit_price = vm.product_filter.selected.products_price_out;
						obj.description = vm.product_filter.selected.products_description,
						obj.total = obj.qty * (obj.price + obj.add_more_price);
						vm.getTotal();
						clearList();
						return;
					}
				}
				vm.model.detail.push({
					add_qty: 1,
					product_id: vm.product_filter.selected.id,
					product_name: vm.product_filter.selected.name,
					description: vm.product_filter.selected.detail,
					product_type: vm.product_filter.selected.type_detail[0].name,
					product_kind_of: 'service',
					add_more_price: 0,
					discount_cash: 0,
					discount_percent: 0,
					product_type_id: vm.product_filter.selected.type_id,
					barcode: vm.product_filter.selected.code,					
					cost: vm.product_filter.selected.price,
					qty: vm.qty,
					discount_cash: 0,
					discount_percent: 0,
					price: vm.product_filter.selected.price,
					price_whole_sale: vm.product_filter.selected.price,
					price_retail: vm.product_filter.selected.price,					
					total: vm.qty * parseInt(vm.product_filter.selected.price)
				});

				vm.getTotal();
				clearList();
			}
		};

		vm.remove = function($index){
			vm.model.detail.splice($index, 1);
			vm.getTotal();
		};
		function clearList(){
			vm.product_filter = {};
			vm.item_filter = '';
			vm.service_text = '';
		};

		// function calculate discount when change value
		vm.inputDiscount = function(){
			if(vm.model.discount_type === "percent"){
				// console.log(vm.model.discount_type, ' input discount percent');
				vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
				vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
				vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
			}else{
				// console.log(vm.discount_type, ' dollar discount $');
				vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
				vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
				vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
			}
		};

		// functionality calculate discount type
		vm.checkTypeDiscount = function() {
			//vm.discount_type = type;
			if(vm.model.discount_type === "percent"){
				// console.log(vm.model.discount_type, ' Percent%');
				vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
				vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
				vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
				// console.log(vm.model.remain, 'Discount Percent');
			}else{
				// console.log(vm.model.discount_type, ' Dollar$');
				vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
				vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
				vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
				// console.log(vm.model.remain, 'Discount Dollar');
			}
		};

		vm.addMorePrice = function(params,$index){
			$scope.formAdd.$submitted = false;
			$("#addMorePrice").modal("show");
			vm.copyModel = angular.copy(params);
			vm.index = $index;
		};

		vm.saveMorePrice = function(){
			if (!$scope.formAdd.$valid) {
				$anchorScroll();
				return;
			}
			vm.model.detail[vm.index] = vm.copyModel;
			$("#addMorePrice").modal("hide");
			vm.getTotal();
		}
	}
]);
