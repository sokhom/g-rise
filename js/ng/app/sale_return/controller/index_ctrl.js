app.controller(
	'sale_return_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$anchorScroll'
	, 'SweetAlert'
	, '$translate'
	, '$rootScope'
	, function ($scope, Restful, Services, $anchorScroll, SweetAlert, $translate, $rootScope){
		$anchorScroll();
		var vm = this;
		vm.service = new Services();
		vm.params = {pagination: 'yes'};
		vm.filterText = '';
		vm.search = function(){
			vm.params.invoice_no = vm.filterText;
			init(vm.params);
		};
		vm.convertDate = function(date){
			return new Date(date);
		};

		var url = 'api/StockOutReturn/';
		function init(params){
			vm.loading = true;
			Restful.get(url, params).success(function(data){
				vm.invoiceList = data;
				console.log(data);
				vm.totalItems = data.count;
			}).finally(function(){
				vm.loading = false;
			});
		};
		init(vm.params);
		
		vm.currentPage = 1;
		vm.pageChanged = function(){
			vm.pageSize = 10 * ( vm.currentPage - 1 );
			vm.params.start = vm.pageSize;
			init(vm.params);
		};

		vm.updateStatus = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.stock_out_no }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					params.sub_total = 0;
					angular.forEach(params.detail, function(obj) {
						console.log(obj);
						obj.add_qty = obj.add_qty ? obj.add_qty : 0;
						var totalAmount = (obj.qty - obj.add_qty) * (obj.price + obj.add_more_price);
						var disAmount = totalAmount * (obj.discount_percent / 100);
						var totalDisAmount = (totalAmount - disAmount) - obj.discount_cash;
						params.sub_total += params.sub_total + totalDisAmount;

						// var qty = element.qty - element.add_qty;
						// params.subTotal += qty * params.price;
					});
					params.grand_total = params.sub_total - params.discount_total_amount;
					params.remain = params.grand_total - params.payment;				
					params.status === 1 ? params.status = 0 : params.status = 1;
					params.sub_total = 0;
					for (var i = 0, l = params.detail.length; i < l; i++) {
						var obj = params.detail[i];
						obj.add_qty = obj.add_qty ? obj.add_qty : 0;
						var totalAmount = (obj.qty) * (obj.price + obj.add_more_price);
						var disAmount = totalAmount * (obj.discount_percent / 100);
						var totalDisAmount = (totalAmount - disAmount) - obj.discount_cash;
						params.sub_total = params.sub_total + totalDisAmount;
					}
					params.sub_total.toFixed(2);
					if(params.discount_type === "percent"){
						// console.log(vm.model.discount_type, ' input discount percent');
						params.discount_total_amount = ((params.discount_amount / 100) * params.sub_total).toFixed(2);
						params.grand_total = params.sub_total - params.discount_total_amount;
						params.remain =  params.grand_total - (params.payment > 0 ? params.payment : 0);
					}else{
						// console.log(vm.discount_type, ' dollar discount $');
						params.discount_total_amount = params.discount_amount ? params.discount_amount : 0;
						params.grand_total = params.sub_total - params.discount_total_amount;
						params.remain = params.grand_total - (params.payment > 0 ? params.payment : 0);
					}
					// params.grand_total = params.sub_total - params.discount_total_amount;
					// params.remain = params.grand_total - (params.payment > 0 ? params.payment : 0);
					console.log(params);	
					
					Restful.patch(url + params.id, params ).success(function(data) {
						console.log(data);
						// call from parent scope function in main.js
						// $rootScope.$emit("InitNotificationMethod", {});
						SweetAlert.swal({
							title: $translate.instant('Disable'), 
							text: $translate.instant('SuccessDisable'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 

		};

	}
]);
