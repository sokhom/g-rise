app.controller(
    'report_purchase_summary_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            vm.loading = true;
            vm.vendor = '';
            vm.init = function(){
                if (!$scope.report.$valid) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    vendor_id: vm.vendor.id,
                    status: 'yes'
                };
                vm.loading = false;
                console.log(data);
                vm.purchase = [];
                Restful.get('api/Purchase', data).success(function(data){
                    vm.purchase = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.sub_total = 0;
                    vm.total_balance = 0;
                    vm.total_payment_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        obj.remain = obj.remain < 0 ? 0 : obj.remain;
                        vm.total_balance = vm.total_balance + obj.remain;
                        vm.total_payment_amount = vm.total_payment_amount + obj.payment;
                        vm.sub_total = vm.sub_total + obj.total;
                    }
                    vm.loading = true;
                });
            };

        }
]);