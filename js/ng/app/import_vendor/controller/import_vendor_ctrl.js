app.controller(
    'import_vendor_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'Upload'
        , '$timeout'
        , function ($scope, Restful, Services, Upload, $timeout){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;

            //functionality upload
            vm.uploadFile = function(file) {
                if (file) {
                    vm.loading = false;
                    file.upload = Upload.upload({
                        url: 'api/ImportVendor',
                        data: {file: file},
                    });
                    file.upload.then(function (response) {
                        vm.status = response.data.status;
                        vm.loading = true;
                        console.log(response);
                    }, function (response) {
                        if (response.status > 0)
                            vm.errorMsgCV = response.status + ': ' + response.data;
                    }, function (evt) {
                        // Math.min is to fix I	E which reports 200% sometimes
                        file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                    });
                }
            };
        }
    ]);