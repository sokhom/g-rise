app.controller(
	'location_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/Location/';
		vm.model = {};

		Restful.get('api/LocationType').success(function(data){
			vm.locationTypeList = data;
		});
		vm.init = function(){
			Restful.get(url, vm.params).success(function(data){
                vm.collection = data;
                vm.totalItems = data.count;
			});
		};
		vm.params = {paginate:'yes'};
		vm.init();
		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				vm.service.alertMessageSuccess();
				console.log(data);
			});
		};

		vm.search = function(id){
			vm.params.filter = vm.search_name;
			vm.init();
		};

		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			vm.params.start = vm.pageSize;
            vm.init();
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#location-type-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.locationForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
					vm.init();
					console.log(data);
					vm.service.alertMessageSuccess();
					$('#location-type-popup').modal('hide');
                    vm.clear();
                    vm.disable = true;
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
                    vm.init();
					$('#location-type-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
					vm.disable = true;
					console.log(data);
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.locationForm.$submitted = false;
            vm.model = {};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.user_name }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(url + params.id, params ).success(function(data) {
						console.log(data);
						vm.init();
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);