app.controller(
    'received_payment_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.service = new Services();
            vm.loading = false;
            function init(){
                vm.loading = true;
                var data = {payment_no: $stateParams.invoice_no};
                Restful.get('api/ReceivePayment/' + $stateParams.id, data).success(function(data){
                    vm.model = data.elements[0];
                    console.log(data);    
                    getExchangeRate(vm.model.exchange_id);
                    getCustomer(vm.model.customer_id);
                    getInvoiceDetail(vm.model.detail[0].invoice_no);
                }).finally(function(response){
                    vm.loading = false;
                });
            };
            init();

            var getExchangeRate = function (id) {
                vm.service.getExchangeRate(id).success(function (data) {
                    vm.exchange_rate_out =  data.elements[0].exchange_rate_out;
                    vm.exchange_rate_in =  data.elements[0].exchange_rate_in;
                    vm.exchange_id =  data.elements[0].id;
                });
            }

            function getInvoiceDetail(id){
                 Restful.get('api/Invoice/', {invoice_no: id}).success(function(data){
                    console.log(data);
                    var data = data.elements[0];
                    // replace sub total model instead of copy from list invoice
                    var model = angular.copy(vm.model);                    
                    model.sub_total = data.sub_total;
                    model.grand_total = data.grand_total - vm.model.discount_total_amount;
                    model.adds = vm.model.payment;  
                    model.discount_before = data.total_discount; 
                    model.deposit = vm.model.detail[0].deposit;//data.payment - vm.model.payment - vm.model.discount_total_amount;
                    model.discount = data.discount_total_amount;
                    vm.invoice_detail = data;
                    vm.printData = {
                        stock: model,
                        stock_detail: data.detail
                    };
                });
            };

            function getCustomer(id){
                 Restful.get('api/CustomerList/', {id: id}).success(function(data){
                    vm.copyCustomer = data.elements[0];
                });
            };

            vm.viewDeail = function(){
                vm.viewDetail = vm.invoice_detail;
            }
            
        }
    ]);