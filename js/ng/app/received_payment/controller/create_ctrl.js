app.controller(
    'received_payment_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $translate){
            var vm = this;
            vm.service = new Services();
            vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 
                'BIDC', 'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank', 'Credit Card', 'Other'];
            $anchorScroll();
            vm.dotorObj = {};
            vm.service = new Services();
            /** get currency rate **/
            vm.service.getExchangeRate().success(function (data) {
                vm.exchange_rate_out =  data.elements[0].exchange_rate_out;
                vm.exchange_rate_in =  data.elements[0].exchange_rate_in;
                vm.exchange_id =  data.elements[0].id;
            });
            vm.initVariable = function () {
                vm.customerObj = null;
                vm.listData = [];
                vm.model = {
                    receive_payment_date: moment().format('YYYY-MM-DD'),
                    payment_method: 'Cash',
                    sub_total: 0,
                    grand_total: 0,
                    discount_total_amount: 0,
                    remain: 0,
                    bank_charge: '',
                    discount_amount: '',
                    discount_type: '',
                };
                vm.viewDetail = {};
            };
            vm.initVariable();
            // init get term condition
            Restful.get('api/InvoiceTerm', {type: 'receive_payment'}).success(function(data){
                vm.termConditions = data.elements;
            });
            
            vm.filterInvoice = function(){
                //10 seconds delay
                $timeout( function(){
                    var input = {
                        invoice_no: vm.invoiceNo,
                        balance: 'yes',
                        status: 1,
                        customer_id: vm.customerObj != null ? vm.customerObj.id : ''
                    };
                    vm.loading = true;
                    console.log(input);
                    Restful.get('api/Invoice', input).success(function(data){
                        vm.copyListInvoice = data.elements;
                        vm.listData = data.elements.map(function (item) {
                            console.log(item);
                            return {
                                invoice_no: item.invoice_no,
                                invoice_date: item.invoice_date,
                                customer_id: item.customer_id,
                                customer_name: item.customer_name,
                                // customer_telephone: item.customer_telephone,
                                customer_code: item.customer_code,
                                balance: item.balance,
                                sub_total: item.sub_total,
                                discount_amount: item.total_discount,
                                grand_total: item.grand_total,
                                payment: '',
                                deposit: item.deposit,
                            };
                        });
                        vm.getTotal();
                    }).finally(function(){
                        vm.loading = false;
                    });
                }, 100 );

            };

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                console.log(vm.copyListInvoice);
                vm.copyListInvoice.splice($index, 1);
                vm.getTotal();
            };
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.sub_total = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    //console.log(obj);
                    //var sub_total = obj.qty * obj.price;
                    vm.model.sub_total = vm.model.sub_total + obj.balance;
                }
                vm.model.sub_total.toFixed(2);
                vm.model.grand_total = vm.model.sub_total;
                vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
            };


            // function calculate discount when change value
            vm.inputDiscount = function(){
                if(vm.model.discount_type === "percent"){
                    console.log(vm.model.discount_type, ' input discount percent');
                    vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                }else{
                    console.log(vm.discount_type, ' dollar discount $');
                    vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                }
            };

            // functionality calculate discount type
            vm.checkTypeDiscount = function() {
                //vm.discount_type = type;
                if(vm.model.discount_type === "percent"){
                    console.log(vm.model.discount_type, ' Percent%');
                    vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                    console.log(vm.model.remain, 'Discount Percent');
                }else{
                    console.log(vm.model.discount_type, ' Dollar$');
                    vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                    console.log(vm.model.remain, 'Discount Dollar');
                }
            };

            // calculate money
            vm.inputPayment = function(){
                if(vm.model.payment > 0){
                    vm.model.remain = (vm.model.grand_total - vm.model.payment).toFixed(2);
                }else{
                    vm.model.remain = vm.model.grand_total;
                }
            };

            /*******************************************
             * Functionality for save Receive Payment  *
             *******************************************/
            vm.save = function () {
                if (!$scope.saleForm.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.listData.length < 0 || !vm.listData){
                    vm.service.alertMessageError($translate.instant('RecordPaymentFound'));
                    return;
                }
                if(vm.listData.length != 1){
                    vm.service.alertMessageError($translate.instant('NotePayment'));
                    return;
                }
                vm.loading = true;
                // copy item list of record to print it out 
                vm.copyListInvoice = vm.copyListInvoice[0];
                console.warn(vm.copyListInvoice);
                vm.model.exchange_id =  vm.exchange_id;
                vm.model.customer_id =  vm.copyListInvoice.customer_id;
                vm.model.customer_name =  vm.copyListInvoice.customer_name;
                vm.model.customer_telephone =  vm.copyListInvoice.customer_tel;
                vm.model.customer_event_date = vm.copyListInvoice.customer_event_date;
                vm.model.customer_staff_responsible = vm.copyListInvoice.customer_staff_responsible;
                vm.model.customer_staff_responsible_id = vm.copyListInvoice.customer_staff_responsible_id;
                vm.model.customer_shoot_date = vm.copyListInvoice.customer_shoot_date;
                vm.model.customer_code =  vm.copyListInvoice.customer_code;
                vm.model.last_overdue = vm.copyListInvoice.overdue;
                vm.listData[0].payment = vm.model.payment;
                if(vm.customerObj){
                    vm.copyCustomer = vm.customerObj;
                }else{
                    Restful.get('api/CustomerList/' + vm.model.customer_id).success(function(data){
                        vm.copyCustomer = data.elements[0];
                        console.log(vm.copyCustomer);
                    });
                }
                var data = {
                    receive_payment: vm.model,
                    cash_flow: {
                        customer_id: vm.model.customer_id,
                        customer_code: vm.model.customer_code,
                        customer_name: vm.model.customer_name,
                        payment_method: vm.model.payment_method,
                        payment: vm.model.payment
                    },
                    receive_payment_detail: vm.listData
                };
                console.log(data);
                Restful.save('api/ReceivePayment', data).success(function(data){
                    console.log(data);
                    // replace sub total model instead of copy from list invoice
                    vm.model.sub_total = vm.copyListInvoice.sub_total;
                    vm.model.grand_total = vm.copyListInvoice.grand_total;
                    // custom variable view for print
                    vm.model.adds = angular.copy(vm.model.payment);
                    vm.model.deposit = vm.copyListInvoice.deposit;    
                    vm.model.discount_before = vm.copyListInvoice.total_discount;    
                                    
                    // End custom
                    vm.printData = {
                        stock: angular.copy(vm.model),
                        stock_detail: vm.copyListInvoice.detail
                    };  
                    console.log(vm.printData);
                    vm.printData.stock.receive_payment_no = data.invoice_no;
                    
                    $scope.saleForm.$submitted = false;
                    $('#invoice-popup').modal('show');

                    vm.service.alertMessageSuccess();
                    vm.initVariable();
                }).finally(function(){
                    vm.loading = false;
                });
            };

            vm.viewDeail = function(params){
                angular.forEach(vm.copyListInvoice, function(item, index){
                    if(item.invoice_no==params.invoice_no){
                        vm.viewDetail = item;
                    }
                });
                console.log(vm.viewDetail);
            };

        }
    ]);