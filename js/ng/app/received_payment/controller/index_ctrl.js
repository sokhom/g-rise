app.controller(
    'received_payment_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , function ($scope, Restful, Services, SweetAlert, $translate){
            var vm = this;
            vm.service = new Services();
            vm.loading = false;
            vm.params = {pagination: 'yes'};
            vm.init = function(){
                vm.loading = true;
                Restful.get('api/ReceivePayment', vm.params).success(function(data){
                    vm.receivedPayments = data;
                    console.log(data);
                    vm.totalItems = data.count;
                }).finally(function(){
                    vm.loading = false;
                });
            };
            vm.init(vm.params);
            // search by invoice
            vm.search = function(){
                vm.params.payment_no = vm.filter;
                vm.init();
            };

            vm.updateStatus = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.receive_payment_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        params.status === 1 ? params.status = 0 : params.status = 1;
                        var data = {
                            status: params.status,
                            payment: params.payment + params.discount_total_amount,
                            invoice_no: params.detail[0].invoice_no,
                            customer_id: params.customer_id,
                            last_overdue: params.last_overdue
                        };
                        console.log(data);
                        Restful.patch('api/ReceivePayment/' + params.id, data ).success(function(data) {
                            //vm.service.alertMessageSuccess();
                            SweetAlert.swal({
                                title: $translate.instant('Disable'), 
                                text: $translate.instant('SuccessDisable'), 
                                type: "success",
                                timer: 1000
                            });
                            console.log(data);
                        });
                    }
                });                
            };

            vm.delete = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.receive_payment_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete('api/ReceivePayment/' + params.id).success(function(data) {
                            vm.pageChanged();
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: $translate.instant('SuccessDelete'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };

            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init(vm.params);
            };

        }
    ]);