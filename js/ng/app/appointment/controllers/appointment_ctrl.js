app.controller(
	'appointment_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$rootScope'
	, function ($scope, Restful, Services, $rootScope){
		'use strict';
		var vm = this;
		vm.loading = false;
		vm.service = new Services();
		var url = 'api/Appointment/';
		vm.init = function(params){
			vm.loading = true;
			params.from_date = vm.from_date;
			params.to_date = vm.to_date;
			Restful.get(url, params).success(function(data){
				vm.appointments = data.elements.map(function(item){
					item.appointment_period = new Date(item.appointment_period.replace(/-/g, "/"));
					return item;
				});
				vm.loading = false;
				vm.totalItems = data.count;
				console.log(vm.appointments);
			});
		};
		vm.params = {pagination:'yes'};
		vm.init(vm.params);
		vm.updateStatus = function(params, number){
			params.status = number;
			Restful.patch(url + params.id, params ).success(function(data) {
				vm.service.alertMessageSuccess();
				// call from parent scope function in main.js
				$rootScope.$emit("InitNotificationMethod", {});
			});
		};

		vm.search = function(){
			vm.params.customer_name = vm.customer_name;
			vm.init(vm.params);
		};
		/**
		 * start functionality pagination
		 */
		vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
			vm.pageSize = 10 * ( vm.currentPage - 1 );
			vm.params.start = vm.pageSize;
			vm.init(vm.params);
		};

		vm.remove = function($index){
			vm.dateList.splice($index, 1);
		};
	}
]);