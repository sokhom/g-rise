app.controller(
	'appointment_create_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$anchorScroll'
	, '$state'
	, '$rootScope'
	, '$stateParams'
	, function ($scope, Restful, Services, $anchorScroll, $state, $rootScope, $stateParams){
		'use strict';
		var vm = this;
		vm.disable = true;

		vm.ismeridian = true;
		vm.hstep = 1;
		vm.mstep = 30;

		vm.service = new Services();
		var url = 'api/Appointment/';
		vm.appointment = {
			appointment_date: moment().format('YYYY-MM-DD'),
			appointment_period: new Date(),
		};
		
		var currentPage = $state.current.name;
		if(currentPage == 'appointment.edit'){
			Restful.get(url + $stateParams.id).success(function (data) {
				console.log(data);
				vm.appointment = data.elements[0];
				// vm.appointment.appointment_period = vm.service.convertDateToUTC(vm.appointment.appointment_period);
				vm.customerObj = data.elements[0].customer_detail[0];
				vm.doctorObj = data.elements[0].doctor_detail[0];
			});
		}
		vm.save = function(){
			if (!$scope.appointmentCreate.$valid) {
				$anchorScroll();
				return;
			}
			vm.appointment.customer_id = vm.customerObj.id;
			vm.appointment.doctor_id = vm.doctorObj.id;
			//@todo refactor
			vm.appointment.appointment_period = moment(vm.appointment.appointment_period).format("YYYY-MM-DD HH:mm:ss");
			console.log(vm.appointment);
			vm.disable = false;
			if(vm.appointment.id) {
				Restful.put( url + vm.appointment.id, vm.appointment).success(function (data) {
					vm.service.alertMessageSuccess();
					vm.back();
					console.log(data);
					vm.disable = true;
					// call from parent scope function in main.js
					$rootScope.$emit("InitNotificationMethod", {});
				});
			}else {
				Restful.save( url, vm.appointment).success(function (data) {
					vm.back();
					console.log(data);
					vm.service.alertMessageSuccess();					
				}).finally(function(response){
					console.log(response);
					vm.disable = true;
					// call from parent scope function in main.js
					$rootScope.$emit("InitNotificationMethod", {});
				});
			}
		};

		vm.back = function(){
			$state.go('appointment.list');
		};

	}
]);