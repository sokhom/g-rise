app.controller(
    'package_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, $translate, $rootScope){
            'use strict';
            var vm = this;
            $anchorScroll();
            vm.service = new Services();
            var url = 'api/Package/';
            vm.init = function(params){
                Restful.get(url + $stateParams.id).success(function (data) {
                    vm.model = data.elements[0];
                });
            };
            vm.init();
        }
    ]);