app.controller(
    'package_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, $translate, $rootScope){
            'use strict';
            var vm = this;
            $anchorScroll();
            vm.model = {
                option_detail: [],
                add_on_detail: []
            };
            vm.service = new Services();
            var url = 'api/Package/';
            vm.init = function(params){
                if($state.current.name == 'package.edit') {
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        console.log(data);
                    });
                }
                Restful.get('api/Option').success(function(data){
                    vm.optionList = data.elements;
                });
                Restful.get('api/AddOn').success(function(data){
                    vm.addOnList = data.elements;
                });
                Restful.get('api/PackageType').success(function(data){
                    vm.packageTypeList = data.elements;
                });
            };
            var params = {pagination: 'yes'};
            vm.init(params);

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        console.log(data);
                        if(data == 'Duplicate'){
                            vm.disable = true;
                            vm.duplicate = 'Duplicate Barcode.';
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('package.list');
                        }
                    }).finally(function(){
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        if(data.error){
                            vm.duplicate = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('package.list');
                        }
                    }).finally(function () {
                        vm.disable = true;
                    });
                }
            };

            vm.addOption = function(){  
                vm.model.option_detail.push({
                    option_id: '',
                    description: ''
                });
            };

            vm.deleteOption = function(index){
                vm.model.option_detail.splice(index, 1);
            };

            vm.addMoreAddOn = function(){
                vm.model.add_on_detail.push({
                    option_id: '',
                    description: ''
                });
            };
 
            vm.deleteAddOn = function(index){
                vm.model.add_on_detail.splice(index, 1);
            };
        }
    ]);