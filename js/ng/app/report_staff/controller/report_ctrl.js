app.controller(
    'report_staff_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.doctor = {};
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['#', 'Name', 'Doctor Type', 'Gender', 'DOB', 'Basic Salary', 'Email',
                'Telephone', 'Address'];
            vm.init = function(){
                vm.csv = [];
                var id = '';
                if(angular.isDefined(vm.doctor.selected)){
                    id = vm.doctor.selected.id;
                }
                var data = {
                    id: id
                };
                vm.loading = false;
                Restful.get('api/DoctorList', data).success(function(data){
                    vm.doctors = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            id: key + 1,
                            Name: value.name,
                            DoctorType: value.doctor_type[0].name,
                            gender: value.sex,
                            dob: vm.service.dateFormat(value.dob),
                            basic_salary: value.basic_salary,
                            email: value.email,
                            telephone: value.phone,
                            address: value.address,
                        });
                    });
                });
            };

        }
    ]);