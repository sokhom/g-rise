app.controller(
    'income_statement_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.init = function(params){
                if (!vm.from_date || !vm.to_date) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date
                };
                vm.loading = true;
                Restful.get('api/IncomeStatement', data).success(function(data){
                    vm.model = data;
                    vm.totalRevenues = data.total_income_item + data.total_income_service;
                    vm.other_expense = 0;
                    data.other_expense.map(function(item){
                        vm.other_expense = vm.other_expense + item.amount;
                    });
                    console.log(vm.other_expense);
                    vm.totalExpenses = data.total_cost_expense + data.total_purchase + data.total_payslip + vm.other_expense;
                    vm.netIncome = vm.totalRevenues - vm.totalExpenses;
                    console.log(vm.totalExpenses);
                    vm.loading = false;
                    console.log(data);
                   // $scope.netIncome = data.total_master[0].total - data.total_master[1].total - data.total_master[2].total;
                });
            };

        }
    ]);