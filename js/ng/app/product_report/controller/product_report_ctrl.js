app.controller(
    'product_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.doctor = {};
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Name','Type', 'Kind Of', "UM Type", "Sale Out", 'Code', 'Cost',
                'Price', 'Qty On Hand', 'Order Alert', 'Description'];
            vm.init = function(){
                vm.csv = [];
                vm.loading = false;
                Restful.get('api/Products', {kind_of: 'item', status: 1}).success(function(data){
                    vm.products = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                    vm.total = 0;
                    angular.forEach(data.elements, function(value, key) {
                        vm.total = vm.total + (value.products_price_in * value.products_quantity);
                        vm.csv.push({
                            Name: value.products_name,
                            Type: value.products_type_fields[0].name,
                            KindOf: value.products_kind_of,
                            um_type: value.um_detail[0].name,
                            um_sale: value.um_sale_detail[0].name,
                            code: value.barcode.length > 10 ? "'" + value.barcode : value.barcode,//add string to detect if barcode is long
                            cost: value.products_price_in,
                            price: value.products_price_out,
                            qty: value.products_quantity,
                            orderAlert: value.order_alert,
                            description: value.products_description
                        });
                    });
                });
            };

        }
    ]);