app.directive('invoicePrint',
    function(){
        return {
            restrict: 'EA',
            templateUrl: 'js/ng/app/invoice/views/popup-invoice.html'
        };
    }
);