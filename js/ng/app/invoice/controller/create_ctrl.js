app.controller(
	'invoice_create_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$anchorScroll'
	, '$translate'
	, '$timeout'
	, '$rootScope'
	, function ($scope, Restful, Services, $anchorScroll, $translate, $timeout, $rootScope){
		var vm = this;
		vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 'BIDC', 
			'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank', 'Credit Card', 'Other'];
		$anchorScroll();
		vm.service = new Services();
		/** get currency rate **/
		vm.service.getExchangeRate().success(function (data) {
			vm.exchange_rate_out = data.elements[0].exchange_rate_out;
			vm.exchange_rate_in = data.elements[0].exchange_rate_in;
			vm.exchange_id = data.elements[0].id;
		});

		// init get term condition
		Restful.get('api/InvoiceTerm', {type: 'invoice'}).success(function(data){
			vm.termConditions = data.elements;
		});
		
		vm.model = {
			invoice_date: moment().format('YYYY-MM-DD'),
			payment_method: 'Cash',
			sub_total: 0,
			grand_total: 0,
			total_discount: 0,
			balance: 0,
			discount: '',
			discount_type: '',
		};

		vm.listData = [];
		// functional get total of all products
		vm.getTotal = function(){
			vm.model.sub_total = 0;
			for (var i = 0, l = vm.listData.length; i < l; i++) {
				var obj = vm.listData[i];
				//console.log(obj);
				//var sub_total = obj.qty * obj.price;
				// var b = ( (obj.unit_price + obj.add_more_price) * obj.qty) - obj.discount_cash - 
				// (( (obj.unit_price + obj.add_more_price) * obj.qty) * obj.discount_percent / 100);
				
				var totalAmount = obj.qty * (obj.unit_price);
				var disAmount = totalAmount * (obj.discount_percent / 100);
				var totalDisAmount = (totalAmount - disAmount) - obj.discount_cash;
				vm.model.sub_total = vm.model.sub_total + totalDisAmount;
			}
			vm.model.sub_total.toFixed(2);
			vm.inputDiscount();
			// vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
			// vm.model.balance = vm.model.grand_total - (vm.model.deposit > 0 ? vm.model.deposit : 0);
		};

		// calculate money
		vm.inputPayment = function(){
			if(vm.model.deposit > 0){
				vm.model.balance = (vm.model.grand_total - vm.model.deposit).toFixed(4);
			}else{
				vm.model.balance = vm.model.grand_total;
			}
		};

		vm.save = function(){
			if (!$scope.saleForm.$valid) {
				$anchorScroll();
				return;
			}
			vm.today = new Date();
			if(vm.listData.length == 0){
				return vm.service.alertMessageError($translate.instant('RecordNotFound'));
			}

			if($scope.company.min_payment > vm.model.deposit){
				return vm.service.alertMessageError($translate.instant('PaymentInvalidMax', {value:$scope.company.min_payment}));
			}
			vm.model.exchange_id =  vm.exchange_id;
			vm.model.customer_id =  vm.customers.id;
			vm.model.customer_name =  vm.customers.customers_first_name + ' & ' + vm.customers.customers_last_name;;
			vm.model.customer_tel =  vm.customers.customers_telephone;
			vm.model.customer_gender =  vm.customers.customers_gender;
			vm.model.customer_code =  vm.customers.customers_code;
			vm.model.customer_type_id =  vm.customers.customers_type_id;
			vm.model.customer_type_name =  vm.customers.customers_type_detail && vm.customers.customers_type_detail.length > 0 ? vm.customers.customers_type_detail[0].name : '';
			vm.model.customer_staff_responsible = vm.customers.staff_responsible_detail && vm.customers.staff_responsible_detail.length > 0 ? vm.customers.staff_responsible_detail[0].name : '';
			vm.model.customer_staff_responsible_id = vm.customers.staff_responsible_detail && vm.customers.staff_responsible_detail.length > 0 ? vm.customers.staff_responsible_detail[0].id : '';
			vm.model.customer_event_date =  vm.customers.event_date;
			vm.model.customer_shoot_date =  vm.customers.shoot_date;
			vm.model.customer_location_shoot = vm.customers.location_shoot;
			vm.model.quotation_id = vm.quotation ? vm.quotation.id : null;
			vm.model.quotation_no = vm.quotation ? vm.quotation.invoice_no : null;
			vm.model.location_id = vm.locationList ? vm.locationList.id : '';
			vm.model.location_name = vm.locationList ? vm.locationList.name : '';
			vm.model.location_type_id = vm.locationType ? vm.locationType.id : '';
			vm.model.location_type_name = vm.locationType ? vm.locationType.name : '';

			var data = {
				invoice: vm.model,
				invoice_detail: vm.listData,
				cash_flow: {
					customer_id: vm.model.customer_id,
					customer_code: vm.model.customer_code,
					customer_name: vm.model.customer_name,
					payment_method: vm.model.payment_method,
					payment: vm.model.deposit
				}
			};
			// copy data to print out
			vm.printData = {
				stock: angular.copy(vm.model),
				stock_detail: angular.copy(vm.listData)
			};
			console.log(data);
			vm.disable = true;
			Restful.save('api/Invoice/', data).success(function (data) {
				// call from parent scope function in main.js
				// $rootScope.$emit("InitNotificationMethod", {});
				console.log(data);
				vm.printData.stock.invoice_no = data.invoice_no;
				vm.printData.stock.customer_code = data.customer_code;
				vm.service.alertMessageSuccess();
				$('#invoice-popup').modal('show');
				clearList();
				clearAll();
			});
		};

		function clearAll(){
			vm.disable = false;
			vm.listData = [];
			vm.type = '';
			$scope.saleForm.$submitted = false;
			vm.locationList = null;
			vm.locationType = null;
			vm.model.sub_total = 0;
			vm.model.grand_total = 0;
			vm.model.total_discount = 0;
			vm.model.balance = 0;
			vm.model.deposit = '';
			vm.model.discount = '';
			vm.model.description = '';
			vm.customers = null;
			vm.quotation = null;
			vm.model.discount_type = '';
		};
		
		vm.addToList = function(type){
			$timeout(function() {
				vm.type = type;
				if(vm.type === 'option'){
					vm.addOption = vm.fromOption;
				} else if (vm.type ==='package'){
					vm.addOption = vm.fromPackage;
				} else {
					vm.addOption = vm.fromAddOn;
				}

				var exist = _.find(vm.listData, function(item){ 
					var bool = false;
					if(item.type === 'package'){
						bool = true;
					}
					return bool;
				});
				//allow can add only one package
				if(exist && vm.type === 'package'){
					return;
				}else{
					vm.add();
				}
				
			}, 50);
		};

		vm.add = function(){
			vm.qty = 1;
			// check if exist in list
			for (var i = 0, l = vm.listData.length; i < l; i++) {
				var obj = vm.listData[i];
				if (obj.product_id === vm.addOption.id && obj.product_name === vm.addOption.name) {
					var old_qty = parseInt(obj.qty) + parseInt(vm.qty);
					obj.qty = old_qty;
					//obj.unit_price = vm.addOption.selected.products_price_out;
					obj.description = vm.addOption.products_description,
					obj.total = obj.qty * (obj.unit_price + obj.add_more_price);
					vm.getTotal();
					clearList();
					return;
				}
			}
			var description = [];
			if(vm.addOption.option_detail && vm.addOption.option_detail.length > 0){
				for(var i =0; i < vm.addOption.option_detail.length; i++){
					var note = vm.addOption.option_detail[i].description ? " (" + vm.addOption.option_detail[i].description + ")" : "";
					description.push(vm.addOption.option_detail[i].detail[0].name + " " + note);
				}
			}
			vm.addOption.price = vm.addOption.price?vm.addOption.price:0;
			vm.listData.push({
				product_id: vm.addOption.id,
				product_name: vm.addOption.name,
				type: vm.type,
				discount_cash: 0,
				description: description,
				discount_percent: 0,
				qty: vm.qty,
				discount_cash: 0,
				discount_percent: 0,
				unit_price: vm.addOption.price,
				total: vm.qty * parseInt(vm.addOption.price)
			});
			console.log(vm.listData);

			vm.getTotal();
			clearList();
		};

		vm.remove = function($index){
			vm.listData.splice($index, 1);
			vm.getTotal();
		};

		function clearList(){
			vm.addOption = null;
			vm.fromAddOn = null;
			vm.fromPackage = null;
		};

		// function calculate discount when change value
		vm.inputDiscount = function(){
			if(vm.model.discount_type === "percent"){
				// console.log(vm.model.discount_type, ' input discount percent');
				vm.model.total_discount = ((vm.model.discount / 100) * vm.model.sub_total).toFixed(4);
				vm.model.grand_total = vm.model.sub_total - vm.model.total_discount;
				vm.model.balance =  vm.model.grand_total - (vm.model.deposit > 0 ? vm.model.deposit : 0);
			}else{
				// console.log(vm.discount_type, ' dollar discount $');
				vm.model.total_discount = vm.model.discount ? vm.model.discount : 0;
				vm.model.grand_total = vm.model.sub_total - vm.model.total_discount;
				vm.model.balance = vm.model.grand_total - (vm.model.deposit > 0 ? vm.model.deposit : 0);
			}
		};

		// functionality calculate discount type
		vm.checkTypeDiscount = function() {
			//vm.discount_type = type;
			if(vm.model.discount_type === "percent"){
				// console.log(vm.model.discount_type, ' Percent%');
				vm.model.total_discount = ((vm.model.discount / 100) * vm.model.sub_total).toFixed(4);
				vm.model.grand_total = vm.model.sub_total - vm.model.total_discount;
				vm.model.balance =  vm.model.grand_total - (vm.model.deposit > 0 ? vm.model.deposit : 0);
				// console.log(vm.model.balance, 'Discount Percent');
			}else{
				// console.log(vm.model.discount_type, ' Dollar$');
				vm.model.total_discount = vm.model.discount ? vm.model.discount : 0;
				vm.model.grand_total = vm.model.sub_total - vm.model.total_discount;
				vm.model.balance = vm.model.grand_total - (vm.model.deposit > 0 ? vm.model.deposit : 0);
				// console.log(vm.model.balance, 'Discount Dollar');
			}
		};

		vm.getCustomer = function(){
			$timeout(function() {
				vm.locationList = {
					id: vm.customers ? vm.customers.location_id:'',
					name: vm.customers ? vm.customers.location_name:''
				};
				vm.locationType = {
					id: vm.customers?vm.customers.location_type_id:'',
					name: vm.customers?vm.customers.location_type_name:''
				};
			}, 50);
		};

		// vm.addMorePrice = function(params,$index){
		// 	$scope.formAdd.$submitted = false;
		// 	$("#addMorePrice").modal("show");
		// 	vm.copyModel = angular.copy(params);
		// 	vm.index = $index;
		// };

		// vm.saveMorePrice = function(){
		// 	if (!$scope.formAdd.$valid) {
		// 		$anchorScroll();
		// 		return;
		// 	}
		// 	vm.listData[vm.index] = vm.copyModel;
		// 	$("#addMorePrice").modal("hide");
		// 	vm.getTotal();
		// }
	}
]);
