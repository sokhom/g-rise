app.controller(
    'report_vendor_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Name', 'Type', 'Company Name', 'Telephone',
                'Email', 'Address', 'Country', 'Description'];

            vm.init = function(params){
                vm.csv = [];

                var data = {
                    name: vm.filters,
                };
                vm.loading = false;
                Restful.get('api/VendorList', data).success(function(data){
                    vm.vendors = data;
                    vm.totalItems = data.elements.length;
                    console.log(vm.totalItems);
                    vm.loading = true;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            name: value.name,
                            type: value.vendor_type ? value.vendor_type[0].name : 'N/A',
                            company_name: value.company_name,
                            tel: value.tel,
                            email: value.email,
                            address: value.address,
                            country: value.country,
                            description: value.description,
                        });
                    });
                });
            };

        }
]);