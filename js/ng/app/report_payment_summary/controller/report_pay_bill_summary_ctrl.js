app.controller(
    'report_pay_bill_summary_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            vm.headers = ['Payment No', 'Payment Date', 'Vendor', 'Discount Amount',
                'Discount Type', 'Total Discount Amount', 'Total Payment'];
            vm.loading = true;
            vm.init = function(params){
                if (!$scope.report.$valid) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    vendor_id: vm.vendor != null ? vm.vendor.id: ''
                };console.log(data);
                vm.loading = false;
                vm.payment = [];
                vm.csv = [];
                Restful.get('api/Payment', data).success(function(data){
                    vm.payment = data;
                    vm.loading = true;
                    console.log(data);
                    vm.total_discount_amount = 0;
                    vm.total_balance = 0;
                    vm.total_payment_amount = 0;
                    vm.total_last_balance = 0;
                    vm.total_bank_amount = 0;
                    vm.total_cash_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];console.log(obj);
                        vm.total_balance = vm.total_balance + obj.total_balance;
                        if( obj.payment_method == 'Cash' ){
                            vm.total_cash_amount = vm.total_cash_amount + obj.payment;
                        }else{
                            vm.total_bank_amount = vm.total_bank_amount + obj.payment;
                        }
                        vm.total_payment_amount = vm.total_payment_amount + obj.payment;
                        vm.total_discount_amount = vm.total_discount_amount + obj.discount_total_amount;
                        //vm.total_last_balance = vm.total_last_balance + obj.total_last_balance;
                    }
                    vm.grandTotal = vm.total_payment_amount - vm.total_discount_amount;
                    console.log(vm.total_bank_amount);
                    console.log(vm.total_payment_amount);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            payment_no: value.payment_no,
                            payment_date: vm.service.dateFormat( value.payment_date),
                            vendor_name: value.vendor_name,
                            discount_amount: value.discount_amount,
                            discount_type: value.discount_type,
                            total_discount_amount: value.discount_total_amount,
                            grand_total: value.grand_total,
                        });
                    });
                });
            };
        }
    ]);