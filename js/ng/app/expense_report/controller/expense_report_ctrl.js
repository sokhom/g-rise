app.controller(
    'expense_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.doctor = {};
            Restful.get('api/ExpenseType', {status: 1}).success(function (data) {
                vm.expenseList = data.elements;
                console.log(vm.expenseList);
            });
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.loading = true;
            vm.headers = ['Title','Expense Date', 'Amount', 'Description'];
            vm.init = function(){
                vm.csv = [];
                var input = {
                    name: vm.filterText,
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    expense_type_id: vm.expense_type_id
                }
                vm.loading = false;
                Restful.get('api/Expense', input).success(function(data){
                    vm.expenses = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                    vm.subTotal = 0;
                    angular.forEach(data.elements, function(value, key) {
                        vm.subTotal = vm.subTotal + value.amount;
                        vm.csv.push({
                            title: value.name,
                            expense_date: value.expense_date,
                            amount: value.amount,
                            description: value.description
                        });
                    });
                });
            };

        }
    ]);