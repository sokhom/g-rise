app.controller(
    'report_sale_form_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            
            vm.init = function(){
                vm.loading = true;
                var bool = $scope.report ? !$scope.report.$valid: false;
                if (bool) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    responsible_id: vm.staff ? vm.staff.id : '',
                    customer_id: vm.customer ? vm.customer.id : ''
                };
                Restful.get('api/RealProcess/', data).success(function(data){
                    vm.model = data;
                    console.log(data);
                    vm.loading = false;
                });
            };
            initTable();
            function initTable(){
                vm.no = 50;
                vm.clientName = 160;
                vm.contactNumber = 130;
                vm.refNo = 160;
                vm.emailFB = 200;
                vm.shooting = 560;
                vm.payment = 200;
                vm.postProduction = 2000;
                vm.totalTableWidth = vm.no + vm.clientName + vm.contactNumber + vm.refNo + vm.shooting + vm.payment + vm.postProduction;
            }
            
        }
]);