app.controller(
    'report_quotation_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            
            vm.init = function(){
                vm.loading = true;
                var bool = $scope.report ? !$scope.report.$valid: false;
                if (bool) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    customer_id: vm.customer ? vm.customer.id : ''
                };
                Restful.get('api/Invoice/', data).success(function(data){
                    vm.invoices = data;
                    console.log(data);
                    vm.loading = false;
                });
            };
            
        }
]);