app.controller(
    'doctor_expense_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.headers = ['Expense Date','Invoice No', 'Doctor Name', 
                'Customer Name', 'Note', 'Services', 'Total Amount'];
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.loading = true;
            vm.init = function(params){
                if (!vm.from_date || !vm.to_date) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    doctor_id: vm.doctor ? vm.doctor.id : '',
                    status: 1
                };
                vm.loading = false;
                vm.csv = [];
                Restful.get('api/DoctorExpense', data).success(function(data){
                    vm.expense = data;console.log(data);
                    vm.loading = true;
                    vm.count = data.count;
                    vm.total_payment_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_payment_amount = vm.total_payment_amount + obj.grand_total;
                    }
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            expense_date: vm.service.dateFormat( value.expense_date),
                            invoice_no: value.invoice_no,
                            doctor_name: value.doctor_name,
                            customer_name: value.customer_name,
                            note: value.note,
                            service: getService(value.doctor_detail),
                            payment_type: i.payment_type == 'percent' ? ( value.payment + '(%) = ' + value.grand_total  ) : value.grand_total
                        });
                    });
                });

            };

            var getService = function(params){
                var string = "";
                if(!params || params.length == 0) return string;
                angular.forEach(params, function(value, key){
                    string += value.product_name + ', ';
                    console.log(string)
                });
                console.log(string);
                return string;
            }
        }
    ]);
