app.controller(
	'invoice_term_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		vm.model = {
			type: 'invoice'
		};
		vm.params = {paginate:'yes', type: 'invoice'};
		var url = 'api/InvoiceTerm/';
		vm.init = function(){
			Restful.get(url, vm.params).success(function(data){
                vm.collections = data;
                vm.totalItems = data.count;
			});
		};
		
		vm.init(vm.params);
		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				console.log(data);
                vm.service.alertMessageSuccess();
			});
		};
		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			vm.params.start = vm.pageSize;
            vm.init(vm.params);
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#modal-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.modalForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
                    vm.init(vm.params);
					vm.service.alertMessageSuccess();
					$('#modal-popup').modal('hide');
                    vm.clear();
                    vm.disable = true;
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
                    vm.init(vm.params);
					$('#modal-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
                    vm.disable = true;
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.modalForm.$submitted = false;
            vm.model = {
				type: 'invoice'
			};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.title }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) { 
					Restful.delete(url + params.id, params ).success(function(data) {
						vm.init(vm.params);
						SweetAlert.swal({
							title: $translate.instant('Disable'), 
							text: $translate.instant('SuccessDisable'), 
							type: "success",
							timer: 1000
						});
					});
				}
			});
		};

	}
]);