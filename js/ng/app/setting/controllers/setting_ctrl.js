app.controller(
	'setting_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'Upload'
	, '$timeout'
	, '$rootScope'
	, function ($scope, Restful, Services, Upload, $timeout, $rootScope){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/Setting/';

		vm.edit = function(params){
            vm.editCompany = angular.copy(params);
			$('#setting-popup').modal('show');
		};

		vm.disable = true;
		vm.save = function(){
            if (!$scope.setting.$valid) {
                return;
            }
            vm.disable = false;
			Restful.put( url + vm.editCompany.id, vm.editCompany).success(function (data) {
                vm.service.alertMessageSuccess();
				$('#setting-popup').modal('hide');
                vm.disable = true;
                // call from parent scope function in main.js
				$rootScope.$emit("InitSettingMethod", {});
			});
		};

		//functionality upload
		vm.uploadPic = function(file) {console.log(file);
			if (file) {
				file.upload = Upload.upload({
					url: 'api/ImageUpload',
					data: {file: file, username: vm.username},
				});
				file.upload.then(function (response) {
					$timeout(function () {
						console.log(response);
						file.result = response.data;
						vm.editCompany.logo = response.data.image;
						//$scope.data.image_thumbnail = response.data.image_thumbnail;
						//file.result.substring(1, file.result.length - 1);
					});
				}, function (response) {
					if (response.status > 0)
                        vm.errorMsg = response.status + ': ' + response.data;
				}, function (evt) {
					// Math.min is to fix IE which reports 200% sometimes
					file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
				});
			}
		};
	}
]);