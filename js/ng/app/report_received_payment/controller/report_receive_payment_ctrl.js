app.controller(
    'report_receive_payment_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Payment No', 'Payment Date', 'Invoice No', 'Invoice Date', 'Customer', 'Discount Amount',
                'Grand Total', 'Deposit', 'Adds', 'Balance'];                
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.init = function(params){
                if (!$scope.report.$valid) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    customer_id: vm.customer_list ? vm.customer_list.id : '',
                };
                vm.loading = false;
                vm.receivePayment = [];
                vm.csv = [];
                Restful.get('api/ReceivePayment', data).success(function(data){
                    vm.receivePayment = data;
                    vm.loading = true;
                    console.log(data);
                    vm.total_discount_amount = 0;
                    vm.total_balance = 0;
                    vm.total_payment_amount = 0;
                    vm.total_last_balance = 0;
                    vm.total_bank_amount = 0;
                    vm.total_cash_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        // vm.total_balance = vm.total_balance + obj.total_balance;
                        // if( obj.payment_method == 'Cash' ){
                        //     vm.total_cash_amount = vm.total_cash_amount + obj.payment;
                        // }else{
                        //     vm.total_bank_amount = vm.total_bank_amount + obj.payment;
                        // }
                        vm.total_payment_amount = vm.total_payment_amount + obj.payment;
                        //vm.total_discount_amount = vm.total_discount_amount + obj.discount_total_amount;
                        //vm.total_last_balance = vm.total_last_balance + obj.total_last_balance;
                    }
                    // console.log(vm.total_bank_amount);
                    // console.log(vm.total_payment_amount);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            payment_no: value.receive_payment_no,
                            payment_date: vm.service.dateFormat(value.receive_payment_date),
                            invoice_no: value.detail[0].invoice_no,
                            invoice_date: vm.service.dateFormat(value.detail[0].invoice_date),
                            customer_name: value.customer_name,
                            discount_amount: value.discount_amount,
                            grand_total: value.grand_total,
                            deposit: value.detail[0].deposit,
                            add: value.payment,
                            balance: value.remain
                        });
                    });
                });
            };

        }
    ]);