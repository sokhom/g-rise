app.controller(
    'role_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , function ($scope, Restful, Services, SweetAlert, $translate){
            'use strict';
            var vm = this;
            vm.disable = true;
            vm.service = new Services();
            var url = 'api/Role/';
            vm.init = function(params){
                Restful.get(url, params).success(function(data){
                    vm.roles = data.elements.map(function(item){
                        item.role_text = JSON.parse(item.role_text);
                        return item;
                    });
                    console.log(vm.roles);
                    vm.totalItems = data.count;
                    // var text = JSON.stringify(data.elements[1].role_text);
                    // console.log(text);
                    // var newText = (text.substring(11)).slice(0, -4);
                    // console.warn(newText);
                    // console.log(JSON.parse("[" + newText + "]"));
                });
            };
            vm.init();            
            Restful.get('api/Feature', params).success(function(data){
                vm.permission = data;
                console.log(data);
            });
            /**
             * start functionality pagination
             */
            var params = {};
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.edit = function(params){
                Restful.get(url, {id: params.id}).success(function(data){
                    var result = data.elements.map(function(item){
                        item.role_text = JSON.parse(item.role_text);
                        return item;
                    });
                    vm.role = result[0];
                    console.log(vm.roles);
                    // vm.role = angular.copy(params);
                    vm.permissionView = vm.role.role_text;
                    $('#role-popup').modal('show');
                });

                // @todo comment this line if use role if not uncomment it for update role from new feature 
                // vm.role = params;
                // $('#role-popup').modal('show');
            };
            vm.disable = true;
            vm.save = function(){
                if (!$scope.role.$valid) {
                    return;
                }
                var updatePermission = '';
                if(vm.role.id){
                    updatePermission = vm.permissionView;
                }else{
                    updatePermission = vm.permissionView[0].children;
                }


                //@todo comment this line for update role when has change feature
                // updatePermission = vm.permissionView[0].children;
                
                vm.role.role_text = JSON.stringify(updatePermission);
                vm.disable = false;
                
                var permission = [];
                angular.forEach(updatePermission, function(value, key) {
                    var ifChildSelect = false;
                    // loop check children if have sub row
                    if(value.children.length > 0){
                        // loop check children if have sub row
                        angular.forEach(value.children, function(value, key) {
                            // loop check children if have sub row
                            if(value.children.length > 0){
                                angular.forEach(value.children, function(value, key) {
                                    permission.push({
                                        feature_name: value.feature_name,
                                        is_selected: value.selected,
                                        label: value.label,
                                    });                                        
                                    if(value.selected){
                                        ifChildSelect = true;
                                    }
                                });
                            }
                    
                            if(value.selected){
                                ifChildSelect = true;
                            }
                            permission.push({
                                feature_name: value.feature_name,
                                is_selected: ifChildSelect,//value.selected,
                                label: value.label,
                            });
                        });
                    }
                    permission.push({
                        feature_name: value.feature_name,
                        is_selected: ifChildSelect,//value.selected,
                        label: value.label,
                    });
                });
                var data = {
                    role: vm.role,
                    permission: permission,
                };
                console.log(data);
                if(vm.role.id) {
                    // data.role = {
                    //     description: vm.role.description,
                    //     role_name: vm.role.role_name
                    // };
                    console.log(vm.role.id);
                    Restful.put( url + vm.role.id, data).success(function (data) {
                        console.log(data);
                        vm.init();
                        vm.service.alertMessageSuccess();
                        $('#role-popup').modal('hide');
                        vm.close();
                        vm.disable = true;
                    });
                }else {
                    Restful.save(url , data).success(function (data) {
                        vm.init();
                        $('#role-popup').modal('hide');
                        console.log(data);
                        vm.close();
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    vm.service.alertMessageSuccess();
                });
            };

            vm.close = function(){
                vm.disable = true;
                vm.role = {};
            };

            vm.delete = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.role_name }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete(url + params.id, params ).success(function(data) {
                            if(data === 'success'){
                                vm.init();
                            }
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: data === 'success' ? $translate.instant('SuccessDelete') : $translate.instant('RecordUsing'), 
                                type: data === 'success' ? "success" : 'error',
                                timer: 1000
                            });
                            // vm.init();
                            // SweetAlert.swal({
                            //     title: $translate.instant('Delete'), 
                            //     text: $translate.instant('SuccessDelete'), 
                            //     type: "success",
                            //     timer: 1000
                            // });
                        });
                    }
                }); 
            };

        }
    ]);