app.controller(
	'branch_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, function ($scope, Restful, Services){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/Branch/';
		vm.init = function(params){
			Restful.get(url, params).success(function(data){
                vm.branch = data;
                vm.totalItems = data.count;
			});
		};
		var params = {paginate:'yes'};
		vm.init(params);
            vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				console.log(data);
                vm.service.alertMessageSuccess();
			});
		};
		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
            vm.init(params);
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#branch-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.branchForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {console.log($scope.id);
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
                    vm.init();
					vm.service.alertMessageSuccess();
					$('#branch-popup').modal('hide');
                    vm.clear();
                    vm.disable = true;
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
                    vm.init();
					$('#branch-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
                    vm.disable = true;
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.branchForm.$submitted = false;
            vm.model = {};
		};
	}
]);