app.controller(
	'member_ship_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/MemberShip/';
		vm.init = function(params){
			Restful.get(url, params).success(function(data){
                vm.collection = data;
                vm.totalItems = data.count;
			});
		};
		var params = {paginate:'yes'};
		vm.init(params);
		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				console.log(data);
                vm.service.alertMessageSuccess();
			});
		};
		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
            vm.init(params);
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#member-ship-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.memberShipForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {console.log($scope.id);
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
					if(data === 'error'){
                        vm.disable = true;
                        vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        $('#member-ship-popup').modal('hide');
                    }else {
                        vm.init();
                        vm.service.alertMessageSuccess();
                        $('#member-ship-popup').modal('hide');
                        vm.clear();
                        vm.disable = true;
                    }
                    console.log(data);
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
					if(data.error){
                        vm.disable = true;
                        vm.service.alertMessageError($translate.instant('DuplicateName'));
                        $('#member-ship-popup').modal('hide');
                    }else {
                        vm.init();
                        vm.service.alertMessageSuccess();
                        $('#member-ship-popup').modal('hide');
                        vm.clear();
                        vm.disable = true;
                    }
                    console.log(data);
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
            vm.model = {};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.name }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(url + params.id, params ).success(function(data) {
						console.log(data);
						vm.init();
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);