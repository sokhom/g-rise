app.controller(
    'service_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.doctor = {};
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Name','Type', 'Code', 'Price', 'Description'];
            vm.init = function(){
                vm.csv = [];
                vm.loading = false;
                Restful.get('api/Service', {status: 1}).success(function(data){
                    vm.products = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            Name: value.name,
                            Type: value.type_detail[0].name,
                            code: value.code,
                            price: value.price,
                            description: value.detail
                        });
                    });
                });
            };

        }
    ]);