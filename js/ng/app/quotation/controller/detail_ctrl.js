app.controller(
    'quotation_detail_ctrl', [
        '$scope'
        , 'Restful'
        , '$stateParams'
        , '$anchorScroll'
        , 'Services'
        , function ($scope, Restful, $stateParams, $anchorScroll, Services) {
            var vm = this;
            $anchorScroll();
            vm.service = new Services();
            /** get currency rate **/
            vm.service.getExchangeRate().success(function (data) {
                vm.exchange_rate_out =  data.elements[0].exchange_rate_out;
                vm.exchange_rate_in =  data.elements[0].exchange_rate_in;
                vm.exchange_id =  data.elements[0].id;
            });
            function initDetail(){
                Restful.get('api/Quotation/', {id: $stateParams.id}).success(function(data){
                    vm.model = data.elements[0];
                    vm.model.detail = vm.model.detail.map(function(value){
                        value.description = value.description_decode;
                        return value;
                    });
                    getCustomer(vm.model.customer_id);
                    var data = {
                        stock: vm.model,
                        stock_detail: vm.model.detail
                    };
                    vm.invoiceNo = vm.model.invoice_no;
                    vm.today = vm.model.invoice_date;
                    // copy data to print out
                    vm.printData = angular.copy(data);
                    console.log(vm.printData.stock.balance);
                });
            };
            initDetail();

            function getCustomer(id){
                 Restful.get('api/CustomerList/', {id: id}).success(function(data){
                    vm.copyCustomer = data.elements[0];
                });
            };
            
            vm.showMorePrice = function(params,$index){
                $scope.formAdd.$submitted = false;
                $("#addMorePrice").modal("show");
                vm.copyModel = angular.copy(params);
                vm.index = $index;
            };

        }
    ]);