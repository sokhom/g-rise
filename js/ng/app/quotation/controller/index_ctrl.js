app.controller(
	'quotation_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$anchorScroll'
	, 'SweetAlert'
	, '$translate'
	, '$rootScope'
	, function ($scope, Restful, Services, $anchorScroll, SweetAlert, $translate, $rootScope){
		$anchorScroll();
		var vm = this;
		vm.service = new Services();
		vm.params = {paginate: 'yes'};
		vm.filterText = '';
		vm.url = 'api/Quotation/';
		vm.search = function(){
			vm.params.invoice_no = vm.filterText;
			init(vm.params);
		};

		function init(params){
			vm.loading = true;
			Restful.get(vm.url, params).success(function(data){
				vm.quotationList = data;
				console.log(data);
				vm.totalItems = data.count;
			}).finally(function(){
				vm.loading = false;
			});
		};
		init(vm.params);

		vm.currentPage = 1;
		vm.pageChanged = function(){
			vm.pageSize = 10 * ( vm.currentPage - 1 );
			vm.params.start = vm.pageSize;
			init(vm.params);
		};

		vm.updateStatus = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.stock_out_no }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					params.status === 1 ? params.status = 0 : params.status = 1;
					Restful.patch(vm.url + params.id, params ).success(function(data) {
						console.log(data);
						// call from parent scope function in main.js
						$rootScope.$emit("InitNotificationMethod", {});
						SweetAlert.swal({
							title: $translate.instant('Disable'), 
							text: $translate.instant('SuccessDisable'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 

		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.invoice_no }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(vm.url + params.id).success(function(data) {
						vm.pageChanged();
						console.log(data);
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);
