app.controller(
    'report_invoice_summary_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            
            vm.init = function(){
                vm.loading = true;
                var bool = $scope.report ? !$scope.report.$valid: false;
                if (bool) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    responsible_id: vm.staff ? vm.staff.id : '',
                    customer_id: vm.customer ? vm.customer.id : ''
                };
                Restful.get('api/Invoice/', data).success(function(data){
                    vm.total_sub_total = 0;
                    vm.total_discount = 0;
                    vm.grand_total = 0;
                    vm.total_deposit = 0;
                    vm.total_balance = 0;
                    vm.invoices = data;
                    console.log(data);
                    data.elements.map(function(item){
                        vm.total_sub_total += item.sub_total;
                        vm.total_discount += item.total_discount;
                        vm.grand_total += item.grand_total;
                        vm.total_deposit += item.deposit;
                        vm.total_balance += item.balance;
                    });
                    vm.loading = false;
                });
            };

            vm.getDetail = function(params){
                var res = params.description_decode.map(function(value){
                    value.description = value.description_decode;
                    return value;
                });
                return res;
            };

            
        }
]);