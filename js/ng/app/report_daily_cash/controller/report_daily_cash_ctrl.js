app.controller(
    'report_daily_cash_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.headers = ['Invoice No', 'Invoice Date','Payment Method', 'Bank Charge', 'Payment'];
            vm.service = new Services();
            vm.loading = true;
            vm.init = function(params){
                if (!vm.from_date || !vm.to_date) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date
                };
                vm.loading = false;
                vm.csv = [];
                vm.caseFlow = [];
                Restful.get('api/CashFlow', data).success(function(data){
                    vm.caseFlow = data;console.log(data);
                    vm.loading = true;
                    vm.count = data.count;
                    vm.total_payment_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_payment_amount = vm.total_payment_amount + obj.payment;
                    }
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            invoice_no: value.invoice_no,
                            invoice_date: vm.service.dateFormat( value.invoice_date),
                            payment_method: value.payment_method,
                            bank_charge: value.bank_charge,
                            payment: value.payment
                        });
                    });
                });

            };

        }
    ]);
