app.controller(
    'feed_back_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.max = 10;
            vm.service = new Services();
            var url = 'api/FeedBack/';
            vm.init = function(params) {
                Restful.get(url + $stateParams.id).success(function (data) {
                    vm.model = data.elements[0];
                    console.log(vm.model);
                });
            };
            vm.init();
        }
    ]);