app.controller(
	'location_type_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/LocationType/';
		vm.model = {
			qty: 1
		};
		
		vm.init = function(params){
			Restful.get(url, params).success(function(data){
                vm.collections = data;
                vm.totalItems = data.count;
			});
		};
		var params = {paginate:'yes'};
		vm.init(params);
            vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
                vm.service.alertMessageSuccess();
			});
		};

		vm.search = function(id){
			params.filter = vm.search_name;
			vm.init(params);
		};

		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
            vm.init(params);
		};

		vm.edit = function(params){
            vm.modelCreate = angular.copy(params);
			$('#location-type-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.locationForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.modelCreate.id) {
				Restful.put(url + vm.modelCreate.id, vm.modelCreate).success(function (data) {
                    vm.init();
					vm.service.alertMessageSuccess();
					$('#location-type-popup').modal('hide');
                    vm.clear();
                    vm.disable = true;
				});
			} else {
				Restful.save(url, vm.modelCreate).success(function (data) {
                    vm.init();
					$('#location-type-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
                    vm.disable = true;
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.locationForm.$submitted = false;
            vm.model = {
				qty: 1
			};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.user_name }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(url + params.id, params ).success(function(data) {
						console.log(data);
						vm.init();
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);