app.controller(
    'pay_bill_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $translate){
            var vm = this;      
            vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 
            'BIDC', 'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank', 'Credit Card', 'Other'];
            $anchorScroll();
            vm.service = new Services();
            vm.initVariable = function () {
                vm.vendor = null;
                vm.listData = [];
                vm.model = {
                    payment_date: moment().format('YYYY-MM-DD'),
                    payment_method: 'Cash',
                    sub_total: 0,
                    grand_total: 0,
                    discount_total_amount: 0,
                    remain: 0,
                    bank_charge: '',
                    discount_amount: '',
                    discount_type: '',
                };
            };
            vm.initVariable();

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                vm.getTotal();
            };
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.sub_total = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    console.log(obj);
                    //var sub_total = obj.qty * obj.price;
                    vm.model.sub_total = vm.model.sub_total + obj.balance;
                }
                vm.model.sub_total.toFixed(2);
                vm.model.grand_total = vm.model.sub_total;
                vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
            };


            // function calculate discount when change value
            vm.inputDiscount = function(){
                if(vm.model.discount_type === "percent"){
                    console.log(vm.model.discount_type, ' input discount percent');
                    vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                }else{
                    console.log(vm.discount_type, ' dollar discount $');
                    vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                }
            };

            // functionality calculate discount type
            vm.checkTypeDiscount = function() {
                //vm.discount_type = type;
                if(vm.model.discount_type === "percent"){
                    console.log(vm.model.discount_type, ' Percent%');
                    vm.model.discount_total_amount = ((vm.model.discount_amount / 100) * vm.model.sub_total).toFixed(2);
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain =  vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                    console.log(vm.model.remain, 'Discount Percent');
                }else{
                    console.log(vm.model.discount_type, ' Dollar$');
                    vm.model.discount_total_amount = vm.model.discount_amount ? vm.model.discount_amount : 0;
                    vm.model.grand_total = vm.model.sub_total - vm.model.discount_total_amount;
                    vm.model.remain = vm.model.grand_total - (vm.model.payment > 0 ? vm.model.payment : 0);
                    console.log(vm.model.remain, 'Discount Dollar');
                }
            };

            // calculate money
            vm.inputPayment = function(){
                if(vm.model.payment > 0){
                    vm.model.remain = (vm.model.grand_total - vm.model.payment).toFixed(2);
                }else{
                    vm.model.remain = vm.model.grand_total;
                }
            };

            vm.save = function () {
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.listData.length < 0 || !vm.listData){
                    vm.service.alertMessageError($translate.instant('RecordPaymentFound'));
                    return;
                }
                if(vm.listData.length != 1){
                    vm.service.alertMessageError($translate.instant('NotePayment'));
                    return;
                }
                vm.loading = true;
                vm.model.vendor_id = vm.listData[0].vendor_id;
                vm.model.vendor_name = vm.listData[0].vendor_name;
                var data = {
                    payment: vm.model,                    
                    payment_detail: vm.listData
                };
                console.log(data);
                Restful.save('api/Payment', data).success(function(data){
                    console.log(data);
                    vm.initVariable();
                    $scope.form.$submitted = false;
                    vm.service.alertMessageSuccess();
                }).finally(function(){
                    vm.loading = false;
                });
            };

            vm.loadingList = false;
            vm.getVendorBalance = function(){
                //10 seconds delay
                $timeout( function(){
                    vm.loadingList = true;
                    var params = {
                        purchase_no: vm.purchaseNo,
                        vendor_id: vm.vendor != null ? vm.vendor.id : '',
                        balance: 'yes',
                        status: 1
                    };
                    console.log(params);
                    Restful.get('api/Purchase', params).success(function(data){
                        vm.listData = data.elements;
                        vm.listData = data.elements.map(function (item) {
                            console.log(item);
                            return {
                                purchase_no: item.reff_no,
                                purchase_date: item.purchase_date,
                                vendor_id: item.supplier_id,
                                vendor_name: item.supplier_name,
                                balance: item.remain
                            };
                        });
                        console.log(data);
                        vm.getTotal();
                        vm.loadingList = false;
                    });
                });
            };

        }
    ]);