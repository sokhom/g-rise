app.controller(
    'pay_bill_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            vm.service = new Services();

            vm.loading = false;
            vm.init = function(params){
                vm.loading = true;
                var input = {id: $stateParams.id};
                Restful.get('api/Payment/', input).success(function(data){
                    vm.model = data.elements[0];
                    console.log(data);
                    vm.loading = false;
                });
            };
            vm.init();

        }
    ]);