app.controller(
    'pay_bill_list_ctrl', [
        '$scope'
        , 'Restful'
        , 'SweetAlert'
        , 'Services'
        , '$translate'
        , function ($scope, Restful, SweetAlert, Services, $translate){
            var vm = this;
            vm.service = new Services();
            vm.loading = false;
            vm.params = {pagination: 'yes'};
            vm.init = function(params){
                console.log(params);
                vm.loading = true;
                Restful.get('api/Payment', params).success(function(data){
                    vm.paymentList = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = false;
                });
            };
            vm.init(vm.params);

            // functionality for filter purchase No
            // search by purchase no
            vm.search = function(){
                vm.params.payment_no = vm.payment_no;
                //vm.params.balance = 'yes';
                vm.init(vm.params);
            };

            vm.currentPage = 1;
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init(vm.params);
            };

            vm.updateStatus = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.stock_out_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        params.status === 1 ? params.status = 0 : params.status = 1;
                        var data = {
                            status: params.status,
                            payment: params.payment + params.discount_total_amount,
                            purchase_no: params.detail[0].purchase_no,
                            vendor_id: params.detail[0].vendor_id
                        };
                        console.log(data);
                        console.log(params);
                        Restful.patch('api/Payment/' + params.id, data ).success(function(data) {
                            console.log(data);
                            SweetAlert.swal({
                                title: $translate.instant('Disable'), 
                                text: $translate.instant('SuccessDisable'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };

        }
    ]);