app.controller(
    'report_staff_birthday_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services ){
            'use strict';
            var vm = this;
            vm.service = new Services;
            var url = 'api/StaffBirthday/';
            vm.init = function(params){
                vm.csv = [];
                vm.staff = [];
                Restful.get(url, params).success(function(data){
                    vm.totalItems = data.count;
                    vm.staff = data;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            id: key + 1,
                            Name: value.name,
                            CustomerType: value.doctor_type[0].name,
                            gender: value.sex,
                            dob: vm.service.dateFormat(value.dob),
                            email: value.email,
                            telephone: value.phone,
                            address: value.address,
                        });
                    });
                });

            };
            vm.init();

            vm.headers = ['#', 'Name', 'Type', 'Gender', 'DOB',
                'Email',  'Telephone', 'Address'];
        }
    ]
);