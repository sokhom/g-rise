app.controller(
    'package_type_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$stateParams'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, $anchorScroll, $state, $stateParams, $translate, $rootScope){
            'use strict';
            var vm = this;
            $anchorScroll();
            vm.model = {};
            vm.service = new Services();
            var url = 'api/PackageType/';
            vm.init = function(params){
                if($state.current.name == 'package_type.edit') {
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        console.log(data);
                    });
                }
            };
            var params = {pagination: 'yes'};
            vm.init(params);

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                console.log(vm.model);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        $state.go('package_type.list');
                    }).finally(function(){
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        console.log(data);
                        if(data.error){
                            vm.duplicate = true;
                            vm.service.alertMessageError($translate.instant('DuplicateCode'));
                        }else {
                            vm.service.alertMessageSuccess();
                            $state.go('package_type.list');
                        }
                    }).finally(function () {
                        vm.disable = true;
                    });
                }
            };

        }
    ]);