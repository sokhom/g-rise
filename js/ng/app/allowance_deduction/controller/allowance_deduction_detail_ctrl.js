app.controller(
    'allowance_deduction_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/AllowanceDeduction/';
            vm.loading = false;
            vm.init = function(params){
                vm.loading = true;
                Restful.get(url, params).success(function(data){
                    vm.model = data.elements[0];
                    vm.loading = false;
                });
            };
            vm.init();

        }
    ]);