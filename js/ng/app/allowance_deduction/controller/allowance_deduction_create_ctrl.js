app.controller(
    'allowance_deduction_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/AllowanceDeduction/';
            vm.init = function(){
                if($state.current.name == 'allowance_deduction.edit') {
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                    });
                }
            };
            vm.init();
            vm.disable = true;
            vm.save = function(){
                if (!$scope.vendorForm.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.disable = false;
                console.log(vm.model);
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.model).success(function(data) {
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        $state.go('allowance_deduction.list');
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url , vm.model).success(function (data) {
                        $state.go('allowance_deduction.list');
                        console.log(data);
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };
        }
    ]);