app.controller(
	'add_on_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, 'SweetAlert'
	, '$translate'
	, function ($scope, Restful, Services, SweetAlert, $translate){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/AddOn/';
		vm.model = {};
		
		// init add on type service
		Restful.get('api/AddOnType/').success(function(data){
			vm.addOnTypeList = data.elements;
		});

		vm.init = function(params){
			params.type = vm.type;
			vm.loading = true;
			Restful.get(url, params).success(function(data){
                vm.collections = data;
                vm.totalItems = data.count;
			}).finally(function(res){
				vm.loading = false;
			});
		};
		var params = {paginate:'yes'};
		vm.init(params);

		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
                vm.service.alertMessageSuccess();
			});
		};

		vm.search = function(id){
			params.filter = vm.search_name;
			vm.init(params);
		};

		/**
		 * start functionality pagination
		 */
        vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function() {
            vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
            vm.init(params);
		};

		vm.edit = function(params){
            vm.model = angular.copy(params);
			$('#add-on-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.addOnForm.$valid) {
                return;
            }
            vm.disable = false;
			if(vm.model.id) {
				Restful.put( url + vm.model.id, vm.model).success(function (data) {
                    vm.init(params);
					vm.service.alertMessageSuccess();
					$('#add-on-popup').modal('hide');
                    vm.clear();
                    vm.disable = true;
				});
			}else {
				Restful.save( url , vm.model).success(function (data) {
					vm.init(params);
					console.log(data);
					$('#add-on-popup').modal('hide');
                    vm.clear();
					vm.service.alertMessageSuccess();
                    vm.disable = true;
				});
			}
		};

		vm.clear = function(){
			vm.disable = true;
			$scope.addOnForm.$submitted = false;
            vm.model = {};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),
				text: $translate.instant('ConfirmDisable', { arg: params.name }),
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					Restful.delete(url + params.id, params ).success(function(data) {
						console.log(data);
						vm.init();
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);