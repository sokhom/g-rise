app.controller(
    'doctor_payslip_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorPayslip/';
            vm.init = function(params) {
                vm.loading = true;
                Restful.get(url + $stateParams.id).success(function (data) {
                    vm.model = data.elements[0];
                    console.log(vm.model);
                }).finally(function(response){
                    vm.loading = false;
                });
            };
            vm.init();

            vm.viewDetail = function(row){
                vm.detail = row;
                $("#allowance-popup").modal("show");
            };
        }
    ]);