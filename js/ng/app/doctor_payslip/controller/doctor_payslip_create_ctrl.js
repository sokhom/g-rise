app.controller(
    'doctor_payslip_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , '$translate'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll, $translate){
            'use strict';
            var vm = this;
            vm.service = new Services();
            vm.model = {
                from_date: moment().startOf('month').format("YYYY-MM-DD"),
                to_date: moment().endOf('month').format("YYYY-MM-DD")
            };
            var url = 'api/DoctorPayslip/';
            
            vm.getDoctorPayment = function(){
                vm.loadingList = true;
                var params = {
                    from_date: vm.model.from_date,
                    to_date: vm.model.to_date
                };
                Restful.get("api/GetPaymentDoctor", params).success(function (data) {
                    console.log(data);
                    vm.doctorPayments = data;                    
                    vm.loadingList = false;
                    vm.calculateTotal();
                });
            };
            
            vm.init = function(params) {
                if ($state.current.name == 'doctor_payslip.edit'){
                    vm.isEdit = true;
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.doctorPayments = vm.model.detail;  
                        console.log(vm.model);
                    });
                }else{
                    vm.getDoctorPayment();
                }  
                Restful.get("api/AllowanceDeduction", {status:1}).success(function (data) {
                    console.log(data);
                    vm.allowanceDeductions = data;
                });
            };
            vm.init();

            
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.doctorPayments.length == 0){
                    return vm.service.alertMessageError($translate.instant('RecordNotFound'));
                }
                
                vm.data = {
                    model: vm.model,
                    model_detail: vm.doctorPayments
                }
                console.log(vm.data);
                vm.loading = true;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, vm.data).success(function (data) {
                        console.log(data);
                        $state.go('doctor_payslip.list');
                        vm.service.alertMessageSuccess();
                    }).finally(function(respone){                        
                        vm.loading = false;
                    });
                }else {
                    Restful.save( url , vm.data).success(function (data) {
                        console.log(data);
                        $state.go('doctor_payslip.list');
                        vm.service.alertMessageSuccess();
                    }).finally(function(respone){                        
                        vm.loading = false;
                    });
                }
            };
            
            
            vm.calculateTotal = function(){
                vm.model.total_net_pay = 0;
                angular.forEach(vm.doctorPayments, function(value) {
                    // console.log(value);   
                    value.other = value.other?value.other:0;
                    value.tax_percentage = value.tax_percentage?value.tax_percentage:0;
                    var deductBasicSalaryAmount = (value.basic_salary * value.deduct_basic_salary / 100);
                    var deductTotalGetAmount = (value.total_payment * value.deduct_total_get / 100);
                    value.net_pay = value.basic_salary + (value.total_payment * value.tax_percentage / 100) + value.other - deductBasicSalaryAmount - deductTotalGetAmount;
                    vm.model.total_net_pay = Math.round( (vm.model.total_net_pay + (value.net_pay ? value.net_pay : 0)) * 100) / 100 ;
                });
            };

            vm.remove = function(index){
                vm.doctorPayments.splice(index, 1);
                vm.calculateTotal();
            };

            // functionality for add more allowance 
            vm.saveAllowance = function(){
                if (!$scope.formAllowance.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.doctorPayments[vm.index].detail){
                    vm.doctorPayments[vm.index].detail.push(vm.allowance);
                }else{
                    vm.doctorPayments[vm.index].detail = [];
                    vm.doctorPayments[vm.index].detail.push(vm.allowance);
                }
                console.log(vm.doctorPayments);
                $("#allowance-popup").modal("hide");
            };

            vm.addAllowance = function(index){
                vm.index = index;
                $("#allowance-popup").modal("show");
                $scope.formAllowance.$submitted = false;
            };
            vm.allowance=[];
            vm.addMoreAllowance = function(){
                vm.doctorPayments[vm.index].detail.push({
                   allowance_id: 1,
                   amount: 0,
                   description: '' 
                });
            }

            vm.removeAllowance = function($index){
                vm.doctorPayments[vm.index].detail.splice($index, 1);
                vm.getTotalOther();
            };

            // functionality for get total other
            vm.getTotalOther = function(row){
                if(!row || row.length == 0) return 0;
                var total = 0;
                angular.forEach(row, function(item, index){
                    total = total + (item.amount ? item.amount : 0);
                });
                vm.calculateTotal();
                return total;
            };

        }
    ]);