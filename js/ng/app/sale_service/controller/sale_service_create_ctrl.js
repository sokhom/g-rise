app.controller(
    'sale_service_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$rootScope'
        , '$translate'
        , '$state'
        , '$stateParams'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $rootScope, $translate, $state, $stateParams){
            var vm = this;
            vm.service = new Services();
            vm.showUploadPhoto = function(){
                $('#crop-image-popup').modal('show');
            };console.log($state.current.name);
            if($state.current.name == 'product_sale_list.edit') {
                    vm.isEdit = true;
                    Restful.get("api/SaleService",{id: $stateParams.id}).success(function (data) {
                        vm.model = data.elements[0];
                        vm.listData = data.elements[0].detail;
                    });
                }
            vm.showSnapPhoto = function(){
                vm.hide = true;
                $('#snap-photo-popup').modal('show');
            };
            vm.model = {cost:0};
            vm.getDefaultItem = function(){
                $timeout(function() {
                    console.log(vm.product_filter);
                    vm.costOfItem = angular.copy(vm.product_filter.products_price_in);
                    vm.add();
                }, 50);
            };

            vm.listData = [];
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.cost = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    obj.service_cost = obj.product_cost / (obj.unit_use?obj.unit_use:0)
                    var service_cost = obj.service_cost ? obj.service_cost : 0;
                    console.log(service_cost);
                    vm.model.cost = vm.model.cost + service_cost;
                }
                vm.model.cost = Number(vm.model.cost.toFixed(2));
            };

            // calculate money
            vm.input = function(){
                if(vm.model.total){
                    var payment = vm.model.payment ? vm.model.payment : 0;
                    vm.model.remain = (vm.model.total - payment).toFixed(2);
                }else{
                    vm.model.remain = 0;
                }
            };

            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.listData.length == 0){
                    return vm.service.alertMessageError($translate.instant('RecordNotFound'));
                }
                vm.loading = true;
                var data = {
                    model: vm.model,
                    model_detail: vm.listData
                };
                vm.loading = true;
                console.log(data);
                if(vm.model.id){
                    Restful.put('api/SaleService/'+vm.model.id, data).success(function (data) {
                        vm.service.alertMessageSuccess();
                        $state.go("product_sale_list");
                        console.log(data);
                    }).finally(function(response){
                        vm.loading = false;
                    });
                }else{
                    Restful.save('api/SaleService', data).success(function (data) {
                        vm.service.alertMessageSuccess();
                        clear();
                        clearProduct();
                        console.log(data);
                    }).finally(function(response){
                        vm.loading = false;
                    });
                }
            };

            function clearProduct(){
                vm.listData = [];
                vm.model = {
                    cost: 0
                };
                $scope.form.$submitted = false;
            };

            // Functionality for scan barcode input
            vm.filterProduct = function(){
                var params = {barcode: vm.product_barcode, status: '1'};
                Restful.get('api/Products', params).success(function(data) {
                    vm.product_barcode = '';
                    if(data.count > 0){
                        vm.product_filter = data.elements[0];
                        vm.add();
                    } else{
                        vm.service.alertMessageError($translate.instant('RecordNotFound'));
                    }
                });
            };

            vm.add = function(){
                // check if exist in list
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    if (obj.product_id === vm.product_filter.id) {
                        // obj.qty = obj.qty + 1;
                        // obj.unit_price = vm.product_filter.products_price_in;
                        // obj.qty = vm.product_filter.products_quantity;
                        // obj.um_type = vm.product_filter.um_detail[0].name ? vm.product_filter.um_detail[0].name: '';
                        
                        // obj.barcode = vm.product_filter.barcode;
                        // obj.total = obj.qty * obj.unit_price;
                        // vm.getTotal();
                        // clear();
                        return;
                    }
                }
                vm.qty = 1;
                var cost = vm.product_filter.products_price_in ? vm.product_filter.products_price_in : 0
                vm.listData.push({
                    product_id: vm.product_filter.id,
                    product_name: vm.product_filter.products_name,
                    product_code: vm.product_filter.barcode,
                    qty: vm.qty,
                    product_cost: cost,
                    um_type: vm.product_filter.um_detail[0] ? vm.product_filter.um_detail[0].name : '',
                    //um_type_id: vm.product_filter.um_type_id ? vm.product_filter.um_type_id : '',
                    //price_out: vm.product_filter.products_price_out ? vm.product_filter.products_price_out : 0,
                    // product_type: vm.product_filter.products_type_fields[0].name,
                    // product_type_id: vm.product_filter.products_type_fields[0].id,
                    // products_kind_of: vm.product_filter.products_kind_of,
                    //qty_on_hand: vm.product_filter.products_quantity,
                    unit_use: 1
                });
                console.log(vm.listData);
                console.log(vm.qty);
                console.log(vm.product_filter.products_price_in);
                vm.getTotal();
                clear();
            };

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                vm.getTotal();
            };

            function clear(){
                vm.product_filter = '';
                vm.product_barcode = '';
            };

        }
]);