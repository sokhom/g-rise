app.controller(
    'sale_service_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, SweetAlert, $translate, $rootScope){
            var vm = this;
            vm.service = new Services();
            var params = {pagination: 'yes'};
            vm.search = function(){
                params.filters = vm.filters;
                init(params);
            };

            function init(params){
                console.log(params);
                vm.loading = true;
                Restful.get('api/SaleService', params).success(function(data){
                    vm.saleServiceList = data;
                    vm.loading = false;
                    console.log(data);
                    vm.totalItems = data.count;
                });
            };
            init(params);
            vm.currentPage = 1;
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                init(params);
            };

            vm.updateStatus = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.receive_payment_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {                        
                        params.status === 1 ? params.status = 0 : params.status = 1;
                        Restful.patch('api/SaleService/' + params.id, params ).success(function(data) {
                             // call from parent scope function in main.js
				            $rootScope.$emit("InitNotificationMethod", {});
                            console.log(data);
                            SweetAlert.swal({
                                title: $translate.instant('Disable'), 
                                text: $translate.instant('SuccessDisable'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                });  
            };

        }
]);