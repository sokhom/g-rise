app.controller(
    'purchase_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , '$rootScope'
        , function ($scope, Restful, Services, SweetAlert, $translate, $rootScope){
            var vm = this;
            vm.service = new Services();
            var params = {pagination: 'yes'};
            vm.search = function(){
                params.reference_no = vm.reference_no;
                params.supplier_name = vm.vendor;
                init(params);
            };

            function init(params){console.log(params);
                vm.loading = true;
                Restful.get('api/Purchase', params).success(function(data){
                    vm.purchaseList = data;
                    vm.loading = false;
                    vm.totalItems = data.count;
                });
            };
            init(params);
            vm.currentPage = 1;
            vm.pageChanged = function(){
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                init(params);
            };

            vm.updateStatus = function(params, $index){
                // if($index != 0){
                //     return SweetAlert.swal({
                //         title: $translate.instant('Warning'), 
                //         text: $translate.instant('PurchaseVoidCannot'), 
                //         type: "warning",
                //         timer: 10000
                //     });
                // }else{
                    SweetAlert.swal({
                        title: $translate.instant('AreYouSure'),//"Are you sure?",
                        text: $translate.instant('ConfirmDisable', { arg: params.purchase_no }),//"Your will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                        cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                        closeOnConfirm: false,
                    }, 
                    function(isConfirm){ 
                        if (isConfirm) {                        
                            params.status === 1 ? params.status = 0 : params.status = 1;
                            Restful.patch('api/Purchase/' + params.id, params ).success(function(data) {
                                // call from parent scope function in main.js
                                $rootScope.$emit("InitNotificationMethod", {});
                                console.log(data);
                                SweetAlert.swal({
                                    title: $translate.instant('Disable'), 
                                    text: $translate.instant('SuccessDisable'), 
                                    type: "success",
                                    timer: 1000
                                });
                            });
                        }
                    });  
                // }
            };

        }
]);