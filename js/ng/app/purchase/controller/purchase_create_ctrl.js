app.controller(
    'purchase_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$timeout'
        , '$rootScope'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $timeout, $rootScope, $translate){
            var vm = this;
            vm.service = new Services();
            vm.banks = ['Cash', 'ACLIDA', 'CPB', 'ABA', 'ANZ Royal Bank', 'FTB', 'BIDC', 
            'CIMB', 'Sacom Bank', 'Maybank', 'Campu Bank', 'Credit Card', 'Other'];
            vm.model = {
                purchase_date: moment().format('YYYY-MM-DD'),
                payment_method: 'Cash',
                total: 0,
                payment: '',
                remain: 0
            };
            //init get um type
            Restful.get('api/UMType').success(function(data){
                vm.um_types = data.elements;
            });

            vm.getDefaultItem = function(){
                $timeout(function() {
                    console.log(vm.product_filter);
                    vm.costOfItem = angular.copy(vm.product_filter.products_price_in);
                    //vm.add();
                    Restful.get('api/Products', {id: vm.product_filter.id}).success(function(data) {
                        if(data.count > 0){
                            vm.product_filter = data.elements[0];
                            vm.add();
                        } else{
                            vm.service.alertMessageError($translate.instant('RecordNotFound'));
                        }
                    });
                }, 50);
            };

            vm.listData = [];
            // functional get total of all products
            vm.getTotal = function(){
                vm.model.total = 0;
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    var price = obj.unit_price ? obj.unit_price : 0;
                    var qty = (obj.qty * obj.um_type_collection.amount / obj.product_um_type_amount);
                    var discount_dollar = obj.discount_dollar ? obj.discount_dollar : 0;
                    var discount_percent = obj.discount_percent ? obj.discount_percent : 0;
                    vm.model.total = vm.model.total + 
                    ( 
                        (qty * price) 
                            - 
                        (( price  * qty) * discount_percent / 100) 
                            - 
                        discount_dollar 
                    );// (obj.qty * price);
                }
                vm.model.total.toFixed(2);
                vm.model.remain = vm.total;
                vm.input();
            };

            // calculate money
            vm.input = function(){
                if(vm.model.total){
                    var payment = vm.model.payment ? vm.model.payment : 0;
                    vm.model.remain = (vm.model.total - payment).toFixed(2);
                }else{
                    vm.model.remain = 0;
                }
            };

            vm.save = function(){
                if (!$scope.purchase.$valid) {
                    $anchorScroll();
                    return;
                }
                if(vm.listData.length == 0){
                    return vm.service.alertMessageError($translate.instant('RecordNotFound'));
                }
                vm.loading = true;
                vm.model.supplier_id = vm.vendor.selected.id;
                vm.model.supplier_name = vm.vendor.selected.name;
                var data = {
                    purchase: vm.model,
                    purchase_detail: vm.listData
                };
                vm.loading = true;
                console.log(data);
                Restful.save('api/Purchase', data).success(function (data) {
                    vm.service.alertMessageSuccess();
                    clear();
                    clearProduct();
                    console.log(data);
                    // call from parent scope function in main.js
				    $rootScope.$emit("InitNotificationMethod", {});
                }).finally(function(response){
                    vm.loading = false;
                });
            };

            function clearProduct(){
                vm.model = {
                    purchase_date: moment().format('YYYY-MM-DD'),
                    payment_method: 'Cash',
                    total: 0,
                    payment: '',
                    remain: 0
                };
                vm.listData = [];
                vm.vendor = {};
                $scope.purchase.$submitted = false;
            };

            // Functionality for scan barcode input
            vm.filterProduct = function(){
                var params = {barcode: vm.product_barcode, status: '1'};
                Restful.get('api/Products', params).success(function(data) {
                    vm.product_barcode = '';
                    if(data.count > 0){
                        vm.product_filter = data.elements[0];
                        vm.add();
                    } else{
                        vm.service.alertMessageError($translate.instant('RecordNotFound'));
                    }
                });
            };

            vm.add = function(){
                // check if exist in list
                for (var i = 0, l = vm.listData.length; i < l; i++) {
                    var obj = vm.listData[i];
                    if (obj.product_id === vm.product_filter.id) {
                        obj.qty = obj.qty + 1;
                        obj.unit_price = vm.product_filter.products_price_in;
                        obj.description = vm.product_filter.products_description;
                        obj.barcode = vm.product_filter.barcode;
                        obj.total = obj.qty * obj.unit_price;
                        vm.getTotal();
                        clear();
                        return;
                    }
                }
                vm.qty = 1;
                console.log(vm.product_filter);
                var price = vm.product_filter.products_price_in ? vm.product_filter.products_price_in : 0
                vm.listData.push({
                    product_id: vm.product_filter.id,
                    product_name: vm.product_filter.products_name,
                    description: vm.product_filter.products_description,
                    barcode: vm.product_filter.barcode,
                    qty: vm.qty,
                    unit_price: price,
                    free_qty: 0,
                    price_out_whole_sale: vm.product_filter.products_price_out_whole_sale ? vm.product_filter.products_price_out_whole_sale : 0,
                    price_out_retail: vm.product_filter.products_price_out_retail ? vm.product_filter.products_price_out_retail : 0,
                    product_type: vm.product_filter.products_type_fields[0].name,
                    product_type_id: vm.product_filter.products_type_fields[0].id,
                    products_kind_of: vm.product_filter.products_kind_of,
                    qty_on_hand: vm.product_filter.products_quantity,
                    cost_before: price,
                    discount_percent: 0,
                    discount_dollar: 0,
                    um_type_collection: {
                        id: vm.product_filter.um_type_id,
                        amount: vm.product_filter.um_detail[0].amount,
                    },
                    um_type_id:  vm.product_filter.um_detail[0].id,
                    um_type_name: vm.product_filter.um_detail[0].name,
                    um_type_amount: vm.product_filter.um_detail[0].amount,
                    product_um_type_id: vm.product_filter.um_detail[0].id,
                    product_um_type_name: vm.product_filter.um_detail[0].name,
                    product_um_type_amount: vm.product_filter.um_detail[0].amount,
                    //get price in from item to store in data to calculate average cost
                    price_in: vm.costOfItem,
                    total: vm.qty * parseInt(price)
                });
                console.log(vm.listData);
                console.log(vm.qty);
                console.log(vm.product_filter.products_price_in);
                vm.getTotal();
                clear();
            };

            /** Functionality for update model change when UM update **/
            vm.updateModel = function(params){
                params.um_type_id = params.um_type_collection.id;
                params.um_type_name = params.um_type_collection.name;
                params.um_type_amount = params.um_type_collection.amount;
                vm.getTotal();
            };

            vm.remove = function($index){
                vm.listData.splice($index, 1);
                vm.getTotal();
            };

            function clear(){
                vm.product_filter = '';
                vm.product_barcode = '';
            };

        }
]);