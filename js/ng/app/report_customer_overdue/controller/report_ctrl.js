app.controller(
    'report_customer_overdue_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = false;
            // vm.from_date = moment().format('YYYY-MM-DD');
            // vm.to_date = moment().format('YYYY-MM-DD');
            vm.init = function(){
                // if($scope.report.$invalid){
                //     return;
                // }
                vm.loading = true;
                var data = {
                    remain: 'yes',
                    overdue: 'yes',
                    customer_id: vm.customerObj != null  ? vm.customerObj.id : '',
                    remain: 'yes',
                    status: 1
                };
                console.log(data);
                Restful.get('api/StockOut', data).success(function(data){
                    vm.invoice = data;console.log(data);
                    vm.totalItems = data.count;
                    vm.total_balance = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_balance = vm.total_balance + obj.remain;
                    }
                }).finally(function(){
                    vm.loading = false;
                });
            };
            vm.init();
        }
    ]);