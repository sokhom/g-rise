app.controller(
    'doctor_expense_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$translate'
        , 'SweetAlert'
        , function ($scope, Restful, Services, $translate, SweetAlert){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorExpense/';
            vm.init = function(params){
                Restful.get(url, params).success(function(data){
                    vm.expenseList = data;
                    vm.totalItems = data.count;
                    console.log(data);
                });
            };
            var params = {paginate: 'yes'};
            vm.init(params);

            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.search = function(id){
                params.filter = vm.filter;
                vm.init(params);
            };

            vm.updateStatus = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),//"Are you sure?",
                    text: $translate.instant('ConfirmDisable', { arg: params.invoice_no }),//"Your will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) { 
                        params.status === 1 ? params.status = 0 : params.status = 1;
                        Restful.patch(url + params.id, params ).success(function(data) {
                            console.log(data);
                            SweetAlert.swal({
                                title: $translate.instant('Disable'), 
                                text: $translate.instant('SuccessDisable'), 
                                type: "success",
                                timer: 1000
                            });
                        });  
                    }
                }); 
            };

        }
    ]);