app.controller(
    'doctor_expense_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorExpense/';
            vm.init = function(){
                Restful.get(url,{id: $stateParams.id}).success(function(data){
                    vm.model = data.elements[0];
                    console.log(data);
                    if(vm.model.payment_type == 'percent'){
                        vm.display = '(%)';
                    }else{
                        vm.display = '($)';
                    }
                });
            };
            vm.init();

        }
    ]);