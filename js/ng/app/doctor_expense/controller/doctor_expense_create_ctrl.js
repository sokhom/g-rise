app.controller(
    'doctor_expense_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$state'
        , '$timeout'
        , '$translate'
        , function ($scope, Restful, Services, $anchorScroll, $state, $timeout, $translate){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/DoctorExpense/';            
            vm.disable = true;
            vm.invoice_type = 'IssueInvoice';
            vm.model = {
                expense_date: moment().format('YYYY-MM-DD'),
                payment_type: 'dollar',
                total: 0
            };
            vm.save = function(isSaveNew){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                // console.log( vm.doctor);
                // console.log( vm.invoice);
                vm.model.invoice_no = vm.invoice_type == 'IssueInvoice' ? vm.invoice.stock_out_no : vm.invoice.receive_payment_no;
                vm.model.invoice_date = vm.invoice_date;
                vm.model.customer_id = vm.invoice.customer_id;
                vm.model.customer_name = vm.invoice.customer_name;
                vm.model.doctor_id = vm.doctor.id;
                vm.model.doctor_name = vm.doctor.name;
                vm.model.doctor_basic_salary = vm.doctor.basic_salary;
                vm.model.customer_payment = vm.invoice.payment;
                vm.model.customer_remain = vm.invoice.remain;
                var data = {
                    doctor: vm.model,
                    doctor_detail: vm.items
                };
               // console.log(data);
                vm.disable = false;
                Restful.save( url , data).success(function (data) {
                    console.log(data);
                    if(data.id){
                        if(isSaveNew){
                            vm.clear();
                        }else{
                            $state.go('doctor_expense');
                        }
                        vm.service.alertMessageSuccess();
                    }else{
                        vm.service.alertMessageError($translate.instant("DuplicateForSaveExpense"));
                    }
                }).finally(function(response){
                    vm.disable = true;
                });
            };

            vm.clear = function(){
                vm.disable = true;
                vm.model = {
                    expense_date: moment().format('YYYY-MM-DD'),
                    payment_type: 'percent',
                    total: 0
                };
                vm.doctor = null;
                $scope.form.$submitted = false;
                vm.invoice = null;
                vm.items = null;
            };

            vm.getInvoiceDetail = function(){                
                $timeout( function(){
                    if(vm.invoice_type == 'IssueInvoice'){
                        vm.items = vm.invoice.detail;
                        //console.log(vm.items);
                        vm.subTotal = 0;
                        angular.forEach(vm.items, function(value, index){
                            console.log(value);
                            
                            var totalAmount = value.qty * (value.price + value.add_more_price);
                            var disAmount = totalAmount * (value.discount_percent / 100);
                            var totalDisAmount = (totalAmount - disAmount) - value.discount_cash;
                            vm.subTotal = vm.subTotal + totalDisAmount;//(value.qty * value.price);
                        });
                        vm.invoice_no = vm.invoice.stock_out_no;
                        vm.invoice_date = vm.invoice.stock_out_date;
                        vm.customer_payment = vm.invoice.payment;
                        vm.remain = vm.invoice.remain;
                    }else{
                        //console.log(vm.invoice);
                        Restful.get('api/StockOut', {invoice_no: vm.invoice.detail[0].invoice_no}).success(function (data) {
                            // console.log(data);
                            vm.invoice_no = data.elements[0].stock_out_no;
                            vm.invoice_date = data.elements[0].stock_out_date;
                            vm.items = data.elements[0].detail;
                            vm.customer_payment = data.elements[0].payment;
                            vm.remain = data.elements[0].remain;
                        });
                    }
                    // console.log(vm.invoice_no);
                    // console.log(vm.invoice);
                }, 50);
            };

            vm.calculateTotalPayment = function(){
                vm.model.total = 0;
                angular.forEach(vm.items, function(element) {
                    // console.log(element);
                    if(element.selected){
                        vm.model.total += element.total;
                    }
                });
                vm.model.total = vm.model.total.toFixed(2);
                vm.checkTypeDiscount();
            };

            // functionality calculate discount type
            vm.checkTypeDiscount = function() {
                var payment = vm.model.payment ? vm.model.payment: 0;
                //vm.payment_type = type;
                if(vm.model.payment_type === "percent"){
                    vm.model.grand_total = (( payment / 100) * vm.model.total).toFixed(2);
                }else{
                    vm.model.grand_total = payment;
                }
            };
        }
    ]);