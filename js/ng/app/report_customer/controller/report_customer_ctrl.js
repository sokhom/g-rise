app.controller(
    'report_customer_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.headers = ['Customer Type', 'Name',
                'DOB', 'Name', 'DOB', 'Occupation', 'Code', 'Member Card', 'Check From', 'Interest Package', 
                'Location Type', 'Location', 'Email', 'Telephone', 'Relative Telephone', 
                'Address', 'Description', 'Country'];
            vm.service = new Services();
            vm.loading = true;
            vm.params = {};
            vm.init = function(){
                vm.csv = [];
                
                vm.params.type_code = vm.typeCode;
                vm.params.search_in_report = 'yes';
                vm.params.customers_type_id = '';
                if(angular.isDefined(vm.customer_list.selected)){
                    vm.params.name = vm.customer_list.selected.customers_name;
                }
                if(angular.isDefined(vm.customer_type.selected)){
                    vm.params.customers_type_id = vm.customer_type.selected.id;
                }
                if(angular.isDefined(vm.customer_branch.selected)){
                    vm.params.branch_id = vm.customer_branch.selected.id;
                }
                vm.params.status = 1;
                
                vm.loading = false;
                vm.customers = [];
                Restful.get('api/CustomerList', vm.params).success(function(data){
                    vm.customers = data;
                    vm.totalItems = data.count;
                    vm.loading = true;
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            customer_type: value.customers_type_detail[0].name,
                            name: value.title + '. ' + value.customers_first_name,
                            dob: value.customers_dob,
                            name1: value.title1 + '. ' +value.customers_last_name,
                            dob1: value.customers_dob1,
                            occupation: value.customers_occupation,
                            code: value.customers_code,
                            member_card: value.member_card,
                            check_from: value.check_from,
                            interest_package: value.interest_package_type,
                            location_type: value.location_type_name,
                            location: value.location_name,
                            email: value.customers_email_address,
                            telephone: "'" + value.customers_telephone,
                            relativeTelephone: value.customers_relative_telephone,
                            address: value.customers_address,
                            description: value.customers_description,
                            country: value.customers_country,
                        });
                    });
                });
            };

            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 100 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init();
            };

            // customer filter
            vm.customer_list = {};
            vm.refreshCustomerList = function(customer_list) {
                var params = {name: customer_list};
                return Restful.get('api/CustomerList', params).then(function(response) {
                    vm.customer = response.data.elements;
                });
            };

            // customer type filter
            vm.customer_type = {};
            vm.refreshCustomerTypeList = function(customer_type) {
                var params = {name: customer_type};
                return Restful.get('api/CustomerType', params).then(function(response) {
                    vm.customerType = response.data.elements;
                });
            };
            // customer barnch filter
            vm.customer_branch = {};
            vm.refreshCustomerBranchList = function(branch) {
                var params = {name: branch};
                return Restful.get('api/Branch', params).then(function(response) {
                    vm.customerBranch = response.data.elements;
                });
            };
            vm.clear = function () {
                vm.customer_type = {};
                vm.customer_list = {}
                vm.customer_branch = {}
            };

        }
    ]);