app.controller(
    'exchange_rate_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/ExchangeRate/';
            vm.init = function(params){
                Restful.get(url, params).success(function(data){
                    vm.exchangeRates = data;
                    console.log(data);
                    vm.totalItems = data.count;
                });
            };
            vm.init();
            /**
             * start functionality pagination
             */
            var params = {};
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                params.start = vm.pageSize;
                vm.init(params);
            };

            vm.disable = true;
            vm.exchange_type = 'Khmer';
            vm.save = function(){
                var data = {
                    exchange_rate_in: vm.exchange_rate_in,
                    exchange_rate_out: vm.exchange_rate_out,
                    exchange_type: vm.exchange_type
                };
                vm.disable = false;

                Restful.save( url , data).success(function (data) {
                    vm.init();
                    console.log(data);
                    $('#exchange-popup').modal('hide');
                    clear();
                    vm.service.alertMessage('<strong>Success: </strong>', 'Save Success.', 'success');
                    vm.disable = true;
                });
            };

            function clear(){
                vm.disable = true;
                vm.exchange_rate_in = '';
                vm.exchange_rate_out = '';
            };
        }
    ]);