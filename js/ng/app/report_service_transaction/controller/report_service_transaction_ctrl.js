app.controller(
    'report_service_transaction_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['Item', 'Type', 'KindOf', 'Barcode',
                'Description', 'Qty On Hand', 'Stock In',
                'Stock Out', 'Balance Qty', 'Cost', 'Price', 'Amount'];
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.init = function(params){
                vm.csv = [];

                var data = {
                    status: vm.status,
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    filter: vm.filters,
                };
                vm.loading = false;
                vm.products = [];
                Restful.get('serviceTransaction.php', data).success(function(data){
                    vm.products = data;
                    vm.totalItems = data.elements.length;
                    console.log(vm.totalItems);
                    vm.loading = true;
                    vm.total_qty = 0;
                    vm.total_price_in = 0;
                    vm.total_amount = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];                        
                        vm.total_amount = vm.total_amount + ( obj.stock_out * obj.price);
                    }
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            item: value.product_name,
                            type: value.product_type,
                            barcode: value.barcode,
                            description: value.product_description,
                            stock_out: value.stock_out,
							price: value.price,
                            amount: value.cost * (value.qty_on_hand + value.stock_in - value.stock_out),
                        });
                    });
                });
            };

        }
]);