app.controller(
    'report_payslip_by_staff_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $anchorScroll){
            var vm = this;
            vm.doctor = {};
            vm.service = new Services();
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            vm.loading = true;
            vm.init = function(){
                 if(!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                var input = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1
                }
                vm.loading = false;
                Restful.get('api/PayslipByStaff', input).success(function(data){
                    vm.doctor_payslips = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    if(data.count > 0){
                        getStaff(vm.doctor_payslips.elements[0].doctor_id);
                    }
                    vm.loading = true;
                });               
            };

            function getStaff(id){
                 Restful.get('api/DoctorList/', {id: id}).success(function(data){
                    vm.model = data.elements[0];                    
                    console.log(data);
                });
            }

        }
    ]);