app.controller(
    'customer_birthday_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services ){
            'use strict';
            var vm = this;
            vm.service = new Services;
            var url = 'api/CustomerBirthday/';
            vm.init = function(params){
                vm.csv = [];
                vm.customerList = [];
                Restful.get(url, params).success(function(data){
                    vm.totalItems = data.count;
                    vm.customerList = data;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            id: key + 1,
                            Name: value.customers_name,
                            CustomerType: value.customers_type_detail[0].name,
                            gender: value.customers_gender,
                            code: value.customers_code,
                            country: value.customers_country,
                            email: value.customers_email_address,
                            telephone: value.customers_telephone,
                            relativeTelephone: value.customers_relative_telephone,
                            address: value.customers_address,
                        });
                    });
                });

            };
            vm.init();

            vm.headers = ['#', 'Name', 'Customer Type', 'Gender',
                'Code', 'Country', 'Email',
                'Telephone', 'Relative Telephone', 'Address'];
        }
    ]
);