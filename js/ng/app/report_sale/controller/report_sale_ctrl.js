app.controller(
    'report_sale_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , function ($scope, Restful, Services, $stateParams){
            var vm = this;
            console.log($stateParams);
            vm.from_date = moment().format('YYYY-MM-DD');
            vm.to_date = moment().format('YYYY-MM-DD');
            vm.service = new Services();
            vm.loading = true;
            vm.stockOut = {};
            vm.init = function(){
                var bool = $scope.report ? !$scope.report.$valid: false;
                if (bool) {
                    return;
                }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    status: 1,
                    customer_id: vm.customer ? vm.customer.id : ''
                };
                vm.loading = false;                
                Restful.get('api/StockOut', data).success(function(data){
                    vm.stockOut = data;console.log(data);
                    vm.loading = true;
                });
            };
            // check if state params has set
            if(!angular.equals({}, $stateParams.data) || ($stateParams.s_date && $stateParams.c_id && $stateParams.c_name)){
                var model = $stateParams.data;console.log($stateParams);
                if($stateParams.s_date && $stateParams.c_id && $stateParams.c_name){
                    var model = {
                        stock_out_date: $stateParams.s_date,
                        customer_id: $stateParams.c_id,
                        customer_name: $stateParams.c_name
                    };
                }                
                vm.from_date = model.stock_out_date;
                vm.to_date = model.stock_out_date;
                vm.customer = {id: model.customer_id, customers_name: model.customer_name};
                vm.init();
            }
        }
]);