app.controller(
    'report_vendor_balance_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            // vm.from_date = moment().format('YYYY-MM-DD');
            // vm.to_date = moment().format('YYYY-MM-DD');            
            vm.init = function(){
                // if (!$scope.report.$valid) {
                //     return;
                // }
                var data = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    vendor_id: vm.vendor != null ? vm.vendor.id : '',
                    balance: 'yes',
                    status: 1
                };
                vm.loading = false;
                vm.vendorBalance = [];
                Restful.get('api/Purchase', data).success(function(data){
                    vm.vendorBalance = data;console.log(data);
                    vm.loading = true;
                    vm.total_balance = 0;
                    for (var i = 0, l = data.elements.length; i < l; i++) {
                        var obj = data.elements[i];
                        vm.total_balance = vm.total_balance + obj.remain;
                    }
                });
            };

        }
    ]);