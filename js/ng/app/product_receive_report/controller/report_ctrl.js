app.controller(
    'product_receive_report_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['ReceiveNO', 'Receive From', 'Receive Date', 'Description'];
            vm.from_date = moment().startOf('month').format("YYYY-MM-DD");
            vm.to_date = moment().endOf('month').format("YYYY-MM-DD");
            
            vm.init = function(){
                vm.csv = [];
                vm.loading = false;
                vm.total_amount = 0;
                var params = {
                    from_date: vm.from_date,
                    to_date: vm.to_date,
                    reference_no: vm.filters,
                    status: 1
                };
                Restful.get('api/ReceiveProduct/', params).success(function(data){
                    vm.receive_products = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    angular.forEach(data.elements, function(value, key) {
                        vm.total_amount += value.total;
                        vm.csv.push({
                            transferNo: value.receive_product_no,
                            transferTo: value.receive_product_branch_name,
                            transferDate: value.receive_product_date,
                            description: value.description,
                        });
                    });
                }).finally(function(response){
                    vm.loading = true;
                });
            };

        }
    ]);