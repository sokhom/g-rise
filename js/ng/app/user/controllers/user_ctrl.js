app.controller(
	'user_ctrl', [
	'$scope'
	, 'Restful'
	, 'Services'
	, '$translate'
	, 'SweetAlert'
	, function ($scope, Restful, Services, $translate, SweetAlert){
		'use strict';
		var vm = this;
		vm.service = new Services();
		var url = 'api/User/';
		var params = {pagination: "yes"};

		function initSetting(){
			Restful.get('api/Role/').success(function(data){
				vm.roles = data;
			});
			Restful.get('api/DoctorList/').success(function(data){
				vm.staff = data;
			});
		};
		initSetting();

		vm.init = function(){
			params.name = vm.fitler_text;
			Restful.get(url, params).success(function(data){
				vm.users = data;
				vm.totalItems = data.count;
				console.log(data);
			});
		};
		vm.init();

		vm.edit = function(params){
			vm.user = angular.copy(params);
			$('#user-popup').modal('show');
		};
		vm.disable = true;
		vm.save = function(){
            if (!$scope.userForm.$valid) {
                return;
            }
			vm.disable = false;
			console.log(vm.user);
			if(vm.user.id) {
				if(vm.isUpdatePaas){
					Restful.put( 'api/UserUpdatePassword/' + vm.user.id, vm.user).success(function (data) {
						vm.init();console.log(data);
						vm.service.alertMessage('<strong>Success: </strong>', 'Update Success.', 'success');
						$('#user-popup').modal('hide');
						clear();
						vm.disable = true;
					});
				}else{
					Restful.put( url + vm.user.id, vm.user).success(function (data) {
						vm.init();console.log(data);
						vm.service.alertMessage('<strong>Success: </strong>', 'Update Success.', 'success');
						$('#user-popup').modal('hide');
						clear();
						vm.disable = true;
					});
				}
			}else {
				Restful.save( url , vm.user).success(function (data) {
					console.log(data);
					if(data.id){
						vm.init();
						console.log('save: ', data);
						$('#user-popup').modal('hide');
						clear();
						vm.service.alertMessage('<strong>Success: </strong>', 'Save Success.', 'success');
					}else{
						vm.disable = true;
						vm.error = true;
					}

				});
			}
		};

		 /**
		 * start functionality pagination
		 */
		vm.currentPage = 1;
		//get another portions of data on page changed
		vm.pageChanged = function(){
			vm.pageSize = 10 * ( vm.currentPage - 1 );
			params.start = vm.pageSize;
			vm.init();
		};
		
		vm.updateStatus = function(params){
			params.status === 1 ? params.status = 0 : params.status = 1;
			Restful.patch(url + params.id, params ).success(function(data) {
				vm.service.alertMessage('<strong>Success: </strong>', 'Update Success.', 'success');
			});
		};

		vm.close = function(){
			clear();
		};

		function clear(){
			vm.error = false;
			vm.disable = true;
			vm.user = {};
		};

		vm.delete = function(params){
			SweetAlert.swal({
				title: $translate.instant('AreYouSure'),//"Are you sure?",
				text: $translate.instant('ConfirmDisable', { arg: params.user_name }),//"Your will not be able to recover this imaginary file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
				cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
				closeOnConfirm: false,
			}, 
			function(isConfirm){ 
				if (isConfirm) {
					params.status === 1 ? params.status = 0 : params.status = 1;
					Restful.delete(url + params.id, params ).success(function(data) {
						vm.init();
						SweetAlert.swal({
							title: $translate.instant('Delete'), 
							text: $translate.instant('SuccessDelete'), 
							type: "success",
							timer: 1000
						});
					});
				}
			}); 
		};
	}
]);