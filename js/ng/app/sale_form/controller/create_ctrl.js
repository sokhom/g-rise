app.controller(
    'sale_form_create_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , '$timeout'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll, $timeout){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/RealProcess/';
                
            vm.ismeridian = true;
            vm.hstep = 1;
            vm.mstep = 10;
            vm.today = new Date();
            vm.optionList = [];
            vm.model = {
                confirm_date: moment().format('YYYY-MM-DD'),
                shoot_time: vm.today,
                dress_select_date: moment().format('YYYY-MM-DD'),
            };
            vm.init = function(params) {  
                if ($state.current.name === 'sale_form.edit'){
                    vm.isEdit = true;
                    if($stateParams.params){
                        vm.model = $stateParams.params;
                        vm.model.shoot_time = vm.model.shoot_time;
                        vm.optionList = $stateParams.params.detail;
                        vm.invoice = $stateParams.params.invoice_detail[0];

                        vm.invoice.customer_shoot_date = vm.model.shoot_date;
                        vm.invoice.customer_location_shoot = vm.model.location;
                        
                        vm.location = {id: vm.model.location_id, name:  vm.model.location_name };
                        vm.locationType = {id: vm.model.location_type_id, name:  vm.model.location_type_name };
                    }else{
                        Restful.get(url + $stateParams.id).success(function (data) {
                            var data = data.elements[0];
                            vm.model = data;
                            vm.model.shoot_time = data.shoot_time;
                            vm.optionList = data.detail;
                            vm.invoice = data.invoice_detail[0];

                            vm.location = {id: vm.model.location_id, name:  vm.model.location_name };
                            vm.locationType = {id: vm.model.location_type_id, name:  vm.model.location_type_name };
                            vm.invoice.customer_shoot_date = vm.model.shoot_date;
                            vm.invoice.customer_location_shoot = vm.model.location;
                        });
                    }                    
                }
            };
            vm.init();

            vm.disable = true;
            vm.save = function(){
                if (!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                vm.model.shoot_date = vm.invoice.customer_shoot_date;
                vm.model.invoice_no = vm.invoice.invoice_no;
                vm.model.customer_id = vm.invoice.customer_id;
                vm.model.location = vm.invoice.customer_location_shoot;
                vm.model.shoot_time = moment(vm.model.shoot_time).format("YYYY-MM-DD HH:mm:ss");
                vm.model.location_id = vm.location ? vm.location.id :""; 
                vm.model.location_name = vm.location ? vm.location.name :""; 
                vm.model.location_type_id = vm.locationType ? vm.locationType.id :""; 
                vm.model.location_type_name = vm.locationType ? vm.locationType.name :""; 
                
                //remove unuse property
                delete vm.model.progress;
                var data = {
                    master: vm.model,
                    detail: vm.optionList
                };
                // console.log(data);
                vm.disable = false;
                if(vm.model.id) {
                    Restful.put( url + vm.model.id, data).success(function (data) {
                        console.log(data);
                        $state.go('sale_form.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }else {
                    Restful.save( url, data).success(function (data) {
                        console.log(data);
                        $state.go('sale_form.list');
                        vm.service.alertMessageSuccess();
                        vm.disable = true;
                    });
                }
            };
            vm.selectOption = function(){
                $timeout(function() {
                    vm.optionList.push({
                        option_id: vm.optionFilter.id,
                        option_name: vm.optionFilter.name,
                        option_code: vm.optionFilter.code,
                    });
                }, 50);
            };

            vm.removeOption = function(index){
                vm.optionList.splice(index, 1);
            };

            vm.getInvoiceObj = function(){
                $timeout(function() {
                    vm.locationType = {id: vm.invoice.location_type_id, name: vm.invoice.location_type_name};
                    vm.location = {id: vm.invoice.location_id, name: vm.invoice.location_name};
                }, 50);
            };
        }
    ]);