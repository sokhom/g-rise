app.controller(
    'sale_form_detail_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(params) {
                if($stateParams.params){
                    vm.model = $stateParams.params;           
                    vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;         
                }else{
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;
                    });
                }  
                
            };
            vm.init();
        }
    ]);