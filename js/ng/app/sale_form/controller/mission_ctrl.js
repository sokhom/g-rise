app.controller(
    'sale_form_mission_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , '$document'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll, $document){
            'use strict';
            var vm = this;
            vm.today = moment().format("YYYY-MM-DD");
            vm.staffList = [];
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(params) {
                if($stateParams.params){
                    vm.model = $stateParams.params;
                    vm.model.mission.map(function(item, index){
                        vm.staffList.push({
                            staff: {
                                id: item.staff_id,
                                name: item.staff_name,
                            },
                            date: item.date,
                            role: item.role,
                            description: item.description
                        });
                        vm.description = vm.model.mission[0].note;
                    });
                } else {
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.mission.map(function(item, index){
                            vm.staffList.push({
                                staff: {
                                    id: item.staff_id,
                                    name: item.staff_name,
                                },
                                role: item.role,
                                date: item.date,
                                description: item.description
                            });
                        });
                        vm.description = vm.model.mission[0].note;
                    });
                }  
            };
            vm.init();

            vm.save = function(){
                vm.staffList.map(function(item){
                    return item.note = vm.description;
                });
                var data = {
                    master: vm.staffList,
                    id: $stateParams.id
                };
                console.log(data);
                vm.disable = true;
                Restful.save('api/RealProcessMission', data).success(function (data) {
                    // console.log(data);
                    $state.go('sale_form.list');
                    vm.service.alertMessageSuccess();
                    vm.disable = false;
                });
            };

            vm.addMoreStaff = function(){
                vm.staffList.push({
                    date: vm.today,
                    role: "",
                    description: ''
                });
            };

            vm.removeStaff = function(index){
                vm.staffList.splice(index, 1);
            };

        }

    ]);