app.controller(
    'sale_form_index_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , 'SweetAlert'
        , '$translate'
        , function ($scope, Restful, Services, SweetAlert, $translate){
            'use strict';
            var vm = this;
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(){
                vm.loading = true;
                vm.params.invoice_no = vm.search_name;
                Restful.get(url, vm.params).success(function(data){
                    vm.collection = data.elements.map(function(item){
                        var mission = item.mission.length > 0 ? 25 : 0;
                        var photo_select = item.photo_select.length > 0 ? 25 : 0;
                        var photo_process = item.photo_process.length > 0 ? 25 : 0;
                        var delevery = item.delevery.length > 0 ? 25 : 0;
                        item.progress = mission + photo_select + photo_process + delevery;
                        return item;
                    });
                    vm.loading = false;
                    vm.totalItems = data.count;
                });
            };
            vm.params = {pagination: 'yes'};
            vm.init(vm.params);
            /**
             * start functionality pagination
             */
            vm.currentPage = 1;
            //get another portions of data on page changed
            vm.pageChanged = function() {
                vm.pageSize = 10 * ( vm.currentPage - 1 );
                vm.params.start = vm.pageSize;
                vm.init(vm.params);
            };

            vm.search = function(id){
                vm.params.name = vm.search_name;
                vm.params.id = vm.search_id;
                vm.init(vm.params);
            };

            vm.updateStatus = function(params){
                params.status === 1 ? params.status = 0 : params.status = 1;
                Restful.patch(url + params.id, params ).success(function(data) {
                    // console.log(data);
                    vm.service.alertMessageSuccess();
                });
            };

            vm.delete = function(params){
                SweetAlert.swal({
                    title: $translate.instant('AreYouSure'),
                    text: $translate.instant('ConfirmDisable', { arg: params.invoice_no }),
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: $translate.instant('DeleteConfirm'),//"Yes, delete it!",
                    cancelButtonText: $translate.instant('CancelConfirm'),//"No, cancel plx!",
                    closeOnConfirm: false,
                }, 
                function(isConfirm){ 
                    if (isConfirm) {
                        Restful.delete(url + params.id, params ).success(function(data) {
                            // console.log(data);
                            vm.init(vm.params);
                            SweetAlert.swal({
                                title: $translate.instant('Delete'), 
                                text: $translate.instant('SuccessDelete'), 
                                type: "success",
                                timer: 1000
                            });
                        });
                    }
                }); 
            };

        }
    ]);