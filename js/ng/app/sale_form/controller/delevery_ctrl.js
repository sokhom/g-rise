app.controller(
    'sale_form_delevery_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.ismeridian = true;
            vm.hstep = 1;
            vm.mstep = 10;
            vm.today = moment().format("YYYY-MM-DD");
            vm.date = new Date();
            vm.entity = {
                delevery_time: vm.date,
                delevery_date: vm.today,
            };
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(params) {
                if($stateParams.params){
                    vm.model = $stateParams.params;
                    vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;
                    if(vm.model.delevery.length > 0){
                        vm.entity = {
                            delevery_time: vm.service.dateConvert( vm.model.delevery[0].delevery_time ),
                            delevery_date: vm.model.delevery[0].delevery_date,
                            description: vm.model.delevery[0].description,
                        };
                        vm.staff = {
                            id: vm.model.delevery[0].delevery_by_id, 
                            name: vm.model.delevery[0].delevery_by
                        };
                    }
                } else {
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;
                        if(vm.model.delevery.length > 0){
                            vm.entity = {
                                delevery_time: vm.service.dateConvert( vm.model.delevery[0].delevery_time ),
                                delevery_date: vm.model.delevery[0].delevery_date,
                                description: vm.model.photo_select[0].description,
                            };
                            vm.staff = {
                                id: vm.model.delevery[0].delevery_by_id, 
                                name: vm.model.delevery[0].delevery_by
                            };
                        }
                    });
                }  
            };
            vm.init();

            vm.save = function(){
                if(!vm.staff) return;
                vm.entity.delevery_time = moment(vm.entity.delevery_time).format("YYYY-MM-DD HH:mm:ss");
                vm.entity.delevery_date = moment(vm.entity.delevery_date).format("YYYY-MM-DD HH:mm:ss");
                vm.entity.delevery_by = vm.staff.name;
                vm.entity.delevery_by_id = vm.staff.id;
                var data = {
                    master: vm.entity,
                    id: $stateParams.id
                };
                // console.log(data);
                vm.disable = true;
                Restful.save('api/RealProcessDelevery', data).success(function (data) {
                    console.log(data);
                    $state.go('sale_form.list');
                    vm.service.alertMessageSuccess();
                }).finally(function(){                    
                    vm.disable = false;
                });
            };

        }

    ]);