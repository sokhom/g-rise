app.controller(
    'sale_form_photo_process_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.slide_show = true;
            vm.today = new Date();
            vm.ismeridian = true;
            vm.hstep = 1;
            vm.mstep = 10;
            vm.entity = {
                photo_complete_date: moment().format("YYYY/MM/DD"),
                photo_complete_time: vm.today,
                video_complete_date: moment().format("YYYY/MM/DD"),
                video_complete_time: vm.today,
                video_start_date: moment().format("YYYY/MM/DD"),
                photo_start_date: moment().format("YYYY/MM/DD"),
                album_date: moment().format("YYYY/MM/DD")
            };
            
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(params) {
                if($stateParams.params){
                    vm.model = $stateParams.params;console.log($stateParams.params);
                    vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;
                    if(vm.model.photo_process.length > 0){
                        setDefaultValue();
                    }
                } else {
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        vm.model.photo_select[0].publish_permission = vm.model.photo_select[0].publish_permission == 1 ? true: false;
                        if(vm.model.photo_process.length > 0){
                            setDefaultValue();
                        }
                    });
                }  
            };
            vm.init();

            function setDefaultValue(){
                vm.entity = {
                    description: vm.model.photo_process[0].description,
                    photo_start_date: vm.model.photo_process[0].photo_start_date,
                    photo_complete_date: moment(vm.model.photo_process[0].photo_complete_date).format("YYYY/MM/DD"),
                    photo_complete_time: vm.model.photo_process[0].photo_complete_time,
                    photo_retouch: vm.model.photo_process[0].photo_retouch,
                    video_complete_date: moment(vm.model.photo_process[0].video_complete_date).format("YYYY/MM/DD"),
                    video_complete_time: vm.model.photo_process[0].video_complete_time,
                    video_start_date: moment(vm.model.photo_process[0].video_start_date).format("YYYY/MM/DD"),
                    album_date: moment(vm.model.photo_process[0].album_date).format("YYYY/MM/DD"),
                    album_print_order: vm.model.photo_process[0].album_print_order
                };
                vm.staff = {
                    name: vm.model.photo_process[0].photo_editor
                };
                vm.videoStaff = {
                    name: vm.model.photo_process[0].video_editor
                };
                vm.albumStaff = {
                    name: vm.model.photo_process[0].album_editor
                };
            };
            vm.save = function(){
                vm.entity.photo_editor = vm.staff ? vm.staff.name : null;
                vm.entity.video_editor = vm.videoStaff ? vm.videoStaff.name : null;
                vm.entity.album_editor = vm.albumStaff ? vm.albumStaff.name : null;
                vm.entity.photo_complete_time = moment(vm.entity.photo_complete_time).format("YYYY-MM-DD HH:mm:ss");
                vm.entity.video_complete_time = moment(vm.entity.video_complete_time).format("YYYY-MM-DD HH:mm:ss");
                var data = {
                    master: vm.entity,
                    id: $stateParams.id
                };
                // console.log(data);
                vm.disable = true;
                Restful.save('api/RealPhotoProcess', data).success(function (data) {
                    console.log(data);
                }).finally(function(){                    
                    vm.disable = false;
                    vm.service.alertMessageSuccess();
                    $state.go('sale_form.list');
                });
            };

        }

    ]);