app.controller(
    'sale_form_photo_selection_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$timeout'
        , '$stateParams'
        , '$state'
        , '$anchorScroll'
        , function ($scope, Restful, Services, $timeout, $stateParams, $state, $anchorScroll){
            'use strict';
            var vm = this;
            vm.slide_show = true;
            vm.publish = true;
            vm.today = moment().format('YYYY/MM/DD');
            vm.entity = {
                book_cover_json: [],
                add_on_json: [],
                date: vm.today,
                deadline: vm.today
            };
            vm.service = new Services();
            var url = 'api/RealProcess/';
            vm.init = function(params) {
                if($stateParams.params){
                    vm.model = $stateParams.params;
                    if(vm.model.photo_select.length > 0){
                        vm.entity = vm.model.photo_select[0];
                        vm.entity.add_on_json = vm.model.photo_select[0].add_on;
                        vm.entity.book_cover_json = vm.model.photo_select[0].book_cover;
                        vm.slide_show = vm.entity.slide_show ? true: false;
                        vm.publish = vm.entity.publish_permission ? true: false;
                    }
                } else {
                    Restful.get(url + $stateParams.id).success(function (data) {
                        vm.model = data.elements[0];
                        if(vm.model.photo_select.length > 0){
                            vm.entity = vm.model.photo_select[0];
                            vm.entity.book_cover_json = vm.model.photo_select[0].book_cover;
                            vm.entity.add_on_json = vm.model.photo_select[0].add_on;
                            vm.publish = vm.entity.publish_permission ? true: false;
                            vm.slide_show = vm.entity.slide_show  ? true: false;
                        }
                    });
                }  
            };
            vm.init();

            vm.save = function(){
                vm.entity.slide_show = vm.slide_show ? 1 : 0;
                vm.entity.publish_permission = vm.publish ? 1 : 0;
                var data = {
                    master: vm.entity,
                    id: $stateParams.id
                };
                // console.log(data);
                vm.disable = true;
                Restful.save('api/RealProcessPhotoSelection', data).success(function (data) {
                    //console.log(data);
                }).finally(function(){                    
                    vm.disable = false;
                    $state.go('sale_form.list');
                    vm.service.alertMessageSuccess();
                });
            };

            vm.addBookCover = function(){
                vm.entity.book_cover_json.push({description: ""});
            }
            
            vm.addAddOn = function(){
                vm.entity.add_on_json.push({description: ""});
            }

            vm.deleteAddOn = function(index){
                vm.entity.add_on_json.splice(index, 1);
            }

            vm.deleteBookCover = function(index){
                vm.entity.book_cover_json.splice(index, 1);
            }

            vm.selectOption = function(){
                $timeout(function() {
                    vm.entity.add_on_json.push({
                        add_on_id: vm.addOnObj.id,
                        add_on_name: vm.addOnObj.name,
                        add_on_price: vm.addOnObj.price,
                        add_on_qty: 1
                    });
                }, 50);
            };

            vm.removeOption = function(index){
                vm.optionList.splice(index, 1);
            };

        }

    ]);