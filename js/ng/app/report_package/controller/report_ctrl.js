app.controller(
    'report_package_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , function ($scope, Restful, Services){
            var vm = this;
            vm.model = {};
            vm.service = new Services();
            vm.loading = true;
            vm.headers = ['#', 'Name', 'Qty', 'Description'];
            vm.init = function(){
                vm.csv = [];
                var data = {
                    filter: vm.search
                };
                vm.loading = false;
                Restful.get('api/Package/', data).success(function(data){
                    vm.model = data;
                    vm.totalItems = data.count;
                    console.log(data);
                    vm.loading = true;
                    angular.forEach(data.elements, function(value, key) {
                        vm.csv.push({
                            id: key + 1,
                            Name: value.name,
                            qty: value.qty,
                            description: value.description
                        });
                    });
                });
            };

        }
    ]);