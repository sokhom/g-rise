app.controller(
    'report_customer_treatment_ctrl', [
        '$scope'
        , 'Restful'
        , 'Services'
        , '$anchorScroll'
        , '$stateParams'
        , function ($scope, Restful, Services, $anchorScroll, $stateParams){
            var vm = this;
            vm.service = new Services();
            console.log($stateParams.data);
            vm.init = function(){
                if(!$scope.form.$valid) {
                    $anchorScroll();
                    return;
                }
                getTreatment();
            };
            function getTreatment(){
                vm.customer_list_copy = angular.copy(vm.customer_list);
                vm.loading = true;
                Restful.get("api/CustomerDocument",{customer_id: vm.customer_list_copy.id}).success(function (data) {
                    vm.model = data;
                    console.log(data);
                }).finally(function(response){
                    vm.loading = false;
                });
            };

            if($stateParams.data){
                vm.customer_list = $stateParams.data;
                
                getTreatment();
            }
        }
    ]);