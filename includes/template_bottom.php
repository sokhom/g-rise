    </div>
    </div>
    </div>
    <footer class="app-footer">
        <div class="wrapper">
            <span class="pull-right">
                2.1
                <a href="#">
                    <i class="fa fa-long-arrow-up"></i>
                </a>
            </span>
            © {{dateCopyRight | date: 'yyyy'}} Copyright.
        </div>
    </footer>
    <!-- Javascript Libs -->
    <script type="text/javascript" src="js/lib_template/jquery.min.js"></script>
    <script type="text/javascript" src="js/underscore/1.7.0/underscore.js"></script>
    <script type="text/javascript" src="js/lib_template/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/plugins/sweet-alert/sweetalert.min.js"></script>
     <script type="text/javascript" src="js/ng/lib/angular-chart/Chart.min.js"></script> 
    <script type="text/javascript" src="js/lib_template/moment.js"></script>
    <!-- Javascript -->
    <script type="text/javascript" src="js/lib_template/app.js"></script>
    <!--<script type="text/javascript" src="js/lib_template/index.js"></script>-->
    <!-- include angularjs script -->
    <script type="text/javascript" src="js/ng/lib/angular/1.5.6/angular.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-ui-route/angular-ui-router.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-sanitize/sanitize.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-chart/angular-chart.min.js"></script> 
    <script type="text/javascript" src="js/ng/lib/angular-ui-bootstrap/ui-bootstrap-tpls-2.4.0.js"></script>
    <!-- <script type="text/javascript" src="js/ng/lib/angular-lazy-load/ocLazyLoad.min.js"></script> -->
    <script type="text/javascript" src="js/ng/lib/bootstrap-notify/bootstrap-notify.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-date-picker/angular-datepicker.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-cookies/angular-cookies.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-translate/angular-translate.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-ui-switch/angular-ui-switch.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-trix/trix.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-trix/angular-trix.min.js"></script>
    <script type="text/javascript" src="js/ng/lib/angular-moment/angular-moment.min.js"></script>    
    <script type="text/javascript" src="js/ng/lib/angular-calendar-bootstrap/js/angular-bootstrap-calendar-tpls.min.js"></script>
    <script src="js/ng/lib/angular-upload/ng-file-upload-shim.js"></script>
    <script src="js/ng/lib/angular-webcam/webcam.js"></script>
    <script src="js/ng/lib/angular-select2/select.min.js"></script>
    <script src="js/ng/lib/angular-cache/angular-cache.min.js"></script>
    <script src="js/ng/lib/angular-upload/ng-file-upload.js"></script>
    <script src="js/ng/lib/angular-crop-image/ng-img-crop.js"></script>
    <script src="js/ng/lib/angular-csv/ng-csv.min.js"></script>
    <script src="js/ng/lib/angular-ivh-treeview/ivh-treeview.min.js"></script>
    <script src="js/ng/lib/angular-sweet-alert/SweetAlert.min.js"></script>    
    <script src="js/lib_template/jQuery.print.js"></script>

    <!-- custom file js ng -->
    <script src="js/ng/app/core/main.js"></script>
    <script src="js/ng/app/core/config/route.js"></script>
    <script src="js/ng/app/core/restful/restful.js"></script>
    <script src="js/ng/app/core/directive/main-directive.js"></script>
    <script src="js/ng/app/core/directive/cropImage/cropImageModal.js"></script>
    <script src="js/ng/app/core/directive/snapPhoto/snapPhotoModal.js"></script>
    <script src="js/ng/app/core/services/services.js"></script>
    <!-- index controllers -->
    
    <script src="js/ng/app/index/controller/index_ctrl.js"></script>
    
    <!-- start staff js plugin controllers  -->
    <script src="js/ng/app/staff_type/controller/staff_type_ctrl.js"></script>
    <script src="js/ng/app/staff_type/controller/staff_type_create_ctrl.js"></script>    
    <script src="js/ng/app/staff_list/controller/index_ctrl.js"></script>
    <script src="js/ng/app/staff_list/controller/detail_ctrl.js"></script>
    <script src="js/ng/app/staff_list/controller/create_ctrl.js"></script>
    <!-- end staff js plugin controllers  -->

    <!-- Start Item Js Plugin Controllers -->


    <!--  Item plugin controllers  -->
    <!-- <script src="js/ng/app/um_type/controller/um_type_ctrl.js"></script>
    <script src="js/ng/app/products_type/controller/products_type_ctrl.js"></script>
    <script src="js/ng/app/products_type/directive/popup.js"></script>
    <script src="js/ng/app/products_list/controller/products_list_ctrl.js"></script>
    <script src="js/ng/app/products_list/controller/products_create_ctrl.js"></script>
    <script src="js/ng/app/products_list/controller/products_detail_ctrl.js"></script> -->
    <script src="js/ng/app/options/controller/index_ctrl.js"></script>
    <script src="js/ng/app/option_type/controller/index_ctrl.js"></script>
    <script src="js/ng/app/add_on/controller/index_ctrl.js"></script>
    <script src="js/ng/app/add_on_type/controller/index_ctrl.js"></script>
    <script src="js/ng/app/packages/controllers/index_ctrl.js"></script>
    <script src="js/ng/app/packages/controllers/create_ctrl.js"></script>
    <script src="js/ng/app/packages/controllers/detail_ctrl.js"></script>
    <script src="js/ng/app/package_type/controllers/index_ctrl.js"></script>
    <script src="js/ng/app/package_type/controllers/create_ctrl.js"></script>
    <script src="js/ng/app/package_type/controllers/detail_ctrl.js"></script>
    <script src="js/ng/app/location/controller/index_ctrl.js"></script>
    <script src="js/ng/app/location_type/controller/index_ctrl.js"></script>
    <script src="js/ng/app/member_ship/controllers/index_ctrl.js"></script>
    <!-- End Item Js Plugin Contorllers -->

     <!-- Start plugin Report Controller -->
     <script src="js/ng/app/customer_birthday/controller/customer_birthday_ctrl.js"></script>
     <script src="js/ng/app/report_staff_birthday/controller/report_staff_birthday_ctrl.js"></script>
     <script src="js/ng/app/report_customer/controller/report_customer_ctrl.js"></script>
     <script src="js/ng/app/report_option/controller/report_ctrl.js"></script>
     <script src="js/ng/app/product_report/controller/product_report_ctrl.js"></script>
     <script src="js/ng/app/report_add_on/controller/report_ctrl.js"></script>
     <script src="js/ng/app/report_package/controller/report_ctrl.js"></script>
     <script src="js/ng/app/report_invoice/controller/report_ctrl.js"></script>
     <script src="js/ng/app/report_invoice_summary/controller/report_ctrl.js"></script>
    <script src="js/ng/app/report_staff/controller/report_ctrl.js"></script>
    <script src="js/ng/app/report_quotation/controller/report_ctrl.js"></script>
    <script src="js/ng/app/report_received_payment/controller/report_receive_payment_ctrl.js"></script>
    <script src="js/ng/app/report_customer_balance/controller/report_customer_balance_ctrl.js"></script>
    <script src="js/ng/app/report_sale_form/controller/report_ctrl.js"></script>
    <script src="js/ng/app/report_daily_cash/controller/report_daily_cash_ctrl.js"></script>
     <!-- End Report Plugin  -->

    <!-- Start plugin Administrator controllers  -->
    <script src="js/ng/app/user/controllers/user_ctrl.js"></script>
    <script src="js/ng/app/user/directive/popup.js"></script>
    <script src="js/ng/app/exchange_rate/controller/exchange_rate_ctrl.js"></script>
    <script src="js/ng/app/license/controller/license_ctrl.js"></script>
    <script src="js/ng/app/role/controller/role_ctrl.js"></script>
    <script src="js/ng/app/setting/controllers/setting_ctrl.js"></script>
    <script src="js/ng/app/setting/directive/popup.js"></script>
    <!-- End Administrator controllers  -->

    <!-- Start Plugin customers controllers  -->
    <script src="js/ng/app/customer_type/controllers/customer_type_ctrl.js"></script>
    <script src="js/ng/app/customer_type/directive/popup.js"></script>
    <script src="js/ng/app/customer_list/controller/customer_list_ctrl.js"></script>
    <script src="js/ng/app/customer_list/controller/customer_create_ctrl.js"></script>
    <script src="js/ng/app/customer_list/controller/customer_detail_ctrl.js"></script>
    <script src="js/ng/app/customer_list/directive/popup.js"></script>

    <script src="js/ng/app/feed_back/controller/index_ctrl.js"></script>
    <script src="js/ng/app/feed_back/controller/create_ctrl.js"></script>
    <script src="js/ng/app/feed_back/controller/detail_ctrl.js"></script>
    <script src="js/ng/app/complaint/controller/index_ctrl.js"></script>
    <script src="js/ng/app/complaint/controller/create_ctrl.js"></script>
    <script src="js/ng/app/complaint/controller/detail_ctrl.js"></script>
    <script src="js/ng/app/complaint/controller/resolve_ctrl.js"></script>
    
    <script src="js/ng/app/sale_form/controller/index_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/create_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/detail_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/mission_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/photo_selection_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/photo_process_ctrl.js"></script>
    <script src="js/ng/app/sale_form/controller/delevery_ctrl.js"></script>
    <!-- End customer controllers  -->

     <!-- Start plugin Invoice Controller -->
     <script src="js/ng/app/invoice/controller/create_ctrl.js"></script>
     <script src="js/ng/app/invoice/controller/index_ctrl.js"></script>
     <script src="js/ng/app/invoice/controller/detail_ctrl.js"></script>
     <script src="js/ng/app/invoice/directive/popUpInvoice.js"></script>
     <script src="js/ng/app/invoice_term/controller/index_ctrl.js"></script>

     <script src="js/ng/app/quotation/controller/create_ctrl.js"></script>
     <script src="js/ng/app/quotation/controller/index_ctrl.js"></script>
     <script src="js/ng/app/quotation/controller/detail_ctrl.js"></script>
     <script src="js/ng/app/quotation/directive/popUpInvoice.js"></script>
     <script src="js/ng/app/quotation_term/controller/index_ctrl.js"></script>
     
     <script src="js/ng/app/received_payment/controller/create_ctrl.js"></script>
     <script src="js/ng/app/received_payment/controller/index_ctrl.js"></script>
     <script src="js/ng/app/received_payment/controller/detail_ctrl.js"></script>
     <script src="js/ng/app/received_payment/directive/popUp.js"></script>
     <script src="js/ng/app/received_payment_term/controller/index_ctrl.js"></script>
     <!-- End Invoie Plugin  -->

     <script src="js/ng/app/calendar/controller/index_ctrl.js"></script>
     
    </body>
</html>
