<?php

use
	OSC\AccountType\Collection
		as AccountTypeCol
	, OSC\AccountType\Object
		as AccountTypeObj
;

class RestApiAccountType extends RestApi {

	public function get($params){
		$ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
        
        
        echo $ipaddress;
        exit;
		if($_SESSION["id"]) {
			$col = new AccountTypeCol();
			$col->orderBy('DESC');
			$params['GET']['status'] ? $col->filterByStatus(1) : '';
			$params['GET']['name'] ? $col->filterByName($params['GET']['name']) : '';
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			if ($params['GET']['paginate']) {
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array($start, $showDataPerPage)
					)
				);
			}
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function post($params){
		if($_SESSION["id"]) {
			$obj = new AccountTypeObj();
			$obj->setProperties($params['POST']);
			$obj->insert();
			return array(
					'data' => array(
							'id' => $obj->getId(),
							'success' => 'success'
					)
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function put($params){
		if($_SESSION["id"]) {
			$obj = new AccountTypeObj();
			$this->setId($this->getId());
			$obj->setProperties($params['PUT']);
			$obj->update($this->getId());
			return array(
					'data' => array(
							'id' => $obj->getId(),
							'success' => 'success'
					)
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new AccountTypeObj();
			$obj->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus();
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function delete(){
		if($_SESSION["id"]) {
			$obj = new AccuntTypeObj();
			$obj->delete($this->getId());
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

}
