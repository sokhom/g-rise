<?php

use
	OSC\DoctorExpense\Collection
		as DoctorExpCol
	, OSC\DoctorExpense\Object
		as DoctorExpObj
	, OSC\DoctorExpenseDetail\Object
		as DoctorExpDetailObj
;

class RestApiDoctorExpense extends RestApi {

	public function get($params){
		$col = new DoctorExpCol();
		$col->sortById('DESC');
		$params['GET']['filter'] ? $col->filterByMulti($params['GET']['filter']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['doctor_id'] ? $col->filterByDoctorId($params['GET']['doctor_id']) : '';
		$params['GET']['invoice'] ? $col->filterByInvoice($params['GET']['invoice']) : '';
		$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		// this happen should be only when user login with has link account for to see
		// doctor expense report monthly from recept create 
		$params['GET']['filter_by_owner'] ? $col->filterByDoctorId($_SESSION["link_to"]) : '';
		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$cols = new DoctorExpCol();
		$cols->filterByDoctorId($params['POST']['doctor']['doctor_id']);
		$cols->filterByStatus(1);
		$cols->filterByInvoice($params['POST']['doctor']['invoice_no']);
		// detect if record has existing
		if( $cols->getTotalCount() > 0 ){
			return array(
				'data' => array(
					'data' => false
				)
			);
		}else{
			$obj = new DoctorExpObj();
			$obj->setCreateBy($_SESSION['user_name']);
			$obj->setProperties($params['POST']['doctor']);
			$obj->insert();
			foreach( $params['POST']['doctor_detail'] as $key => $value){
				$objDetail = new DoctorExpDetailObj();
				$objDetail->setDoctorExpenseId($obj->getId());
				$objDetail->setProductKindOf($value['product_kind_of']);
				$objDetail->setPrice($value['price']);
				$objDetail->setProductName($value['product_name']);
				$objDetail->setSelected($value['selected']);
				$objDetail->setDescription($value['description']);
				$objDetail->setStockOutNo($params['POST']['doctor']['invoice_no']);
				$objDetail->setProductId($value['product_id']);
				$objDetail->setTotal($value['total']);
				$objDetail->setQty($value['qty']);
				$objDetail->setDiscountCash($value['discount_cash']);
				$objDetail->setDiscountPercent($value['discount_percent']);
				$objDetail->insert();
				unset($value);
			}
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
				)
			);
		}
	}

	public function put($params){
		$obj = new DoctorExpObj();
		$this->setId($this->getId());
		$obj->setProperties($params['PUT']);
		$obj->update($this->getId());
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new DoctorExpObj();
		$obj->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

}
