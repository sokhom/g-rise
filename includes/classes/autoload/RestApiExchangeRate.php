<?php

use
	OSC\ExchangeRate\Collection
		as ExchangeRateCol
	, OSC\ExchangeRate\Object
		as ExchangeRateObj
;

class RestApiExchangeRate extends RestApi {

	public function get($params){
		$col = new ExchangeRateCol();
		// start limit page
		$col->sortById('DESC');
		if($params['GET']['get_last_record']){
			$this->applyLimit($col,
				array(
					'limit' => array(0, 1)
				)
			);
		}else {
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array($start, $showDataPerPage)
				)
			);
		}

		$params['GET']['id'] ? $col->filterById($params['GET']['id']): '';
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new ExchangeRateObj();
		$obj->setCreateBy($_SESSION['user_name']);
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

}
