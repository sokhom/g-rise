<?php

class RestApiExpenseGrahpic extends RestApi{

    public function get(){
        $fromDate = $_GET['from_date'] . " 00:00:00";//2016-09-15 00:00:00
        $endDate = $_GET['to_date']. ' 23:59:59';//2016-09-17 23:59:59
        $array = [ data => []];
        // query other expense
        $otherExpenseQuery = tep_db_query("
            select et.name, sum(e.amount) as amount
            from expense e inner join expense_type et on e.expense_type_id = et.id
            where e.status = 1 and e.expense_date between '". $fromDate ."' AND '". $endDate ."'
            group by e.expense_type_id
        ");
        $expenseArray = [];
        while( $expense = tep_db_fetch_array($otherExpenseQuery)){
            $expenseArray[] = [
                'name' => $expense['name'],
                'amount' => doubleval($expense['amount'])
            ];
        };
        if(count($expenseArray)==0){
            $expenseArray[] = ["name" => "No Record Found.","amount" => 0];
        }
        return ['data' => ['elements' => $expenseArray]];
    }

}
