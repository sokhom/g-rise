<?php

class RestApiImportCSV extends RestApi {

	public function post($params){
		if($_SESSION["id"]) {
			//validate whether uploaded file is a csv file
			$csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv', 'text/xlsx', 'text/xls');
			if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					//open uploaded csv file with read only mode
					$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
					//skip first line
					fgetcsv($csvFile);
					$currentDate = date("Y/m/d h:i");
					//parse data from csv file line by line
					while(($line = fgetcsv($csvFile, 10000, ",")) !== FALSE){
						//check whether product already exists in database with same barcode
//						$prevQuery = tep_db_query("SELECT id FROM products WHERE barcode = '".$line[3]."'");
//						$prevResult = tep_db_num_rows($prevQuery);
//						if($prevResult > 0){
//							//update member data
//							//$db->query("UPDATE members SET name = '".$line[0]."', phone = '".$line[2]."', created = '".$line[3]."', modified = '".$line[3]."', status = '".$line[4]."' WHERE email = '".$line[1]."'");
//							$data = tep_db_fetch_array($prevQuery);
//							$sql_data_array = array(
//									'products_name' => $line[0],
//									'products_type_id' => $line[1],
//									'products_kind_of' => $line[2],
//									'barcode' => $line[3],
//									'products_price_in' => $line[4],
//									'products_price_out' => $line[5],
//									'products_quantity' => $line[6],
//									'products_description' => $line[7],
//									'status' => $line[8] == 'Active' ? 1 : 0,
//									'create_by' => $_SESSION['user_name'],
//									'create_date' => date("Y/m/d h:i"),
//							);
//							tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "id = '" . (int)$data['id'] . "'");
//						}else{
							// verify to get product type ID
							$queryProType = tep_db_query("SELECT id FROM products_type WHERE name = '".$line[1]."'");
							$numRow = tep_db_num_rows($queryProType);
							$productTypeId = 0;
							if($numRow > 0){
								$data = tep_db_fetch_array($queryProType);
								$productTypeId = $data['id'];
							}
							// verify to get um type ID
							$queryUMType = tep_db_query("SELECT id, name, amount FROM um_type WHERE name = '".$line[3]."'");
							$numUMRow = tep_db_num_rows($queryUMType);
							$umTypeId = 1;
							$umTypeAmount = 1;
							if($numUMRow > 0){
								$dataUoM = tep_db_fetch_array($queryUMType);
								$umTypeId = $dataUoM['id'];
								$umTypeName = $dataUoM['name'];
								$umTypeAmount = $dataUoM['amount'];
							}
							//insert product
							$sql_data_array = array(
								'products_name' => $line[0],
								'products_type_id' => $productTypeId,
								'products_kind_of' => 'item',
								'barcode' => str_replace("'", '', $line[2]),
								// 'barcode' => $line[0],
								'products_price_in' => $line[6],
								'products_price_out_whole_sale' => $line[6],
								'products_price_out_retail' => $line[6],
								'products_quantity' => $line[4],
								'order_alert' => $line[5],
								'products_description' => $line[7] . '. company: ' .  $line[8],
								'status' => 1,
								'is_sale' => 1,
								'um_type_id' => $umTypeId,
								'um_type_sale_id' => $umTypeId,
								'create_by' => $_SESSION['user_name'],
								'create_date' => $currentDate,
							);
							tep_db_perform(TABLE_PRODUCTS, $sql_data_array);
							$productId = tep_db_insert_id();

							//insert into stock transaction
							// $sql_data_stock_transaction_array = array(
							// 	'product_id' => $productId,
							// 	'product_name' => $line[0],
							// 	'product_type_id' => $productTypeId,
							// 	'product_kind_of' => 'item',
							// 	'barcode' => str_replace("'", '', $line[5]),
							// 	'cost' => $line[6],
							// 	'whole_sale_price' => $line[6],
							// 	'retail_price' => $line[6],
							// 	'qty_on_hand' => $line[8],
							// 	'product_description' => $line[10],
							// 	'status' => 1,
							// 	'um_type' => $umTypeName,
							// 	'um_type_amount' => $umTypeAmount,
							// 	'um_type_retail' => $umTypeName,
							// 	'um_type_retail_amount' => $umTypeAmount,
							// 	'create_by' => $_SESSION['user_name'],
							// 	'create_date' => $currentDate,
							// );
							// tep_db_perform('stock_transaction', $sql_data_stock_transaction_array);
//						}
					}

					//close opened csv file
					fclose($csvFile);

					$status = 'success';
				}else{
					$status = 'error';
				}
			}else{
				$status = 'invalid_file';
			}

			return array(
				'data' => array(
					'status' => $status
				)
			);
		}
	}

}
