<?php

use
	OSC\LocationType\Collection
		as LocationCol
	, OSC\LocationType\Object
		as LocationObj
;

class RestApiLocationType extends RestApi {

	public function get($params){
		$col = new LocationCol();
		$col->sortByName("ASC");
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['filter'] ? $col->filterByName($params['GET']['filter']) : '';
		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new LocationObj();
		$obj->setCreateBy($_SESSION['user_name']);
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new LocationObj();
		$this->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setProperties($params['PUT']);
		$obj->update($this->getId());
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new LocationObj();
		$obj->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

	public function delete(){
		$obj = new LocationObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
