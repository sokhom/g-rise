<?php

use
	OSC\RealProcessPhotoSelection\Collection
		as RealProcessPhotoSelectionCol
	, OSC\RealProcessPhotoSelection\Object
		as RealProcessPhotoSelectionObj
;

class RestApiRealProcessPhotoSelection extends RestApi {

	public function get($params){
		$col = new RealProcessPhotoSelectionCol();
		
		// $col->filterById($this->getId());
		// $params['GET']['invoice_no'] ? $col->filterByInvoice($params['GET']['invoice_no']) : '';
		// $params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new RealProcessPhotoSelectionObj();
		$obj->setRealProcessId($params['POST']['id']);
		$obj->delete();
		$obj->setProperties($params['POST']['master']);
		//var_dump($params['POST']['master']);exit;
		$obj->insert();

		return array(
			'data' => array(
				//'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	// public function put($params){
	// 	$obj = new RealProcessPhotoSelectionObj();
	// 	$obj->setRealProcessId($params['PUT']['real_process_id']);
	// 	$obj->delete();
	// 	// start insert data into detail
	// 	foreach( $params['PUT']['master'] as $key => $value){
	// 		$obj->setProperties($value);
	// 		$obj->insert();
	// 		unset($value);
	// 	}
	// 	return array(
	// 		'data' => array(
	// 			'id' => $obj->getId(),
	// 			'success' => 'success'
	// 		)
	// 	);
	// }

	public function delete(){
		$obj = new RealProcessPhotoSelectionObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
