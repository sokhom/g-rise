<?php
class RestApiFeature  extends RestApi {

	public function get($params){
		$array = array(
		    data => array([
                label => 'Pages',
                selected => true,
                children => array(
                    /**
                     * Dashboard Page Role
                     */
                    [
                        label => 'Dashboard Pages',
                        feature_name => 'Dashboard_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Dashboard Group',
                                feature_name => 'Dashboard_Group',
                                selected => true,
                                children => [
                                    [
                                        label => 'View Customer Total',
                                        selected => true,
                                        feature_name => 'Customer_Total_View',
                                    ],
                                    [
                                        label => 'View Customer Balance',
                                        selected => true,
                                        feature_name => 'Customer_Balance_View',
                                    ],
                                    [
                                        label => 'View Product',
                                        selected => true,
                                        feature_name => 'Product_Total_View',
                                    ],
                                    [
                                        label => 'View Service',
                                        selected => true,
                                        feature_name => 'Service_Total_View',
                                    ],
                                    [
                                        label => 'View Vendor Balance',
                                        selected => true,
                                        feature_name => 'Vendor_Balance_View',
                                    ],
                                    [
                                        label => 'View Total Supplier',
                                        selected => true,
                                        feature_name => 'Total_Supplier_View',
                                    ],
                                    [
                                        label => 'View Total Staff',
                                        selected => true,
                                        feature_name => 'Total_Staff_View',
                                    ],
                                    [
                                        label => 'View Top 10 Sale Best By Service',
                                        selected => true,
                                        feature_name => 'Sale_Best_By_Service_Graph',
                                    ],
                                    [
                                        label => 'View Top 10 Sale Best By Item',
                                        selected => true,
                                        feature_name => 'Sale_Best_By_Item_Graph',
                                    ],
                                    [
                                        label => 'View Top 10 Expense',
                                        selected => true,
                                        feature_name => 'Expense_Graph',
                                    ],
                                    [
                                        label => 'View Income Statement Graphic',
                                        selected => true,
                                        feature_name => 'Income_Statement_Graph'
                                    ]
                                ]
                            ],
                        ]

                    ],
                    /**
                     * Notification Page Role
                     */
                    [
                        label => 'Notification Pages',
                        feature_name => 'Notification_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Customer Birth Day',
                                feature_name => 'Customer_Birth_Day_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Birth_Day_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Birth_Day_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Staff Birthday Report',
                                feature_name => 'Staff_Birthday_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Birthday_Report_View',
                                    ],
                                        [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Staff_Birthday_Report_Export',
                                    ]
                                ]
                            ],
                            [
                                label => 'Customer Appointment Report',
                                feature_name => 'Customer_Appointment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Appointment_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Appointment_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'My Appointment Report',
                                feature_name => 'Appointment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Appointment_Report_View',
                                    ],
                                ]
                            ],
                            [
                                label => 'Stock Alert Report',
                                feature_name => 'Stock_Alert_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Stock_Alert_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Stock_Alert_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Customer Payment Overdue Report',
                                feature_name => 'Customer_Overdue_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Overdue_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Overdue_Report_Export',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Customer Page Role
                     */
                    [
                        label => 'Customer Pages',
                        feature_name => 'Customer_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Customer Type',
                                feature_name => 'Customer_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Customer_Type_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Customer_Type_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Customer_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Customer_Type_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Type_View',
                                    ]
                                ]
                            ],
                             [
                                label => 'Branch',
                                feature_name => 'Branch',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Branch_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Branch_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Branch_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Branch_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Branch_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Customer List',
                                feature_name => 'Customer_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Customer_List_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Customer_List_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Customer_List_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Customer_List_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_List_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Customer Report',
                                feature_name => 'Customer_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Report_Export',
                                    ],
                                ]
                            ],
                             [
                                label => 'Customer Treatment Report',
                                feature_name => 'Customer_Treatment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Treatment_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Treatment_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Customer Balance Report',
                                feature_name => 'Customer_Balance_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Balance_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Balance_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Customer Payment',
                                feature_name => 'Customer_Payment',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Customer_Payment_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Customer_Payment_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Customer_Payment_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Customer_Payment_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Payment_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Customer Payment Report',
                                feature_name => 'Customer_Payment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Payment_Report_View',
                                    ],
                                     [
                                        label => 'Export to Excel',
                                        selected => true,
                                        feature_name => 'Customer_Payment_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Appointment',
                                feature_name => 'Appointment',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Appointment_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Appointment_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Appointment_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Appointment_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Appointment_View',
                                    ]
                                ]
                            ],
                            
                            [
                                label => 'Service Type',
                                feature_name => 'Service_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Service_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Service_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Service_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Service_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Service_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Service List',
                                feature_name => 'Service_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Service_List_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Service_List_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Service_List_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Service_List_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Service_List_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Service Report',
                                feature_name => 'Service_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Service_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Service_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Appointment Report',
                                feature_name => 'Appointment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Appointment_Report',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Appointment_Report_Export',
                                    ],
                                ]
                            ],
                            // [
                            //     label => 'Account Payable Report',
                            //     feature_name => 'Account_Payable_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Account_Payable_Report',
                            //         ],
                            //     ]
                            // ],
                            [
                                label => 'Service Transaction Report',
                                feature_name => 'Service_Transaction_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Service_Transaction_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Service_Transaction_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Sell',
                                feature_name => 'Sell',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Sell_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Sell_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Sell_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Sell_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Sell_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Sell Summary Report',
                                feature_name => 'Sell_Summary_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Sell_Summary_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Sell_Summary_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Sell Detail Report',
                                feature_name => 'Sell_Detail_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Sell_Detail_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Sell_Detail_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Product Receive',
                                feature_name => 'Product_Receive_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Receive_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Product_Receive_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Product_Receive_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Product_Receive_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Product_Receive_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Product Receive Report',
                                feature_name => 'Product_Receive_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Receive_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Product_Receive_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Return Invoice',
                                feature_name => 'Return_Invoice_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Return_Invoice_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Return Invoice Report',
                                feature_name => 'Return_Invoice_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Return_Invoice_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Staff Expense',
                                feature_name => 'Doctor_Expense',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Doctor_Expense_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Staff Expense Report',
                                feature_name => 'Doctor_Expense_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Doctor_Expense_Report_Export',
                                    ]
                                ]
                            ],
                            // expense feature
                            [
                                label => 'Expense',
                                feature_name => 'Expense_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Expense_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Expense_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Expense_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Expense_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Expense_Disable',
                                    ],
                                ]
                            ],
                            // expense type feature
                            [
                                label => 'Expense Type',
                                feature_name => 'Expense_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Expense_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Expense_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Expense_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Expense_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Expense_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Expense Report',
                                feature_name => 'Expense_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Expense_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Expense_Report_Export',
                                    ],
                                ]
                            ],
                        ]

                    ],
                    /**
                     * Doctor Page Role
                     */
                    [
                        label => 'Staff Pages',
                        feature_name => 'Staff_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Staff Type',
                                feature_name => 'Staff_Type',
                                label => 'Staff Type',
                                feature_name => 'Staff_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Staff_Type_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Staff_Type_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Staff_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Staff_Type_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Type_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Staff List',
                                feature_name => 'Staff_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Staff_List_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Staff_List_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Staff_List_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Staff_List_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_List_View',
                                    ]
                                ]
                            ],
                             [
                                label => 'Staff Report',
                                feature_name => 'Staff_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Staff_Report_Export',
                                    ]
                                ]
                            ],
                            [
                                label => 'Allowance And Deduction',
                                feature_name => 'Allowance_Deduction',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Allowance_Deduction_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Allowance_Deduction_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Allowance_Deduction_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Allowance_Deduction_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Allowance_Deduction_View',
                                    ]
                                ]
                            ],
                            // Doctor Payslip Feature
                            [
                                label => 'Staff Payroll',
                                feature_name => 'Doctor_Payslip',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Doctor_Payslip_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Staff Payroll Report',
                                feature_name => 'Doctor_Payslip_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Doctor_Payslip_Report_Export',
                                    ]
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Vendor Page
                     */
                    [
                        label => 'Vendor Pages',
                        feature_name => 'Vendor_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Vendor Type',
                                feature_name => 'Vendor_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Vendor_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Vendor_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Vendor_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Vendor_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Vendor List',
                                feature_name => 'Vendor_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_List_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Vendor_List_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Vendor_List_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Vendor_List_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Vendor_List_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Vendor Report',
                                feature_name => 'Vendor_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Vendor_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Vendor Payment',
                                feature_name => 'Vendor_Payment',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Vendor Payment Report',
                                feature_name => 'Vendor_Payment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Vendor_Payment_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Vendor Balance Report',
                                feature_name => 'Vendor_Balance_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Vendor_Balance_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Vendor_Balance_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Purchase',
                                feature_name => 'Purchase',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Purchase_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Purchase_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Purchase_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Purchase_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Purchase_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Purchase Summary Report',
                                feature_name => 'Purchase_Summary_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Purchase_Summary_Report_View',
                                    ],
                                ]
                            ],
                            [
                                label => 'Purchase Detail Report',
                                feature_name => 'Purchase_Detail_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Purchase_Detail_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Purchase_Detail_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Account Payable Report',
                                feature_name => 'Account_Payable_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Account_Payable_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Account_Payable_Report_Export',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Item Page
                     */
                    [
                        label => 'Item Pages',
                        feature_name => 'Item_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'UM Type',
                                feature_name => 'UM_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'UM_Type_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'UM_Type_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'UM_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'UM_Type_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'UM_Type_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Product Type',
                                feature_name => 'Product_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Product_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Product_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Product_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Product_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Product List',
                                feature_name => 'Product_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_List_View',
                                    ],
                                    // [
                                    //     label => 'View Product',
                                    //     selected => true,
                                    //     feature_name => 'Product_List_View_Product',
                                    // ],
                                    // [
                                    //     label => 'View Service',
                                    //     selected => true,
                                    //     feature_name => 'Product_List_View_Service',
                                    // ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Product_List_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Product_List_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Product_List_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Product_List_Disable',
                                    ],
                                ]
                            ],
                            /* open it when u need coz it use i think only for restaurants*/
                            // [
                            //     label => 'Product Sale List',
                            //     feature_name => 'Product_Sale',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Product_Sale_View',
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'Product_Sale_Edit',
                            //         ],
                            //         [
                            //             label => 'Create',
                            //             selected => true,
                            //             feature_name => 'Product_Sale_Create',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'Product_Sale_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'Product_Sale_Disable',
                            //         ],
                            //     ]
                            // ],
                            [
                                label => 'Product Report',
                                feature_name => 'Product_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Product_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Transfer Type',
                                feature_name => 'Transfer_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Transfer_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Transfer_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Transfer_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Tramsfer_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Transfer_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Product Transfer',
                                feature_name => 'Product_Transfer_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Transfer_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Product Transfer Report',
                                feature_name => 'Product_Transfer_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Product_Transfer_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Stock Transaction Report',
                                feature_name => 'Stock_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Stock_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Stock_Report_Export',
                                    ],
                                ]
                            ],
                            // stock adjustment feature
                            [
                                label => 'Stock Adjustment',
                                feature_name => 'Stock_Adjustment',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Sell_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Stock Adjustment Report',
                                feature_name => 'Stock_Adjustment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Stock_Adjustment_Report_Export',
                                    ],
                                ]
                            ],
                            
                        ]
                    ],
                    /**
                     * Accounting Report Page
                     */
                    // [
                    //     label => 'Accounting Report Pages',
                    //     feature_name => 'Accounting_Report_Pages',
                    //     selected => true,
                    //     children => [
                    //         [
                    //             label => 'Balance Sheet',
                    //             feature_name => 'Balance_Sheet',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Balance_Sheet_View',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'Case Flow',
                    //             feature_name => 'Case_Flow',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Case_Flow_View',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'Journal',
                    //             feature_name => 'Journal',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Journal_View',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'General Ledger',
                    //             feature_name => 'General_Ledger',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'General_Ledger_View',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'Income Statement',
                    //             feature_name => 'Income_Statement',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Income_Statement_View',
                    //                 ],
                    //             ]
                    //         ],
                    //     ]
                    // ],
                    /**
                     * Accounting Page
                     */
                    // [
                    //     label => 'Accounting Pages',
                    //     feature_name => 'Accounting_Pages',
                    //     selected => true,
                    //     children => [
                    //         [
                    //             label => 'Account Type',
                    //             feature_name => 'Account_Type',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Account_Type_View',
                    //                 ],
                    //                 [
                    //                     label => 'Edit',
                    //                     selected => true,
                    //                     feature_name => 'Account_Type_Edit',
                    //                 ],
                    //                 [
                    //                     label => 'Create',
                    //                     selected => true,
                    //                     feature_name => 'Account_Type_Create',
                    //                 ],
                    //                 [
                    //                     label => 'Delete',
                    //                     selected => true,
                    //                     feature_name => 'Account_Type_Delete',
                    //                 ],
                    //                 [
                    //                     label => 'Disable',
                    //                     selected => true,
                    //                     feature_name => 'Account_Type_Disable',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'Account Chart',
                    //             feature_name => 'Account_Chart',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Account_Chart_View',
                    //                 ],
                    //                 [
                    //                     label => 'Edit',
                    //                     selected => true,
                    //                     feature_name => 'Account_Chart_Edit',
                    //                 ],
                    //                 [
                    //                     label => 'Create',
                    //                     selected => true,
                    //                     feature_name => 'Account_Chart_Create',
                    //                 ],
                    //                 [
                    //                     label => 'Delete',
                    //                     selected => true,
                    //                     feature_name => 'Account_Chart_Delete',
                    //                 ],
                    //                 [
                    //                     label => 'Disable',
                    //                     selected => true,
                    //                     feature_name => 'Account_Chart_Disable',
                    //                 ],
                    //             ]
                    //         ],
                    //         [
                    //             label => 'Journal Entry',
                    //             feature_name => 'Journal_Entry',
                    //             selected => true,
                    //             children => [
                    //                 [
                    //                     label => 'View',
                    //                     selected => true,
                    //                     feature_name => 'Journal_Entry_View',
                    //                 ],
                    //                 [
                    //                     label => 'Edit',
                    //                     selected => true,
                    //                     feature_name => 'Journal_Entry_Edit',
                    //                 ],
                    //                 [
                    //                     label => 'Create',
                    //                     selected => true,
                    //                     feature_name => 'Journal_Entry_Create',
                    //                 ],
                    //                 [
                    //                     label => 'Delete',
                    //                     selected => true,
                    //                     feature_name => 'Journal_Entry_Delete',
                    //                 ],
                    //                 [
                    //                     label => 'Disable',
                    //                     selected => true,
                    //                     feature_name => 'Journal_Entry_Disable',
                    //                 ],
                    //             ]
                    //         ],
                    //     ]
                    // ],
                    /**
                     * Administrator Page
                     */
                    [
                        label => 'Administrator Pages',
                        feature_name => 'Administrator_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'User',
                                feature_name => 'User',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'User_View',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'User_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'User_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'User_Disable',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Account_Type_Edit',
                                    ],
                                ]
                            ],
                            [
                                label => 'Role',
                                feature_name => 'Role',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Role_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Role_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Role_Create',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Role_Disable',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Setting Page
                     */
                    [
                        label => 'Setting Pages',
                        feature_name => 'Setting_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Exchange Rate',
                                feature_name => 'Exchange_Rate',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_View',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Create',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Delete',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Company Profile',
                                feature_name => 'Company_Profile',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Company_Profile_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Company_Profile_Edit',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /*
                      Report For Staff like appointment and payslip 
                      this only for account that user link to  
                    */
                    [
                        label => 'Report',
                        feature_name => 'Report_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Income Statement Report',
                                feature_name => 'Income_Statement_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Income_Statement_Report_View',
                                    ],
                                ]
                            ],                            
                            [
                                label => 'Daily Cash Report',
                                feature_name => 'Daily_Case_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Daily_Case_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Daily_Case_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Payslip Report',
                                feature_name => 'Payslip_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Payslip_Report_View',
                                    ],
                                ]
                            ],
                            [
                                label => 'My Expense Report',
                                feature_name => 'My_Expense_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'My_Expense_Report_View',
                                    ],
                                ]
                            ],
                        ]
                    ],
                )
            ])
        );
		return $array;
	}

}
