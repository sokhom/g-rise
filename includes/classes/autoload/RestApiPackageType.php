<?php

use
	OSC\PackageType\Collection
		as packageCol
	, OSC\PackageType\Object
		as packageObj
;

class RestApiPackageType extends RestApi {

	public function get($params){
		$col = new packageCol();
		$col->sortByName("ASC");
		$this->getId() ? $col->filterById($this->getId()) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['filter'] ? $col->filterByName($params['GET']['filter']) : '';
		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new packageObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);

	}

	public function put($params){
		$obj = new packageObj();
		$packageId = $this->getId();
		$obj->setId($packageId);
		$obj->setProperties($params['PUT']);
		$obj->update($packageId);
		
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new packageObj();
		$obj->setId($this->getId());
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

	public function delete(){
		$obj = new packageObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
