<?php

use
	OSC\CustomerDocument\Object
		as CustomerDocumentObj	
	, OSC\CustomerDocument\Collection
		as CustomerDocumentCol
;

class RestApiCustomerDocument extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new CustomerDocumentCol();
			$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}
	}

}
