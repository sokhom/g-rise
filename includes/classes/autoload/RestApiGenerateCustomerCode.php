<?php

class RestApiGenerateCustomerCode extends RestApi{

    public function get(){
      $query = tep_db_query("
			  SELECT 
          count(customers_code) as total
        FROM 
          customers 
        WHERE
          customers_code IS NOT NULL  
            &&
          customers_code != '' 
    ");
		$queryTransaction = tep_db_fetch_array($query);
		$count = (int)$queryTransaction['total'];
		$count < 0 ? $count = 1 : $count = $count + 1;
		if($count < 999999){
			$string = '000000' . (string)$count;
			// sub string with 6digit
			$stringConcat =  substr($string, -6);
		}else{
			$stringConcat =  (string)$count;
		}
    $code = $_SESSION['customerCode'] . $stringConcat;
    
        // $queryCustomer = tep_db_query("
        //     SELECT 
        //       customers_code
        //     FROM 
        //       customers 
        //     WHERE
        //       customers_code IS NOT NULL  
        //         OR 
        //       customers_code != '' 
        //         ORDER BY 
        //       customers_code 
        //         DESC LIMIT 1
        // ");
        // $customer = tep_db_fetch_array($queryCustomer);
        // $code = (int)$customer['customers_code'] + 1;
        return [
            'data' => [
                'code' => $code
            ]
        ];
    }
}