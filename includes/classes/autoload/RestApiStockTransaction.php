<?php
use
    OSC\StockTransaction\Collection as  StockTransactionCol
;
class RestApiStockTransaction extends RestApi{

    public function get($params){

        $col=new StockTransactionCol;
        $col->sortByProductName('ASC');
        $params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
        return $this->getReturn($col,$params);



        $query = tep_db_query("
          SELECT
              product_id,
              product_name,
              product_description,
              qty_on_hand,
              SUM(stock_in) as stock_in,
              SUM(stock_out) as stock_out
          FROM
              stock_transaction
          WHERE
              create_date
                BETWEEN
              '2016-09-15 00:00:00'
                AND
              '2016-09-17 23:59:59'
          GROUP BY
            product_id
        ");
        $array = [];
        while($stockTransaction = tep_db_fetch_array($query)){
            $array[] = $stockTransaction;
        }
        //var_dump($stockTransaction);
        echo json_encode(
            array(
            'elements' => $array)
        );
    }


}
