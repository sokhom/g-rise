<?php

use
	OSC\DoctorList\Collection
		as StaffListCol
;

class RestApiStaffBirthday extends RestApi {

	public function get($params)
	{
		$col = new StaffListCol();
		$col->sortById('DESC');
		$col->filterByStatus(1);
		$col->filterByBirthday();
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

}
