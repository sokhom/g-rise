<?php

use
	OSC\TransferType\Collection
		as TransferTypeCol
	, OSC\TransferType\Object
		as TransferTypeObj
;

class RestApiTransferType extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new TransferTypeCol();
			// start limit page
			$col->sortByName("ASC");
			$col->sortById("DESC");
			$col->filterByName($params['GET']['name']);
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			$params['GET']['status'] ? $col->filterByStatus(1) : '';
			if($params['GET']['pagination']){
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array( $start, $showDataPerPage )
					)
				);
			}
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}
	public function post($params){
		if($_SESSION["id"]) {
			$obj = new TransferTypeObj();
			$obj->setCreateBy($_SESSION['user_name']);
			$obj->setProperties($params['POST']);
			$obj->insert();
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
				)
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function put($params){
		if($_SESSION["id"]) {
			$obj = new TransferTypeObj();
			$this->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setProperties($params['PUT']);
			$obj->update($this->getId());
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
			));
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

//	public function delete(){
//		$obj = new TransferTypeObj();
//		$obj->delete($this->getId());
//	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new TransferTypeObj();
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setId($this->getId());
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus();
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

}
