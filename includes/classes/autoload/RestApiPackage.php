<?php

use
	OSC\Package\Collection
		as packageCol
	, OSC\Package\Object
		as packageObj
	, OSC\PackageAddOn\Object
		as packageAddOnObj
	, OSC\PackageOption\Object
		as packageOptionObj
;

class RestApiPackage extends RestApi {

	public function get($params){
		$col = new packageCol();
		$col->sortByName("ASC");
		$this->getId() ? $col->filterById($this->getId()) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['filter'] ? $col->filterByName($params['GET']['filter']) : '';
		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new packageObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		$packageId = $obj->getId();

		$objAddOnDetail = new packageAddOnObj(); 
		foreach( $params['POST']['add_on_detail'] as $key => $value){
            $objAddOnDetail->setPackageId($packageId);
			$objAddOnDetail->setAddOnId($value['add_on_id']);
			$objAddOnDetail->setDescription($value['description']);
			$objAddOnDetail->insert();
			unset($value);
		}

		$objOptionDetail = new packageOptionObj(); 
		foreach( $params['POST']['option_detail'] as $key => $value){
            $objOptionDetail->setPackageId($packageId);
			$objOptionDetail->setOptionId($value['option_id']);
			$objOptionDetail->setDescription($value['description']);
			$objOptionDetail->insert();
			unset($value);
		}
		
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);

	}

	public function put($params){
		$obj = new packageObj();
		$packageId = $this->getId();
		$obj->setId($packageId);
		$obj->setProperties($params['PUT']);
		$obj->update($packageId);
		$objAddOnDetail = new packageAddOnObj(); 
		$objAddOnDetail->setPackageId($packageId);
		$objAddOnDetail->deleteByPackage();
		foreach( $params['PUT']['add_on_detail'] as $key => $value){            
            $objAddOnDetail->setPackageId($packageId);
			$objAddOnDetail->setAddOnId($value['add_on_id']);
			$objAddOnDetail->setDescription($value['description']);
            $objAddOnDetail->insert();
		}

		$objOptionDetail = new packageOptionObj(); 
		$objOptionDetail->setPackageId($packageId);
		$objOptionDetail->deleteByPackage();
		foreach( $params['PUT']['option_detail'] as $key => $value){
            $objOptionDetail->setPackageId($packageId);
			$objOptionDetail->setOptionId($value['option_id']);
			$objOptionDetail->setDescription($value['description']);
            $objOptionDetail->insert();
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new packageObj();
		$obj->setId($this->getId());
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

	public function delete(){
		$obj = new packageObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
