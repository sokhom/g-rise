<?php
class RestApiFeature  extends RestApi {

	public function get($params){
		$array = array(
		    data => array([
                label => 'Pages',
                selected => true,
                children => array(
                    /**
                     * Dashboard Page Role
                     */
                    [
                        label => 'Dashboard Pages',
                        feature_name => 'Dashboard_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Dashboard Group',
                                feature_name => 'Dashboard_Group',
                                selected => true,
                                children => [
                                    [
                                        label => 'View Customer Total',
                                        selected => true,
                                        feature_name => 'Customer_Total_View',
                                    ],
                                    [
                                        label => 'View Customer Balance',
                                        selected => true,
                                        feature_name => 'Customer_Balance_View',
                                    ],
                                    [
                                        label => 'View Package',
                                        selected => true,
                                        feature_name => 'Package_Total_View',
                                    ],
                                    [
                                        label => 'View Option',
                                        selected => true,
                                        feature_name => 'Option_Total_View',
                                    ],
                                    // [
                                    //     label => 'View Vendor Balance',
                                    //     selected => true,
                                    //     feature_name => 'Vendor_Balance_View',
                                    // ],
                                    // [
                                    //     label => 'View Total Supplier',
                                    //     selected => true,
                                    //     feature_name => 'Total_Supplier_View',
                                    // ],
                                    [
                                        label => 'View Total Staff',
                                        selected => true,
                                        feature_name => 'Total_Staff_View',
                                    ],
                                    // [
                                    //     label => 'View Top 10 Sale Best By Service',
                                    //     selected => true,
                                    //     feature_name => 'Sale_Best_By_Service_Graph',
                                    // ],
                                    // [
                                    //     label => 'View Top 10 Sale Best By Item',
                                    //     selected => true,
                                    //     feature_name => 'Sale_Best_By_Item_Graph',
                                    // ],
                                    // [
                                    //     label => 'View Top 10 Expense',
                                    //     selected => true,
                                    //     feature_name => 'Expense_Graph',
                                    // ],
                                    // [
                                    //     label => 'View Income Statement Graphic',
                                    //     selected => true,
                                    //     feature_name => 'Income_Statement_Graph'
                                    // ]
                                ]
                            ],
                        ]

                    ],
                    /**
                     * Notification Page Role
                     */
                    [
                        label => 'Notification Pages',
                        feature_name => 'Notification_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Customer Birth Day',
                                feature_name => 'Customer_Birth_Day_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Birth_Day_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Birth_Day_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Staff Birthday Report',
                                feature_name => 'Staff_Birthday_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Birthday_Report_View',
                                    ],
                                        [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Staff_Birthday_Report_Export',
                                    ]
                                ]
                            ],
                            // [
                            //     label => 'Customer Appointment Report',
                            //     feature_name => 'Customer_Appointment_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Customer_Appointment_Report_View',
                            //         ],
                            //         [
                            //             label => 'Export To Excel',
                            //             selected => true,
                            //             feature_name => 'Customer_Appointment_Report_Export',
                            //         ],
                            //     ]
                            // ],
                            // [
                            //     label => 'My Appointment Report',
                            //     feature_name => 'Appointment_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Appointment_Report_View',
                            //         ],
                            //     ]
                            // ],
                            // [
                            //     label => 'Stock Alert Report',
                            //     feature_name => 'Stock_Alert_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Stock_Alert_Report_View',
                            //         ],
                            //         [
                            //             label => 'Export To Excel',
                            //             selected => true,
                            //             feature_name => 'Stock_Alert_Report_Export',
                            //         ],
                            //     ]
                            // ],
                            // [
                            //     label => 'Customer Payment Overdue Report',
                            //     feature_name => 'Customer_Overdue_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Customer_Overdue_Report_View',
                            //         ],
                            //         [
                            //             label => 'Export To Excel',
                            //             selected => true,
                            //             feature_name => 'Customer_Overdue_Report_Export',
                            //         ],
                            //     ]
                            // ],
                        ]
                    ],
                    /**
                     * Calendar Menu
                     */
                    [
                        label => 'Calendar',
                        feature_name => 'Calendar_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Calendar',
                                feature_name => 'Calendar',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        feature_name => 'Calendar_View',
                                        selected => true,
                                    ]
                                ]
                            ]
                        ]
                    ],
                    /**
                     * New Page Menu
                     */
                    [
                        label => 'New Pages',
                        feature_name => 'New_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Client Log',
                                feature_name => 'New',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Client_Log_Create',
                                        selected => true,
                                    ]
                                ]
                            ],
                            [
                                label => 'Quotation Page',
                                feature_name => 'Quotation',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Quotation_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Quotation_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Quotation_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Quotation_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Quotation_View',
                                    ],
                                    [
                                        label => 'View Quotation Term',
                                        selected => true,
                                        feature_name => 'Quotation_Term_View',
                                    ],
                                    [
                                        label => 'Create Quotation Term',
                                        selected => true,
                                        feature_name => 'Quotation_Term_Create',
                                    ]
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Customer Page Role
                     */
                    [
                        label => 'Customer Pages',
                        feature_name => 'Customer_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Client Register',
                                feature_name => 'Client_Register',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Client_Register_Create',
                                        selected => true,
                                    ]
                                ]
                            ],
                            [
                                label => 'Customer Type',
                                feature_name => 'Customer_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Customer_Type_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Customer_Type_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Customer_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Customer_Type_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Type_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Member Ship',
                                feature_name => 'Member_Ship',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Member_Ship_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Member_Ship_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Member_Ship_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Member_Ship_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Member_Ship_View',
                                    ]
                                ]
                            ],
                            //  [
                            //     label => 'Branch',
                            //     feature_name => 'Branch',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'Create',
                            //             feature_name => 'Branch_Create',
                            //             selected => true,
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'Branch_Edit',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'Branch_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'Branch_Disable',
                            //         ],
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Branch_View',
                            //         ]
                            //     ]
                            // ],
                            [
                                label => 'Customer List',
                                feature_name => 'Customer_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Customer_List_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Customer_List_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Customer_List_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Customer_List_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_List_View',
                                    ],
                                    [
                                        label => 'Create Follow Up',
                                        selected => true,
                                        feature_name => 'Create_Follow_Up_Customer',
                                    ],
                                    [
                                        label => 'Delete Follow Up',
                                        selected => true,
                                        feature_name => 'Delete_Follow_Up_Customer',
                                    ]
                                ]
                            ],

                            [
                                label => 'Feed Back',
                                feature_name => 'Feed_Back',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Feed_Back_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Feed_Back_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Feed_Back_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Feed_Back_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Feed_Back_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Complaint',
                                feature_name => 'Complaint',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Complaint_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Complaint_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Complaint_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Complaint_Disable',
                                    ],
                                    [
                                        label => 'Resolve',
                                        selected => true,
                                        feature_name => 'Complaint_Resolve',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Complaint_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Sale Form',
                                feature_name => 'Sale_Form',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Sale_Form_Create',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Sale_Form_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        feature_name => 'Sale_Form_Delete',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Sale_Form_Disable',
                                    ],
                                    [
                                        label => 'Service Team',
                                        selected => true,
                                        feature_name => 'Sale_Form_Mission',
                                    ],
                                    [
                                        label => 'Photo Selection',
                                        selected => true,
                                        feature_name => 'Sale_Form_Photo_Selection',
                                    ],     
                                    [
                                        label => 'Photo Process',
                                        selected => true,
                                        feature_name => 'Sale_Form_Photo_Process',
                                    ],                                   
                                    [
                                        label => 'Delevery',
                                        selected => true,
                                        feature_name => 'Sale_Form_Delevery',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Sale_Form_View',
                                    ]
                                ]
                            ],
                        ]

                    ],
                    /**
                     * Staff Page Role
                     */
                    [
                        label => 'Staff Pages',
                        feature_name => 'Staff_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Staff Type',
                                feature_name => 'Staff_Type',
                                label => 'Staff Type',
                                feature_name => 'Staff_Type',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Staff_Type_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Staff_Type_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Staff_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Staff_Type_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Type_View',
                                    ]
                                ]
                            ],
                            [
                                label => 'Staff List',
                                feature_name => 'Staff_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Staff_List_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Staff_List_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Staff_List_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Staff_List_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_List_View',
                                    ]
                                ]
                            ],
                        ]
                    ],

                    /**
                     * Invoice Page Role
                     */
                    [
                        label => 'Invoice Pages',
                        feature_name => 'Invoice_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Invoice Page',
                                feature_name => 'Invoice',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Invoice_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Invoice_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Invoice_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Invoice_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Invoice_View',
                                    ],
                                    [
                                        label => 'View Invoice Term',
                                        selected => true,
                                        feature_name => 'Invoice_Term_View',
                                    ],
                                    [
                                        label => 'Create Invoice Term',
                                        selected => true,
                                        feature_name => 'Invoice_Term_Create',
                                    ]
                                ]
                            ],
                            // [
                            //     label => 'Quotation Page',
                            //     feature_name => 'Quotation',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'Create',
                            //             feature_name => 'Quotation_Create',
                            //             selected => true,
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'Quotation_Edit',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'Quotation_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'Quotation_Disable',
                            //         ],
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Quotation_View',
                            //         ],
                            //         [
                            //             label => 'View Quotation Term',
                            //             selected => true,
                            //             feature_name => 'Quotation_Term_View',
                            //         ],
                            //         [
                            //             label => 'Create Quotation Term',
                            //             selected => true,
                            //             feature_name => 'Quotation_Term_Create',
                            //         ]
                            //     ]
                            // ],
                            [
                                label => 'Receive_Payment Page',
                                feature_name => 'Receive_Payment',
                                selected => true,
                                children => [
                                    [
                                        label => 'Create',
                                        feature_name => 'Receive_Payment_Create',
                                        selected => true,
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Edit',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Disable',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Receive_Payment_View',
                                    ],
                                    [
                                        label => 'View Receive Payment Term',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Term_View',
                                    ],
                                    [
                                        label => 'Create Receive Payment Term',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Term_Create',
                                    ]
                                ]
                            ],
                        ]
                    ],

                    /**
                     * Item Page
                     */
                    [
                        label => 'Item Pages',
                        feature_name => 'Item_Pages',
                        selected => true,
                        children => [
                            // [
                            //     label => 'UM Type',
                            //     feature_name => 'UM_Type',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'Create',
                            //             feature_name => 'UM_Type_Create',
                            //             selected => true,
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'UM_Type_Edit',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'UM_Type_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'UM_Type_Disable',
                            //         ],
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'UM_Type_View',
                            //         ]
                            //     ]
                            // ],
                            // [
                            //     label => 'Product Type',
                            //     feature_name => 'Product_Type',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Product_Type_View',
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'Product_Type_Edit',
                            //         ],
                            //         [
                            //             label => 'Create',
                            //             selected => true,
                            //             feature_name => 'Product_Type_Create',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'Product_Type_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'Product_Type_Disable',
                            //         ],
                            //     ]
                            // ],
                            // [
                            //     label => 'Product List',
                            //     feature_name => 'Product_List',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Product_List_View',
                            //         ],
                            //         [
                            //             label => 'Edit',
                            //             selected => true,
                            //             feature_name => 'Product_List_Edit',
                            //         ],
                            //         [
                            //             label => 'Create',
                            //             selected => true,
                            //             feature_name => 'Product_List_Create',
                            //         ],
                            //         [
                            //             label => 'Delete',
                            //             selected => true,
                            //             feature_name => 'Product_List_Delete',
                            //         ],
                            //         [
                            //             label => 'Disable',
                            //             selected => true,
                            //             feature_name => 'Product_List_Disable',
                            //         ],
                            //     ]
                            // ],
                            [
                                label => 'Option Type',
                                feature_name => 'Option_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Option_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Option_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Option_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Option_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Option_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Options',
                                feature_name => 'Option_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Option_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Option_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Option_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Option_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Option_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Add On Type',
                                feature_name => 'Add_On_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Add_On_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Add_On_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Add_On_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Add_On_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Add_On_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Add On',
                                feature_name => 'Add_On_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Add_On_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Add_On_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Add_On_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Add_On_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Add_On_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Package Type',
                                feature_name => 'Package_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Package_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Package_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Package_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Package_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Package_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Package',
                                feature_name => 'Package_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Package_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Package_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Package_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Package_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Package_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Location Type',
                                feature_name => 'Location_Type_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Location_Type_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Location_Type_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Location_Type_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Location_Type_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Location_Type_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Location',
                                feature_name => 'Location_List',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Location_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Location_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Location_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'Location_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Location_Disable',
                                    ],
                                ]
                            ]
                        ]
                    ],
                    /**
                     * Report Page
                     */
                    [
                        label => 'Report Pages',
                        feature_name => 'Report_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Customer Report',
                                feature_name => 'Customer_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Customer Balance Report',
                                feature_name => 'Customer_Balance_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Customer_Balance_Report_View',
                                    ],
                                     [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Customer_Balance_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Staff Report',
                                feature_name => 'Staff_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Staff_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Staff_Report_Export',
                                    ]
                                ]
                            ],
                            // [
                            //     label => 'Product Report',
                            //     feature_name => 'Product_Report',
                            //     selected => true,
                            //     children => [
                            //         [
                            //             label => 'View',
                            //             selected => true,
                            //             feature_name => 'Product_Report_View',
                            //         ],
                            //         [
                            //             label => 'Export To Excel',
                            //             selected => true,
                            //             feature_name => 'Product_Report_Export',
                            //         ],
                            //     ]
                            // ],
                            [
                                label => 'Option Report',
                                feature_name => 'Option_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Option_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Option_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Add On Report',
                                feature_name => 'Add_On_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Add_On_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Add_On_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Package Report',
                                feature_name => 'Package_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Package_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Package_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Invoice Summary Report',
                                feature_name => 'Invoice_Summary_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Invoice_Summary_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Invoice_Summary_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Invoice Report',
                                feature_name => 'Invoice_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Invoice_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Invoice_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Quotation Report',
                                feature_name => 'Quotation_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Quotation_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Quotation_Report_Export',
                                    ],
                                ]
                            ],[
                                label => 'Receive Payment Report',
                                feature_name => 'Receive_Payment_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Receive_Payment_Report_Export',
                                    ],
                                ]
                            ],[
                                label => 'Cash Flow Report',
                                feature_name => 'Cash_Flow_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Cash_Flow_Report_View',
                                    ]
                                ]
                            ],[
                                label => 'Sale Form Report',
                                feature_name => 'Sale_Form_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Sale_Form_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Sale_Form_Report_Export',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Administrator Page
                     */
                    [
                        label => 'Administrator Pages',
                        feature_name => 'Administrator_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'User',
                                feature_name => 'User',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'User_View',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'User_Create',
                                    ],
                                    [
                                        label => 'Delete',
                                        selected => true,
                                        feature_name => 'User_Delete',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'User_Disable',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Account_Type_Edit',
                                    ],
                                ]
                            ],
                            [
                                label => 'Role',
                                feature_name => 'Role',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Role_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Role_Edit',
                                    ],
                                    [
                                        label => 'Create',
                                        selected => true,
                                        feature_name => 'Role_Create',
                                    ],
                                    [
                                        label => 'Disable',
                                        selected => true,
                                        feature_name => 'Role_Disable',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /**
                     * Setting Page
                     */
                    [
                        label => 'Setting Pages',
                        feature_name => 'Setting_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Exchange Rate',
                                feature_name => 'Exchange_Rate',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_View',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Create',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Delete',
                                    ],
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Exchange_Rate_Disable',
                                    ],
                                ]
                            ],
                            [
                                label => 'Company Profile',
                                feature_name => 'Company_Profile',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Company_Profile_View',
                                    ],
                                    [
                                        label => 'Edit',
                                        selected => true,
                                        feature_name => 'Company_Profile_Edit',
                                    ],
                                ]
                            ],
                        ]
                    ],
                    /*
                      Report For Staff like appointment and payslip 
                      this only for account that user link to  
                    */
                    [
                        label => 'Report',
                        feature_name => 'Report_Pages',
                        selected => true,
                        children => [
                            [
                                label => 'Income Statement Report',
                                feature_name => 'Income_Statement_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Income_Statement_Report_View',
                                    ],
                                ]
                            ],                            
                            [
                                label => 'Daily Cash Report',
                                feature_name => 'Daily_Case_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Daily_Case_Report_View',
                                    ],
                                    [
                                        label => 'Export To Excel',
                                        selected => true,
                                        feature_name => 'Daily_Case_Report_Export',
                                    ],
                                ]
                            ],
                            [
                                label => 'Payslip Report',
                                feature_name => 'Payslip_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'Payslip_Report_View',
                                    ],
                                ]
                            ],
                            [
                                label => 'My Expense Report',
                                feature_name => 'My_Expense_Report',
                                selected => true,
                                children => [
                                    [
                                        label => 'View',
                                        selected => true,
                                        feature_name => 'My_Expense_Report_View',
                                    ],
                                ]
                            ],
                        ]
                    ],
                )
            ])
        );
		return $array;
	}

}
