<?php

use
	OSC\CustomerList\Collection
		as CustomerListCol
	, OSC\CustomerList\Object
		as CustomerListObj
	, OSC\CustomerDocument\Object
		as CustomerDocumentObj
;

class RestApiCustomerList extends RestApi {

	public function get($params){
		$col = new CustomerListCol();
		// start limit page
		$col->sortByName('ASC');
		$params['GET']['name'] ? $col->filterByName($params['GET']['name'], true) : '';
		$params['GET']['customers_type_id'] ? $col->filterByCustomerTypeId($params['GET']['customers_type_id']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['branch_id'] ? $col->filterByBranchId($params['GET']['branch_id']) : '';
		$params['GET']['telephone'] ? $col->filterByTelephone($params['GET']['telephone']) : '';
		$params['GET']['filter'] ? $col->filterByFilterFromDropdown($params['GET']['filter']) : '';

		$params['GET']['type_code'] == 'has code' ? $col->filterByCustomerCodeNotNull() : ($params['GET']['type_code'] == 'no code' ? $col->filterByCustomerCodeNull() :'');

		if ($params['GET']['search_in_invoice']) {
			$showDataPerPage = 10;
		} elseif ($params['GET']['search_in_report']) {
			$showDataPerPage = 100;
		} else {
			$showDataPerPage = 10;
		}
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$start = $params['GET']['start'];
		$this->applyLimit($col,
			array(
				'limit' => array($start, $showDataPerPage)
			)
		);
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	
	public function post($params){
	
		// Validate customer with duplicate code
		if($params['POST']['customers_code']){
			$query = tep_db_query("
				SELECT
					customers_code
				FROM
					customers
				WHERE
					customers_code = '" . $params['POST']['customers_code'] . "'
			");
			$countCustomer = tep_db_num_rows($query);
			
			if($countCustomer > 0){
				return array(
					'data' => array(
						'error' => 'Duplicate customer code.'
					)
				);
			}
		}
		$obj = new CustomerListObj();
		$obj->setCreateBy($_SESSION['user_name']);
		$obj->setProperties($params['POST']);
		$obj->insert();

		// start insert data into customer document
		// $customerDocument = $params['POST']['customers_documents'];
		// if($customerDocument || sizeof( $customerDocument > 1)){
		// 	foreach( $customerDocument as $key => $value){
		// 		$objDetail = new CustomerDocumentObj();
		// 		$objDetail->setCustomersId($obj->getId());
		// 		$objDetail->setProperties($value);
		// 		$objDetail->insert();
		// 		unset($value);
		// 	}
		// }

		return array(
            'data' => array(
                'id' => $obj->getId(),
                'success' => 'success'
            )
		);
	}


	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new CustomerListObj();
			$obj->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus();
		}
	}

	public function put($params){
		if($params['PUT']['customers_code']){
			// Validate customer with duplicate code
			$query = tep_db_query("
				SELECT
					customers_code
				FROM
					customers
				WHERE
					customers_code = '" . $params['PUT']['customers_code'] . "'
						AND
					id != ".$this->getId()."
			");
			$countCustomer = tep_db_num_rows($query);
			
			if($countCustomer > 0){
				echo 'error';
				return;
			}
		}
		$obj = new CustomerListObj();
		$this->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setProperties($params['PUT']);
		$obj->update($this->getId());

		$objDetail = new CustomerDocumentObj();
		$objDetail->delete($this->getId());
		// start insert data into customer document
        // foreach( $params['PUT']['customers_documents'] as $key => $value){
		// 	$objDetail->setCustomersId($this->getId());
		// 	$objDetail->setName($value['name']);
		// 	$objDetail->setDescription($value['description']);
		// 	$objDetail->insert();
        //     unset($value);
        // }
		// $customerDocument=$params['PUT']['customers_documents'];
		// if($customerDocument || sizeof( $customerDocument > 1)){
		// 	foreach( $customerDocument as $key => $value){
		// 		//$objDetail = new CustomerDocumentObj();
		// 		$objDetail->setCustomersId($obj->getId());
		// 		$objDetail->setProperties($value);
		// 		$objDetail->insert();
		// 		unset($value);
		// 	}
		// }

		return array(
            'data' => array(
                'id' => $obj->getId(),
                'success' => 'success'
            )
		);
		
	}

	public function delete(){
		if($_SESSION["id"]) {
			$obj = new CustomerListObj();
			$obj->delete($this->getId());
		}
	}

}
