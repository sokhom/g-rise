<?php

use
	OSC\Service\Collection
		as ServiceCol
	, OSC\Service\Object
		as ServiceObj
	, OSC\StockTransaction\Object
		as StockTransactionObj
;

class RestApiService extends RestApi {

	public function get($params){
		if(!$_SESSION["id"]) {
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
		$col = new ServiceCol();
		// start limit page
		$col->sortById('DESC');
		$col->filterByName($params['GET']['name']);
		$params['GET']['code'] ? $col->filterByCode($params['GET']['code']) : '';
		$col->filterById($params['GET']['id']);
		$params['GET']['customer_type_id'] ? $col->filterByCustomerTypeId($params['GET']['customer_type_id']) : '';
		if ($params['GET']['pagination']) {
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
		
	}

	public function post($params){
		if(!$_SESSION["id"]) {
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
		$obj = new ServiceObj();
		$obj->setCreateBy($_SESSION['user_name']);
		$obj->setProperties($params['POST']);
		$obj->insert();

		// insert into stock transaction
		$objPro = new StockTransactionObj();

		$objPro->setProductName($params['POST']['name']);
		$objPro->setProductId($obj->getId());
		$objPro->setProductTypeId($params['POST']['type_id']);
		//$objPro->setProductKindOf($params['POST']['products_kind_of']);
		//$objPro->setProductTypeName($params['POST']['products_type_name']);
		$objPro->setProductDescription($params['POST']['detail']);
		$objPro->setBarcode($params['POST']['code']);
		//$objPro->setQtyOnHand($params['POST']['products_quantity']);
		$objPro->setCost($params['POST']['price']);
		$objPro->setWholeSalePrice($params['POST']['price']);
		$objPro->setRetailPrice($params['POST']['price']);
		// $objPro->setUmType($params['POST']['um_detail']['name']);
		// $objPro->setUmTypeAmount($params['POST']['um_detail']['amount']);
		// $objPro->setUmTypeRetail($params['POST']['um_sale_detail']['name']);
		// $objPro->setUmTypeRetailAmount($params['POST']['um_sale_detail']['amount']);
		$objPro->setCreateBy($_SESSION['user_name']);
		$objPro->insert();
		
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		if(!$_SESSION["id"]) {
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
		$obj = new ServiceObj();
		$obj->setProperties($params['PUT']);
		$obj->update($this->getId());
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		if(!$_SESSION["id"]) {
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
		$obj = new ServiceObj();
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus($this->getId());
	}

}
