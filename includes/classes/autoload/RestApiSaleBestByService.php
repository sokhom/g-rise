<?php

    class RestApiSaleBestByService extends RestApi {

        public function get($params){
            $where = '';
            if($_GET['from_date'] && $_GET['to_date']){
                $fromDate = $_GET['from_date'] . " 00:00:00";//2016-09-15 00:00:00
                $endDate = $_GET['to_date']. ' 23:59:59';//2016-09-17 23:59:59
                $where = " AND create_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";
            }
            
            $query = tep_db_query("
                SELECT
                    st.product_id, 
                    (
                        SELECT 
                            product_name 
                        FROM 
                            stock_transaction 
                        WHERE 
                            product_id = st.product_id 
                                AND 
                            status = 1  
                        ORDER BY id DESC LIMIT 1
                    ) as product_name,
                    sum(st.stock_out) as total_sale
                FROM
                    stock_transaction st
                WHERE
                    status = 1 and product_kind_of = 'service'
                    " . $where . "
                group by 
                    product_id
                Order by 
                    sum(stock_out) desc
                limit 10
            ");

            $array = [];
            while($stockTransaction = tep_db_fetch_array($query)){
                $array[] = array(
                    "product_name" => $stockTransaction['product_name'],
                    "total_sale" => doubleval($stockTransaction['total_sale']),
                );
            }
            return array(
				'data' => array(
					'elements' => $array
				)
            );
            
        }
    }