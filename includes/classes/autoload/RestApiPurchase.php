<?php
use
    OSC\PurchaseMaster\Collection as  PurchaseCol,
    OSC\PurchaseMaster\Object as  PurchaseObj,
    OSC\PurchaseDetail\Object as  PurchaseDetailObj,
    OSC\Products\Object as productObj,
    OSC\StockTransaction\Object as StockTransactionObj
;

class RestApiPurchase extends RestApi{

    public function get($params){
        if ($_SESSION["id"]) {
            $col = new PurchaseCol;
            $col->sortById('DESC');
            $params['GET']['supplier_name'] ? $col->filterBySupplierName($params['GET']['supplier_name']) : '';
            $params['GET']['reference_no'] ? $col->filterByReferenceNO($params['GET']['reference_no']) : '';
            $params['GET']['vendor_id'] ? $col->filterByVendorId($params['GET']['vendor_id']) : '';
            $params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';            
            $params['GET']['balance'] ? $col->filterByBalance() : '';
            $params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
            $params['GET']['status'] ? $col->filterByStatus(1) : '';
            if ($params['GET']['pagination']) {
                $showDataPerPage = 10;
                $start = $params['GET']['start'];
                $this->applyLimit($col,
                    array(
                        'limit' => array($start, $showDataPerPage)
                    )
                );
            }
            return $this->getReturn($col, $params);
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    public function post($params){
        if ($_SESSION["id"]) {
            $obj = new PurchaseObj();
            $obj->setCreateBy($_SESSION['user_name']);
            $obj->setProperties($params['POST']['purchase']);
            $milliseconds = substr(round(microtime(true) * 1000), 7);
			/***************
			 * Get Date ****
			 ***************/
			$date = $params['POST']['purchase']["purchase_date"];// date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
			$transferDate = date('dmy', strtotime($date));
			/**************************
			 * generate transfer no ****
			 **************************/
			// count number unit today in stock
			$query = tep_db_query("
				SELECT  COUNT(id) total FROM  purchase_master WHERE purchase_date = '" . $date . "'
			");
			$stockTransaction = tep_db_fetch_array($query);
			$count = (int)$stockTransaction['total'];
			$count < 0 ? $count = 1 : $count = $count + 1;
			$string = '0' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -2);
			$transactionNo = 'PUR-' . $transferDate . '-' . $stringConcat;
			$obj->setPurchaseNo($transactionNo);
            $obj->insert();
            $purchaseId = $obj->getId();
            // start insert data into detail
            foreach( $params['POST']['purchase_detail'] as $key => $value){
                $objPurchaseDetail = new PurchaseDetailObj();
                $objPurchaseDetail->setPurchaseId($purchaseId);
                $objPurchaseDetail->setProductId($value['product_id']);
                $objPurchaseDetail->setProductName($value['product_name']);
                $objPurchaseDetail->setDescription($value['description']);
                $objPurchaseDetail->setBarcode($value['barcode']);
                $objPurchaseDetail->setQty($value['qty']);
                $objPurchaseDetail->setFreeQty($value['free_qty']);
                $objPurchaseDetail->setQtyOnHand($value['qty_on_hand']);
                $objPurchaseDetail->setProductUmTypeName($value['product_um_type_name']);
                $objPurchaseDetail->setProductUmTypeAmount($value['product_um_type_amount']);
                $objPurchaseDetail->setCostBefore($value['cost_before']);
                $objPurchaseDetail->setDiscountDollar($value['discount_dollar']);
                $objPurchaseDetail->setDiscountPercent($value['discount_percent']);
                $objPurchaseDetail->setPriceOutRetail($value['price_out_retail']);
                $objPurchaseDetail->setPriceOutWholeSale($value['price_out_whole_sale']);
                $objPurchaseDetail->setUmTypeAmount($value['um_type_amount']);
                $objPurchaseDetail->setUmTypeId($value['um_type_id']);
                $objPurchaseDetail->setUmTypeName($value['um_type_name']);
                $objPurchaseDetail->setPriceIn($value['unit_price']);
                $objPurchaseDetail->setTotal($value['total']);
                $objPurchaseDetail->insert();

                // update stock and average cost in product
                $objPro = new productObj();
                $objPro->setProductsQuantity($value['qty']);
                // $objPro->setProductsPriceIn($value['unit_price']);
                $objPro->setProductsPriceIn($value['total']/$value['qty']);
                $objPro->updateStockInWithUM($value['product_id'], $value['qty'] + $value['free_qty'], $value['um_type_amount']);

                // insert into stock transaction
                $objStockTrans = new StockTransactionObj();
                $objStockTrans->setReferenceId($transactionNo);
                $objStockTrans->setProductName($value['product_name']);
                $objStockTrans->setTransactionDate($date);
                $objStockTrans->setProductKindOf($value['products_kind_of']);
                $objStockTrans->setProductId($value['product_id']);
                $objStockTrans->setProductTypeName($value['product_type']);
                $objStockTrans->setProductTypeId($value['product_type_id']);
                $objStockTrans->setProductDescription($value['description']);
                
                $objStockTrans->setBarcode($value['barcode']);
                $objStockTrans->setQtyOnHand($value['qty_on_hand']);
                $objStockTrans->setCost( $objPro->getProductsPriceIn() );
                $objStockTrans->setRetailPrice($value['price_out_retail']);
                $objStockTrans->setWholeSalePrice($value['price_out_whole_sale']);
                $objStockTrans->setStockIn($value['qty'] + $value['free_qty']);
                $objStockTrans->setUmType($value['um_type_name']);
                $objStockTrans->setUmTypeAmount($value['um_type_amount']);
                $objStockTrans->insert();

                unset($value);
                unset($objStockTrans);
                unset($objPro);
                unset($objPurchaseDetail);
            }
            return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'success' => 'success'
                )
            );
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    public function put($params){
        if ($_SESSION["id"]) {
            $obj = new PurchaseObj();
            $this->setId($this->getId());
            $obj->setProperties($params['PUT']);
            $obj->update($this->getId());
            return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'success' => 'success'
                )
            );
        }
    }

    public function delete(){
        if ($_SESSION["id"]) {
            $obj = new PurchaseObj();
            $obj->delete($this->getId());
        }
    }

    public function patch($params){
        if ($_SESSION["id"]) {
            $obj = new PurchaseObj();
            $status = (int)$params['PATCH']['status'];
            $obj->setStatus($params['PATCH']['status']);
            $obj->setUpdateBy($_SESSION['user_name']);
            if ($status > 0) {
                foreach ($params['PATCH']['detail'] as $key => $value) {
                    tep_db_query("
                        UPDATE
                            products
                        SET
                            products_quantity = products_quantity + " . $value['qty'] + $value['free_qty']  . "
                        WHERE
                            id = " . (int)$value['product_id'] . "
                    ");
                }
            } else {
                foreach ($params['PATCH']['detail'] as $key => $value) {
                    // start filter UM amount to calculate
                    $query = tep_db_query("
                        SELECT
                            u.amount
                        FROM
                            products p INNER JOIN um_type u on p.um_type_id = u.id
                        WHERE
                            p.id = '" . (int)$value['product_id'] . "'
                    ");
                    $product = tep_db_fetch_array($query);

                    $qtyAmount = ( ($value['qty'] + $value['free_qty']) * $value['um_type_amount']) / doubleval($product['amount']);

                    tep_db_query("
                        UPDATE
                            products
                        SET
                            products_quantity = products_quantity - " . $qtyAmount . ",
                            products_price_in = " . $value['cost_before'] . "
                        WHERE
                            id = " . (int)$value['product_id'] . "
                    ");
                }
            }
            // update stock transaction status
            $objStockTrans = new StockTransactionObj();
            $objStockTrans->setStatus($status);
            $objStockTrans->setReferenceId($params['PATCH']['purchase_no']);            
            $objStockTrans->updateStatus();
            // tep_db_query("
            //     UPDATE
            //         stock_transaction
            //     SET
            //         status = " . $params['PATCH']['status'] . "
            //     WHERE
            //         reference_id = " . $this->getId() . "
            // ");
            $obj->setId($this->getId());
            $obj->updateStatus();
            echo 'update successfully.';
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

}
