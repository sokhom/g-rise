<?php

use
	OSC\StockOut\Collection
		as StockOutCol
	, OSC\StockOut\Object
		as StockOutObj
	, OSC\StockOutDetail\Object
		as StockOutDetailObj
	, OSC\Products\Object
		as productObj
	, OSC\StockTransaction\Object
		as StockTransactionObj
	, OSC\CaseFlow\Object 
		as CaseFlowObj
	, OSC\StockOutDetailDoctor\Object 
		as StockOutDetailDoctorObj
;

class RestApiStockOut extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new StockOutCol();
			$col->sortById("DESC");
			$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
			$params['GET']['remain'] ? $col->filterByRemain() : '';
			$params['GET']['overdue'] ? $col->filterByOverdue(30) : '';
			$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
			$params['GET']['invoice_no'] ? $col->filterByInvoice($params['GET']['invoice_no']) : '';
			$params['GET']['status'] ? $col->filterByStatus($params['GET']['status']) : '';
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			// start limit page
			if($params['GET']['pagination']){
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array( $start, $showDataPerPage )
					)
				);
			}
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function post($params){
		if($_SESSION["id"]){
			$obj = new StockOutObj();
			$obj->setProperties($params['POST']['stock']);
			$obj->setOverdue($params['POST']['stock']['stock_out_date']);
			$milliseconds = substr(round(microtime(true) * 1000), 7);
			/***************
			 * Get Date ****
			 ***************/
			$date = $params['POST']['stock']["stock_out_date"];//date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
			$invoiceDate = date('dmy', strtotime($date));
			/**************************
			 * generate invoice no ****
			 **************************/
			// count number unit today in stock
			$query = tep_db_query("
				SELECT  COUNT(id) total FROM  stock_out WHERE stock_out_date = '" . $date . "'
			");
			$stockTransaction = tep_db_fetch_array($query);
			$count = (int)$stockTransaction['total'];
			$count < 0 ? $count = 1 : $count = $count + 1;
			$string = '0' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -2);
			$invoiceNo = $_SESSION['invoiceNo'] .'-'. $invoiceDate . '-' . $stringConcat;
			$obj->setStockOutNo($invoiceNo);
			$obj->insert();
			$stockId = $obj->getId();

			// start insert doctor case flow
			$objCaseFlow = new CaseFlowObj();
			$objCaseFlow->setStatus(1);
			$objCaseFlow->setInvoiceNo($invoiceNo);
			$objCaseFlow->setInvoiceDate($params['POST']['stock']['stock_out_date']);
			$objCaseFlow->setCustomerId($params['POST']['stock']['customer_id']);
			$objCaseFlow->setCustomerName($params['POST']['stock']['customer_name']);
			$objCaseFlow->setDoctorId($params['POST']['stock']['doctor_id']);
			$objCaseFlow->setDoctorName($params['POST']['stock']['doctor_name']);
			$objCaseFlow->setPayment($params['POST']['stock']['payment']);
			$objCaseFlow->setPaymentMethod($params['POST']['stock']['payment_method']);
			$objCaseFlow->setBankCharge($params['POST']['stock']['bank_charge']);
			$objCaseFlow->setCreateBy($_SESSION['user_name']);
			$objCaseFlow->insert();

			foreach( $params['POST']['stock_detail'] as $key => $value){
				$objStockOut = new StockOutDetailObj();
				$objStockOut->setStockOutId($stockId);
				$objStockOut->setStockOutNo($invoiceNo);
				$objStockOut->setStockOutDate($params['POST']['stock']['stock_out_date']);
				$objStockOut->setDiscountCash($value['discount_cash']);
				$objStockOut->setDiscountPercent($value['discount_percent']);
				$objStockOut->setProductId($value['product_id']);
				$objStockOut->setProductName($value['product_name']);
				$objStockOut->setProductKindOf($value['products_kind_of']);
				$objStockOut->setProductTypeId($value['product_type_id']);
				$objStockOut->setProductCode($value['barcode']);
				$objStockOut->setProductImage($value['products_image']);
				$objStockOut->setRemark($value['remark']);
				$objStockOut->setAddMorePrice($value['add_more_price']);
				$objStockOut->setQty($value['qty']);
				$objStockOut->setPrice($value['unit_price']);
				$objStockOut->setRetailPrice($value['retail_price']);
				$objStockOut->setWholeSalePrice($value['whole_sale_price']);
				$objStockOut->setType($value['type']);

				$objStockOut->setUmTypeRetailAmount($value['um_type_retail_amount']);
				$objStockOut->setUmTypeRetailName($value['um_type_retail_name']);
				$objStockOut->setUmTypeRetailId($value['um_type_retail_id']);

				$objStockOut->setUmTypeWholeSaleAmount($value['um_type_whole_sale_amount']);
				$objStockOut->setUmTypeWholeSaleName($value['um_type_whole_sale_name']);
				$objStockOut->setUmTypeWholeSaleId($value['um_type_whole_sale_id']);

				$objStockOut->setCost($value['cost']);
				$objStockOut->setTotal($value['total']);
				$objStockOut->insert();

				// insert into stock transaction
				$objStockTransaction = new StockTransactionObj();
				$objStockTransaction->setReferenceId($invoiceNo);
				$objStockTransaction->setProductName($value['product_name']);
				$objStockTransaction->setProductKindOf($value['products_kind_of']);
				$objStockTransaction->setProductId($value['product_id']);
				$objStockTransaction->setProductTypeId($value['product_type_id']);
				$objStockTransaction->setProductDescription($value['description']);
				$objStockTransaction->setBarcode($value['barcode']);
				$objStockTransaction->setQtyOnHand($value['qty_on_hand']);
				$objStockTransaction->setCost($value['cost']);
				//$objStockTransaction->setPrice($value['unit_price']);
				$objStockTransaction->setRetailPrice($value['unit_price']);
				$objStockTransaction->setWholeSalePrice($value['unit_price']);
				$objStockTransaction->setTransactionDate($params['POST']['stock']['stock_out_date']);
				$objStockTransaction->setUmType($value['um_type_whole_sale_name']);
				$objStockTransaction->setUmTypeAmount($value['um_type_whole_sale_amount']);
				$objStockTransaction->setUmTypeRetail($value['um_type_retail_name']);
				$objStockTransaction->setUmTypeRetailAmount($value['um_type_retail_amount']);

				$objStockTransaction->setStockOut($value['qty']);
				$objStockTransaction->insert();

				// update stock from product list
				if($value['products_kind_of'] == 'item'){
					$objStockTransaction = new productObj();
					$objStockTransaction->setProductsQuantity($value['qty']);
					// get amount UM
					if($value['type'] == "whole_sale"){
						$umAmount = $value['um_type_whole_sale_amount'];
					}else{
						$umAmount = $value['um_type_retail_amount'];
					}
					$objStockTransaction->updateStockOutWithUM($value['product_id'], $value['qty'], $umAmount);
				}
				unset($value);
			}

			// start insert into doctor detail 
			foreach( $params['POST']['stock']['doctor_detail'] as $key => $value){
				$objStockoutDoctor = new StockOutDetailDoctorObj();
				$objStockoutDoctor->setDoctorId($value['id']);
				$objStockoutDoctor->setDoctorName($value['name']);
				$objStockoutDoctor->setStockOutId($stockId);
				$objStockoutDoctor->setStockOutNo($invoiceNo);
				$objStockoutDoctor->insert();
			}

			return array( data => array(
				id => $stockId,
				invoice_no => $invoiceNo
			));
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}

	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new StockOutObj();
			$status = (int)$params['PATCH']['status'];
			$obj->setStatus($status);
			$obj->setUpdateBy($_SESSION['user_name']);
			//var_dump($status);
	//		if($status > 0){
				foreach ($params['PATCH']['detail'] as $key => $value) {
					if($value['type'] == "whole_sale" && $value['product_kind_of'] == 'item'){
						$qtyAmount = ($value['qty'] * $value['um_type_whole_sale_amount']) / $value['um_type_whole_sale_amount'];
						tep_db_query("
							UPDATE
								products
							SET
								products_quantity = products_quantity + " . $qtyAmount . "
							WHERE
								id = " . (int)$value['product_id'] . "
						");
					}
					// else{
					// 	$qtyAmount = ($value['qty'] * $value['um_type_retail_amount']) / $value['um_type_whole_sale_amount'];
					// }                   

					// if($value['product_kind_of'] == 'item'){
					// 	tep_db_query("
					// 		UPDATE
					// 			products
					// 		SET
					// 			products_quantity = products_quantity + " . $qtyAmount . "
					// 		WHERE
					// 			id = " . (int)$value['product_id'] . "
					// 	");
					// }
				}
	//		}else {
	//			foreach ($params['PATCH']['detail'] as $key => $value) {
	//				tep_db_query("
	//                    UPDATE
	//                        products
	//                    SET
	//                        products_quantity = products_quantity - " . (int)$value['qty'] . "
	//                    WHERE
	//                        id = " . (int)$value['product_id'] . "
	//                ");
	//			}
	//		}
			// update stock transaction status
			tep_db_query("
				UPDATE
					stock_transaction
				SET
					status = " . $status . "
				WHERE
					reference_id = '" . $params['PATCH']['stock_out_no'] . "'
			");
			// update status in stock out detail
			
			tep_db_query("
				UPDATE
					stock_out_detail
				SET
					status = " . $status . "
				WHERE
					stock_out_no = '" . $params['PATCH']['stock_out_no'] . "'
			");
			$obj->setId($this->getId());
			$obj->setStockOutNo($params['PATCH']['stock_out_no']);
			$obj->updateStatus();
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}
}
