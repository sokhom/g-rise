<?php

class RestApiImportVendor extends RestApi {

	public function post($params){
		if($_SESSION["id"]) {
			//validate whether uploaded file is a csv file
			$csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv', 'text/xlsx', 'text/xls');
			if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					//open uploaded csv file with read only mode
					$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
					//skip first line
					fgetcsv($csvFile);
					//parse data from csv file line by line
					while(($line = fgetcsv($csvFile, 10000, ",")) !== FALSE){
						// $gender = strtolower($line[2]); 
						// $gender = $gender == 'f' ? 'Female' : 'Male';
						// $time = strtotime($line[3]);							
						// $dob = date('Y-m-d',$time);
						$sql_data_array = array(
							'name' => $line[0],
							'company_name' => $line[0],
							'supplier_type_id' => 1,
							// 'sex' => $gender,
							// 'dob' => $dob,
							// 'basic_salary' => '',
							'email' => $line[5],
							'tel' => $line[1],// str_replace(array("'", ' '), array('', ''), $line[1]), // replace singal quote and space
							'address' => $line[4],
							'note' => $line[2] . ". " . $line[3],
							'status' => 1,
							'country' => 'Cambodia',
							'create_by' => $_SESSION['user_name'],
							'create_date' => date("Y/m/d h:i"),
						);
						tep_db_perform("supplier_list", $sql_data_array);							
					}

					//close opened csv file
					fclose($csvFile);

					$status = 'success';
				}else{
					$status = 'error';
				}
			}else{
				$status = 'invalid_file';
			}

			return array(
				'data' => array(
					'status' => $status
				)
			);
		}
	}

}
