<?php

use
	OSC\DoctorPayslip\Collection
		as DoctorPayslipCol
	, OSC\DoctorPayslip\Object
		as DoctorPayslipObj
	, OSC\DoctorPayslipDetail\Object
		as DoctorPayslipDetailObj
	, OSC\DoctorPayslipAllowanceDetail\Object
		as DoctorPayslipAllowanceDetailObj
;

class RestApiDoctorPayslip extends RestApi {

	public function get($params){
		$col = new DoctorPayslipCol();
		// start limit page
		$col->sortById('DESC');
		$col->filterByName($params['GET']['name']);
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new DoctorPayslipObj();
		$obj->setProperties($params['POST']['model']);
		$obj->insert();
		$id = $obj->getId();
		foreach( $params['POST']['model_detail'] as $key => $value){
			$objDetail = new DoctorPayslipDetailObj(); 
			$objDetail->setDoctorPayslipId($id);
			$objDetail->setFromDate($params['POST']['model']['from_date']);
			$objDetail->setToDate($params['POST']['model']['to_date']);
			$objDetail->setProperties($value);
			$objDetail->insert();

			foreach( $value['detail'] as $key => $value){
				$objAllowanceDetail = new DoctorPayslipAllowanceDetailObj(); 
				$objAllowanceDetail->setDoctorPayslipDetailId($objDetail->getId());
				$objAllowanceDetail->setAllowanceId($value['allowance_id']);
				$objAllowanceDetail->setAmount($value['amount']);
				$objAllowanceDetail->setDescription($value['description']);
				$objAllowanceDetail->insert();
				unset($value);
			}
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new DoctorPayslipObj();
		$this->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setProperties($params['PUT']['model']);
		$obj->update($this->getId());
		$objDetail = new DoctorPayslipDetailObj(); 
		$objDetail->delete($this->getId());
		$objAllowanceDetail = new DoctorPayslipAllowanceDetailObj();
		$objAllowanceDetail->delete($this->getId());
		foreach( $params['PUT']['model_detail'] as $key => $value){
			$objDetail->setId($value['id']);
			$objDetail->setFromDate($params['POST']['model']['from_date']);
			$objDetail->setToDate($params['POST']['model']['to_date']);
			$objDetail->setProperties($value);
			$objDetail->insert();

			foreach( $value['detail'] as $key => $value){ 
				$objAllowanceDetail->setDoctorPayslipDetailId($this->getId());
				$objAllowanceDetail->setAllowanceId($value['allowance_id']);
				$objAllowanceDetail->setAmount($value['amount']);
				$objAllowanceDetail->setDescription($value['description']);
				$objAllowanceDetail->insert();
				unset($value);
			}
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new DoctorPayslipObj();
		$obj->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();

		//update status in detail
		$objDetail = new DoctorPayslipDetailObj();
		$objDetail->setId($this->getId());
		$objDetail->setStatus($params['PATCH']['status']);
		$objDetail->updateStatus();
	}

	public function delete(){
		$this->removeAll($this->getId());
	}

	function removeAll($id){
		$obj = new DoctorPayslipObj();
		$obj->delete($id);

		$objDetail = new DoctorPayslipDetailObj(); 
		$objDetail->delete($id);

		$objAllowanceDetail = new DoctorPayslipAllowanceDetailObj(); 
		$objAllowanceDetail->delete($id);
	}

}
