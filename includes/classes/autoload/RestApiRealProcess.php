<?php

use
	OSC\RealProcessMaster\Collection
		as RealProcessCol
	, OSC\RealProcessMaster\Object
		as RealProcessObj
	, OSC\RealProcessDetail\Object
		as RealProcessDetailObj
;

class RestApiRealProcess extends RestApi {

	public function get($params){
		$col = new RealProcessCol();
		$col->sortByInvoiceNo("DESC");
		$col->filterById($this->getId());
		$params['GET']['invoice_no'] ? $col->filterByInvoice($params['GET']['invoice_no']) : '';
		$params['GET']['customer_id'] ? $col->filterByCustomer($params['GET']['customer_id']) : '';
		if($params['GET']['start'] && $params['GET']['end']){
			$col->filterByDate($params['GET']['start'], $params['GET']['end']);
		}
		
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new RealProcessObj();
		$obj->setProperties($params['POST']['master']);
		$obj->insert();

		$objDetail = new RealProcessDetailObj();
		// start insert data into detail
		foreach( $params['POST']['detail'] as $key => $value){
			$objDetail->setRealProcessId($obj->getId());
			$objDetail->setProperties($value);
			$objDetail->insert();
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new RealProcessObj();
		$obj->setId($this->getId());
		$obj->setProperties($params['PUT']['master']);
		$obj->update();

		$objDetail = new RealProcessDetailObj();
		$objDetail->setId($this->getId());
		$objDetail->delete();
		// start insert data into detail
		foreach( $params['PUT']['detail'] as $key => $value){
			$objDetail->setRealProcessId($obj->getId());
			$objDetail->setProperties($value);
			$objDetail->insert();
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function delete(){
		$obj = new RealProcessObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
