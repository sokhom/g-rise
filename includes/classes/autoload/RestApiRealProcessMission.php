<?php

use
	OSC\RealProcessMission\Collection
		as RealProcessMissionCol
	, OSC\RealProcessMission\Object
		as RealProcessMissionObj
;

class RestApiRealProcessMission extends RestApi {

	public function get($params){
		$col = new RealProcessMissionCol();
		
		//$col->filterById($this->getId());
		$params['GET']['invoice_no'] ? $col->filterByInvoice($params['GET']['invoice_no']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new RealProcessMissionObj();
		$obj->setRealProcessId($params['POST']['id']);
		$obj->delete();
		// start insert data into detail
		foreach( $params['POST']['master'] as $key => $value){			
			$obj->setDescription($value['description']);
			$obj->setDate($value['date']);
			$obj->setStaffName($value['staff']["name"] ? $value['staff']["name"] : null);
			$obj->setStaffId($value['staff']["id"] ? $value['staff']["id"] : null);
			$obj->setRealProcessId($params['POST']['id']);
			$obj->setNote($value['note']);
			$obj->setRole($value['role']);
			$obj->insert();
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new RealProcessMissionObj();
		$obj->setRealProcessId($params['PUT']['real_process_id']);
		$obj->delete();
		// start insert data into detail
		foreach( $params['PUT']['master'] as $key => $value){
			$obj->setProperties($value);
			$obj->insert();
			unset($value);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function delete(){
		$obj = new RealProcessMissionObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
