<?php

use
	OSC\DoctorPayslip\Collection
		as DoctorPayslipCol
	, OSC\DoctorPayslip\Object
		as DoctorPayslipObj
	, OSC\DoctorPayslipDetail\Object
		as DoctorPayslipDetailObj
	, OSC\DoctorPayslipDetail\Collection
		as DoctorPayslipDetailCol
;

class RestApiDoctorPayslipDetail extends RestApi {

	public function get($params){
		$col = new DoctorPayslipDetailCol();
		// start limit page
		$col->sortById('DESC');
		$col->filterByDoctorName($params['GET']['name']);
		$params['GET']['staff_id'] ? $col->filterByDoctorId($params['GET']['staff_id']) : "";
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['from_date'] && $params['GET']['to_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

}
