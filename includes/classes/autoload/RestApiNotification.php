<?php

class RestApiNotification extends RestApi{

    public function get(){
        //get customer appointment for each doctor
        $queryAppointmentEachStaff = tep_db_query("
            SELECT 
              count(a.id) as total
            FROM 
              appointment a inner join permission p 
            WHERE 
              DATEDIFF(a.appointment_date, NOW()) <= 7 
                AND 
              a.status = 1
                AND 
              a.doctor_id = '" . $_SESSION['link_to'] ."'
                AND
              p.feature_name = 'Appointment_Report_View' 
                AND 
              p.is_selected = 1
                AND 
              p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countAppointmentEachStaff = tep_db_fetch_array($queryAppointmentEachStaff);
        
        // Count all appointment
        $queryAppointment = tep_db_query("
          SELECT 
            count(a.id) as total
          FROM 
            appointment a inner join permission p 
          WHERE 
            DATEDIFF(a.appointment_date, NOW()) <= 7 
              AND 
            a.status = 1
              AND
            p.feature_name = 'Customer_Appointment_Report_View' 
              AND 
            p.is_selected = 1
              AND 
            p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countAppointment = tep_db_fetch_array($queryAppointment);

        //get customer birthday
        $queryBirthday = tep_db_query("
          select
            count(c.id) as total
          from
            customers c inner join permission p 
          where
            DAY(c.customers_dob) = DAY(NOW())
              AND
            MONTH(c.customers_dob) = MONTH(NOW())
              AND 
            c.status = 1
              AND
            p.feature_name = 'Customer_Birth_Day_Report_View' 
              AND 
            p.is_selected = 1
              AND 
            p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countBirthDay = tep_db_fetch_array($queryBirthday);

        //get staff birthday
        $queryStaffBirthday = tep_db_query("
          select
            count(d.id) as total
          from
            doctor_list d inner join permission p 
          where
            DAY(d.dob) = DAY(NOW())
              AND
            MONTH(d.dob) = MONTH(NOW())
             AND 
            d.status = 1
             AND
            p.feature_name = 'Staff_Birthday_Report_View' 
               AND 
            p.is_selected = 1
              AND 
            p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countStaffBirthDay = tep_db_fetch_array($queryStaffBirthday);

        //get stock alert
        $queryProduct = tep_db_query("
          select
            count(pr.id) as total
          from
            products pr inner join permission p 
          where
            pr.order_alert >= pr.products_quantity
              AND
            pr.status = 1
              AND
            pr.products_kind_of = 'item'
              AND
            p.feature_name = 'Stock_Alert_Report_View' 
              AND 
            p.is_selected = 1
              AND 
            p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countProductOrder = tep_db_fetch_array($queryProduct);

        // count customer for notification overdue 
        $queryCustomerPaymentOverdue = tep_db_query("
          select
            count(s.id) as total
          from
            stock_out s inner join permission p 
          where
            s.remain > 0
              AND
            s.status = 1
              AND
            DATEDIFF(now(), s.overdue) >= 30
              AND
            p.feature_name = 'Customer_Overdue_Report_View' 
              AND 
            p.is_selected = 1 
              AND 
            p.role_id = '" . $_SESSION['role_id'] ."'
        ");
        $countCustomerOverdue = tep_db_fetch_array($queryCustomerPaymentOverdue);

        return [
            'data' => [
                'appointment_count' => (int)$countAppointment['total'],
                'my_appointment_count' => (int)$countAppointmentEachStaff['total'],
                'birthDay_count' => (int)$countBirthDay['total'],
                'staff_birthDay_count' => (int)$countStaffBirthDay['total'],
                'product_order' => (int)$countProductOrder['total'],
                'customer_payment_overdue' => (int)$countCustomerOverdue['total']
            ]
        ];
    }
}