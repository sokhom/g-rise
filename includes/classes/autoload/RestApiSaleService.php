<?php
use
    OSC\SaleService\Collection as  SaleServiceCol,
    OSC\SaleService\Object as  SaleServiceObj,
    OSC\SaleServiceDetail\Object as  SaleServiceDetailObj
;

class RestApiSaleService extends RestApi{

    public function get($params){
        if ($_SESSION["id"]) {
            $col = new SaleServiceCol;
            $col->sortById('DESC');
            $params['GET']['filters'] ? $col->filterByMulti($params['GET']['filters']) : '';
            $params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
            $params['GET']['status'] ? $col->filterByStatus(1) : '';
            if ($params['GET']['pagination']) {
                $showDataPerPage = 10;
                $start = $params['GET']['start'];
                $this->applyLimit($col,
                    array(
                        'limit' => array($start, $showDataPerPage)
                    )
                );
            }
            return $this->getReturn($col, $params);
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    public function post($params){
        if ($_SESSION["id"]) {
            $obj = new SaleServiceObj();
            $obj->setProperties($params['POST']['model']);            
            $obj->insert();
            $id = $obj->getId();
            // start insert data into detail
            foreach( $params['POST']['model_detail'] as $key => $value){
                $objDetail = new SaleServiceDetailObj();
                $objDetail->setSaleServiceId($id);
                $objDetail->setProperties($value);
                // $objDetail->setProductId($value["product_id"]);
                // $objDetail->setProductCost($value["product_cost"]);
                // $objDetail->setProductName($value["product_name"]);
                // $objDetail->setProductCode($value["product_code"]);
                // $objDetail->setQty($value["qty"]);
                // $objDetail->setServiceCost($value["service_cost"]);
                // $objDetail->setUnitUse($value["unit_use"]);
                // $objDetail->setUnitDeductStock($value["unit_deduct_stock"]);
                // $objDetail->setUmType($value["um_type"]);
                $objDetail->insert();
                unset($value);
            }
            return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'success' => 'success'
                )
            );
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    public function put($params){
        if ($_SESSION["id"]) {
            $obj = new SaleServiceObj();
            $obj->setId($this->getId());
            $obj->setProperties($params['PUT']['model']);
            $obj->update();
            
            $objDetail = new SaleServiceDetailObj();
            $objDetail->setSaleServiceId($this->getId());
            $objDetail->delete();
             // start insert data into detail
            foreach( $params['PUT']['model_detail'] as $key => $value){ 
                $objDetail->setProperties($value);
                $objDetail->insert();
                unset($value);
            }
            return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'success' => 'success'
                )
            );
        }
    }

    public function patch($params){
        if ($_SESSION["id"]) {
            $obj = new SaleServiceObj();
            $status = (int)$params['PATCH']['status'];
            $obj->setId($this->getId());
            $obj->setStatus($params['PATCH']['status']);
            $obj->updateStatus();
            echo 'update successfully.';
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

}
