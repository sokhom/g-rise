<?php
//use
//    OSC\AccountTypeIncomeStatement\Collection as  IncomeStatementCol
//;

class RestApiIncomeStatement extends RestApi{

    public function get(){
        $fromDate = $_GET['from_date'] . " 00:00:00";//2016-09-15 00:00:00
        $endDate = $_GET['to_date']. ' 23:59:59';//2016-09-17 23:59:59
        $array = [ data => []];
        // sum product item
        $sellItemQuery = tep_db_query("
            select sum(total) as total_item 
            from stock_out_detail 
            where status = 1 and product_kind_of = 'item'
                and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
                
        ");
        $incomeItem = tep_db_fetch_array($sellItemQuery);
        $array[data]['total_income_item'] = doubleval($incomeItem['total_item']);
        
        // sum product service
        $sellServiceQuery = tep_db_query("
            select sum(total) as total_service 
            from stock_out_detail 
            where status = 1 and product_kind_of = 'service'
                and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
               
        ");
        $incomeService = tep_db_fetch_array($sellServiceQuery);        
        $array[data]['total_income_service'] = doubleval($incomeService['total_service']);
        
        // sum product cost
        $costQuery = tep_db_query("
            select SUM(CASE WHEN type = 'whole_sale' THEN (cost / um_type_whole_sale_amount) * (qty * um_type_whole_sale_amount) 
                    ELSE (cost / um_type_whole_sale_amount) * (qty * um_type_retail_amount) END )
                as total_cost 
            from stock_out_detail 
            where status = 1 and product_kind_of = 'item' 
                and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
        ");
        $costExpense = tep_db_fetch_array($costQuery);        
        $costExpense['total_cost'] != null ? $array[data]['total_cost_expense'] = doubleval($costExpense['total_cost']) : $array[data]['total_cost_expense'] = 0;

        // sum purchase item
        $purchaseQuery = tep_db_query("
            select sum(total) as total_purchase 
            from purchase_master 
            where status = 1 and purchase_date between '". $fromDate ."' AND '". $endDate ."'
        ");
        $purchase = tep_db_fetch_array($purchaseQuery);
        $purchase['total_purchase'] != null ? $array[data]['total_purchase'] = doubleval($purchase['total_purchase']) : $array[data]['total_purchase'] = 0;

        // sum staff payroll
        $staffPayslipQuery = tep_db_query("
            select sum(total_net_pay) as total_pay 
            from doctor_payslip 
            where status = 1 and from_date between '". $fromDate ."' AND '". $endDate ."'
        ");
        $payslip = tep_db_fetch_array($staffPayslipQuery);
        $payslip['total_pay'] != null ? $array[data]['total_payslip'] = doubleval($payslip['total_pay']) : $array[data]['total_payslip'] = 0;

        // query other expense
        $otherExpenseQuery = tep_db_query("
            select et.name, sum(e.amount) as amount
            from expense e inner join expense_type et on e.expense_type_id = et.id
            where e.status = 1 and e.expense_date between '". $fromDate ."' AND '". $endDate ."'
            group by e.expense_type_id
        ");
        // $expense = tep_db_fetch_array($otherExpenseQuery);
        // $expense['total_expense_amount'] != null ? $array[data]['total_expense_amount'] = doubleval($expense['total_expense_amount']) : '';
        $expenseArray = [];
        while( $expense = tep_db_fetch_array($otherExpenseQuery)){
            $expenseArray[] = [
                'name' => $expense['name'],
                'amount' => doubleval($expense['amount'])
            ];
            // $expenseArray['name'] = $expense['name'];
            // $expenseArray['amount'] = doubleval($expense['amount']);
        };
        $array[data]['other_expense'] = $expenseArray;
        
        return $array;        
    }

    public function oldFunctionLinkWithAccount($params){

        $incomeQuery = tep_db_query("
            select id, name from account_type where income_statement = 1
        ");

        $array = array();
        $arrayDetail = array();
        $sum = array();
        $sumDetail = array();
        $from = $params['GET']['from_date'];
        $to = $params['GET']['to_date'];
        if($from && $to){
            $addWhere = " AND trans_date BETWEEN '" . $from . "' AND '" . $to . "'";
        }
        while ($income = tep_db_fetch_array($incomeQuery)) {
            $array[] = $income;

            $incomeQueryDetail = tep_db_query("
                select id, name, account_type_id from account_chart where account_type_id = ". $income['id']."
            ");
            while ($income_detail = tep_db_fetch_array($incomeQueryDetail)) {
                $arrayDetail[] = $income_detail;

                //set conditional depend on type of account has income statement
                // 3 for revenues and 4 for expense 5 for cost of sales
                $sumSub = tep_db_query("
                    SELECT account_chart_id,
                        SUM(
                            CASE
                                WHEN
                                    type_of_account_report = 4 THEN debit - credit
                                WHEN
                                    type_of_account_report = 3 THEN credit - debit
                                WHEN
                                    type_of_account_report = 5 THEN debit - credit
                                ELSE
                                    0
                            END
                        ) as total
                    FROM
                        account_transaction_detail
                    WHERE
                        account_chart_id = '" . (int)$income_detail['id'] . "'
                            AND
                        status = 1 " . $addWhere . "
                ");
                $sumDetail[] = tep_db_fetch_array($sumSub);
            }
            //set conditional depend on type of account has income statement
            // 3 for revenues and 4 for expense 5 for cost of sales
            $sumMain = tep_db_query("
                SELECT account_type_id,
                    SUM(
                        CASE
                            WHEN
                                type_of_account_report = 4 THEN debit - credit
                            WHEN
                                type_of_account_report = 3 THEN credit - debit
                            WHEN
                                type_of_account_report = 5 THEN debit - credit
                            ELSE
                                0
                        END
                    )
                    as total
                FROM
                    account_transaction_detail
                WHERE
                    account_type_id = '" . (int)$income['id'] . "'
                        AND
                    status = 1 " . $addWhere . "
            ");
            $sum[] = tep_db_fetch_array($sumMain);
        }

        return array(data => array(
            'master' => $array,
            'master_detail' => $arrayDetail,
            'total_master' => $sum,
            'total_master_detail' => $sumDetail,
        ));

//        $col=new IncomeStatementCol;
//        $col->filterByIncomeStatementId();
//        $params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date']) : '';
//        return $this->getReturn($col,$params);
    }

}
