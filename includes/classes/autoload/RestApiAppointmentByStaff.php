<?php

use
	OSC\Appointment\Collection
		as appointmentCol
;

class RestApiAppointmentByStaff extends RestApi {

	public function get($params){
		$col = new appointmentCol();
		$staffId = $_SESSION["link_to"];
		if(isset($staffId)){
			$col->sortById("DESC");
			$col->filterByDoctorId($staffId);
			$params['GET']['status'] ? $col->filterByStatus(1) : '';
			$params['GET']['customer_name'] ? $col->filterByCustomerName($params['GET']['customer_name']) : '';
			if($params['GET']['from_date'] && $params['GET']['to_date']){
				$col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']);
			}
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}else{
			return [ 'data' => ['success'=> 'false']];
		}
	}

}
