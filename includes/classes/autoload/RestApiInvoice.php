<?php

use
	OSC\Invoice\Collection as InvoiceCol
	, OSC\Invoice\Object as InvoiceObj
	, OSC\InvoiceDetail\Object as InvoiceDetailObj
	, OSC\Appointment\Object as appointmentObj
	, OSC\AppointmentDetail\Object as appointmentDetailObj
	, OSC\CaseFlow\Object as CashFlowObj
;

class RestApiInvoice extends RestApi {

	public function get($params){
		$col = new InvoiceCol();
		// start limit page
		$col->sortById('DESC');
		$params['GET']['balance'] ? $col->filterByBalance() : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['customer_type_id'] ? $col->filterByCustomerTypeId($params['GET']['customer_type_id']) : '';
		$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
		$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		$params['GET']['responsible_id'] ? $col->filterByResponsibleId($params['GET']['responsible_id']) : '';
		
		$params['GET']['invoice_no'] ? $col->filterByInvoiceNo($params['GET']['invoice_no']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';

		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new InvoiceObj();
		$obj->setProperties($params['POST']['invoice']);
		$milliseconds = substr(round(microtime(true) * 1000), 7);
		/***************
		 * Get Date ****
		 ***************/
		$date = $params['POST']['invoice']["invoice_date"];//date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
		$invoiceDate = date('Y', strtotime($date));
		/**************************
		 * generate invoice no ****
		 **************************/
		// count record sale
		// $query = tep_db_query("
		// 	SELECT COUNT(id) total FROM  invoice WHERE invoice_date = '" . $date . "'
		// ");
		$query = tep_db_query("
			SELECT COUNT(id) total FROM  invoice WHERE YEAR(invoice_date) = YEAR(CURDATE())
		");
		$queryTransaction = tep_db_fetch_array($query);
		$count = (int)$queryTransaction['total'];
		$count < 0 ? $count = 1 : $count = $count + 1;
		if($count < 9999){
			$string = '0000' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -4);
		}else{
			$stringConcat =  (string)$count;
		}
		$invoiceNo = $_SESSION['invoiceNo'] . $invoiceDate . $stringConcat;
		$obj->setInvoiceNo($invoiceNo);

		
		// check if customer code has create
		$customerCode = $params['POST']['invoice']['customer_code'];
		if(!$customerCode){			
			$query = tep_db_query("
				SELECT 
					customers_code
				FROM 
					customers 
				WHERE
					customers_code IS NOT NULL  
						OR 
					customers_code != '' 
						ORDER BY 
					customers_code 
						DESC LIMIT 1
			");
			$res = tep_db_fetch_array($query);
			$getCode = (float)$res["customers_code"];
			$customerCode = $getCode + 1;

			tep_db_query("
				UPDATE
					customers
				SET
					customers_code = '" . $customerCode. "'
				WHERE id = ". (int)$params['POST']['invoice']['customer_id'] ."
			");
		}
		$params['POST']['invoice']['customer_code'] = $customerCode;
		$obj->setCustomerCode($customerCode);
		$obj->insert();
		$invoiceId = $obj->getId();

		// start insert cash flow
		$objCaseFlow = new CashFlowObj();
		$objCaseFlow->setInvoiceNo($invoiceNo);
		$objCaseFlow->setInvoiceId($invoiceId);
		$objCaseFlow->setInvoiceDate($invoiceDate);
		$objCaseFlow->setProperties($params['POST']['cash_flow']);
		$objCaseFlow->insert();

		// start insert data into detail
		foreach( $params['POST']['invoice_detail'] as $key => $value){
			$objDetail = new InvoiceDetailObj();
			$objDetail->setInvoiceId($invoiceId);
			$objDetail->setInvoiceNo($invoiceNo);
			$objDetail->setProperties($value);
			$objDetail->insert();
			unset($value);
		}
		
		return array(
			'data' => array(
				'id' => $invoiceId,
				'success' => 'success',
				'invoice_no' => $invoiceNo,
				'customer_code' => $customerCode,
			)
		);
	}

//	public function put($params){
//		$obj = new InvoiceObj();
//		$obj->setProperties($params['PUT']);
//		$obj->update($this->getId());
//		return array(
//			'data' => array(
//				'id' => $obj->getId(),
//				'success' => 'success'
//			)
//		);
//	}

	public function patch($params){
		$obj = new InvoiceObj();
		$obj->setStatus($params['PATCH']['status']);
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setInvoiceNo($params['PATCH']['invoice_no']);
		$obj->updateStatus();
	}

	public function delete($params){
		$obj = new InvoiceObj();
		// $query = tep_db_query("select invoice_no from invoice where id = ". $this->getId() ." ");
		// $select = tep_db_fetch_array($query);		
		// $obj->setInvoiceNo($select['invoice_no']);
		$obj->setId($this->getId());
		$obj->delete();
	}

}
