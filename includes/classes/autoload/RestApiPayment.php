<?php
use
    OSC\PaymentMaster\Collection as  PaymentCol,
    OSC\PaymentMaster\Object as  PaymentObj,
    OSC\PaymentMasterDetail\Object as  PaymentDetailObj,
    OSC\PurchaseMaster\Object as  PurchaseObj
;

class RestApiPayment extends RestApi{

    public function get($params){
        $col=new PaymentCol;
        $col->sortById('DESC');
        $params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
        $params['GET']['payment_no'] ? $col->filterBypaymentNo($params['GET']['payment_no']) : '';
        $params['GET']['vendor_id'] ? $col->filterByVendorId($params['GET']['vendor_id']) : '';
        $params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
        $params['GET']['status'] ? $col->filterByStatus($params['GET']['status']) : '';
        if($params['GET']['pagination']){
            $showDataPerPage = 10;
            $start = $params['GET']['start'];
            $this->applyLimit($col,
                array(
                    'limit' => array( $start, $showDataPerPage )
                )
            );
        }      
        return $this->getReturn($col,$params);
    }

    public function post($params){
        $obj = new PaymentObj();
        $obj->setCreateBy($_SESSION['user_name']);
        $obj->setProperties($params['POST']['payment']);
        $vendorPaymentNo = $this->getPaymentNo();
        $obj->setPaymentNo($vendorPaymentNo);
        $obj->insert();
        $paymentId = $obj->getId();
        // start insert data into detail
        foreach( $params['POST']['payment_detail'] as $key => $value){
            $objDetail = new PaymentDetailObj(); 
            $objDetail->setPaymentId($paymentId);
            $objDetail->setPaymentNo($vendorPaymentNo);
            $objDetail->setProperties($value);
            $objDetail->insert();

            // update balance
            $objPurchase = new PurchaseObj();
            $objPurchase->setReffNo($value['purchase_no']);
            $totalPayment = $params['POST']['payment']['payment'] + $params['POST']['payment']['discount_total_amount'];
            $objPurchase->setPayment($totalPayment);
            $objPurchase->setUpdateBy($_SESSION['user_name']);
            $objPurchase->update();
            unset($value);
        }

    
        return array(
            'data' => array(
                'id' => $obj->getId(),
                'success' => 'success'
            )
        );
    }

    public function put($params){
        $obj = new PurchaseObj();
        $this->setId($this->getId());
        $obj->setProperties($params['PUT']);
        $obj->update($this->getId());
        return array(
            'data' => array(
                'id' => $obj->getId(),
                'success' => 'success'
            )
        );
    }

    public function delete(){
        $obj = new PurchaseObj();
        $obj->delete($this->getId());
    }

    public function patch($params){
		if ($_SESSION["id"]) {
			$obj = new PaymentObj();
			$obj->setId($this->getId());
			$obj->setCreateBy($_SESSION["user_name"]);
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus();
			// start to update balance customer
			tep_db_query("
				UPDATE
					purchase_master
				SET
					remain = remain + '" . $params['PATCH']['payment'] . "',
					update_by = '" . $_SESSION["user_name"] . "'
				WHERE
					reff_no = '" . $params['PATCH']['purchase_no'] . "'
						and
					supplier_id = '" . $params['PATCH']['vendor_id'] . "'
			");
			
		}else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
	}

    public function getPaymentNo(){
        /***************
        * Get Date ****
        ***************/
        $date = date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
        $invoiceDate = date('dmY', strtotime($date));
        /**************************
        * generate invoice no ****
        **************************/
        // count number unit today in stock
        $query = tep_db_query("
            SELECT  COUNT(id) total FROM  payment_master WHERE payment_date >= '" . $date . "'
        ");
        $transaction = tep_db_fetch_array($query);
        $count = (int)$transaction['total'];
        $count < 0 ? $count = 1 : $count = $count + 1;
        $string = '000' . (string)$count;
        // sub string with 4digit
        $stringConcat =  substr($string, -4);
        $invoiceNo = 'VP-' . $invoiceDate . '-' . $stringConcat;
        return $invoiceNo;   
    }
}
