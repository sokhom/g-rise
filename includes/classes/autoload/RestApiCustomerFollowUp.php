<?php

use
	OSC\CustomerFollowUp\Collection
		as CustomerFollowUpCol
	, OSC\CustomerFollowUp\Object
		as CustomerFollowUpObj
;

class RestApiCustomerFollowUp extends RestApi {

	public function get($params){
		$col = new CustomerFollowUpCol();
		// $col->sortByName("ASC");
		// $params['GET']['status'] ? $col->filterByStatus(1) : '';
		// $params['GET']['name'] ? $col->filterByName($params['GET']['name']) : '';
		// if($params['GET']['paginate']){
		// 	$showDataPerPage = 10;
		// 	$start = $params['GET']['start'];
		// 	$this->applyLimit($col,
		// 		array(
		// 			'limit' => array( $start, $showDataPerPage )
		// 		)
		// 	);
		// }
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new CustomerFollowUpObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	// public function put($params){
	// 	// Validate customer with duplicate code
	// 	$query = tep_db_query("
	// 		SELECT
	// 			id
	// 		FROM
	// 			customer_type
	// 		WHERE
	// 			name = '" . $params['PUT']['name'] . "'
	// 				and
	// 			id != ". $this->getId() ."
	// 	");
	// 	$countQuery = tep_db_num_rows($query);
		
	// 	if($countQuery > 0){
	// 		echo 'error';
	// 		return;
	// 	}
	// 	$obj = new CustomerTypeObj();
	// 	$this->setId($this->getId());
	// 	$obj->setProperties($params['PUT']);
	// 	$obj->update($this->getId());
	// 	return array(
	// 		'data' => array(
	// 			'id' => $obj->getId(),
	// 			'success' => 'success'
	// 		)
	// 	);
	// }

	// public function patch($params){
	// 	$obj = new CustomerTypeObj();
	// 	$obj->setId($this->getId());
	// 	$obj->setUpdateBy($_SESSION['user_name']);
	// 	$obj->setStatus($params['PATCH']['status']);
	// 	$obj->updateStatus();
	// }

	public function delete(){
		$obj = new CustomerFollowUpObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
