<?php

use
	OSC\Administrator\Collection
		as UserCol
	, OSC\Administrator\Object
	as UserObj
;

class RestApiUserUpdatePassword extends RestApi {

	public function put($params){
		if($_SESSION["id"]) {
			$obj = new UserObj();
			$obj->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			$params['PUT']['user_password'] = tep_encrypt_password($params['PUT']['user_password']);
			$obj->setProperties($params['PUT']);
			$obj->updatePassword();
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
				)
			);
		}
	}

}
