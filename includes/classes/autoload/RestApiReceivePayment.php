<?php

use
	OSC\ReceivePayment\Collection as ReceivePaymentCol
	, OSC\ReceivePayment\Object as ReceivePaymentObj
	, OSC\ReceivePaymentDetail\Object as ReceivePaymentDetailObj
	, OSC\Invoice\Object as InvoiceObj
	, OSC\CaseFlow\Object as CaseFlowObj
;

class RestApiReceivePayment extends RestApi {

	public function get($params){
		$col = new ReceivePaymentCol();
		// start limit page
		$col->sortById('DESC');
		$params['GET']['balance'] ? $col->filterByBalance() : '';
		$params['GET']['payment_no'] ? $col->filterByPaymentNo($params['GET']['payment_no']) : '';
		$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
		$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		$params['GET']['status'] ? $col->filterByStatus($params['GET']['status']) : '';
		if ($params['GET']['pagination']) {
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
		
	}

	public function post($params){
		$obj = new ReceivePaymentObj();
		
		$obj->setProperties($params['POST']['receive_payment']);

		/***************
		 * Get Date ****
		 ***************/
		$date = $params['POST']['receive_payment']['receive_payment_date'];//date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
		$invoiceDate = date('Y', strtotime($date));
		/**************************
		 * generate invoice no ****
		 **************************/
		// count data in year
		$query = tep_db_query("
			SELECT COUNT(id) total FROM  receive_payment WHERE YEAR(receive_payment_date) = YEAR(CURDATE())
		");
		// $query = tep_db_query("
		// 	SELECT COUNT(id) total FROM  receive_payment WHERE receive_payment_date >= '" . $date . "'
		// ");
		$stockTransaction = tep_db_fetch_array($query);
		$count = (int)$stockTransaction['total'];
		$count < 0 ? $count = 1 : $count = $count + 1;
		if($count < 9999){
			$string = '0000' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -4);
		}else{
			$stringConcat =  (string)$count;
		}
		$invoiceNo = $_SESSION['receiptNo'] . $invoiceDate . $stringConcat;
		$obj->setReceivePaymentNo($invoiceNo);
		$obj->insert();		


		$payment = $params['POST']['receive_payment']['payment'] +
				$params['POST']['receive_payment']['discount_total_amount'];

		$receivePaymentId = $obj->getId();

		// start insert cash flow
		$objCaseFlow = new CaseFlowObj();
		$objCaseFlow->setInvoiceId($receivePaymentId);
		$objCaseFlow->setInvoiceNo($invoiceNo);
		$objCaseFlow->setInvoiceDate($invoiceDate);
		$objCaseFlow->setProperties($params['POST']['cash_flow']);
		$objCaseFlow->insert();
		
		// start insert data into detail
		foreach( $params['POST']['receive_payment_detail'] as $key => $value){
			$objDetail = new ReceivePaymentDetailObj();
			$objDetail->setReceivePaymentId($receivePaymentId);
			$objDetail->setProperties($value);
			$objDetail->insert();

			// update balance in invoice
			$objInvoice = new InvoiceObj();
			$objInvoice->setInvoiceNo($value['invoice_no']);
			$objInvoice->setDeposit($payment);
			$objInvoice->setOverdue($date);
			$objInvoice->setCustomerId($value['customer_id']);
			$objInvoice->updateBalance();

			unset($value);
		}

		return array(
			'data' => array(
				'id' => $receivePaymentId,
				'invoice_no' => $invoiceNo,
				'success' => 'success',
					'date' => $obj->getReceivePaymentDate()
			)
		);
	}

//	public function put($params){
//		$obj = new InvoiceObj();
//		$obj->setProperties($params['PUT']);
//		$obj->update($this->getId());
//		return array(
//			'data' => array(
//				'id' => $obj->getId(),
//				'success' => 'success'
//			)
//		);
//	}
//
	public function patch($params){
		$obj = new ReceivePaymentObj();
		$obj->setId($this->getId());
		$obj->setCreateBy($_SESSION["user_name"]);
		$obj->setStatus($params['PATCH']['status']);
		$obj->setReceivePaymentNo($params['PATCH']['receive_payment_no']);
		$obj->updateStatus();
		
		// update invoice balance 
		$objInvoice = new InvoiceObj();
		$objInvoice->setInvoiceNo($params['PATCH']['invoice_no']);
		$objInvoice->setBalance($params['PATCH']['payment']);
		$objInvoice->setDeposit($params['PATCH']['payment']);
		$objInvoice->setCustomerId($params['PATCH']['customer_id']);
		$objInvoice->setOverdue($params['PATCH']['last_overdue']);
		$objInvoice->updateBalanceWhenVoidFromReceivePayment(); // start to update balance customer
		echo "save successful";
	}

	public function delete($params){
		$query = tep_db_query("
			select 
				r.receive_payment_no,
				r.discount_total_amount,
				r.customer_id,
				r.payment,
				r.last_overdue,
				rd.invoice_no
			from 
				receive_payment r inner join receive_payment_detail rd on r.id = rd.receive_payment_id
			where 
				r.id = ". $this->getId() ." 
		");
		$data = tep_db_fetch_array($query);
		$obj = new ReceivePaymentObj();
		$obj->setId($this->getId());
		$obj->setReceivePaymentNo($data['receive_payment_no']);
		$obj->delete();

		// update invoice balance 
		$objInvoice = new InvoiceObj();
		$objInvoice->setInvoiceNo($data['invoice_no']);
		$objInvoice->setDeposit( doubleval($data['payment']) + doubleval($data['discount_total_amount']) );
		$objInvoice->setCustomerId($data['customer_id']);
		$objInvoice->setOverdue($data['last_overdue']);
		$objInvoice->updateBalanceWhenVoidFromReceivePayment(); // start to update balance customer
		echo "save successful";
	}


}
