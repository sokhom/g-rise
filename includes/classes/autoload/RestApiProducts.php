<?php

use
	OSC\Products\Collection
		as ProductCol
	, OSC\Products\Object
		as ProductObj
	, OSC\StockTransaction\Object
		as StockTransactionObj
;

class RestApiProducts extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new ProductCol();
			$id = $params['GET']['id'];
			//$col->sortByDate("DESC");
			$col->filterById($id);
			$params['GET']['stock_alert'] ? $col->filterByStockAlert() : '';
			$params['GET']['kind_of'] ? $col->filterByKindOf($params['GET']['kind_of']) : '';
			if( $params['GET']['status'] == 1 ){
				$col->filterByStatus(1);
			}
			if( $params['GET']['status'] == 0 && $params['GET']['status'] != ''){
				$col->filterByStatus(0);
			}
			$params['GET']['type'] ? $col->filterByType($params['GET']['type']) : '';
			$params['GET']['barcode'] ? $col->filterByBarcode($params['GET']['barcode']) : '';
			$col->filterByName($params['GET']['name']);
			$params['GET']['is_sale'] ? $col->filterByUseFilter($params['GET']['is_sale']) : '';
			$col->sortByName("ASC");
			// start limit page
			if($params['GET']['pagination']){
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array( $start, $showDataPerPage )
					)
				);
			}
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function post($params){
		if($_SESSION["id"]) {
			// start filter product barcode
			if($params['POST']['barcode']){
				$query = tep_db_query("
					SELECT
						barcode
					FROM
						products
					WHERE
						barcode = '" . $params['POST']['barcode'] . "'
				");
				$countProductBarcode = tep_db_num_rows($query);
				if($countProductBarcode > 0){
					return array(
						'data' => array(
							'error' => 'Duplicate barcode.'
						)
					);
				}
			}
			$productObject = new ProductObj();
			$productObject->setCreateBy($_SESSION['user_name']);
			$productObject->setProperties($params['POST']);
			$productObject->insert();

			// insert into stock transaction
			$objPro = new StockTransactionObj();

			$objPro->setProductName($params['POST']['products_name']);
			$objPro->setProductId($productObject->getId());
			$objPro->setProductTypeId($params['POST']['products_type_id']);
			$objPro->setTransactionDate(date("Y/m/d"));
			$objPro->setProductKindOf($params['POST']['products_kind_of']);
			$objPro->setProductTypeName($params['POST']['products_type_name']);
			$objPro->setProductDescription($params['POST']['products_description']);
			$objPro->setBarcode($params['POST']['barcode']);
			$objPro->setQtyOnHand($params['POST']['products_quantity']);
			$objPro->setCost($params['POST']['products_price_in']);
			$objPro->setWholeSalePrice($params['POST']['products_price_out_whole_sale']);
			$objPro->setRetailPrice($params['POST']['products_price_out_retail']);
			$objPro->setUmType($params['POST']['um_detail']['name']);
			$objPro->setUmTypeAmount($params['POST']['um_detail']['amount']);
			$objPro->setUmTypeRetail($params['POST']['um_sale_detail']['name']);
			$objPro->setUmTypeRetailAmount($params['POST']['um_sale_detail']['amount']);
			$objPro->setCreateBy($_SESSION['user_name']);
			$objPro->insert();

			return array(
				'data' => array(
					'id' => $productObject->getId()
				)
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function put($params){
		if($_SESSION["id"]){
			if($params['PUT']['barcode']) {
				// start filter product barcode
				$query = tep_db_query("
					SELECT
						barcode
					FROM
						products
					WHERE
						barcode = '" . $params['PUT']['barcode'] . "'
							AND
						id != '" . (int)$this->getId() . "'
				");
				$countProductBarcode = tep_db_num_rows($query);

				if($countProductBarcode >= 1){
					echo 'Duplicate';
					return;
				}
			}
			$obj = new ProductObj();
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setProperties($params['PUT']);
			$obj->update($this->getId());
			echo 'update success.';
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

//	public function delete(){
//		$obj = new ProductObj();
//		$obj->delete($this->getId());
//		return array(
//			'data' => array(
//				'data' => 'success'
//			)
//		);
//	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new ProductObj();
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus($this->getId());
			// let update status in stock transaction
			tep_db_query("
				UPDATE
					stock_transaction
				SET
					status = " . (int)$params['PATCH']['status'] . "
				WHERE
					product_id = " . (int)$this->getId() . "
			");
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

}

