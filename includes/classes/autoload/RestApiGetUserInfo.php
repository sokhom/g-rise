<?php

use
    OSC\UserInfo\Collection
    as UserCol;

class RestApiGetUserInfo extends RestApi
{

    public function get($params)
    {
        $col = new UserCol();
        $col->filterById($_SESSION['id']);
        $this->applyFilters($col, $params);
        $this->applySortBy($col, $params);
        return $this->getReturn($col, $params);
    }


}
