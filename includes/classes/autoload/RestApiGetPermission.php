<?php

use OSC\Permission\Collection as PermissionCol;

class RestApiGetPermission extends RestApi {

	public function get($params){
		if($_SESSION["id"] && $_SESSION["role_id"]) {
			$col = new PermissionCol();
			// $array = ["Branch_Create", "Branch_View", "Branch_Delete"];

			$array = json_decode(stripslashes($params['GET']['data']));
			$list =  implode("','", $array);
			//var_dump($list);
			// var_dump($params['GET']);var_dump($array);exit;
			$col->filterByRoleFeature($list);
			$col->filterByRoleId($_SESSION["role_id"]);
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

}
