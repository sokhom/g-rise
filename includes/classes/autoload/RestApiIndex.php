<?php

class RestApiIndex extends RestApi {

	public function get(){
		$package_query = tep_db_query("select count(id) as total from packages where status = 1");
		$option_query = tep_db_query("select count(id) as total from options where status = 1");
		$package = tep_db_fetch_array($package_query);
		$option = tep_db_fetch_array($option_query);

		// count customer
		$customer_query = tep_db_query("select count(id) as total from customers where status = 1");
		$customer = tep_db_fetch_array($customer_query);
		// count supplier
		$supplier_query = tep_db_query("select count(id) as total from supplier_list where status = 1");
		$supplier = tep_db_fetch_array($supplier_query);
		// count staff
		$staff_query = tep_db_query("select count(id) as total from doctor_list where status = 1");
		$staff = tep_db_fetch_array($staff_query);

		$sum_balance_customer = tep_db_query("
			SELECT
				SUM(remain) as total
			FROM
				stock_out
			WHERE
				status = 1 and remain > 0
		");
		$total_balance_cus = tep_db_fetch_array($sum_balance_customer);

		$sum_balance_vendor = tep_db_query("
			SELECT
				SUM(remain) as total
			FROM
				purchase_master
			WHERE
				status = 1 and remain > 0
		");
		$total_balance_vendor = tep_db_fetch_array($sum_balance_vendor);

		return array( data =>
			array(
				package_total => (int)$package['total'],
				option_total => (int)$option['total'],
				customer_total => (int)$customer['total'],
				customer_balance => doubleval($total_balance_cus['total']),
				vendor_balance => doubleval($total_balance_vendor['total']),				
				staff_total => doubleval($staff['total']),				
				supplier_total => doubleval($supplier['total'])
			)
		);
	}
}
