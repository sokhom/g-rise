<?php

use
	OSC\AdjustmentStock\Collection
		as AdjustmentStockCol
	, OSC\AdjustmentStock\Object
		as AdjustmentStockObj
	, OSC\AdjustmentStockDetail\Object
		as AdjustmentStockDetailObj
	, OSC\Products\Object 
		as productObj
;

class RestApiAdjustmentStock extends RestApi {

	public function get($params){
		$col = new AdjustmentStockCol();
		// start limit page
		$col->sortById('DESC');
		$params['GET']['name'] ? $col->filterByName($params['GET']['name']) : '';
		$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		$params['GET']['filter'] ? $col->filterByMulti($params['GET']['filter']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new AdjustmentStockObj();
		$obj->setCreateBy($_SESSION['user_name']);
		$obj->setProperties($params['POST']['model']);
		$milliseconds = substr(round(microtime(true) * 1000), 7);
		/***************
		* Get Date ****
		***************/
		$date =$params['POST']['model']["adjustment_date"];
		$transferDate = date('dmY', strtotime($date));
		/**************************
		* generate adjustmentNo ****
		**************************/
		// count number unit today in stock
		$query = tep_db_query("
			SELECT  COUNT(id) total FROM  adjustment_stock WHERE adjustment_date = '" . $date . "'
		");
		$countTotal = tep_db_fetch_array($query);
		$count = (int)$countTotal['total'];
		$count < 0 ? $count = 1 : $count = $count + 1;
		$string = '000' . (string)$count;
		// sub string with 4digit
		$stringConcat =  substr($string, -4);
		$transactionNo = 'AJS-' . $transferDate . '-' . $stringConcat;
		$obj->setAdjustmentNo($transactionNo);
		$obj->insert();
		$id = $obj->getId();
		foreach( $params['POST']['model_detail'] as $key => $value){
			$objDetail = new AdjustmentStockDetailObj(); 
			$objDetail->setAdjustmentId($id);
			$objDetail->setProperties($value);
			$objDetail->insert();
			// update product stock qty
			$objPro = new productObj();
			$qty = $value['adjust_qty'] * $value['um_type_amount'] / $value['product_um_type_amount'];
			$objPro->updateStockFromTransferWhenSave($value['product_id'], $qty);

			unset($objPro);
			unset($objDetail);
		}
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new AdjustmentStockObj();
		$obj->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setStatus($params['PATCH']['status']);
		
		//update stock reverse from adjust
		foreach ($params['PATCH']['detail'] as $key => $value) {
			$objPro = new productObj();
			$qty = $value['adjust_qty'] * $value['um_type_amount'] / $value['product_um_type_amount'];
			$objPro->updateStockFromTransferWhenVoid($value['product_id'], $qty);
		}
		$obj->updateStatus();
		echo 'successful.';
	}

}
