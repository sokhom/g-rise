<?php

use
	// return sale
	OSC\StockOutReturn\Collection as StockOutReturnCol
	, OSC\StockOutReturn\Object as StockOutReturnObj

	// return sale detail
	, OSC\StockOutReturnDetail\Object as StockOutReturnDetailObj

	// stock out
	, OSC\StockOut\Object as StockOutObj
	, OSC\StockOutDetail\Object as StockOuDetailObj

	, OSC\Products\Object as productObj

	, OSC\StockTransaction\Object as StockTransactionObj
	, OSC\CaseFlow\Object as CaseFlowObj
;

class RestApiStockOutReturn extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new StockOutReturnCol();
			$col->sortById("DESC");
			$params['GET']['status'] ? $col->filterByStatus($params['GET']['status']) : '';
			$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
			$params['GET']['remain'] ? $col->filterByRemain() : '';
			$params['GET']['overdue'] ? $col->filterByOverdue(30) : '';
			$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
			$params['GET']['invoice_no'] ? $col->filterByInvoice($params['GET']['invoice_no']) : '';
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			// start limit page
			if($params['GET']['pagination']){
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array( $start, $showDataPerPage )
					)
				);
			}
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function post($params){
		if($_SESSION["id"]){
			$obj = new StockOutReturnObj();
			$obj->setCreateBy($_SESSION['user_name']);
			$obj->setProperties($params['POST']);
			//$obj->setOverdue($params['POST']['stock']['stock_out_date']);
			// $milliseconds = substr(round(microtime(true) * 1000), 7);
			/***************
			 * Get Date ****
			 ***************/
			// $date = $params['POST']['stock']["stock_out_date"];//date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
			// $invoiceDate = date('dmY', strtotime($date));
			/**************************
			 * generate invoice no ****
			 **************************/
			// count number unit today in stock
			// $query = tep_db_query("
			// 	SELECT  COUNT(id) total FROM  stock_out WHERE stock_out_date = '" . $date . "'
			// ");
			// $stockTransaction = tep_db_fetch_array($query);
			// $count = (int)$stockTransaction['total'];
			// $count < 0 ? $count = 1 : $count = $count + 1;
			// $string = '000' . (string)$count;
			// // sub string with 4digit
			// $stringConcat =  substr($string, -4);
			// $invoiceNo = 'INV-' . $invoiceDate . '-' . $stringConcat;
			// $obj->setStockOutNo($invoiceNo);
			$obj->insert();
			$returnId = $obj->getId();

			// update balance and sub total of item
			$stockOutObj = new StockOutObj();
			$stockOutObj->setGrandTotal($params['POST']['grand_total']);
			$stockOutObj->setSubTotal($params['POST']['sub_total']);
			$stockOutObj->setRemain($params['POST']['remain']);
			$stockOutObj->setStockOutNo($params['POST']['stock_out_no']);
			$stockOutObj->setDiscountTotalAmount($params['POST']['discount_total_amount']);
			$stockOutObj->setCustomerId($params['POST']['customer_id']);
			$stockOutObj->updateBalanceAndTotalOfItem();

			// start insert doctor case flow
			// $objCaseFlow = new CaseFlowObj();
			// $objCaseFlow->setStatus(1);
			// $objCaseFlow->setInvoiceNo($invoiceNo);
			// $objCaseFlow->setInvoiceDate($params['POST']['stock']['stock_out_date']);
			// $objCaseFlow->setCustomerId($params['POST']['stock']['customer_id']);
			// $objCaseFlow->setCustomerName($params['POST']['stock']['customer_name']);
			// $objCaseFlow->setDoctorId($params['POST']['stock']['doctor_id']);
			// $objCaseFlow->setDoctorName($params['POST']['stock']['doctor_name']);
			// $objCaseFlow->setPayment($params['POST']['stock']['payment']);
			// $objCaseFlow->setPaymentMethod($params['POST']['stock']['payment_method']);
			// $objCaseFlow->setBankCharge($params['POST']['stock']['bank_charge']);
			// $objCaseFlow->setCreateBy($_SESSION['user_name']);
			// $objCaseFlow->insert();

			foreach( $params['POST']['detail'] as $key => $value){
				$objStockOut = new StockOutReturnDetailObj();
				$objStockOut->setProperties($value);
				$objStockOut->setStockOutNo($params['POST']['stock_out_no']);
				$objStockOut->setStockOutReturnId($returnId);
				$objStockOut->insert();

				//update sub item of sale stock
				$saleOutDetail = new StockOuDetailObj();
				$saleOutDetail->setQty($value['qty'] + $value['add_qty']);
				$saleOutDetail->setId($value['id']);
				$saleOutDetail->setRemark($value['remark']);
				$saleOutDetail->setTotal($value['total']);
				$saleOutDetail->setPrice($value['price']);
				$saleOutDetail->setProductName($value['product_name']);
				$saleOutDetail->setDiscountCash($value['discount_cash']);
				$saleOutDetail->setDiscountPercent($value['discount_percent']);
				$saleOutDetail->setAddMorePrice($value['add_more_price']);
				$saleOutDetail->updateItem();

				// insert into stock transaction
				// $objStockTransaction = new StockTransactionObj();
				// $objStockTransaction->setReferenceId($invoiceNo);
				// $objStockTransaction->setProductName($value['product_name']);
				// $objStockTransaction->setProductKindOf($value['products_kind_of']);
				// $objStockTransaction->setProductId($value['product_id']);
				// $objStockTransaction->setProductTypeId($value['product_type_id']);
				// $objStockTransaction->setProductDescription($value['description']);
				// $objStockTransaction->setBarcode($value['barcode']);
				// $objStockTransaction->setQtyOnHand($value['qty_on_hand']);
				// $objStockTransaction->setCost($value['cost']);
				// //$objStockTransaction->setPrice($value['unit_price']);
				// $objStockTransaction->setRetailPrice($value['retail_price']);
				// $objStockTransaction->setWholeSalePrice($value['whole_sale_price']);

				// $objStockTransaction->setUmType($value['um_type_whole_sale_name']);
				// $objStockTransaction->setUmTypeAmount($value['um_type_whole_sale_amount']);
				// $objStockTransaction->setUmTypeRetail($value['um_type_retail_name']);
				// $objStockTransaction->setUmTypeRetailAmount($value['um_type_retail_amount']);

				// $objStockTransaction->setStockOut($value['qty']);
				// $objStockTransaction->insert();

				// // update stock from product list
				// if($value['products_kind_of'] == 'item'){
				// 	$objStockTransaction = new productObj();
				// 	$objStockTransaction->setProductsQuantity($value['qty']);
				// 	// get amount UM
				// 	if($value['type'] == "whole_sale"){
				// 		$umAmount = $value['um_type_whole_sale_amount'];
				// 	}else{
				// 		$umAmount = $value['um_type_retail_amount'];
				// 	}
				// 	$objStockTransaction->updateStockOutWithUM($value['product_id'], $value['qty'], $umAmount);
				// }
				unset($value);
			}
			return array( data => array(
				id => $returnId,
				invoice_no => $invoiceNo
			));
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}

	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new StockOutReturnObj();
			$status = (int)$params['PATCH']['status'];
			$obj->setStatus($status);
			$obj->setId($this->getId());
			$stockOutNo = $params['PATCH']['stock_out_no'];
			$obj->setStockOutNo($stockOutNo);
			$obj->updateStatus();

			// start update sub totoal of sale stock out back
			$stockOutObj = new StockOutObj();
			$stockOutObj->setGrandTotal($params['PATCH']['grand_total']);
			$stockOutObj->setSubTotal($params['PATCH']['sub_total']);
			$stockOutObj->setRemain($params['PATCH']['remain']);
			$stockOutObj->setDiscountTotalAmount($params['PATCH']['discount_total_amount']);
			$stockOutObj->setStockOutNo($params['PATCH']['stock_out_no']);
			$stockOutObj->setCustomerId($params['PATCH']['customer_id']);
			$stockOutObj->updateBalanceAndTotalOfItemAfterHasVoid();

			$saleOutDetail = new StockOuDetailObj();
			foreach ($params['PATCH']['detail'] as $key => $value) {
				//update sub item of sale stock
				$saleOutDetail->setQty($value['qty']);
				$saleOutDetail->setProductId($value['product_id']);
				$saleOutDetail->setStockOutNo($stockOutNo);
				$saleOutDetail->setTotal($value['total_before']);
				$saleOutDetail->setPrice($value['price_before']);
				$saleOutDetail->setProductName($value['product_name_before']);
				$saleOutDetail->setDiscountCash($value['discount_cash_before']);
				$saleOutDetail->setDiscountPercent($value['discount_percent_before']);
				// $saleOutDetail->setRemark($value['remark']);
				// $saleOutDetail->setAddMorePrice($value['add_more_price']);
				$saleOutDetail->updateItemBackAfterVoid();

				// if($value['type'] == "whole_sale" && $value['product_kind_of'] == 'item'){
				// 	$qtyAmount = ($value['qty'] * $value['um_type_whole_sale_amount']) / $value['um_type_whole_sale_amount'];
				// 	tep_db_query("
				// 		UPDATE
				// 			products
				// 		SET
				// 			products_quantity = products_quantity + " . $qtyAmount . "
				// 		WHERE
				// 			id = " . (int)$value['product_id'] . "
				// 	");
				// }
				// else{
				// 	$qtyAmount = ($value['qty'] * $value['um_type_retail_amount']) / $value['um_type_whole_sale_amount'];
				// }

				// if($value['product_kind_of'] == 'item'){
				// 	tep_db_query("
				// 		UPDATE
				// 			products
				// 		SET
				// 			products_quantity = products_quantity + " . $qtyAmount . "
				// 		WHERE
				// 			id = " . (int)$value['product_id'] . "
				// 	");
				// }
			}

			// update stock transaction status
			// tep_db_query("
			// 	UPDATE
			// 		stock_transaction
			// 	SET
			// 		status = " . $status . "
			// 	WHERE
			// 		reference_id = '" . $params['PATCH']['stock_out_no'] . "'
			// ");
			// update status in stock out detail

			// tep_db_query("
			// 	UPDATE
			// 		stock_out_detail
			// 	SET
			// 		status = " . $status . "
			// 	WHERE
			// 		stock_out_no = '" . $params['PATCH']['stock_out_no'] . "'
			// ");
			echo 'udate succesful';
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}
}
