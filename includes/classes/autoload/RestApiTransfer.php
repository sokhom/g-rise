<?php
use
    OSC\Transfer\Collection as  TransferCol,
    OSC\Transfer\Object as  TransferObj,
    OSC\TransferDetail\Object as  TransferDetailObj,
    OSC\Products\Object as productObj,
    OSC\StockTransaction\Object as StockTransactionObj
;

class RestApiTransfer extends RestApi{

    public function get($params){
        if ($_SESSION["id"]) {
            $col = new TransferCol;
            $col->sortById('DESC');
            $params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
            $params['GET']['branch_id'] ? $col->filterByBranchId($params['GET']['branch_id']) : '';
            $params['GET']['transfer_type_id'] ? $col->filterByTransferTypeId($params['GET']['transfer_type_id']) : '';
            $params['GET']['reference_no'] ? $col->filterByTransferNo($params['GET']['reference_no']) : '';
            $params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
            $params['GET']['status'] ? $col->filterByStatus(1) : '';
            if ($params['GET']['pagination']) {
                $showDataPerPage = 10;
                $start = $params['GET']['start'];
                $this->applyLimit($col,
                    array(
                        'limit' => array($start, $showDataPerPage)
                    )
                );
            }
            return $this->getReturn($col, $params);
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    public function post($params){
        if ($_SESSION["id"]) {
            $obj = new TransferObj();
            $obj->setProperties($params['POST']['transfer']);

            $milliseconds = substr(round(microtime(true) * 1000), 7);
			/***************
			 * Get Date ****
			 ***************/
            $date = $params['POST']['transfer']["transfer_date"];
			//$date = date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
			$transferDate = date('dmy', strtotime($date));
			/**************************
			 * generate transfer no ****
			 **************************/
			// count number unit today in stock
			$query = tep_db_query("
				SELECT  COUNT(id) total FROM  transfer_master WHERE transfer_date = '" . $date . "'
			");
			$stockTransaction = tep_db_fetch_array($query);
			$count = (int)$stockTransaction['total'];
			$count < 0 ? $count = 1 : $count = $count + 1;
			$string = '0' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -2);
			$transferNo = 'PTR-' . $transferDate . '-' . $stringConcat;
			$obj->setTransferNo($transferNo);
            $obj->insert();
            $transferId = $obj->getId();
            // start insert data into detail
            foreach( $params['POST']['transfer_detail'] as $key => $value){
                $objDetail = new TransferDetailObj();
                $objDetail->setTransferId($transferId);
                $objDetail->setProductId($value['product_id']);
                $objDetail->setProductName($value['product_name']);
                $objDetail->setProductBarcode($value['barcode']);
                $objDetail->setQty($value['qty']);
                $objDetail->setProductTypeName($value['product_type']);
                $objDetail->setProductTypeId($value['product_type_id']);
                $objDetail->setBalanceQty($value['balance_qty']);
                $objDetail->setQtyOnHand($value['qty_on_hand']);
                $objDetail->setPrice($value['price_out']);
                $objDetail->setCost($value['price_in']);
                $objDetail->setProductUmTypeName($value['product_um_type_name']);
                $objDetail->setProductUmTypeId($value['product_um_type_id']);
                $objDetail->setProductUmTypeAmount($value['product_um_type_amount']);
                $objDetail->setUmTypeAmount($value['um_type_amount']);
                $objDetail->setUmTypeId($value['um_type_id']);
                $objDetail->setUmTypeName($value['um_type_name']);
                $objDetail->insert();

                // update stock and average cost in product
                $objPro = new productObj();
                $objPro->updateStockOutWithUM($value['product_id'], $value['qty'], $value['um_type_amount']);
                //$objPro->updateStockFromTransferWhenSave($value['product_id'], $value['qty']);

                // insert into stock transaction
                $objStockTrans = new StockTransactionObj();
                // $objStockTrans->setReferenceId($transferNo);
                // $objStockTrans->setProductName($value['product_name']);
                // $objStockTrans->setProductId($value['product_id']);
                // $objStockTrans->setProductKindOf('item');
                // $objStockTrans->setStockIn($value['qty']);
                // $objStockTrans->setProductTypeName($value['product_type']);
                // $objStockTrans->setProductTypeId($value['product_type_id']);
                // $objStockTrans->setProductDescription($value['description']);
                // $objStockTrans->setBarcode($value['barcode']);
                // $objStockTrans->setQtyOnHand($value['qty_on_hand']);
                // $objStockTrans->setCost($value['price_in']);
               // $objStockTrans->setCost( $objPro->getProductsPriceIn() );
                //$objStockTrans->setPrice($value['price_out']);
                // $objStockTrans->setCreateBy($_SESSION['user_name']);
                // $objStockTrans->setStockOut($value['qty']);
                // $objStockTrans->insert();

                $objStockTrans->setReferenceId($transferNo);
                $objStockTrans->setProductName($value['product_name']);
                $objStockTrans->setProductKindOf('item');
                $objStockTrans->setProductId($value['product_id']);
                $objStockTrans->setProductTypeName($value['product_type']);
                $objStockTrans->setTransactionDate($date);
                $objStockTrans->setProductTypeId($value['product_type_id']);
                $objStockTrans->setProductDescription($value['description']);
                $objStockTrans->setBarcode($value['barcode']);
                $objStockTrans->setQtyOnHand($value['qty_on_hand']);
                $objStockTrans->setCost( $value['price_in'] );
                $objStockTrans->setRetailPrice($value['price_out']);
                $objStockTrans->setWholeSalePrice($value['price_out']);
                $objStockTrans->setStockOut($value['qty']);
                $objStockTrans->setUmType($value['um_type_name']);
                $objStockTrans->setUmTypeAmount($value['um_type_amount']);
                $objStockTrans->setCreateBy($_SESSION['user_name']);
                $objStockTrans->insert();

                unset($value);
                unset($objStockTrans);
                unset($objPro);
                unset($objDetail);
            }
            return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'transfer_no' => $obj->getTransferNo(),
                    'success' => 'success'
                )
            );
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

    // public function put($params){
    //     if ($_SESSION["id"]) {
    //         $obj = new PurchaseObj();
    //         $this->setId($this->getId());
    //         $obj->setProperties($params['PUT']);
    //         $obj->update($this->getId());
    //         return array(
    //             'data' => array(
    //                 'id' => $obj->getId(),
    //                 'success' => 'success'
    //             )
    //         );
    //     }
    // }

    // public function delete(){
    //     if ($_SESSION["id"]) {
    //         $obj = new PurchaseObj();
    //         $obj->delete($this->getId());
    //     }
    // }

    public function patch($params){
        if ($_SESSION["id"]) {
            $obj = new TransferObj();
            $status = (int)$params['PATCH']['status'];
            $obj->setStatus($status);

            if($status != 0) return;
            // update stock in product
            foreach ($params['PATCH']['transfer_detail'] as $key => $value) {
                // $objPro = new productObj();
                // $objPro->updateStockFromTransferWhenVoid($value['product_id'], $value['qty']);

                $qtyAmount = ($value['qty'] * $value['um_type_amount']) / $value['product_um_type_amount'];                
                tep_db_query("
                    UPDATE
                        products
                    SET
                        products_quantity = products_quantity + " . $qtyAmount . "
                    WHERE
                        id = " . (int)$value['product_id'] . "
                ");
            }
            

            // if ($status > 0) {
            //     foreach ($params['PATCH']['transfer_detail'] as $key => $value) {
            //         tep_db_query("
            //             UPDATE
            //                 products
            //             SET
            //                 products_quantity = products_quantity + " . (int)$value['qty'] . "
            //             WHERE
            //                 id = " . (int)$value['product_id'] . "
            //         ");
            //     }
            // } else {
            //     foreach ($params['PATCH']['transfer_detail'] as $key => $value) {
            //         tep_db_query("
            //             UPDATE
            //                 products
            //             SET
            //                 products_quantity = products_quantity + " . (int)$value['qty'] . "
            //             WHERE
            //                 id = " . (int)$value['product_id'] . "
            //         ");
            //     }
            // }
            // update stock transaction status
            $objStockTrans = new StockTransactionObj();
            $objStockTrans->setStatus($status);
            $objStockTrans->setReferenceId($params['PATCH']['transfer_no']);            
            $objStockTrans->updateStatus();
            // tep_db_query("
            //     UPDATE
            //         stock_transaction
            //     SET
            //         status = " . $status . "
            //     WHERE
            //         reference_id = " . $this->getId() . "
            // ");

            //update status of transfer master 
            $obj->setId($this->getId());
            $obj->updateStatus();
            echo 'update successfully.';
        }else{
            return array(
                'data' => array(
                    message => 'Unauthorized'
                )
            );
        }
    }

}
