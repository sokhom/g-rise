<?php

use
	OSC\Quotation\Collection as InvoiceCol
	, OSC\Quotation\Object as InvoiceObj
	, OSC\QuotationDetail\Object as InvoiceDetailObj
	, OSC\CaseFlow\Object as CashFlowObj
;

class RestApiQuotation extends RestApi {

	public function get($params){
		$col = new InvoiceCol();
		// start limit page
		$col->sortById('DESC');
		$params['GET']['balance'] ? $col->filterByBalance() : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		// $params['GET']['customer_type_id'] ? $col->filterByCustomerTypeId($params['GET']['customer_type_id']) : '';
		$params['GET']['customer_id'] ? $col->filterByCustomerId($params['GET']['customer_id']) : '';
		$params['GET']['from_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
		
		$params['GET']['invoice_no'] ? $col->filterByInvoiceNo($params['GET']['invoice_no']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';

		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new InvoiceObj();
		$obj->setProperties($params['POST']['invoice']);
		$milliseconds = substr(round(microtime(true) * 1000), 7);
		/***************
		 * Get Date ****
		 ***************/
		$date = $params['POST']['invoice']["invoice_date"];//date('Y-m-d', strtotime(date("Y-m-d H:i:s")));
		$invoiceDate = date('Y', strtotime($date));
		/**************************
		 * generate invoice no ****
		 **************************/
		// count record sale
		$query = tep_db_query("
			SELECT COUNT(id) total FROM  quotation WHERE YEAR(invoice_date) = YEAR(CURDATE())
		");
		$queryTransaction = tep_db_fetch_array($query);
		$count = (int)$queryTransaction['total'];
		$count < 0 ? $count = 1 : $count = $count + 1;
		if($count < 9999){
			$string = '0000' . (string)$count;
			// sub string with 4digit
			$stringConcat =  substr($string, -4);
		}else{
			$stringConcat =  (string)$count;
		}
		
		$invoiceNo = $_SESSION['quotationNo'] . $invoiceDate . $stringConcat;
		$obj->setInvoiceNo($invoiceNo);
		$obj->insert();
		$invoiceId = $obj->getId();

		// start insert data into detail
		foreach( $params['POST']['invoice_detail'] as $key => $value){
			$objDetail = new InvoiceDetailObj();
			$objDetail->setInvoiceId($invoiceId);
			$objDetail->setInvoiceNo($invoiceNo);
			$objDetail->setProperties($value);
			$objDetail->insert();
			unset($value);
		}
		
		return array(
			'data' => array(
				'id' => $invoiceId,
				'success' => 'success',
				'invoice_no' => $invoiceNo
			)
		);
	}

//	public function put($params){
//		$obj = new InvoiceObj();
//		$obj->setProperties($params['PUT']);
//		$obj->update($this->getId());
//		return array(
//			'data' => array(
//				'id' => $obj->getId(),
//				'success' => 'success'
//			)
//		);
//	}

	public function patch($params){
		$obj = new InvoiceObj();
		$obj->setStatus($params['PATCH']['status']);
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setInvoiceNo($params['PATCH']['invoice_no']);
		$obj->updateStatus();
	}

	public function delete($params){
		$obj = new InvoiceObj();
		$obj->setId($this->getId());
		$obj->delete();
	}

}
