<?php

use
	OSC\Role\Collection
		as RoleCol
	, OSC\Role\Object
		as RoleObj
    , OSC\Permission\Object
        as PermissionObj
;

class RestApiRole extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			if($params['GET']['id']){
				$query = tep_db_query("
					SELECT
						role_name,
						role_text,
						description,
						status,
						id
					FROM
						role
					where id = ". (int)$params['GET']['id'] ."
				");
			}else{
				$query = tep_db_query("
					SELECT
						role_name,
						status,
						description,
						id
					FROM
						role
					ORDER BY role_name ASC
				");
			}			
		
			$array = [];
			while($item = tep_db_fetch_array($query)){				
				$array[] = array(
					"id" => (int)$item['id'],
					"role_text" => json_decode($item['role_text']),
					"status" => (int)$item['status'],
					"description" => $item['description'],					
					"role_name" => $item['role_name'],
				);
			}
			return array('data' => [
				'elements' => $array, 'id'=> $params['GET']['id']
				]
			);
			// $col = new RoleCol();
			// return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function patch($params){
		$obj = new RoleObj();
		$obj->setId($this->getId());
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

	public function put($params){
		if($_SESSION["id"]) {
			$obj = new RoleObj();
			$obj->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			
			$obj->setProperties($params['PUT']['role']);
			$obj->setRoleText(json_encode($params['PUT']['role']['role_text']));

			$obj->update();

			$objPermission = new PermissionObj();
			$objPermission->deleteByRoleId($this->getId());
            // save role permission
            $fields = $params['PUT']['permission'];
            foreach ( $fields as $k => $v){
                $objPermission->setRoleId($obj->getId());
                $objPermission->setCreateBy($_SESSION['user_name']);
                $objPermission->setProperties($v);
                $objPermission->insert();
                unset($v);
            }
			return true;
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function post($params){
		if($_SESSION["id"]) {
			$obj = new RoleObj();
			$obj->setCreateBy($_SESSION['user_name']);
			$obj->setProperties($params['POST']['role']);
			$obj->setRoleText(json_encode($params['POST']['role']['role_text']));

			// echo json_decode($obj->getRoleText());
			// exit;

			$obj->insert();

			$objPermission = new PermissionObj();
            // save role permission
            $fields = $params['POST']['permission'];
            foreach ( $fields as $k => $v){
                $objPermission->setRoleId($obj->getId());
                $objPermission->setCreateBy($_SESSION['user_name']);
                $objPermission->setProperties($v);
                $objPermission->insert();
                unset($v);
            }
			return array(
                'data' => array(
                    'id' => $obj->getId(),
                    'success' => 'success'
                )
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function delete($params){
		$query = tep_db_query("select count(id) as total from administrators where role_id = ". $this->getId()." ");
		$count = tep_db_fetch_array($query);
		if((int)$count['total'] == 0){
			$obj = new RoleObj();
			$obj->delete($this->getId());
			echo 'success';
		}else{
			echo 'record is using now cannot delete.';
		}
	}
	

}
