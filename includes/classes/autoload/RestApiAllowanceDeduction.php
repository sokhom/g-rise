<?php

use
	OSC\AllowanceDeduction\Collection
		as AllowanceDeductionCol
	, OSC\AllowanceDeduction\Object
		as AllowanceDeductionObj
;

class RestApiAllowanceDeduction extends RestApi {

	public function get($params){
		$col = new AllowanceDeductionCol();
		// start limit page
		$col->sortById('ASC');
		$col->filterByName($params['GET']['name']);
		$params['GET']['status'] ? $col->filterByStatus($params['GET']['status']) : '';
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$showDataPerPage = 10;
		$start = $params['GET']['start'];
		$this->applyLimit($col,
			array(
				'limit' => array( $start, $showDataPerPage )
			)
		);
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new AllowanceDeductionObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new AllowanceDeductionObj();
		$obj->setProperties($params['PUT']);
		$obj->setId($this->getId());
		$obj->update();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function delete(){
		$obj = new AllowanceDeductionObj();
		$obj->delete($this->getId());
	}

	public function patch($params){
		$obj = new AllowanceDeductionObj();
		$obj->setId($this->getId());
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

}
