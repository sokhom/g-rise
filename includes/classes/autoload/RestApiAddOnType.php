<?php

use
	OSC\AddOnType\Collection
		as addOnTypeCol
	, OSC\AddOnType\Object
		as addOnTypeObj
;

class RestApiAddOnType extends RestApi {

	public function get($params){
		$col = new addOnTypeCol();
		$col->sortByName("ASC");
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$params['GET']['filter'] ? $col->filterByName($params['GET']['filter']) : '';
		if($params['GET']['paginate']){
			$showDataPerPage = 10;
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new addOnTypeObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new addOnTypeObj();
		$this->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setProperties($params['PUT']);
		$obj->update($this->getId());
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function patch($params){
		$obj = new addOnTypeObj();
		$obj->setId($this->getId());
		$obj->setUpdateBy($_SESSION['user_name']);
		$obj->setStatus($params['PATCH']['status']);
		$obj->updateStatus();
	}

	public function delete(){
		$query = tep_db_query("select count(id) as total from add_ons where add_on_type_id = ". $this->getId()." ");
		$count = tep_db_fetch_array($query);
		if((int)$count['total'] == 0){
			$obj = new addOnTypeObj();
			$obj->setId($this->getId());
			$obj->delete();
			echo 'success';
		}else{			
			echo 'record is using now cannot delete.';
		}
	}

}
