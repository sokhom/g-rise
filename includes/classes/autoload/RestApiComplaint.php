<?php

use
	OSC\Complaint\Collection
		as ComplaintCol
	, OSC\Complaint\Object
		as ComplaintObj
;

class RestApiComplaint extends RestApi {

	public function get($params){
		$col = new ComplaintCol();
		// start limit page
		$col->sortById('DESC');
		$col->filterByName($params['GET']['name']);
		$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
		$params['GET']['status'] ? $col->filterByStatus(1) : '';
		$showDataPerPage = 10;
		if($params['GET']['pagination']){
			$start = $params['GET']['start'];
			$this->applyLimit($col,
				array(
					'limit' => array( $start, $showDataPerPage )
				)
			);
		}
		$this->applyFilters($col, $params);
		$this->applySortBy($col, $params);
		return $this->getReturn($col, $params);
	}

	public function post($params){
		$obj = new ComplaintObj();
		$obj->setProperties($params['POST']);
		$obj->insert();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function put($params){
		$obj = new ComplaintObj();
		$this->setId($this->getId());
		$obj->setProperties($params['PUT']);
		if($obj->getResolveBy()){
			$obj->setStatus(2);
		}
		$obj->update();
		return array(
			'data' => array(
				'id' => $obj->getId(),
				'success' => 'success'
			)
		);
	}

	public function delete(){
		$obj = new ComplaintObj();
		$obj->delete($this->getId());
	}

}
