<?php

class RestApiImportService extends RestApi {

	public function post($params){
		if($_SESSION["id"]) {
			//validate whether uploaded file is a csv file
			$csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv', 'text/xlsx', 'text/xls');
			if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					//open uploaded csv file with read only mode
					$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
					//skip first line
					fgetcsv($csvFile);
					$currentDate = date("Y/m/d h:i");
					//parse data from csv file line by line
					while(($line = fgetcsv($csvFile, 10000, ",")) !== FALSE){
						//check whether product already exists in database with same barcode
//						$prevQuery = tep_db_query("SELECT id FROM products WHERE barcode = '".$line[3]."'");
//						$prevResult = tep_db_num_rows($prevQuery);
//						if($prevResult > 0){
//							//update member data
//							//$db->query("UPDATE members SET name = '".$line[0]."', phone = '".$line[2]."', created = '".$line[3]."', modified = '".$line[3]."', status = '".$line[4]."' WHERE email = '".$line[1]."'");
//							$data = tep_db_fetch_array($prevQuery);
//							$sql_data_array = array(
//									'products_name' => $line[0],
//									'products_type_id' => $line[1],
//									'products_kind_of' => $line[2],
//									'barcode' => $line[3],
//									'products_price_in' => $line[4],
//									'products_price_out' => $line[5],
//									'products_quantity' => $line[6],
//									'products_description' => $line[7],
//									'status' => $line[8] == 'Active' ? 1 : 0,
//									'create_by' => $_SESSION['user_name'],
//									'create_date' => date("Y/m/d h:i"),
//							);
//							tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "id = '" . (int)$data['id'] . "'");
//						}else{
							// verify to get product type ID
							// $queryProType = tep_db_query("SELECT id FROM products_type WHERE name = '".$line[1]."'");
							// $numRow = tep_db_num_rows($queryProType);
							// $productTypeId = 0;
							// if($numRow > 0){
							// 	$data = tep_db_fetch_array($queryProType);
							// 	$productTypeId = $data['id'];
							// }
							//insert product
							$serviceName = $line[0] . '( ' . $line[1] . ' )';
							$sql_data_array = array(
								'name' => $serviceName,
								'type_id' => 1,
								'code' => $line[2],
								'price' => $line[3],
								'create_by' => $_SESSION['user_name'],
								'status' => 1,
								'create_date' => $currentDate,
							);
							tep_db_perform('services', $sql_data_array);
							$serviceId = tep_db_insert_id();

							//insert into stock transaction
							$sql_data_stock_transaction_array = array(
								'product_id' => $serviceId,
								'product_name' => $serviceName,
								'product_type_id' => 1,
								'product_kind_of' => 'service',
								'status' => 1,
								'create_by' => $_SESSION['user_name'],
								'create_date' => $currentDate,
							);
							tep_db_perform('stock_transaction', $sql_data_stock_transaction_array);
//						}
					}

					//close opened csv file
					fclose($csvFile);

					$status = 'success';
				}else{
					$status = 'error';
				}
			}else{
				$status = 'invalid_file';
			}

			return array(
				'data' => array(
					'status' => $status
				)
			);
		}
	}

}
