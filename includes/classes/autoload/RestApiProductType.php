<?php

use
	OSC\ProductsType\Collection
		as ProductsTypeCol
	, OSC\ProductsType\Object
		as ProductsTypeObj
;

class RestApiProductType extends RestApi {

	public function get($params){
		if($_SESSION["id"]) {
			$col = new ProductsTypeCol();
			// start limit page
			$col->sortByName("ASC");
			$col->sortById("DESC");
			$col->filterByName($params['GET']['name']);
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			$params['GET']['status'] ? $col->filterByStatus(1) : '';
			if($params['GET']['pagination']){
				$showDataPerPage = 10;
				$start = $params['GET']['start'];
				$this->applyLimit($col,
					array(
						'limit' => array( $start, $showDataPerPage )
					)
				);
			}
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}
	public function post($params){
		// Validate customer with duplicate code
		$query = tep_db_query("
			SELECT
				id
			FROM
				products_type
			WHERE
				name = '" . $params['POST']['name'] . "'
		");
		$countQuery = tep_db_num_rows($query);
		
		if($countQuery > 0){
			return array(
				'data' => array(
					'error' => 'Duplicate Name.'
				)
			);
		}
		if($_SESSION["id"]) {
			$obj = new ProductsTypeObj();
			$obj->setCreateBy($_SESSION['user_name']);
			$obj->setProperties($params['POST']);
			$obj->insert();
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
				)
			);
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

	public function put($params){
		// Validate customer with duplicate code
		$query = tep_db_query("
			SELECT
				id
			FROM
				products_type
			WHERE
				name = '" . $params['PUT']['name'] . "'
					and
				id != ". $this->getId() ."
		");
		$countQuery = tep_db_num_rows($query);
		
		if($countQuery > 0){
			echo 'error';
			return;
		}
		if($_SESSION["id"]) {
			$obj = new ProductsTypeObj();
			$this->setId($this->getId());
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setProperties($params['PUT']);
			$obj->update($this->getId());
			return array(
				'data' => array(
					'id' => $obj->getId(),
					'success' => 'success'
			));
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

//	public function delete(){
//		$obj = new ProductsTypeObj();
//		$obj->delete($this->getId());
//	}

	public function patch($params){
		if($_SESSION["id"]) {
			$obj = new ProductsTypeObj();
			$obj->setUpdateBy($_SESSION['user_name']);
			$obj->setId($this->getId());
			$obj->setStatus($params['PATCH']['status']);
			$obj->updateStatus();
		}else{
			return array(
				'data' => array(
					message => 'Unauthorized'
				)
			);
		}
	}

}
