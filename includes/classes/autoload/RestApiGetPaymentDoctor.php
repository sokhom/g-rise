<?php

class RestApiGetPaymentDoctor extends RestApi {

	public function get($params){
		$where = '';
		if($_GET['from_date'] && $_GET['to_date']){
			$fromDate = $_GET['from_date'] . " 00:00:00";//2016-09-15 00:00:00
			$endDate = $_GET['to_date']. ' 23:59:59';//2016-09-17 23:59:59
			$where = " AND expense_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";
		}
		// $query = tep_db_query("
		// 	SELECT 
		// 		d.doctor_id, 
		// 		d.doctor_name, 
		// 		SUM(d.grand_total) as total_payment, 
		// 		(SELECT basic_salary FROM doctor_list WHERE id = d.doctor_id) as basic_salary
		// 	FROM 
		// 		doctor_expense d
		// 	WHERE 
		// 		d.status = 1 ". $where ."
		// 	GROUP BY 
		// 		d.doctor_id 
		// 	ORDER BY 
		// 		d.doctor_name ASC
		// ");
		$query = tep_db_query("
			SELECT 
				d.id, 
				d.name, 
				(SELECT SUM(grand_total) FROM doctor_expense WHERE doctor_id = d.id AND status = 1 ". $where .") as total_payment, 				 
				d.basic_salary
			FROM 
				doctor_list d
			WHERE 
				d.status = 1 
			GROUP BY 
				d.id 
			ORDER BY 
				d.name ASC
		");

		$array = [];
		while($transaction = tep_db_fetch_array($query)){
			$array[] = array(
				"doctor_id" => (int)$transaction['id'],
				"doctor_name" => $transaction['name'],
				//"phone" => $transaction['phone'],
				//"sex" => $transaction['sex'],
				"basic_salary" => doubleval($transaction['basic_salary']),
				"total_payment" => doubleval($transaction['total_payment']),
				"tax_percentage" => 0, 
				"other" => 0,
				"net_pay" => 0,
				"deduct_basic_salary" => 0,
				"deduct_total_get" => 0,
				"detail" => []
			);
		}

		return array(
			'data' => $array
		); 
	}

}
