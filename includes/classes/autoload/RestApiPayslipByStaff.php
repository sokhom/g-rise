<?php

use
	OSC\DoctorPayslipDetail\Collection
		as DoctorPayslipDetailCol
;

class RestApiPayslipByStaff extends RestApi {

	public function get($params){
		$col = new DoctorPayslipDetailCol();
		$staffId = $_SESSION["link_to"];
		if(isset($staffId)){
			$col->sortById('DESC');
			$col->filterByDoctorId($staffId);
			$params['GET']['id'] ? $col->filterById($params['GET']['id']) : '';
			$params['GET']['status'] ? $col->filterByStatus(1) : '';
			$params['GET']['from_date'] && $params['GET']['to_date'] ? $col->filterByDate($params['GET']['from_date'], $params['GET']['to_date']) : '';
			$this->applyFilters($col, $params);
			$this->applySortBy($col, $params);
			return $this->getReturn($col, $params);
		}else{
			return [ 'data' => ['success'=> 'false']];
		}
	}

}
