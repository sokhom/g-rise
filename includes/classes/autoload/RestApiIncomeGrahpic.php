<?php

class RestApiIncomeGrahpic extends RestApi{

    public function get(){
        $array = [];
        $year = (int)$_GET['year'];
        for ($i=1; $i<=12; $i++) { 
            $fromDate =  date('Y-m-01 00:00:00', strtotime("$year-$i-01")) . '<br/>';
            $endDate = date('Y-m-t 12:59:59', strtotime("$year-$i-01")) . '<br/>';
            
            // sum product item
            $sellItemQuery = tep_db_query("
                select sum(total) as total_item 
                from stock_out_detail 
                where status = 1 and product_kind_of = 'item'
                    and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
                    
            ");
            $incomeItem = tep_db_fetch_array($sellItemQuery);
            $totalIncomeFromItem = doubleval($incomeItem['total_item']);
            
            // sum product service
            $sellServiceQuery = tep_db_query("
                select sum(total) as total_service 
                from stock_out_detail 
                where status = 1 and product_kind_of = 'service'
                    and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
                
            ");
            $incomeService = tep_db_fetch_array($sellServiceQuery);        
            $totalIncomeFromService = doubleval($incomeService['total_service']);
            
            // sum product cost
            $costQuery = tep_db_query("
                select SUM(CASE WHEN type = 'whole_sale' THEN (cost / um_type_whole_sale_amount) * (qty * um_type_whole_sale_amount) 
                        ELSE (cost / um_type_whole_sale_amount) * (qty * um_type_retail_amount) END )
                    as total_cost 
                from stock_out_detail 
                where status = 1 and product_kind_of = 'item' 
                    and stock_out_date between '". $fromDate ."' AND '". $endDate ."'
            ");
            $costExpense = tep_db_fetch_array($costQuery);        
            $totalCostExpense = doubleval($costExpense['total_cost']);

            // sum purchase item
            $purchaseQuery = tep_db_query("
                select sum(payment) as total_purchase 
                from purchase_master 
                where status = 1 and purchase_date between '". $fromDate ."' AND '". $endDate ."'
            ");
            $purchase = tep_db_fetch_array($purchaseQuery);
            $totalPurchase = doubleval($purchase['total_purchase']);

            // sum staff payroll
            $staffPayslipQuery = tep_db_query("
                select sum(total_net_pay) as total_pay 
                from doctor_payslip 
                where status = 1 and from_date between '". $fromDate ."' AND '". $endDate ."'
            ");
            $payslip = tep_db_fetch_array($staffPayslipQuery);
            $totalPayslip = doubleval($payslip['total_pay']);

            // query other expense
            $otherExpenseQuery = tep_db_query("
                select sum(amount) as amount
                from expense
                where status = 1 and expense_date between '". $fromDate ."' AND '". $endDate ."'
            ");
            $expense = tep_db_fetch_array($otherExpenseQuery);
            $totalExpense = $expense['amount'];
            $array[] = [
                month => date("F", strtotime("$year-$i-01")),
                income => doubleval( $totalIncomeFromItem + $totalIncomeFromService ),
                expense => doubleval( $totalCostExpense + $totalExpense + $totalPayslip + $totalPurchase )
            ];
        }
        return [data => $array];        
    }

}
