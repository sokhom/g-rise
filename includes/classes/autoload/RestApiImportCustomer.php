<?php

class RestApiImportCustomer extends RestApi {

	public function post($params){
		if($_SESSION["id"]) {
			//validate whether uploaded file is a csv file
			$csvMimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv', 'text/xlsx', 'text/xls');
			if(!empty($_FILES['file']['name']) && in_array($_FILES['file']['type'],$csvMimes)){
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					//open uploaded csv file with read only mode
					$csvFile = fopen($_FILES['file']['tmp_name'], 'r');
					//skip first line
					fgetcsv($csvFile);
					//parse data from csv file line by line
					while(($line = fgetcsv($csvFile, 10000, ",")) !== FALSE){
						//check whether product already exists in database with same barcode
//						$prevQuery = tep_db_query("SELECT id FROM products WHERE barcode = '".$line[3]."'");
//						$prevResult = tep_db_num_rows($prevQuery);
//						if($prevResult > 0){
//							//update member data
//							//$db->query("UPDATE members SET name = '".$line[0]."', phone = '".$line[2]."', created = '".$line[3]."', modified = '".$line[3]."', status = '".$line[4]."' WHERE email = '".$line[1]."'");
//							$data = tep_db_fetch_array($prevQuery);
//							$sql_data_array = array(
//									'products_name' => $line[0],
//									'products_type_id' => $line[1],
//									'products_kind_of' => $line[2],
//									'barcode' => $line[3],
//									'products_price_in' => $line[4],
//									'products_price_out' => $line[5],
//									'products_quantity' => $line[6],
//									'products_description' => $line[7],
//									'status' => $line[8] == 'Active' ? 1 : 0,
//									'create_by' => $_SESSION['user_name'],
//									'create_date' => date("Y/m/d h:i"),
//							);
//							tep_db_perform(TABLE_PRODUCTS, $sql_data_array, 'update', "id = '" . (int)$data['id'] . "'");
//						}else{
							// verify to get customer type ID
							// $queryCustomerType = tep_db_query("SELECT id FROM customer_type WHERE name = '".$line[1]."'");
							// $customerNumRow = tep_db_num_rows($queryCustomerType);
							// $customerTypeId = 1; // default set id 1 is general and make sure the must have one record that id is 1
							// if($customerNumRow > 0){
							// 	$data = tep_db_fetch_array($queryCustomerType);
							// 	$customerTypeId = $data['id'];
							// }
							// // verify to get Branch ID
							// $queryBranchType = tep_db_query("SELECT id FROM branch WHERE name = '".$line[2]."'");
							// $branchNumRow = tep_db_num_rows($queryBranchType);
							// $branchTypeId = 1; // default set id 1 is general and make sure the must have one record that id is 1
							// if($branchNumRow > 0){
							// 	$data = tep_db_fetch_array($queryBranchType);
							// 	$branchTypeId = $data['id'];
							// }
							//insert product
							$gender = preg_replace('/[0-9]+/', '', $line[4]); // replace numbner to empty 
							$gender = $gender == ' F' ? 'Female' : 'Male';
							$string = $line[11] . ' ' . $line[12];
							$sql_data_array = array(
								'customers_name' => $line[2] . " " . $line[3],
								'customers_type_id' => 1,// $customerTypeId,
								'branch_id' => 1,// $branchTypeId,
								'customers_gender' => $gender,
								'customers_dob' => '',
								'customers_code' => $line[1],
								'customers_country' => 'Cambodia',//$line[6],
								'customers_email_address' => '',
								'customers_telephone' => str_replace(array("'", ' '), array('', ''), $line[7]), // replace singal quote and space
								'customers_relative_contact' =>  str_replace(array("'", ' '), array('', ''), $line[8]),
								'customers_address' =>  $line[9],
								'customers_description' =>  $line[10] . ' ' . $string,
								'status' => 1,
								'create_by' => $_SESSION['user_name'],
								'create_date' => date("Y/m/d h:i"),
							);
							tep_db_perform(TABLE_CUSTOMERS, $sql_data_array);
							
//						}
					}

					//close opened csv file
					fclose($csvFile);

					$status = 'success';
				}else{
					$status = 'error';
				}
			}else{
				$status = 'invalid_file';
			}

			return array(
				'data' => array(
					'status' => $status
				)
			);
		}
	}

}
