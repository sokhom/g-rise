<?php
if (!tep_session_is_registered('id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html <?php echo HTML_PARAMS; ?> data-ng-app="main">
<head>
<meta charset="<?php echo CHARSET; ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-se=1cal">
    <title update-title></title>
    <link rel="shortcut icon" href="<?php echo $company['logo'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- To prevent only Google web crawlers from indexing a page -->
    <meta name="robots" content="noindex">
    <meta name="googlebot" content="noindex">
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="css/lib_template/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="js/plugins/sweet-alert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="css/date-time-picker/angular-datepicker.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.css">
<!--    <link rel="stylesheet" type="text/css" href="css/lib_template/animate.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/lib_template/bootstrap-switch.min.css">
    <!-- CSS App -->
<!--    <link rel="stylesheet" type="text/css" href="css/lib_template/jquery-ui.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/lib_template/style.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/select.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/flat-blue.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/select2.min.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/checkbox3.min.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-trix/trix.css">
    <!-- Select2 theme -->
    <link rel="stylesheet" href="js/ng/lib/angular-select2/select.min.css" type="text/css">
    <link rel="stylesheet" href="js/ng/lib/angular-select2/select2.css" type="text/css">
<!--    <link rel="stylesheet" type="text/css" href="css/date-time-picker/helper.css">-->
    <link rel="stylesheet" type="text/css" href="css/print_table.css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_table.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ivh-treeview/ivh-treeview.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ivh-treeview/ivh-treeview-theme-basic.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-crop-image/ng-img-crop.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ui-grid/ui-grid.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ui-switch/angular-ui-switch.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-calendar-bootstrap/css/angular-bootstrap-calendar.min.css"/>
    
</head>
<body class="flat-blue" data-ng-controller="main_ctrl" data-ng-cloak>
<input type="text" name="user_name" id="user_name" hidden value="<?php echo $_SESSION['user_name']?>" id="">
<div class="app-container">
    <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-expand-toggle">
                        <i class="fa fa-bars icon"></i>
                    </button>
                    <ol class="breadcrumb navbar-breadcrumb">
                        <li>
                            <span data-ng-bind-html="company.company_name"></span>
                            <span data-ng-if="!online" class="alert alert-warning">
                                  <i class="fa fa-warning"></i>
                                  No Internet Connection.
                            </span>
                        </li>
                    </ol>
                    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                        <i class="fa fa-th icon"></i>
                    </button>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                        <i class="fa fa-times icon"></i>
                    </button>

                    <li class="dropdown danger" ng-if="featureRole">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            {{totalNotification}}
                        </a>
                        <ul class="dropdown-menu danger animated InDown">
                            <li class="title">
                                {{'Notification' | translate}}
                                <span class="badge pull-right">
                                    {{totalNotification}}
                                </span>
                            </li>
                            <li role-permission permission="'Customer_Birth_Day_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="customer-birthday">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{birthDayCustomerNotification}}
                                            </span>
                                            <i class="fa fa-birthday-cake"></i>
                                            {{'CustomerBirthday' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Staff_Birthday_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="staff_birthday">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{birthDayStaffNotification}}
                                            </span>
                                            <i class="fa fa-birthday-cake"></i>
                                            {{'StaffBirthday' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <!-- <li role-permission permission="'Customer_Appointment_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="appointment_report({appointment_alert: 'yes'})">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{appointmentNotification}}
                                            </span>
                                            <i class="fa fa-tags"></i>
                                            {{'CustomerAppointment' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Appointment_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="report-appointment-by-staff({ data: {appointment_alert: 'yes'}})">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{myAppointmentNotification}}
                                            </span>
                                            <i class="fa fa-tags"></i>
                                            {{'MyAppointment' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Stock_Alert_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="stock_alert_report">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{ProductNotification}}
                                            </span>
                                            <i class="fa fa-tasks"></i>
                                            {{'StockAlert' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Customer_Overdue_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="customer_overdue_report">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{CustomerOverdueNotification}}
                                            </span>
                                            <i class="fa fa-dollar"></i>
                                            {{'CustomerOverdue' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li> -->
                        </ul>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                            {{'Language' | translate}}
                        </a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li class="title">
                                <a data-ng-click="changeLanguage('kh')" href="">
                                    <img src="images/kh.gif" width="25px" />
                                    {{ 'TextKhmer' | translate}}
                                </a>
                            </li>
                            <li class="title">
                                <a data-ng-click="changeLanguage('en')" href="">
                                    <img src="images/en.gif" width="25px" />
                                    {{ 'TextEnglish' | translate}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown profile">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span id="user">
                                <?php
                                    echo $_SESSION['user_name'];
                                ?>
                            </span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li>
                                <div class="profile-info">
                                    <span ng-bind="'Welcome' | translate"></span>:
                                    <span class="welcome">
                                        <?php
                                            echo $_SESSION['user_name'];
                                        ?>
                                    </span>
                                    <br/><br/>
                                    <div class="btn-group margin-bottom-2x" role="group">
                                        <a href="logoff.php" class="btn btn-primary btn-sm">
                                            <i class="fa fa-sign-out"></i>
                                            {{'Logout' | translate}}
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="side-menu sidebar-inverse">
            <nav class="navbar navbar-default" role="navigation" ng-if="featureRole">
                <div class="side-menu-container">
                    <div class="navbar-header">
                        <a class="navbar-brand">
                            <div class="icon fa fa fa-institution"></div>
                            <div class="title">
                                <span translate="Menu"></span>
                            </div>
                        </a>
                        <!--<button type="button" class="navbar-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>-->
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a ui-sref="dashboard">
                                <span class="icon fa fa-tachometer"></span>
                                <span class="title">
                                    {{'DashBoard' | translate }}
                                </span>
                            </a>
                        </li>

                        <!-- Calendar Menu -->
                        <li role-permission permission="'Calendar_Pages'" 
                            feature="featureRole">
                            <a ui-sref="calendar">
                                <span class="icon fa fa-calendar"></span>
                                <span class="title">
                                    <span translate="Calendar"></span>
                                </span>
                            </a>
                        </li>

                        <!-- New Menu -->
                        <li class="panel panel-default dropdown"
                             role-permission permission="'New_Pages'" 
                             feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-new" onclick="return false;">
                                <span class="icon fa fa-address-card-o"></span>
                                <span class="title">
                                    <span translate="New"></span>
                                </span>
                            </a>
                            <div id="dropdown-new" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Client_Log_Create'" feature="featureRole">
                                            <a ui-sref="customer_list_create">
                                                <span translate="ClientLog"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Quotation_View'" feature="featureRole">
                                            <a ui-sref="quotation">
                                                <span translate="Quotation"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Customer Menu  -->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Customer_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-element" onclick="return false;">
                                <span class="icon fa fa-address-book-o"></span>
                                <span class="title">
                                    {{'Customers' | translate }}
                                </span>
                             </a>                            
                            <div id="dropdown-element" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Client_Register_Create'" feature="featureRole">
                                            <a ui-sref="customer_list_create">
                                                <span translate="ClientRegister"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Type_View'" feature="featureRole">
                                            <a ui-sref="customer_type">
                                                <span translate="CustomerType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Member_Ship_View'" feature="featureRole">
                                            <a ui-sref="member_ship">
                                                <span translate="MemberShip"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_List_View'" feature="featureRole">
                                            <a ui-sref="customer_list">
                                                <span translate="CustomerList"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Feed_Back_View'" feature="featureRole">
                                            <a ui-sref="feed_back">
                                                <span translate="FeedBack"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Complaint_View'" feature="featureRole">
                                            <a ui-sref="complaint">
                                                <span translate="Complaint"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Sale_Form_View'" feature="featureRole">
                                            <a ui-sref="sale_form">
                                                <span translate="SaleForm"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Staff Menu -->
                        <li class="panel panel-default dropdown"
                             role-permission permission="'Staff_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-doctor" onclick="return false;">
                                <span class="icon fa fa-address-card-o"></span>
                                <span class="title">
                                    <span translate="SetupStaff"></span>
                                </span>
                            </a>
                            <div id="dropdown-doctor" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Staff_Type_View'" feature="featureRole">
                                            <a ui-sref="staff_type">
                                                <span translate="StaffType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Staff_List_View'" feature="featureRole">
                                            <a ui-sref="staff_list">
                                                <span translate="StaffList"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        
                        <!-- Invoice Menu  -->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Invoice_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-invoice" onclick="return false;">
                                <span class="icon fa fa-industry"></span>
                                <span class="title">
                                    {{'Invoice' | translate }}
                                </span>
                             </a>                            
                            <div id="dropdown-invoice" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Invoice_View'" feature="featureRole">
                                            <a ui-sref="invoice">
                                                <span translate="Invoice"></span>
                                            </a>
                                        </li>
                                        <!-- <li role-permission permission="'Quotation_View'" feature="featureRole">
                                            <a ui-sref="quotation">
                                                <span translate="Quotation"></span>
                                            </a>
                                        </li> -->
                                        <li role-permission permission="'Receive_Payment_View'" feature="featureRole">
                                            <a ui-sref="received_payment">
                                                <span translate="ReceivePayment"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Setup Item Menu-->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Item_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#component-example" onclick="return false;">
                                <span class="icon fa fa-cubes"></span>
                                <span class="title">
                                    <span translate="Setup"></span>
                                </span>
                            </a>
                            <div id="component-example" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <!-- <li role-permission permission="'UM_Type_View'" feature="featureRole">
                                            <a ui-sref="um_type">
                                                <span translate="UMType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Product_Type_View'" feature="featureRole">
                                            <a ui-sref="product_type">
                                                <span translate="ProductType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Product_List_View'" feature="featureRole">
                                            <a ui-sref="product_list">
                                                <span translate="ProductList"></span>
                                            </a>
                                        </li> -->
                                        <li role-permission permission="'Option_Type_View'" feature="featureRole">
                                            <a ui-sref="option_type">
                                                <span translate="OptionType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Option_View'" feature="featureRole">
                                            <a ui-sref="option">
                                                <span translate="Options"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Add_On_Type_View'" feature="featureRole">
                                            <a ui-sref="add_on_type">
                                                <span translate="AddOnType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Add_On_View'" feature="featureRole">
                                            <a ui-sref="add_on">
                                                <span translate="AddOn"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Package_Type_View'" feature="featureRole">
                                            <a ui-sref="package_type">
                                                <span translate="PackageType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Package_View'" feature="featureRole">
                                            <a ui-sref="package">
                                                <span translate="Package"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Location_Type_View'" feature="featureRole">
                                            <a ui-sref="location_type">
                                                <span translate="LocationType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Location_View'" feature="featureRole">
                                            <a ui-sref="location">
                                                <span translate="Location"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Report Menu -->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Report_Pages'" 
                            feature="featureRole">
                            <a data-toggle="collapse" 
                                href="#dropdown-report" 
                                onclick="return false;">
                                <span class="icon fa fa-file-text-o"></span>
                                <span class="title">
                                    <span translate="Report"></span>
                                </span>
                            </a>
                            <div id="dropdown-report" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Customer_Report_View'" feature="featureRole">
                                            <a ui-sref="customer_report">
                                                <span translate="CustomerReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Balance_Report_View'" feature="featureRole">
                                            <a ui-sref="customer_balance_report">
                                                <span translate="CustomerBalanceReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Staff_Report_View'" feature="featureRole">
                                            <a ui-sref="staff_report">
                                                <span translate="StaffReport"></span>
                                            </a>
                                        </li>
                                        <!-- <li role-permission permission="'Product_Report_View'" feature="featureRole">
                                            <a ui-sref="product_report">
                                                <span translate="ProductReport"></span>
                                            </a>
                                        </li> -->
                                        <li role-permission permission="'Option_Report_View'" feature="featureRole">
                                            <a ui-sref="report_option">
                                                <span translate="OptionReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Add_On_Report_View'" feature="featureRole">
                                            <a ui-sref="report_add_on">
                                                <span translate="AddOnReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Package_Report_View'" feature="featureRole">
                                            <a ui-sref="report_package">
                                                <span translate="PackageReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Invoice_Summary_Report_View'" feature="featureRole">
                                            <a ui-sref="report_invoice_summary">
                                                <span translate="InvoiceSummaryReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Invoice_Report_View'" feature="featureRole">
                                            <a ui-sref="report_invoice">
                                                <span translate="InvoiceReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Quotation_Report_View'" feature="featureRole">
                                            <a ui-sref="report_quotation">
                                                <span translate="QuotationReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Receive_Payment_Report_View'" feature="featureRole">
                                            <a ui-sref="report_receive_payment">
                                                <span translate="ReceivePaymentReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Cash_Flow_Report_View'" feature="featureRole">
                                            <a ui-sref="cash_flow">
                                                <span translate="CashFlowReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Sale_Form_Report_View'" feature="featureRole">
                                            <a ui-sref="report_sale_form">
                                                <span translate="SaleFormReport"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Administrator Menu -->
                        <li class="panel panel-default dropdown"
                             role-permission permission="'Administrator_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-user" onclick="return false;">
                                <span class="icon fa fa-user-circle-o"></span>
                                <span class="title">
                                    <span translate="Administrator"></span>
                                </span>
                            </a>
                            <div id="dropdown-user" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'User_View'" feature="featureRole">
                                            <a ui-sref="user">
                                                <span translate="User"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Role_View'" feature="featureRole">
                                            <a ui-sref="role">
                                                <span translate="Role"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <!-- Setting Menu -->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Setting_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#appSetting" onclick="return false;">
                                <span class="icon fa fa-cog"></span>
                                <span class="title">
                                    <span translate="Setting"></span>
                                </span>
                            </a>
                            <div id="appSetting" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Exchange_Rate_View'" feature="featureRole">
                                            <a ui-sref="exchange-rate">
                                                <span translate="ExchangeRate"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Company_Profile_View'" feature="featureRole">
                                            <a ui-sref="company-profile">
                                                <span translate="CompanyProfile"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a ui-sref="license">
                                <span class="icon fa fa-book"></span>
                                <span class="title">
                                    <span translate="License"></span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </div>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">