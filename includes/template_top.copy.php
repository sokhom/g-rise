<?php
if (!tep_session_is_registered('id')) {
    $navigation->set_snapshot();
    tep_redirect(tep_href_link(FILENAME_LOGIN, '', 'SSL'));
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html <?php echo HTML_PARAMS; ?> data-ng-app="main">
<head>
<meta charset="<?php echo CHARSET; ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-se=1cal">
    <title update-title></title>
    <link rel="shortcut icon" href="<?php echo $company['logo'];?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Fonts -->
<!--    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300,400' rel='stylesheet' type='text/css'>-->
<!--    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,900' rel='stylesheet' type='text/css'>-->
    <!-- CSS Libs https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.css -->
    <link rel="stylesheet" type="text/css" href="css/lib_template/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="js/plugins/sweet-alert/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="css/date-time-picker/angular-datepicker.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome/css/font-awesome.css">
<!--    <link rel="stylesheet" type="text/css" href="css/lib_template/animate.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/lib_template/bootstrap-switch.min.css">
    <!-- CSS App -->
<!--    <link rel="stylesheet" type="text/css" href="css/lib_template/jquery-ui.min.css">-->
    <link rel="stylesheet" type="text/css" href="css/lib_template/style.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/select.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/flat-blue.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/select2.min.css">
    <link rel="stylesheet" type="text/css" href="css/lib_template/checkbox3.min.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-trix/trix.css">
    <!-- Select2 theme -->
    <link rel="stylesheet" href="js/ng/lib/angular-select2/select.min.css" type="text/css">
    <link rel="stylesheet" href="js/ng/lib/angular-select2/select2.css" type="text/css">
<!--    <link rel="stylesheet" type="text/css" href="css/date-time-picker/helper.css">-->
    <link rel="stylesheet" type="text/css" href="css/print_table.css" media="print">
    <link rel="stylesheet" type="text/css" href="css/print_table.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ivh-treeview/ivh-treeview.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ivh-treeview/ivh-treeview-theme-basic.css">
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-crop-image/ng-img-crop.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ui-grid/ui-grid.min.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/angular-ui-switch/angular-ui-switch.min.css"/>

    <!-- <link rel="stylesheet" type="text/css" href="js/ng/lib/jquery-fix-header-table/960.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/jquery-fix-header-table/defaultTheme.css"/>
    <link rel="stylesheet" type="text/css" href="js/ng/lib/jquery-fix-header-table/myTheme.css"/> -->
</head>
<body class="flat-blue" data-ng-controller="main_ctrl" data-ng-cloak>
<input type="text" name="user_name" id="user_name" hidden value="<?php echo $_SESSION['user_name']?>" id="">
<div class="app-container">
    <div class="row content-container">
        <nav class="navbar navbar-default navbar-fixed-top navbar-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-expand-toggle">
                        <i class="fa fa-bars icon"></i>
                    </button>
                    <ol class="breadcrumb navbar-breadcrumb">
                        <li>
                            <span data-ng-bind-html="company.company_name"></span>
                            <span data-ng-if="!online" class="alert alert-warning">
                                  <i class="fa fa-warning"></i>
                                  No Internet Connection.
                            </span>
                        </li>
                    </ol>
                    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                        <i class="fa fa-th icon"></i>
                    </button>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                        <i class="fa fa-times icon"></i>
                    </button>

                    <li class="dropdown danger" ng-if="featureRole">
                        <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-bell"></i>
                            {{totalNotification}}
                        </a>
                        <!-- <ul class="dropdown-menu danger  animated     InDown">
                            <li class="title">
                                {{'Notification' | translate}}
                                <span class="badge pull-right">
                                    {{totalNotification}}
                                </span>
                            </li>
                            <li role-permission permission="'Customer_Birth_Day_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="customer-birthday">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{birthDayCustomerNotification}}
                                            </span>
                                            <i class="fa fa-birthday-cake"></i>
                                            {{'CustomerBirthday' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Staff_Birthday_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="staff_birthday">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{birthDayStaffNotification}}
                                            </span>
                                            <i class="fa fa-birthday-cake"></i>
                                            {{'StaffBirthday' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Customer_Appointment_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="appointment_report({appointment_alert: 'yes'})">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{appointmentNotification}}
                                            </span>
                                            <i class="fa fa-tags"></i>
                                            {{'CustomerAppointment' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Appointment_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="report-appointment-by-staff({ data: {appointment_alert: 'yes'}})">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{myAppointmentNotification}}
                                            </span>
                                            <i class="fa fa-tags"></i>
                                            {{'MyAppointment' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Stock_Alert_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="stock_alert_report">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{ProductNotification}}
                                            </span>
                                            <i class="fa fa-tasks"></i>
                                            {{'StockAlert' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                            <li role-permission permission="'Customer_Overdue_Report_View'" feature="featureRole">
                                <ul class="list-group notifications">
                                    <a ui-sref="customer_overdue_report">
                                        <li class="list-group-item">
                                            <span class="badge">
                                                {{CustomerOverdueNotification}}
                                            </span>
                                            <i class="fa fa-dollar"></i>
                                            {{'CustomerOverdue' | translate}}
                                        </li>
                                    </a>
                                </ul>
                            </li>
                        </ul> -->
                    </li>
                    <li class="dropdown">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-language"></i>
                            {{'Language' | translate}}
                        </a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li class="title">
                                <a data-ng-click="changeLanguage('kh')" href="">
                                    <img src="images/kh.gif" width="25px" />
                                    {{ 'TextKhmer' | translate}}
                                </a>
                            </li>
                            <li class="title">
                                <a data-ng-click="changeLanguage('en')" href="">
                                    <img src="images/en.gif" width="25px" />
                                    {{ 'TextEnglish' | translate}}
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown profile">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span id="user">
                                <?php
                                    echo $_SESSION['user_name'];
                                ?>
                            </span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu animated fadeInDown">
                            <li>
                                <div class="profile-info">
                                    <span ng-bind="'Welcome' | translate"></span>:
                                    <span class="welcome">
                                        <?php
                                            echo $_SESSION['user_name'];
                                        ?>
                                    </span>
                                    <br/><br/>
                                    <div class="btn-group margin-bottom-2x" role="group">
                                        <!--<a ui-sref="company-profile" class="btn btn-success btn-sm">
                                            <i class="fa fa-cog"></i>
                                            {{'CompanyProfile' | translate}}
                                        </a>-->
                                        <a href="logoff.php" class="btn btn-primary btn-sm">
                                            <i class="fa fa-sign-out"></i>
                                            {{'Logout' | translate}}
                                        </a>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="side-menu sidebar-inverse">
            <nav class="navbar navbar-default" role="navigation" ng-if="featureRole">
                <div class="side-menu-container">
                    <div class="navbar-header">
                        <a class="navbar-brand">
                            <div class="icon fa fa fa-institution"></div>
                            <div class="title">
                                <span translate="Menu"></span>
                            </div>
                        </a>
                        <!--<button type="button" class="navbar-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>-->
                    </div>
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a ui-sref="dashboard">
                                <span class="icon fa fa-tachometer"></span>
                                <span class="title">
                                    {{'DashBoard' | translate }}
                                </span>
                            </a>
                        </li>
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Customer_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-element" onclick="return false;">
                                <span class="icon fa fa-address-book-o"></span>
                                <span class="title">
                                    {{'Customers' | translate }}
                                </span>
                             </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-element" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Branch_View'" feature="featureRole">
                                            <a ui-sref="branch">
                                                <span translate="Branch"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Type_View'" feature="featureRole">
                                            <a ui-sref="customer_type">
                                                <span translate="CustomerType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_List_View'" feature="featureRole">
                                            <a ui-sref="customer_list">
                                                <span translate="CustomerList"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Report_View'" feature="featureRole">
                                            <a ui-sref="customer_report">
                                                <span translate="CustomerReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Treatment_Report_View'" feature="featureRole">
                                            <a ui-sref="customer_treatment_report">
                                                <span translate="CustomerTreatmentReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Balance_Report_View'" feature="featureRole">
                                            <a ui-sref="customer_balance">
                                                <span translate="CustomerBalanceReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Payment_View'" feature="featureRole">
                                            <a ui-sref="received_payment">
                                                <span translate="CustomerPayment"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Payment_Report_View'" feature="featureRole">
                                            <a ui-sref="account_receivable_summary">
                                                <span translate="CustomerPaymentReport"></span>
                                            </a>
                                        </li>
                                        <!--<li role-permission permission="'Customer_Payment_View'" feature="featureRole">
                                            <a ui-sref="service">
                                                <span translate="SetupService"></span>
                                            </a>
                                        </li>-->
                                        <li role-permission permission="'Appointment_View'" feature="featureRole">
                                            <a ui-sref="appointment">
                                                <span translate="Appointment"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Appointment_Report_View'" feature="featureRole">
                                            <a ui-sref="appointment_report">
                                                <span translate="AppointmentReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Sell_View'" feature="featureRole">
                                            <a ui-sref="stock_out">
                                                <span translate="Sale"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Sell_Summary_Report_View'" feature="featureRole">
                                            <a ui-sref="report_sale_summary">
                                                <span translate="SaleSummaryReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Sell_Detail_Report_View'" feature="featureRole">
                                            <a ui-sref="report_sale">
                                                <span translate="SaleDetailReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Return_Invoice_View'" feature="featureRole">
                                            <a ui-sref="sale_return">
                                                <span translate="SaleReturn"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Return_Invoice_Report_View'" feature="featureRole">
                                            <a ui-sref="sale_return_report">
                                                <span translate="SaleReturnReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Product_Receive_View'" feature="featureRole">
                                            <a ui-sref="product_receive">
                                                <span translate="ProductReceive"></span>
                                            </a>
                                        </li>
                                         <li role-permission permission="'Product_Receive_Report_View'" feature="featureRole">
                                            <a ui-sref="product_receive_report">
                                                <span translate="ProductReceiveReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Invoice_Return_View'" feature="featureRole">
                                            <a ui-sref="invoice_return">
                                                <span translate="InvoicReturn"></span>
                                            </a>
                                        </li>
                                         <li role-permission permission="'Invoice_Return_Report_View'" feature="featureRole">
                                            <a ui-sref="invoice_return_report">
                                                <span translate="InvoiceReturnReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Service_Type_View'" feature="featureRole">
                                            <a ui-sref="service_type">
                                                <span translate="ServiceType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Service_List_View'" feature="featureRole">
                                            <a ui-sref="service_list">
                                                <span translate="ServiceList"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Service_Report_View'" feature="featureRole">
                                            <a ui-sref="service_report">
                                                <span translate="ServiceReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Service_Transaction_Report_View'" feature="featureRole">
                                            <a ui-sref="service_transaction_report">
                                                <span translate="ServiceTransactionReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Expense_Type_View'" feature="featureRole">
                                            <a ui-sref="expense_type">
                                                <span translate="ExpenseType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Expense_View'" feature="featureRole">
                                            <a ui-sref="expense">
                                                <span translate="Expense"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Expense_Report_View'" feature="featureRole">
                                            <a ui-sref="expense_report">
                                                <span translate="ExpenseReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Doctor_Expense_View'" feature="featureRole">
                                            <a ui-sref="doctor_expense">
                                                <span translate="StaffExpense"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Doctor_Expense_Report_View'" feature="featureRole">
                                            <a ui-sref="doctor_expense_report">
                                                <span translate="StaffExpenseReport"></span>
                                            </a>
                                        </li>
                                        <!--<li role-permission permission="'Customer_Payment_View'" feature="featureRole">
                                            <a ui-sref="create_invoice">
                                                Issue Invoice
                                            </a>
                                        </li>
                                        <li role-permission permission="'Customer_Payment_View'" feature="featureRole">
                                            <a ui-sref="report_invoice">
                                                Issue Invoice Report
                                            </a>
                                        </li>-->
                                        <!--<li role-permission permission="'Customer_Payment_View'" feature="featureRole">
                                            <a ui-sref="account_payable">
                                                Account Payable Report
                                            </a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="panel panel-default dropdown"
                             role-permission permission="'Doctor_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-doctor" onclick="return false;">
                                <span class="icon fa fa-address-card-o"></span>
                                <span class="title">
                                    <span translate="SetupStaff"></span>
                                </span>
                            </a>
<!--                            Dropdown level 1-->
                            <div id="dropdown-doctor" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Doctor_Type_View'" feature="featureRole">
                                            <a ui-sref="doctor_type">
                                                <span translate="StaffType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Doctor_List_View'" feature="featureRole">
                                            <a ui-sref="doctor_list">
                                                <span translate="StaffList"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Staff_Report_View'" feature="featureRole">
                                            <a ui-sref="doctor_report">
                                                <span translate="StaffReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Allowance_Deduction_View'" feature="featureRole">
                                            <a ui-sref="allowance_deduction">
                                                <span translate="AllowanceDeduction"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Doctor_Payslip_View'" feature="featureRole">
                                            <a ui-sref="doctor_payslip">
                                                <span translate="StaffPayslip"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Doctor_Payslip_Report'" feature="featureRole">
                                            <a ui-sref="doctor_payslip_report">
                                                <span translate="StaffPayslipReport"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="panel panel-default dropdown"
                             role-permission permission="'Vendor_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-form" onclick="return false;">
                                <span class="icon fa fa-users"></span>
                                <span class="title">
                                    <span translate="SetupVendor"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-form" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Vendor_Type_View'" feature="featureRole">
                                            <a ui-sref="vendor_type">
                                                <span translate="VendorType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Vendor_List_View'" feature="featureRole">
                                            <a ui-sref="vendor_list">
                                                <span translate="VendorList"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Vendor_Report_View'" feature="featureRole">
                                            <a ui-sref="vendor_report">
                                                <span translate="ReportVendor"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Vendor_Payment_View'" feature="featureRole">
                                            <a ui-sref="pay_bill">
                                                <span translate="VendorPayment"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Vendor_Payment_Report_View'" feature="featureRole">
                                            <a ui-sref="account_payable_summary">
                                                <span translate="VendorPaymentReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Vendor_Balance_Report_View'" feature="featureRole">
                                            <a ui-sref="vendor_balance">
                                                <span translate="VendorBalanceReport"></span>
                                            </a>
                                        </li>
<!--                                        <li>-->
<!--                                            <a ui-sref="purchase_order">-->
<!--                                                <span translate="PurchaseOrder"></span>-->
<!--                                            </a>-->
<!--                                        </li>-->
                                        <li role-permission permission="'Purchase_View'" feature="featureRole">
                                            <a ui-sref="purchase">
                                                <span translate="Purchase"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Purchase_Summary_Report_View'" feature="featureRole">
                                            <a ui-sref="report_purchase_summary">
                                                <span translate="PurchaseSummaryReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Purchase_Detail_Report_View'" feature="featureRole">
                                            <a ui-sref="report_purchase">
                                                <span translate="PurchaseDetailReport"></span>
                                            </a>
                                        </li>
                                        <!--<li role-permission permission="'Account_Payable_Report_View'" feature="featureRole">
                                            <a ui-sref="account_payable">
                                                <span translate="AccountPayableReport"></span>
                                            </a>
                                        </li>-->
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Setup Item-->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Item_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#component-example" onclick="return false;">
                                <span class="icon fa fa-cubes"></span>
                                <span class="title">
                                    <span translate="SetupItem"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="component-example" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                         <li role-permission permission="'UM_Type_View'" 
                                            feature="featureRole">
                                            <a ui-sref="um_type">
                                                <span translate="UMType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Product_Type_View'" feature="featureRole">
                                            <a ui-sref="product_type">
                                                <span translate="ProductType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Product_List_View'" feature="featureRole">
                                            <a ui-sref="product_list">
                                                <span translate="ProductList"></span>
                                            </a>
                                        </li>                                        
                                        <!--<li role-permission permission="'Product_Sale_View'" feature="featureRole">
                                            <a ui-sref="product_sale_list">
                                                <span translate="ProductSale"></span>
                                            </a>
                                        </li>-->
                                        <li role-permission permission="'Product_Report_View'" feature="featureRole">
                                            <a ui-sref="product_report">
                                                <span translate="ProductReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Stock_Report_View'" feature="featureRole">
                                            <a ui-sref="stock_report">
                                                <span translate="StockReport"></span>
                                            </a>
                                        </li>
                                         <li role-permission permission="'Transfer_Type_View'" feature="featureRole">
                                            <a ui-sref="transfer_type">
                                                <span translate="TransferType"></span>
                                            </a>
                                        </li>
                                         <li role-permission permission="'Product_Transfer_View'" feature="featureRole">
                                            <a ui-sref="product_transfer">
                                                <span translate="ProductTransfer"></span>
                                            </a>
                                        </li>
                                         <li role-permission permission="'Product_Transfer_Report_View'" feature="featureRole">
                                            <a ui-sref="product_transfer_report">
                                                <span translate="ProductTransferReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Stock_Adjustment_View'" feature="featureRole">
                                            <a ui-sref="stock_adjustment">
                                                <span translate="StockAdjustment"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Stock_Adjustment_Report_View'" feature="featureRole">
                                            <a ui-sref="stock_adjustment.report">
                                                <span translate="StockAdjustmentReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Import_Product_View'" feature="featureRole">
                                            <a ui-sref="import_product">
                                                <span translate="ImportProduct"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <?php /*
                        <!-- Dropdown-->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Accounting_Report_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-example" onclick="return false;">
                                <span class="icon fa fa-slack"></span>
                                <span class="title">
                                    <span translate="AccountReport"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-example" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Balance_Sheet_View'" feature="featureRole">
                                            <a ui-sref="balance_sheet">
                                                <span translate="BalanceSheet"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Case_Flow_View'" feature="featureRole">
                                            <a ui-sref="cash_flow">
                                                <span translate="CashFlow"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Journal_View'" feature="featureRole">
                                            <a ui-sref="journal_report">
                                                <span translate="Journal"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'General_Ledger_View'" feature="featureRole">
                                            <a ui-sref="ledger_report">
                                                <span translate="GeneralLedger"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Income_Statement_View'" feature="featureRole">
                                            <a ui-sref="income_statement">
                                                <span translate="IncomeStatement"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Dropdown-->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Accounting_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-icon" onclick="return false;">
                                <span class="icon fa fa-archive"></span>
                                <span class="title">
                                    <span translate="Accounting"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-icon" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Account_Type_View'" feature="featureRole">
                                            <a ui-sref="account_type">
                                                <span translate="AccountType"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Account_Chart_View'" feature="featureRole">
                                            <a ui-sref="chart_account">
                                                <span translate="ChartAccount"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Journal_Entry_View'" feature="featureRole">
                                            <a ui-sref="journal_entry">
                                                <span translate="JournalEntry"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        */ ?>
                        <li class="panel panel-default dropdown"
                             role-permission permission="'Administrator_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#dropdown-user" onclick="return false;">
                                <span class="icon fa fa-user-circle-o"></span>
                                <span class="title">
                                    <span translate="Administrator"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="dropdown-user" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'User_View'" feature="featureRole">
                                            <a ui-sref="user">
                                                <span translate="User"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Role_View'" feature="featureRole">
                                            <a ui-sref="role">
                                                <span translate="Role"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Setting_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#appSetting" onclick="return false;">
                                <span class="icon fa fa-cog"></span>
                                <span class="title">
                                    <span translate="Setting"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="appSetting" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Exchange_Rate_View'" feature="featureRole">
                                            <a ui-sref="exchange-rate">
                                                <span translate="ExchangeRate"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Company_Profile_View'" feature="featureRole">
                                            <a ui-sref="company-profile">
                                                <span translate="CompanyProfile"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <!-- Report by Staff -->
                        <li class="panel panel-default dropdown"
                            role-permission permission="'Report_Pages'" feature="featureRole">
                            <a data-toggle="collapse" href="#report" onclick="return false;">
                                <span class="icon fa fa-file-text-o"></span>
                                <span class="title">
                                    <span translate="Report"></span>
                                </span>
                            </a>
                            <!-- Dropdown level 1 -->
                            <div id="report" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="nav navbar-nav">
                                        <li role-permission permission="'Income_Statement_Report_View'" feature="featureRole">
                                            <a ui-sref="income_statement">
                                                <span translate="IncomeStatement"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Daily_Case_Report'" feature="featureRole">
                                            <a ui-sref="daily_case_report">
                                                <span translate="CashFlowReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Appointment_Report_View'" feature="featureRole">
                                            <a ui-sref="report-appointment-by-staff">
                                                <span translate="MyAppointment"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'My_Expense_Report_View'" feature="featureRole">
                                            <a ui-sref="report-expense-staff">
                                                <span translate="MyExpenseReport"></span>
                                            </a>
                                        </li>
                                        <li role-permission permission="'Payslip_Report_View'" feature="featureRole">
                                            <a ui-sref="report-payslip-staff">
                                                <span translate="Payslip"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        
                        <li>
                            <a ui-sref="license">
                                <span class="icon fa fa-book"></span>
                                <span class="title">
                                    <span translate="License"></span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </div>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">