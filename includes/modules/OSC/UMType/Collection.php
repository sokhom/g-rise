<?php

namespace OSC\UMType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('um_type', 'um');
		$this->idField = 'um.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("um.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("um.name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("um.status = '" . (int)$arg. "' ");
	}
	
	public function sortByName($arg){
		$this->addOrderBy('um.name', $arg);
	}
}
