<?php

namespace OSC\ReceiveProductDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('receive_product_detail', 'rp');
		$this->idField = 'rp.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("rp.id = '" . (int)$arg. "' ");
	}

	public function filterByReceiveProductId( $arg ){
		$this->addWhere("rp.receive_product_id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("rp.status = '" . (int)$arg. "' ");
	}

}
