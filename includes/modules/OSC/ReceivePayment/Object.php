<?php
namespace OSC\ReceivePayment;
use
    Aedea\Core\Database\StdObject as DbObj,
    OSC\ReceivePaymentDetail\Collection as ReceivePaymentDetailCol
;

class Object extends DbObj{
     protected
        $customerName,
        $customerId,
        $customerTelephone,
        $customerCode,
        $receivePaymentNo,
        $receivePaymentDate,
        $note,
        $discountType,
        $discountAmount,
        $discountTotalAmount,
        $grandTotal,
        $paymentMethod,
        $bankCharge,
        $detail,
        $exchangeId,
        $subTotal,
        $creator,
        $payment,
        $remain,
        $lastOverdue,
        $customerStaffResponsible,
        $customerStaffResponsibleId,
        $customerShootDate,
        $customerEventDate,
        $termCondition
     ;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new ReceivePaymentDetailCol();
    }

    public function toArray($params=array()){
        $args= array(
            'include'=>array(
                'id',
                'customer_id',
                'term_condition',
                'customer_name',
                'customer_telephone',
                'customer_code',
                'bank_charge',
                'receive_payment_date',
                'receive_payment_no',
                'note',
                'last_overdue',
                'discount_type',
                'discount_amount',
                'creator',
                'discount_total_amount',
                'payment_method',
                'detail',
                'grand_total',
                'sub_total',
                'payment',
                'create_by',
                'remain',
                'status',
                'create_date',
                'update_name',
                'update_date',
                'exchange_id'
            )
        );
        return parent::toArray($args);
    }
    public function load( $params = array() ){
        $q = $this->dbQuery("
			SELECT
                customer_id,
                customer_name,
                customer_telephone,
                term_condition,
                customer_code,
                receive_payment_date,
                receive_payment_no,
                note,
                discount_type,
                exchange_id,
                discount_amount,
                discount_total_amount,
                payment_method,
                create_by as creator,
                bank_charge,
                last_overdue,
                grand_total,
                sub_total,
                payment,
                status,
                remain,
                create_date,
                update_by as update_name,
                modify_date as update_date
			FROM
				receive_payment s
			WHERE
				id = '" . (int)$this->getId() . "'
		");

        if( ! $this->dbNumRows($q) ){
            throw new \Exception(
                "404: Receive Payment not found",
                404
            );
        }
        $this->setProperties($this->dbFetchArray($q));

        $this->detail->setFilter('receive_payment_id', $this->getId());
        $this->detail->populate();
    }

    public function insert(){
        $this->dbQuery("
			INSERT INTO
				receive_payment
			(
                term_condition,
				customer_id,
                customer_name,
                customer_telephone,
                customer_code,
                customer_shoot_date,
                customer_event_date,
                customer_staff_responsible,
                customer_staff_responsible_id,
                exchange_id,
                receive_payment_date,
                receive_payment_no,
                note,
                grand_total,
                sub_total,
                payment,
                remain,
                discount_type,
                discount_amount,
                discount_total_amount,
                payment_method,
                bank_charge,
                last_overdue,
                create_by,
                create_date
			)
				VALUES
			(
                '" . $this->dbEscape( $this->getTermCondition() ) . "',
                '" . $this->getCustomerId() . "',
                '" . $this->getCustomerName() . "',
                '" . $this->getCustomerTelephone() . "',
                '" . $this->getCustomerCode() . "',
                '" . $this->getCustomerShootDate() . "',
                '" . $this->getCustomerEventDate() . "',
                '" . $this->getCustomerStaffResponsible() . "',
                '" . $this->getCustomerStaffResponsibleId() . "',
				'" . $this->getExchangeId() . "',
				'" . $this->getReceivePaymentDate() . "',
				'" . $this->getReceivePaymentNo() . "',
				'" . $this->getNote() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getSubTotal() . "',
				'" . $this->getPayment() . "',
				'" . $this->getRemain() . "',
				'" . $this->getDiscountType() . "',
				'" . $this->getDiscountAmount() . "',
				'" . $this->getDiscountTotalAmount() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getBankCharge() . "',
				'" . $this->getLastOverdue() . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
        $this->setId( $this->dbInsertId() );
    }

    public function update(){
        $this->dbQuery("
			UPDATE
				receive_payment
			SET
                remain = '" . $this->getRemain() . "',
                payment = payment + '" . $this->getPayment() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function delete(){
        if (!$this->getReceivePaymentNo()) {
            throw new Exception("delete method requires receive_payment_no to be set");
        }
        $this->dbQuery("
			DELETE FROM
				receive_payment
			WHERE
                receive_payment_no = '" . $this->getReceivePaymentNo() . "'
        ");
        
        $this->dbQuery("
			DELETE FROM
				receive_payment_detail
			WHERE
                receive_payment_id = '" . $this->getId() . "'
        ");

        $this->dbQuery("
            DELETE FROM
                cash_flow
            WHERE
                invoice_no = '" . $this->getReceivePaymentNo() . "'
        ");


    }

    public function updateStatus(){
        $this->dbQuery("
			UPDATE
				receive_payment
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
        ");
        
		// update case flow doctor
		$this->dbQuery("
        UPDATE
            cash_flow
        SET
            status = '" . (int)$this->getStatus() . "',
            update_by = '" . $this->getUpdateBy() . "'
        WHERE
            invoice_id = '" . (int)$this->getId() . "'
    ");
    }

	public function setCreator( $string ){
		$this->creator = $string;
	}

	public function getCreator(){
		return $this->creator;
    }
    
	public function setLastOverdue( $string ){
		$this->lastOverdue = $string;
	}

	public function getLastOverdue(){
		return $this->lastOverdue;
    }
    
	public function setCustomerTelephone( $string ){
		$this->customerTelephone = $string;
	}

	public function getCustomerTelephone(){
		return $this->customerTelephone;
	}

	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}

	public function getCustomerCode(){
		return $this->customerCode;
    }
    
    public function setBankCharge( $string ){
        $this->bankCharge = doubleval($string);
    }
    public function getBankCharge(){
        return $this->bankCharge;
    }

    public function setCustomerId($string){
        $this->customerId = (int)$string;
    }
    public function getCustomerId(){
        return $this->customerId;
    }

    public function setDetail($string){
        $this->detail = $string;
    }
    public function getDetail(){
        return $this->detail;
    }

    public function setReceivePaymentDate($date){
        $this->receivePaymentDate = $date;
    }
    public function getReceivePaymentDate(){
        return $this->receivePaymentDate;
    }

    public function setCustomerName($string){
        $this->customerName = $string;
    }
    public function getCustomerName(){
        return $this->customerName;
    }

    public function setReceivePaymentNo($string){
        $this->receivePaymentNo = $string;
    }
    public function getReceivePaymentNo(){
        return $this->receivePaymentNo;
    }

    public function setNote($string){
        $this->note = $string;
    }
    public function getNote(){
        return $this->note;
    }

    public function setPaymentMethod($string){
        $this->paymentMethod = $string;
    }
    public function getPaymentMethod(){
        return $this->paymentMethod;
    }

    public function setDiscountType($string){
        $this->discountType = $string;
    }
    public function getDiscountType(){
        return $this->discountType;
    }

    public function setDiscountAmount($string){
        $this->discountAmount = doubleval( $string );
    }
    public function getDiscountAmount(){
        return $this->discountAmount;
    }

    public function setDiscountTotalAmount($string){
        $this->discountTotalAmount = doubleval( $string );
    }
    public function getDiscountTotalAmount(){
        return $this->discountTotalAmount;
    }

    public function setGrandTotal($string){
        $this->grandTotal = doubleval( $string );
    }
    public function getGrandTotal(){
        return $this->grandTotal;
    }

    public function setExchangeId($string){
        $this->exchangeId =(int)$string;
    }
    public function getExchangeId(){
        return $this->exchangeId;
    }

    public function setSubTotal($string){
        $this->subTotal = doubleval( $string );
    }
    public function getSubTotal(){
        return $this->subTotal;
    }

    public function setPayment($string){
        $this->payment = doubleval( $string );
    }
    public function getPayment(){
        return $this->payment;
    }

    public function setRemain($string){
        $this->remain = doubleval( $string );
    }
    public function getRemain(){
        return $this->remain;;
    }

    public function setCustomerEventDate( $string ){
		$this->customerEventDate = $string;
	}
	public function getCustomerEventDate(){
		return $this->customerEventDate;
	}

	public function setCustomerShootDate( $string ){
		$this->customerShootDate = $string;
	}
	public function getCustomerShootDate(){
		return $this->customerShootDate;
	}
	public function setCustomerStaffResponsible( $string ){
		$this->customerStaffResponsible = $string;
	}
	public function getCustomerStaffResponsible(){
		return $this->customerStaffResponsible;
	}

	public function setCustomerStaffResponsibleId( $string ){
		$this->customerStaffResponsibleId = (int)$string;
	}
	public function getCustomerStaffResponsibleId(){
		return $this->customerStaffResponsibleId;
	}

	public function setTermCondition( $string ){
		$this->termCondition = $string;
	}
	public function getTermCondition(){
		return $this->termCondition;
	}

    
}
