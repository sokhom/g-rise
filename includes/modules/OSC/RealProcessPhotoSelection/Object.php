<?php

namespace OSC\RealProcessPhotoSelection;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$realProcessId
		, $basic
		, $date
		, $description 
		, $additional
		, $retouch
		, $bookCover
		, $bookCoverJson
		, $addOn
		, $addOnJson
		, $slideShow
		, $deadline
		, $extra
		, $title
		, $publishPermission
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'basic',
				'title',
				'date',
				'additional',
				'retouch',
				'deadline',
				'book_cover',
				'publish_permission',
				'add_on',
				'extra',
				'slide_show',
				'description',
				'real_process_id'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				date,
				basic,
				extra,
				publish_permission,
				additional,
				retouch,
				title,
				book_cover_json as book_cover,
				add_on_json as add_on,
				slide_show,
				description,
				deadline,
				real_process_id
			FROM
				real_process_photo_shot_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process photo selection shot sep 1 not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete(){
		if( !$this->getRealProcessId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process_photo_shot_detail
			WHERE
				real_process_id = " . $this->getRealProcessId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process_photo_shot_detail
			(
				title,
				basic,
				extra,
				additional,
				retouch,
				publish_permission,
				date,
				deadline,
				book_cover_json,
				add_on_json,
				slide_show,
				description,
				real_process_id,
				create_date,
				create_by
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getTitle()) . "',
				'" . $this->getBasic() . "',
				'" . $this->getExtra() . "',
				'" . $this->getAdditional(). "',
				'" . $this->getRetouch(). "',
				'" . $this->getPublishPermission(). "',
				'" . $this->getDate(). "',
				'" . $this->getDeadline(). "',
				'" . $this->getBookCoverJson(). "',
				'" . $this->getAddOnJson(). "',
				'" . $this->dbEscape( $this->getSlideShow() ) . "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				'" . $this->getRealProcessId(). "',
				NOW(),
				'" . $this->getCreateBy(). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setEditorName($string ){
		$this->editorName = $string;
	}
	public function getEditorName(){
		return $this->editorName;
	}

	public function setPublishPermission($string ){
		$this->publishPermission = (int)$string;
	}
	public function getPublishPermission(){
		return $this->publishPermission;
	}

	public function setDate($string ){
		$this->date = $string;
	}
	public function getDate(){
		return $this->date;
	}

	public function setAddOnJson( $string ){
		$this->addOnJson = json_encode($string);
	}	
	public function getAddOnJson(){
		return $this->addOnJson;
	}
	
	public function setAddOn( $string ){
		$this->addOn = json_decode( $string, true);
	}	
	public function getAddOn(){
		return $this->addOn;
	}


	public function setBookCoverJson( $string ){
		$this->bookCoverJson = json_encode($string);
	}	
	public function getBookCoverJson(){
		return $this->bookCoverJson;
	}

	public function setBookCover( $string ){
		$this->bookCover = json_decode( $string, true);
	}	
	public function getBookCover(){
		return $this->bookCover;
	}

	public function setRealProcessId( $string ){
		$this->realProcessId = (int)$string;
	}	
	public function getRealProcessId(){
		return $this->realProcessId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}	
	public function getDescription(){
		return $this->description;
	}

	public function setCinemaGraphyCompleteTime( $string ){
		$this->cinemaGraphyCompleteTime = $string;
	}	
	public function getCinemaGraphyCompleteTime(){
		return $this->cinemaGraphyCompleteTime;
	}

	public function setCinemaGraphyCompleteDate( $string ){
		$this->cinemaGraphyCompleteDate = $string;
	}	
	public function getCinemaGraphyCompleteDate(){
		return $this->cinemaGraphyCompleteDate;
	}

	public function setRetouch( $string ){
		$this->retouch = (int)$string;
	}	
	public function getRetouch(){
		return $this->retouch;
	}
	
	public function setAdditional( $string ){
		$this->additional = (int)$string;
	}
	public function getAdditional(){
		return $this->additional;
	}

	public function setBasic( $string ){
		$this->basic = (int)$string;
	}	
	public function getBasic(){
		return $this->basic;
	}

	public function setExtra( $string ){
		$this->extra = (int)$string;
	}	
	public function getExtra(){
		return $this->extra;
	}
	
	public function setSlideShow( $string ){
		$this->slideShow = $string;
	}	
	public function getSlideShow(){
		return $this->slideShow;
	}
	
	public function setTitle( $string ){
		$this->title = $string;
	}	
	public function getTitle(){
		return $this->title;
	}

	public function setDeadline( $string ){
		$this->deadline = $string;
	}	
	public function getDeadline(){
		return $this->deadline;
	}
}
