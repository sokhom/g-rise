<?php

namespace OSC\PackageAddOn;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\AddOns\Collection as  addOnCol
;

class Object extends DbObj {
		
	protected
		$packageId
		, $addOnId
		, $description
		, $detail
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new addOnCol();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'add_on_id',
				'description',
				'detail',
			)
		);
		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				add_on_id,
				description
			FROM
				packages_add_on
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: packages add on not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('id', $this->getAddOnId());
        $this->detail->populate();

	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages_add_on
			SET
				add_on_id = '" . $this->getAddOnId() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages_add_on
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function deleteByPackage(){
		if( !$this->getPackageId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				packages_add_on
			WHERE
				package_id = " . $this->getPackageId() . "
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				packages_add_on
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				packages_add_on
			(
				add_on_id,
				package_id,
				description
			)
				VALUES
			(
				'" . $this->getAddOnId() . "',
				'" . $this->getPackageId() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setPackageId( $string ){
		$this->packageId = (int)$string;
	}	
	public function getPackageId(){
		return $this->packageId;
	}

	public function setAddOnId( $string ){
		$this->addOnId = doubleval($string);
	}	
	public function getAddOnId(){
		return $this->addOnId;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}	
	public function getDetail(){
		return $this->detail;
	}

}
