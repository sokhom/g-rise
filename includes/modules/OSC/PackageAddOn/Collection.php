<?php

namespace OSC\PackageAddOn;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('packages_add_on', 'pkao');
		$this->idField = 'pkao.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("pkao.id = '" . (int)$arg. "' ");
	}
	
	public function filterByPackageId( $arg ){
		$this->addWhere("pkao.package_id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("pkao.name LIKE '%" . $arg. "%' or pkao.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pkao.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('pkao.name', $arg);
	}


}
