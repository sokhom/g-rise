<?php

namespace OSC\RealProcessPhotoShot;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('real_process_photo_selection', 'r');
		$this->idField = 'r.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("r.id = '" . (int)$arg. "' ");
	}

	public function filterByRealProcessId( $arg ){
			$this->addWhere("r.real_process_id = '" . (int)$arg. "' ");
	}

	public function filterByInvoice( $arg ){
		$this->addWhere("r.invoice_no LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("r.status = '" . (int)$arg. "' ");
	}


}
