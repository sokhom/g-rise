<?php

namespace OSC\RealProcessPhotoShot;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$realProcessId
		, $description 
		, $photoStartDate
		, $photoCompleteTime
		, $photoCompleteDate
		, $photoRetouch
		, $photoEditor
		, $videoStartDate
		, $videoCompleteDate
		, $videoCompleteTime
		, $videoEditor
		, $albumDate
		, $albumEditor
		, $albumPrintOrder
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'photo_start_date',
				'photo_complete_time',
				'photo_complete_date',
				'photo_retouch',
				'photo_editor',
				'video_start_date',
				'video_complete_date',
				'video_complete_time',
				'video_editor',
				'album_date',
				'album_editor',
				'album_print_order',
				'description',
				'real_process_id'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				photo_start_date,
				photo_complete_time,
				photo_complete_date,
				photo_retouch,
				photo_editor,
				video_start_date,
				video_complete_date,
				video_complete_time,
				video_editor,
				album_date,
				album_editor,
				album_print_order,
				description,
				real_process_id
			FROM
				real_process_photo_selection
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process photo selection not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete(){
		if( !$this->getRealProcessId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process_photo_selection
			WHERE
				real_process_id = " . $this->getRealProcessId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process_photo_selection
			(
				photo_start_date,
				photo_complete_time,
				photo_complete_date,
				photo_retouch,
				photo_editor,
				video_start_date,
				video_complete_date,
				video_complete_time,
				video_editor,
				album_date,
				album_editor,
				album_print_order,
				description,
				real_process_id,
				create_date,
				create_by
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getPhotoStartDate() ). "',
				'" . $this->dbEscape( $this->getPhotoCompleteTime() ) . "',
				'" . $this->dbEscape( $this->getPhotoCompleteDate()) . "',
				'" . $this->dbEscape( $this->getPhotoRetouch() ). "',
				'" . $this->dbEscape( $this->getPhotoEditor() ). "',
				'" . $this->dbEscape( $this->getVideoStartDate() ). "',
				'" . $this->dbEscape( $this->getVideoCompleteDate() ). "',
				'" . $this->dbEscape( $this->getVideoCompleteTime() ). "',
				'" . $this->dbEscape( $this->getVideoEditor() ). "',
				'" . $this->dbEscape( $this->getAlbumDate() ). "',
				'" . $this->dbEscape( $this->getAlbumEditor() ). "',
				'" . $this->dbEscape( $this->getAlbumPrintOrder() ). "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				'" . $this->getRealProcessId(). "',
				NOW(),
				'" . $this->getCreateBy(). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setPhotoCompleteDate($string ){
		$this->photoCompleteDate = $string;
	}
	public function getPhotoCompleteDate(){
		return $this->photoCompleteDate;
	}

	public function setPhotoCompleteTime($string ){
		$this->photoCompleteTime = $string;
	}
	public function getPhotoCompleteTime(){
		return $this->photoCompleteTime;
	}

	public function setPhotoEditor( $string ){
		$this->photoEditor = $string;
	}	
	public function getPhotoEditor(){
		return $this->photoEditor;
	}

	public function setPhotoRetouch( $string ){
		$this->photoRetouch = $string;
	}	
	public function getPhotoRetouch(){
		return $this->photoRetouch;
	}
	
	public function setPhotoStartDate( $string ){
		$this->photoStartDate = $string;
	}	
	public function getPhotoStartDate(){
		return $this->photoStartDate;
	}

	public function setVideoCompleteDate( $string ){
		$this->videoCompleteDate = $string;
	}	
	public function getVideoCompleteDate(){
		return $this->videoCompleteDate;
	}

	public function setVideoCompleteTime( $string ){
		$this->videoCompleteTime = $string;
	}	
	public function getVideoCompleteTime(){
		return $this->videoCompleteTime;
	}

	public function setRealProcessId( $string ){
		$this->realProcessId = (int)$string;
	}	
	public function getRealProcessId(){
		return $this->realProcessId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}	
	public function getDescription(){
		return $this->description;
	}
	
	public function setVideoEditor( $string ){
		$this->videoEditor = $string;
	}	
	public function getVideoEditor(){
		return $this->videoEditor;
	}

	public function setVideoStartDate( $string ){
		$this->videoStartDate = $string;
	}	
	public function getVideoStartDate(){
		return $this->videoStartDate;
	}
	
	public function setAlbumDate( $string ){
		$this->albumDate = $string;
	}	
	public function getAlbumDate(){
		return $this->albumDate;
	}
	
	public function setAlbumEditor( $string ){
		$this->albumEditor = $string;
	}	
	public function getAlbumEditor(){
		return $this->albumEditor;
	}

	public function setAlbumPrintOrder( $string ){
		$this->albumPrintOrder = $string;
	}	
	public function getAlbumPrintOrder(){
		return $this->albumPrintOrder;
	}

}
