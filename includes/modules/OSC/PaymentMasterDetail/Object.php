<?php

namespace OSC\PaymentMasterDetail;

use
	Aedea\Core\Database\StdObject as DbObj
	;

class Object extends DbObj {

	protected
		$paymentId
		, $paymentNo
		, $vendorId
		, $balance
		, $vendorName
		, $purchaseNo
		, $purchaseDate
	;

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'payment_id',
				'payment_no',
				'vendor_id',
				'purchase_no',
				'purchase_date',
				'vendor_name',
				'balance',
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				payment_id,
				payment_no,
				purchase_date,
				purchase_no,
				vendor_id,
				balance,
				vendor_name
			FROM
				payment_master_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");

		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Payment Master Detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				products_type
			SET
				name = '" . $this->getName() . "',
				description = '" . $this->getDescription() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				products_type
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				payment_master_detail
			(
				payment_id,
				payment_no,
				purchase_no,
				purchase_date,
				vendor_id,
				balance,
				vendor_name,
				create_date
			)
				VALUES
			(
				'" . $this->getPaymentId() . "',
				'" . $this->getPaymentNo() . "',
				'" . $this->getPurchaseNo() . "',
				'" . $this->getPurchaseDate() . "',
				'" . $this->getVendorId() . "',
				'" . $this->getBalance() . "',
				'" . $this->getVendorName() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setPurchaseNo( $string ){
		$this->purchaseNo = $string;
	}
	public function getPurchaseNo(){
		return $this->purchaseNo;
	}

	public function setPurchaseDate( $string ){
		$this->purchaseDate = $string;
	}
	public function getPurchaseDate(){
		return $this->purchaseDate;
	}

	public function setVendorId( $string ){
		$this->vendorId = $string;
	}
	public function getVendorId(){
		return $this->vendorId;
	}

	public function setPaymentId( $string ){
		$this->paymentId = $string;
	}
	public function getPaymentId(){
		return $this->paymentId;
	}

	public function setPaymentNo( $string ){
		$this->paymentNo = $string;
	}
	public function getPaymentNo(){
		return $this->paymentNo;
	}

	public function setVendorName( $string ){
		$this->vendorName = $string;
	}
	public function getVendorName(){
		return $this->vendorName;
	}

	public function setBalance( $string ){
		if($string < 0){
			$string = 0;
		}
		$this->balance = $string;
	}
	public function getBalance(){
		return $this->balance;
	}

}
