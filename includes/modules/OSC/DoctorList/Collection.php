<?php

namespace OSC\DoctorList;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('doctor_list', 'dl');
		$this->idField = 'dl.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		if($arg){
			$this->addWhere("dl.name LIKE '%" . $arg. "%' OR dl.phone LIKE '%" . $arg. "%' ");
		}
	}

	public function filterByType( $arg ){
		$this->addWhere("dl.doctor_type_id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("dl.status = '" . (int)$arg. "' ");
	}
	
	public function filterByUseFiler( $arg ){
		$this->addWhere("dl.use_filter = '" . (int)$arg. "' ");
	}

	public function filterByBirthday(){
		$this->addWhere("DAY(dl.dob) = DAY(NOW()) AND MONTH(dl.dob) = MONTH(NOW())");
	}

	public function filterById( $arg ){
		$this->addWhere("dl.id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('dl.name', $arg);
	}
}
