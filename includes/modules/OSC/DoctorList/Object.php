<?php

namespace OSC\DoctorList;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\DoctorType\Collection
		as  DoctorTypeCol
;

class Object extends DbObj {
		
	protected
		$name
		, $detail
		, $sex
		, $address
		, $phone
		, $doctorTypeId
		, $doctorType
		, $email
		, $basicSalary
		, $dob
		, $useFilter
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'sex',
				'detail',
				'email',
				'doctor_type_id',
				'doctor_type',
				'address',
				'dob',
				'status',
				'basic_salary',
				'phone',
				'use_filter'
			)
		);

		return parent::toArray($args);
	}

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->doctorType = new DoctorTypeCol();
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				detail,
				sex,
				use_filter,
				address,
				phone,
				dob,
				email,
				doctor_type_id,
				basic_salary,
				status
			FROM
				doctor_list
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Doctor List not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->doctorType->setFilter('id', $this->getDoctorTypeId());
		$this->doctorType->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_list
			SET
				name = '" . $this->getName() . "',
				email = '" . $this->getEmail() . "',
				dob = '" . $this->getDob() . "',
				detail = '" . $this->getDetail() . "',
				use_filter = '" . $this->getUseFilter() . "',
				sex = '" . $this->getSex() . "',
				basic_salary = '" . $this->getBasicSalary() . "',
				phone = '" . $this->getPhone() . "',
				address = '" . $this->getAddress() . "',
				doctor_type_id = '" . $this->getDoctorTypeId() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_list
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				doctor_list
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_list
			(
				name,
				basic_salary,
				dob,
				detail,
				sex,
				phone,
				email,
				address,
				doctor_type_id,
				use_filter,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getName() ). "',
				'" . $this->getBasicSalary() . "',
				'" . $this->getDob() . "',
				'" . $this->dbEscape($this->getDetail() ). "',
				'" . $this->getSex() . "',
				'" . $this->getPhone() . "',
				'" . $this->getEmail() . "',
				'" . $this->dbEscape( $this->getAddress()) . "',
				'" . $this->getDoctorTypeId() . "',
				'" . $this->getUseFilter() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setUseFilter( $string ){
		$this->useFilter = (int)$string;
	}

	public function getUseFilter(){
		return $this->useFilter;
	}

	public function setDob( $string ){
		$this->dob = $string;
	}

	public function getDob(){
		return $this->dob;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}

	public function setBasicSalary( $string ){
		$this->basicSalary = doubleval($string);
	}

	public function getBasicSalary(){
		return $this->basicSalary;
	}

	public function setName( $string ){
		$this->name = $string;
	}
	
	public function getName(){
		return $this->name;
	}

	public function setAddress( $string ){
		$this->address = $string;
	}

	public function getAddress(){
		return $this->address;
	}

	public function setPhone( $string ){
		$this->phone = $string;
	}

	public function getPhone(){
		return $this->phone;
	}

	public function setSex( $string ){
		$this->sex = $string;
	}

	public function getSex(){
		return $this->sex;
	}

	public function setDoctorTypeId( $string ){
		$this->doctorTypeId = (int)$string;
	}

	public function getDoctorTypeId(){
		return $this->doctorTypeId;
	}

	public function setDoctorType( $string ){
		$this->doctorType = $string;
	}

	public function getDoctorType(){
		return $this->doctorType;
	}

	public function setEmail( $string ){
		$this->email = $string;
	}

	public function getEmail(){
		return $this->email;
	}
}
