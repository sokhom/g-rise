<?php

namespace OSC\RealProcessMaster;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\RealProcessDetail\Collection as PDetail,
	OSC\RealProcessMission\Collection as MissionDetail,
	OSC\RealProcessPhotoSelection\Collection as PhotoSelectionDetail,
	OSC\RealProcessPhotoShot\Collection as PhotoProcessDetail,
	OSC\RealProcessDelevery\Collection as DeleveryDetail,
	OSC\Invoice\Collection as invoiceDetail
;

class Object extends DbObj {
		
	protected
		$invoiceNo
		, $confirmDate
		, $location
		, $shootDate
		, $shootTime
		, $dressSelectDate
		, $description
		, $detail
		, $customerId
		, $invoiceDetail
		, $mission
		, $photoSelect
		, $photoProcess
		, $delevery
		, $locationId
		, $locationName
		, $locationTypeId
		, $locationTypeName
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new PDetail();
		$this->invoiceDetail = new invoiceDetail();
		$this->mission = new MissionDetail();
		$this->photoSelect = new PhotoSelectionDetail();
		$this->delevery = new DeleveryDetail();
		$this->photoProcess = new PhotoProcessDetail();
   }

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'invoice_no',
				'confirm_date',
				'location_id',
				'location_name',
				'location_type_id',
				'location_type_name',
				'shoot_time',
				'description',
				'shoot_date',
				'dress_select_date',
				'customer_id',
				'detail',
				'creator',
				'invoice_detail',
				'mission',
				'delevery',
				'photo_select',
				'photo_process'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				invoice_no,
				confirm_date,
				customer_id,
				shoot_time,
				location_id,
				location_name,
				location_type_id,
				location_type_name,
				shoot_date,
				dress_select_date,
				description,
				create_by as creator,
				status
			FROM
				real_process
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('real_process_id', $this->getId());
 		$this->detail->populate();

		$this->photoSelect->setFilter('real_process_id', $this->getId());
		$this->photoSelect->populate();
		 
		$this->photoProcess->setFilter('real_process_id', $this->getId());
		$this->photoProcess->populate();
		 
		$this->mission->setFilter('real_process_id', $this->getId());
		$this->mission->populate();

		$this->delevery->setFilter('real_process_id', $this->getId());
		$this->delevery->populate();

		$this->invoiceDetail->setFilter('invoice_no_only', $this->getInvoiceNo());
 		$this->invoiceDetail->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				real_process
			SET
				invoice_no = '" . $this->dbEscape(  $this->getInvoiceNo() ). "',
				customer_id = '" . $this->dbEscape(  $this->getCustomerId() ). "',
				confirm_date = '" . $this->getConfirmDate() . "',
				shoot_time = '" . $this->getShootTime() . "',
				shoot_date = '" . $this->getShootDate() . "',
				location_id = '" . $this->getLocationId() . "',
				location_name = '" . $this->getLocationName() . "',
				location_type_id = '" . $this->getLocationTypeId() . "',
				location_type_name = '" . $this->getLocationTypeName() . "',
				dress_select_date = '" . $this->getDressSelectDate() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				real_process
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process
			WHERE
				id = " . $this->getId() . "
		");
		$this->dbQuery("
			DELETE FROM
				real_process_detail
			WHERE
				real_process_id = " . $this->getId() . "
		");

		$this->dbQuery("
			DELETE FROM
				real_process_photo_shot_detail
			WHERE
				real_process_id = " . $this->getId() . "
		");

		$this->dbQuery("
			DELETE FROM
				real_process_photo_shot_detail_add_on
			WHERE
				real_process_id = " . $this->getId() . "
		");

		$this->dbQuery("
			DELETE FROM
				real_process_photo_selection
			WHERE
				real_process_id = " . $this->getId() . "
		");

		$this->dbQuery("
			DELETE FROM
				real_process_staff_mission
			WHERE
				real_process_id = " . $this->getId() . "
		");

		$this->dbQuery("
			DELETE FROM
				real_process_delevery
			WHERE
				real_process_id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process
			(
				invoice_no,
				customer_id,
				confirm_date,
				location_id,
				location_name,
				location_type_id,
				location_type_name,
				shoot_time,
				shoot_date,
				dress_select_date,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getInvoiceNo() ). "',
				'" . $this->getCustomerId(). "',
				'" . $this->getConfirmDate(). "',
				'" . $this->getLocationId(). "',
				'" . $this->getLocationName(). "',
				'" . $this->getLocationTypeId(). "',
				'" . $this->getLocationTypeName(). "',
				'" . $this->getShootTime(). "',
				'" . $this->getShootDate(). "',
				'" . $this->getDressSelectDate(). "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDelevery( $string ){
		$this->delevery = $string;
	}
	public function getDelevery(){
		return $this->delevery;
	}

	public function setMission( $string ){
		$this->mission = $string;
	}
	public function getMission(){
		return $this->mission;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}	
	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

	public function setConfirmDate( $string ){
		$this->confirmDate = $string;
	}	
	public function getConfirmDate(){
		return $this->confirmDate;
	}

	public function setDressSelectDate( $string ){
		$this->dressSelectDate = $string;
	}	
	public function getDressSelectDate(){
		return $this->dressSelectDate;
	}

	public function setLocation( $string ){
		$this->location = $string;
	}	
	public function getLocation(){
		return $this->location;
	}
	
	public function setDetail( $string ){
		$this->detail = $string;
	}	
	public function getDetail(){
		return $this->detail;
	}

	public function setShootDate( $string ){
		$this->shootDate = $string;
	}	
	public function getShootDate(){
		return $this->shootDate;
	}

	public function setShootTime( $string ){
		$this->shootTime = $string;
	}	
	public function getShootTime(){
		return $this->shootTime;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}	
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setInvoiceDetail( $string ){
		$this->invoiceDetail = $string;
	}	
	public function getInvoiceDetail(){
		return $this->invoiceDetail;
	}
	
	public function setPhotoSelect( $string ){
		$this->photoSelect = $string;
	}	
	public function getPhotoSelect(){
		return $this->photoSelect;
	}

	public function setPhotoProcess( $string ){
		$this->photoProcess = $string;
	}	
	public function getPhotoProcess(){
		return $this->photoProcess;
	}

    public function setLocationId( $string ){
        $this->locationId = (int)$string;
    }
    public function getLocationId(){
        return $this->locationId;
    }

    public function setLocationName( $string ){
        $this->locationName = $string;
    }
    public function getLocationName(){
        return $this->locationName;
    }
    
    public function setLocationTypeId( $string ){
        $this->locationTypeId = (int)$string;
    }
    public function getLocationTypeId(){
        return $this->locationTypeId;
    }

    public function setLocationTypeName( $string ){
        $this->locationTypeName = $string;
    }
    public function getLocationTypeName(){
        return $this->locationTypeName;
    }
    
}
