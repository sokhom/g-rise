<?php

namespace OSC\RealProcessMaster;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('real_process', 'pr');
		$this->idField = 'pr.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		if(!$arg) return;
		$this->addWhere("pr.id = '" . (int)$arg. "' ");
	}

	public function filterByInvoice( $arg ){
		$this->addTable("customers", "c");
		// $this->addWhere("pr.invoice_no LIKE '%" . $arg. "%' OR c.customers_first_name LIKE '%" . $arg . "%'  OR c.customers_last_name LIKE '%" . $arg . "%' ");
		$this->addWhere("pr.invoice_no LIKE '%" . $arg. "%' 
			OR CONCAT(c.customers_first_name, ' ', c.customers_last_name) LIKE '%" . $arg . "%' 
		");
		$this->addWhere("c.id = pr.customer_id");
	}
	
	public function filterByDate($start, $end){
		$this->addWhere("pr.create_date BETWEEN '" . $start . "' AND '" . $end . "' ");
	}

	public function filterByCustomer( $arg ){
		$this->addWhere("pr.customer_id = ". (int)$arg. " ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pr.status = '" . (int)$arg. "' ");
	}

	public function sortByInvoiceNo( $arg ){
		$this->addOrderBy('pr.invoice_no', $arg);
	}


}
