<?php

namespace OSC\SaleServiceDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('sale_service_detail', 'ssd');
		$this->idField = 'ssd.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}
	
	
	public function filterByStatus( $arg ){
		$this->addWhere("ssd.status = '" . (int)$arg. "' ");
	}

	public function filterByProductId( $arg ){
		$this->addWhere("ssd.product_id = '" . (int)$arg. "' ");
	}

	public function filterBySaleServiceId( $arg ){
		$this->addWhere("ssd.sale_service_id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy("ssd.id", $arg);
	}
}
