<?php

namespace OSC\SaleServiceDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$saleServiceId,
		$productId,
		$productName,
		$productCode,
		$productCost,
		$qty,
		$unitUse,
		$unitDeductStock,
		$serviceCost,
		$umType
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'sale_service_id',
				'product_id',
				'product_name',
				'product_code',
				'product_cost',
				'qty',
				'unit_use',
				'unit_deduct_stock',
				'um_type',
				'service_cost',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				sale_service_id,
				product_id,
				product_name,
				product_code,
				product_cost,
				qty,
				unit_use,
				um_type,
				unit_deduct_stock,
				service_cost
			FROM
				sale_service_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: sale_service_detail not found",
				404
			);
		}
		
		$this->setProperties($this->dbFetchArray($q));
		
	}
	
	public function insert(){
		$this->dbQuery("
			INSERT INTO
				sale_service_detail
			(
				sale_service_id,
				product_code,
				um_type,
				product_id,
				product_cost,
				product_name,
				qty,
				unit_use,
				unit_deduct_stock,
				service_cost,
				status,
				create_by_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getSaleServiceId() . "',
				'" . $this->getProductCode() . "',
				'" . $this->getUmType() . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductCost() . "',
				'" . $this->getProductName() . "',
				'" . $this->getQty() . "',
				'" . $this->getUnitUse() . "',
				'" . $this->getUnitDeductStock() . "',
				'" . $this->getServiceCost() . "',
				1,
				'" . $this->getCreateById() . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				sale_service_detail
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . $this->getId() . "'
		");
	}

	public function update(){
		$this->dbQuery("
			UPDATE
				sale_service_detail
			SET
                product_id = '" . $this->getProductId() . "',
				product_name = '" . $this->getProductName() . "',
				product_cost = '" . $this->getProductCost() . "',
				qty = '" . $this->getQty() . "',
				unit_use = '" . $this->getUnitUse() . "',
				unit_deduct_stock = '" . $this->getUnitDeductStock() . "',
				service_cost = '" . $this->getServiceCost() . "',
                update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . $this->getId() . "'
		");
	}

	public function delete(){
		$this->dbQuery("
			DELETE FROM
				sale_service_detail
			WHERE
				sale_service_id = '" . $this->getSaleServiceId() . "'
		");
	}

	public function setUmType( $string ){
		$this->umType = $string;
	}
	
	public function getUmType(){
		return $this->umType;
	}

	public function setSaleServiceId( $string ){
		$this->saleServiceId = (int)$string;
	}
	
	public function getSaleServiceId(){
		return $this->saleServiceId;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}
	
	public function getProductId(){
		return $this->productId;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}
	
	public function getProductName(){
		return $this->productName;
	}

	public function setProductCode( $string ){
		$this->productCode = $string;
	}
	
	public function getProductCode(){
		return $this->productCode;
	}

	public function setProductCost( $string ){
		$this->productCost = doubleval($string);
	}
	
	public function getProductCost(){
		return $this->productCost;
	}

	public function setQty( $string ){
		$this->qty = doubleval($string);
	}
	
	public function getQty(){
		return $this->qty;
	}

	public function setUnitUse( $string ){
		$this->unitUse = doubleval($string);
	}
	
	public function getUnitUse(){
		return $this->unitUse;
	}

	public function setServiceCost( $string ){
		$this->serviceCost = doubleval($string);
	}
	
	public function getServiceCost(){
		return $this->serviceCost;
	}

	public function setUnitDeductStock( $string ){
		$this->unitDeductStock = doubleval($string);
	}
	
	public function getUnitDeductStock(){
		return $this->unitDeductStock;
	}
	
}
