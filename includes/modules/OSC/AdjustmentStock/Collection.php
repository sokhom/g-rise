<?php

namespace OSC\AdjustmentStock;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('adjustment_stock', 'ast');
		$this->idField = 'ast.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByDate($from, $to){
		$this->addWhere("ast.adjustment_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function filterByMulti( $arg ){
		$this->addWhere("ast.title LIKE '%" . $arg. "%' OR ast.adjustment_no LIKE '%" . $arg. "%' OR ast.reason LIKE '%" . $arg. "%'");
	}

	public function filterByName( $arg ){
		$this->addWhere("ast.title LIKE '%" . $arg. "%' ");
	}

	public function filterById( $arg ){	
		$this->addWhere("ast.id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("ast.status = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('ast.adjustment_date', $arg);
		$this->addOrderBy('ast.id', $arg);
	}
}
