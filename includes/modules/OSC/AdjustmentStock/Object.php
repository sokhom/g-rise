<?php

namespace OSC\AdjustmentStock;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\AdjustmentStockDetail\Collection as AdjustDetailCol
;

class Object extends DbObj {
		
	protected
		$title,
		$adjustmentDate,
		$adjustmentNo,
		$reason,
		$remark,
		$totalAdjustment,
		$detail
	;
	public function __construct( $params = array() )
	{
		parent::__construct($params);
		$this->detail = new AdjustDetailCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'title',
				'adjustment_date',
				'reason',
				'adjustment_no',
				'remark',
				'total_adjustment',
				'status',
				'detail'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				title,
				adjustment_date,
				adjustment_no,
				reason,
				remark,
				total_adjustment,
				status
			FROM
				adjustment_stock
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Adjustment Stock Not Found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('adjustment_id', $this->getId());
		$this->detail->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				adjustment_stock
			SET
				title = '" . $this->getTitle() . "',
				adjustment_date = '" . $this->getAdjustmentDate() . "',
				reason = '" . $this->getReason() . "',
				remark = '" . $this->getRemark() . "',
				total_adjustment = '" . $this->getTotalAdjustment() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				adjustment_stock
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				adjustment_stock
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				adjustment_stock
			(
				title,
				adjustment_date,
				adjustment_no,
				reason,
				remark,
				total_adjustment,
				status,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getTitle() ). "',
				'" . $this->getAdjustmentDate() . "',
				'" . $this->getAdjustmentNo() . "',
				'" . $this->dbEscape( $this->getReason() ). "',
				'" . $this->dbEscape( $this->getRemark() ). "',
				'" . $this->getTotalAdjustment() . "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setAdjustmentNo( $string ){
		$this->adjustmentNo = $string;
	}

	public function getAdjustmentNo(){
		return $this->adjustmentNo;
	}

	public function setAdjustmentDate( $string ){
		$this->adjustmentDate = $string;
	}

	public function getAdjustmentDate(){
		return $this->adjustmentDate;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}
	
	public function getTitle(){
		return $this->title;
	}

	public function setTotalAdjustment( $string ){
		$this->totalAdjustment = doubleval($string);
	}
	
	public function getTotalAdjustment(){
		return $this->totalAdjustment;
	}

	public function setRemark( $string ){
		$this->remark = $string;
	}
	
	public function getRemark(){
		return $this->remark;
	}

	public function setReason( $string ){
		$this->reason = $string;
	}
	
	public function getReason(){
		return $this->reason;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}
	
	public function getDetail(){
		return $this->detail;
	}
}
