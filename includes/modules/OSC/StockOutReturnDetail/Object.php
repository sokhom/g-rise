<?php

namespace OSC\StockOutReturnDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$stockOutId
		, $stockOutReturnId
		, $stockOutNo
		, $stockOutDate
		, $productId
		, $discountPercent
		, $discountCash
		, $productName
		, $qty
		, $price
		, $wholeSalePrice
		, $retailPrice
		, $type
		, $umTypeWholeSaleId
		, $umTypeWholeSaleName
		, $umTypeWholeSaleAmount
		, $umTypeRetailId
		, $umTypeRetailName
		, $umTypeRetailAmount
		, $cost
		, $productKindOf
		, $productCode
		, $productImage
		, $productTypeId
		, $productTypeName
		, $total
		, $description
		, $remark
		, $addMorePrice
		, $addQty
		, $discountPercentBefore
		, $discountCashBefore
		, $productNameBefore
		, $priceBefore
		, $totalBefore
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'product_name',
				'qty',
				'stock_out_no',
				'discount_percent',
				'discount_cash',
				'product_id',
				'total',
				'product_kind_of',
				'stock_out_id',
				'description',
				'price',
				'type',
				'product_code',
				'whole_sale_price',
				'add_qty',
				'retail_price',
				'um_type_whole_sale_id',
				'um_type_whole_sale_amount',
				'um_type_whole_sale_name',
				'um_type_retail_id',
				'um_type_retail_amount',
				'um_type_retail_name',
				'remark',
				'add_more_price',
				'discount_percent_before',
				'discount_cash_before',
				'total_before',
				'price_before',
				'product_name_before',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				stock_out_return_id,
				product_id,
				product_name,
				product_code,
				discount_percent,
				discount_cash,
				qty,
				total,
				remark,
				add_qty,
				add_more_price,
				description,
				product_kind_of,
				price,
				type,
				whole_sale_price,
				retail_price,
				um_type_retail_id,
				um_type_retail_amount,
				um_type_retail_name,
				um_type_whole_sale_id,
				um_type_whole_sale_name,
				um_type_whole_sale_amount,
				discount_cash_before,		
				total_before,
				price_before,			
				product_name_before,	
				discount_percent_before
			FROM
				stock_out_return_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock Out Detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				stock_out_return_detail
			(
				stock_out_return_id,
				stock_out_no,
				stock_out_date,
				discount_percent,
				discount_cash,
				product_id,
				product_name,
				qty,
				add_qty,
				total,
				description,
				price,
				whole_sale_price,
				retail_price,
				um_type_retail_id,
				um_type_retail_amount,
				um_type_retail_name,
				um_type_whole_sale_id,
				um_type_whole_sale_name,
				um_type_whole_sale_amount,
				type,
				cost,
				product_image,
				product_code,
				product_type_id,
				product_kind_of,
				status,
				add_more_price,
				remark,
				discount_cash_before,		
				total_before,
				price_before,			
				product_name_before,	
				discount_percent_before,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getStockOutReturnId() . "',
				'" . $this->getStockOutNo() . "',
				'" . $this->getStockOutDate() . "',
				'" . $this->getDiscountPercent() . "',
				'" . $this->getDiscountCash() . "',
				'" . $this->getProductId() . "',
				'" . $this->dbEscape( $this->getProductName() ) . "',
				'" . $this->getQty() . "',
				'" . $this->getAddQty() . "',
				'" . $this->getTotal() . "',
				'" . $this->dbEscape( $this->getDescription() ) . "',
				'" . $this->getPrice() . "',
				'" . $this->getWholeSalePrice() . "',
				'" . $this->getRetailPrice() . "',
				'" . $this->getUmTypeRetailId() . "',
				'" . $this->getUmTypeRetailAmount() . "',
				'" . $this->getUmTypeRetailName() . "',
				'" . $this->getUmTypeWholeSaleId() . "',
				'" . $this->getUmTypeWholeSaleName() . "',
				'" . $this->getUmTypeWholeSaleAmount() . "',
				'" . $this->getType() . "',
				'" . $this->getCost() . "',
				'" . $this->getProductImage() . "',
				'" . $this->getProductCode() . "',
				'" . $this->getProductTypeId() . "',
				'" . $this->getProductKindOf() . "',
				1,
				'" . $this->getAddMorePrice() . "',
				'" . $this->dbEscape( $this->getRemark() ) . "',
				'" . $this->getDiscountCashBefore() . "',
				'" . $this->getTotalBefore() . "',
				'" . $this->getPriceBefore() . "',
				'" . $this->getProductNameBefore() . "',
				'" . $this->getDiscountPercentBefore() . "',
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}
	
	public function setAddQty( $string ){
		$this->addQty = doubleval($string);
	}

	public function getAddQty(){
		return $this->addQty;
	}

	public function setRemark( $string ){
		$this->remark = $string;
	}

	public function getRemark(){
		return $this->remark;
	}

	public function setAddMorePrice( $string ){
		$this->addMorePrice = doubleval($string);
	}

	public function getAddMorePrice(){
		return $this->addMorePrice;
	}

	public function setProductTypeName( $string ){
		$this->productTypeName = $string;
	}

	public function getProductTypeName(){
		return $this->productTypeName;
	}

	public function setProductTypeId( $string ){
		$this->productTypeId = (int)$string;
	}

	public function getProductTypeId(){
		return $this->productTypeId;
	}

	public function setProductImage( $string ){
		$this->productImage = $string;
	}

	public function getProductImage(){
		return $this->productImage;
	}

	public function setCost( $string ){
		$this->cost = doubleval($string);
	}

	public function getCost(){
		return $this->cost;
	}

	public function setProductCode( $string ){
		$this->productCode = $string;
	}

	public function getProductCode(){
		return $this->productCode;
	}

	public function setProductKindOf( $string ){
		$this->productKindOf = $string;
	}

	public function getProductKindOf(){
		return $this->productKindOf;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}

	public function getProductName(){
		return $this->productName;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}

	public function getTotal(){
		return $this->total;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}

	public function getPrice(){
		return $this->price;
	}

	public function setQty( $string ){
		$this->qty = doubleval($string);
	}

	public function getQty(){
		return $this->qty;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}

	public function getProductId(){
		return $this->productId;
	}

	public function setStockOutId( $string ){
		$this->stockOutId = (int)$string;
	}
	
	public function getStockOutId(){
		return $this->stockOutId;
	}

	public function setStockOutReturnId( $string ){
		$this->stockOutReturnId = (int)$string;
	}
	
	public function getStockOutReturnId(){
		return $this->stockOutReturnId;
	}


	public function setStockOutNo( $string ){
		$this->stockOutNo = (string)$string;
	}

	public function getStockOutNo(){
		return $this->stockOutNo;
	}

	public function setDiscountPercent( $string ){
		$this->discountPercent = doubleval($string);
	}

	public function getDiscountPercent(){
		return $this->discountPercent;
	}

	public function setDiscountCash( $string ){
		$this->discountCash = doubleval($string);
	}

	public function getDiscountCash(){
		return $this->discountCash;
	}

	public function setStockOutDate( $string ){
		$this->stockOutDate = $string;
	}

	public function getStockOutDate(){
		return $this->stockOutDate;
	}
	
	public function getWholeSalePrice(){
		return $this->wholeSalePrice;
	}

	public function setWholeSalePrice( $string ){
		$this->wholeSalePrice = doubleval($string);
	}
	
	public function getRetailPrice(){
		return $this->retailPrice;
	}

	public function setRetailPrice( $string ){
		$this->retailPrice = doubleval($string);
	}

	public function getType(){
		return $this->type;
	}

	public function setType( $string ){
		$this->type = $string;
	}

	public function getUmTypeWholeSaleId(){
		return $this->umTypeWholeSaleId;
	}

	public function setUmTypeWholeSaleId( $string ){
		$this->umTypeWholeSaleId = (int)$string;
	}

	public function getUmTypeWholeSaleName(){
		return $this->umTypeWholeSaleName;
	}

	public function setUmTypeWholeSaleName( $string ){
		$this->umTypeWholeSaleName = $string;
	}

	public function getUmTypeWholeSaleAmount(){
		return $this->umTypeWholeSaleAmount;
	}

	public function setUmTypeWholeSaleAmount( $string ){
		$this->umTypeWholeSaleAmount = doubleval($string);
	}

	public function getUmTypeRetailId(){
		return $this->umTypeRetailId;
	}

	public function setUmTypeRetailId( $string ){
		$this->umTypeRetailId = (int)$string;
	}
	
	public function getUmTypeRetailAmount(){
		return $this->umTypeRetailAmount;
	}

	public function setUmTypeRetailAmount( $string ){
		$this->umTypeRetailAmount = doubleval($string);
	}	
	
	public function getUmTypeRetailName(){
		return $this->umTypeRetailName;
	}

	public function setUmTypeRetailName( $string ){
		$this->umTypeRetailName = $string;
	}



	public function getProductNameBefore(){
		return $this->productNameBefore;
	}
	public function setProductNameBefore( $string ){
		$this->productNameBefore = $string;
	}

	public function getDiscountCashBefore(){
		return $this->discountCashBefore;
	}
	public function setDiscountCashBefore( $string ){
		$this->discountCashBefore = doubleval($string);
	}
	
	public function getDiscountPercentBefore(){
		return $this->discountPercentBefore;
	}
	public function setDiscountPercentBefore( $string ){
		$this->discountPercentBefore = doubleval($string);
	}


	public function getPriceBefore(){
		return $this->priceBefore;
	}
	public function setpriceBefore( $string ){
		$this->priceBefore = doubleval($string);
	}

	public function getTotalBefore(){
		return $this->totalBefore;
	}
	public function setTotalBefore( $string ){
		$this->totalBefore = doubleval($string);
	}

	
}
