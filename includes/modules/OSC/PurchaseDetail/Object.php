<?php
namespace
    OSC\PurchaseDetail;
use
    Aedea\Core\Database\StdObject as DbObj;

class Object extends DbObj{
     protected
        $purchaseId,
        $productId,
        $productName,
        $qty,
        $priceIn,
        $unitPrice,
        $priceOutRetail,
        $priceOutWholeSale,
        $umTypeId,
        $qtyOnHand,
        $umTypeName,
        $costBefore,
        $umTypeAmount,
        $description,
        $total,
        $barcode,
        $productUmTypeAmount,
        $productUmTypeName,
        $freeQty,
        $discountPercent,
        $discountDollar
     ;
    public function toArray($params=array()){
        $args= array(
            'include'=>array(
                'id',
                'product_id',
                'product_name',
                'barcode',
                'description',
                'free_qty',
                'qty',
                'discount_percent',
                'discount_dollar',
                'qty_on_hand',
                'price_out_whole_sale',
                'price_out_retail',
                'um_type_name',
                'cost_before',
                'um_type_amount',
                'price_in',
                'total',
                'product_um_type_amount',
                'product_um_type_name',
            )
        );
        return parent::toArray($args);
    }
    public function load( $params = array() ){
        $q = $this->dbQuery("
			SELECT
				product_id,
				price_in,
                purchase_id,
                barcode,
                free_qty,
                product_name,
                description,
                discount_percent,
                discount_dollar,
                qty,
                price_out_whole_sale,
                qty_on_hand,
                cost_before,
                price_out_retail,
                um_type_name,
                um_type_amount,
                total,
                product_um_type_amount,
                product_um_type_name
			FROM
				purchase_detail
			WHERE
				id = '" . (int)$this->getId() . "'
		");

        if( ! $this->dbNumRows($q) ){
            throw new \Exception(
                "404: Purchase Detail not found",
                404
            );
        }
        $this->setProperties($this->dbFetchArray($q));
    }

    public function insert(){
        $this->dbQuery("
			INSERT INTO
				purchase_detail
			(
                product_id,
                product_name,
                description,
                cost_before,
                barcode,
                qty,
                free_qty,
                price_out_retail,
                price_out_whole_sale,
                um_type_id,
                um_type_name,
                um_type_amount,
                price_in,
                total,
                discount_percent,
                discount_dollar,
                purchase_id,
                qty_on_hand,
                product_um_type_name,
                product_um_type_amount
			)
				VALUES
			(
				'" . $this->getProductId() . "',
				'" . $this->getProductName() . "',
				'" . $this->getDescription() . "',
                '" . $this->getCostBefore() . "',
				'" . $this->getBarcode() . "',
                '" . $this->getQty() . "',
                '" . $this->getFreeQty() . "',
				'" . $this->getPriceOutRetail() . "',
                '" . $this->getPriceOutWholeSale() . "',
                '" . $this->getUmTypeId() . "',
                '" . $this->getUmTypeName() . "',
                '" . $this->getUmTypeAmount() . "',
				'" . $this->getPriceIn() . "',
                '" . $this->getTotal() . "',
                '" . $this->getDiscountPercent() . "',
                '" . $this->getDiscountDollar() . "',
				'" . $this->getPurchaseId() . "',
                '" . $this->getQtyOnHand() . "',
                '" . $this->getProductUmTypeName() . "',
                '" . $this->getProductUmTypeAmount() . "'
			)
		");
        $this->setId( $this->dbInsertId() );
    }

    public function update(){
        $this->dbQuery("
			UPDATE
				purchase_detail
			SET
                product_id = '" . $this->getProductId() . "',
                product_name = '" . $this->getProductName() . "',
                qty = '" . $this->getQty() . "',
                price_in = '" . $this->getPriceIn() . "',
                total = '" . $this->getTotal() . "',
                price_out = '" . $this->getPriceOut() . "',
			WHERE
				pd_id = '" . (int)$this->getId() . "'
		");
    }

    public function delete()
    {
        if (!$this->getPurchaseId()) {
            throw new Exception("delete method requires id to be set");
        }
        $this->dbQuery("
			DELETE FROM
				purchase_detail
			WHERE
				purchase_id = '" . (int)$this->getPurchaseId() . "'
		");
    }

    public function setFreeQty($string){
        $this->freeQty = doubleval($string);
    }
    public function getFreeQty(){
        return $this->freeQty;
    }
    
    public function setDiscountPercent($string){
        $this->discountPercent = doubleval($string);
    }
    public function getDiscountPercent(){
        return $this->discountPercent;
    }
    
    public function setDiscountDollar($string){
        $this->discountDollar = doubleval($string);
    }
    public function getDiscountDollar(){
        return $this->discountDollar;
    }
    
    public function setProductUmTypeName($string){
        $this->productUmTypeName = $string;
    }
    public function getProductUmTypeName(){
        return $this->productUmTypeName;
    }
    
    public function setProductUmTypeAmount($string){
        $this->productUmTypeAmount = doubleval($string);
    }
    public function getProductUmTypeAmount(){
        return $this->productUmTypeAmount;
    }

    public function setQtyOnHand($string){
        $this->qtyOnHand = doubleval($string);
    }
    public function getQtyOnHand(){
        return $this->qtyOnHand;
    }

    public function setBarcode($string){
        $this->barcode = $string;
    }
    public function getBarcode(){
        return $this->barcode;
    }

    public function setProductId($string){
        $this->productId = (int)$string;
    }
    public function getProductId(){
        return $this->productId;
    }

    public function setPurchaseId($string){
        $this->purchaseId = (int)$string;
    }
    public function getPurchaseId(){
        return $this->purchaseId;
    }

    public function setProductName($string){
        $this->productName = $string;
    }
    public function getProductName(){
        return $this->productName;
    }

    public function setPriceIn($string){
        $this->priceIn = $string;
    }
    public function getPriceIn(){
        return $this->priceIn;
    }

    public function setUnitPrice($string){
        $this->unitPrice = $string;
    }
    public function getUnitPrice(){
        return $this->unitPrice;
    }

    public function setDescription($string){
        $this->description = $string;
    }
    public function getDescription(){
        return $this->description;
    }

    public function setQty($string){
        $this->qty = doubleval($string);
    }
    public function getQty(){
        return $this->qty;
    }

    public function setPriceOutWholeSale($string){
        $this->priceOutWholeSale =doubleval($string);
    }
    public function getPriceOutWholeSale(){
        return $this->priceOutWholeSale;
    }

    public function setPriceOutRetail($string){
        $this->priceOutRetail = doubleval($string);
    }
    public function getPriceOutRetail(){
        return $this->priceOutRetail;
    }

    public function setTotal($string){
        $this->total = $string;
    }
    public function getTotal(){
        return $this->total;
    }

    public function setUmTypeId($string){
        $this->umTypeId = (int)$string;
    }
    public function getUmTypeId(){
        return $this->umTypeId;
    }
    
    public function setUmTypeName($string){
        $this->umTypeName = $string;
    }
    public function getUmTypeName(){
        return $this->umTypeName;
    }

    public function setUmTypeAmount($string){
        $this->umTypeAmount = doubleval($string);
    }
    public function getUmTypeAmount(){
        return $this->umTypeAmount;
    }

    
    public function setCostBefore($string){
        $this->costBefore = doubleval($string);
    }
    public function getCostBefore(){
        return $this->costBefore;
    }

}
