<?php

namespace OSC\InvoiceDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$invoiceId
		, $invoiceNo
		, $productId
		, $productName
		, $type
		, $size
		, $discountCash
		, $discountPercent
		, $unitPrice
		, $qty
		, $total
		, $description
		, $descriptionDecode
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'invoice_id',
				'invoice_no',
				'product_id',
				'product_name',
				'description_decode',
				'type',
				'size',
				'discount_percent',
				'discount_cash',
				'unit_price',
				'qty',
				'total',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				invoice_id,
				invoice_no,
				product_id,
				product_name,
				description as description_decode,
				type,
				size,
				discount_percent,
				discount_cash,
				unit_price,
				qty,
				total
			FROM
				invoice_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Invoice Detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				invoice_detail
			SET
				name = '" . $this->getName() . "',
				detail = '" . $this->getDetail() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function deleteByInvoice(){
		if( !$this->getInvoiceNo ) {
			throw new Exception("delete method requires invoice no to be set");
		}
		$this->dbQuery("
			DELETE FROM
				invoice_detail
			WHERE
				invoice_no = '" . $this->getInvoiceNo() . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				invoice_detail
			(
				invoice_id,
				invoice_no,
				description,
				product_id,
				product_name,
				type,
				size,
				discount_cash,
				discount_percent,
				unit_price,
				qty,
				total
			)
				VALUES
			(
				'" . $this->getInvoiceId() . "',
				'" . $this->getInvoiceNo() . "',
				'" . $this->dbEscape( $this->getDescription() ) . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductName() . "',
				'" . $this->getType() . "',
				'" . $this->getSize() . "',
				'" . $this->getDiscountCash() . "',
				'" . $this->getDiscountPercent() . "',
				'" . $this->getUnitPrice() . "',
				'" . $this->getQty() . "',
				'" . $this->getTotal() . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setInvoiceId( $string ){
		$this->invoiceId = (int)$string;
	}

	public function getInvoiceId(){
		return $this->invoiceId;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}

	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}

	public function getProductName(){
		return $this->productName;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}

	public function getProductId(){
		return $this->productId;
	}

	public function setType( $string ){
		$this->type = $string;
	}

	public function getType(){
		return $this->type;
	}

	public function setSize( $string ){
		$this->size = $string;
	}

	public function getSize(){
		return $this->size;
	}

	public function setUnitPrice( $string ){
		$this->unitPrice = doubleval($string);
	}

	public function getUnitPrice(){
		return $this->unitPrice;
	}

	public function setQty( $string ){
		$this->qty = (int)$string;
	}

	public function getQty(){
		return $this->qty;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}

	public function getTotal(){
		return $this->total;
	}

	public function setDiscountCash( $string ){
		$this->discountCash = doubleval($string);
	}

	public function getDiscountCash(){
		return $this->discountCash;
	}

	public function setDiscountPercent( $string ){
		$this->discountPercent = doubleval($string);
	}

	public function getDiscountPercent(){
		return $this->discountPercent;
	}

	public function setDescription( $string ){
		$this->description = json_encode($string);
	}
	public function getDescription(){
		return $this->description;
	}
	
	public function setDescriptionDecode( $string ){
		$this->descriptionDecode = json_decode($string);
	}
	public function getDescriptionDecode(){
		return $this->descriptionDecode;
	}
}

