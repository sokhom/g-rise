<?php

namespace OSC\Role;

use
	Aedea\Core\Database\StdObject as DbObj
    , OSC\Permission\Collection as PermissionCol
;

class Object extends DbObj {
		
	protected
		$roleName
		, $roleText
		, $roleJson
        , $description
        , $permission
	;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->permission = new PermissionCol();
    }

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'role_text',
                'description',
				'permission',
				'role_name'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				role_name,
				role_text,
				description
			FROM
				role
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Role not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

        $this->permission->setFilter('role_id', $this->getId());
        $this->permission->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				role
			SET
				role_name = '" . $this->dbEscape( $this->getRoleName() ). "',
				role_text = '" . $this->dbEscape( $this->getRoleText() ) . "',
				description = '" . $this->dbEscape( $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				role
			SET
				status = '" . $this->dbEscape( $this->getStatus() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");

	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				role
			(
				role_name,
				role_text,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getRoleName() ). "',
				'" . $this->dbEscape( $this->getRoleText() ) . "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}
	
	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				role
			WHERE
				id = '" . (int)$id . "'
		");
		// delete from permission which has permission id
		$this->dbQuery("
			DELETE FROM
				permission 
			WHERE
				role_id = '" . (int)$id . "'
		");
	}

	public function setRoleJson( $string ){
		$this->roleJson = $string;
	}

	public function getRoleJson(){
		return $this->roleJson;
	}

	public function setRoleText( $string ){
		$this->roleText = $string;
	}

	public function getRoleText(){
		return $this->roleText;
	}

	public function setRoleName( $string ){
		$this->roleName = $string;
	}

	public function getRoleName(){
		return $this->roleName;
	}

    public function setDescription( $string ){
        $this->description = $string;
    }

    public function getDescription(){
        return $this->description;
    }

    public function setPermission( $string ){
        $this->permission = $string;
    }

    public function getPermission(){
        return $this->permission;
    }
}
