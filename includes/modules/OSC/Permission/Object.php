<?php

namespace OSC\Permission;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$roleId,
		$userId,
		$isSelected,
		$featureName,
        $label
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'feature_name',
				'is_selected',
                'label',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				feature_name,
				label,
				role_id,
				is_selected
			FROM
				permission
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Permission not found",
				404
			);
		}
		
		$this->setProperties($this->dbFetchArray($q));
		
	}


	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				permission
			SET
				feature_name = '" . $this->getFeatureName() . "',
				role_id = '" . $this->getRoleId() . "',
				label = '" . $this->getLabel() . "',
				is_selected = '" . $this->getIsSelected() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				permission
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function deleteByRoleId($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				permission
			WHERE
				role_id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				permission
			(
				feature_name,
				role_id,
				is_selected,
				user_id,
				label,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getFeatureName() . "',
				'" . $this->getRoleId() . "',
				'" . $this->getIsSelected() . "',
				'" . $this->getUserId() . "',
				'" . $this->getLabel() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setIsSelected( $string ){
		$this->isSelected = ($string == 1 ? true: false);
	}

	public function getIsSelected(){
		return $this->isSelected;
	}

    public function setLabel( $string ){
        $this->label = $string;
    }

    public function getLabel(){
        return $this->label;
    }

	public function setFeatureName( $string ){
		$this->featureName = $string;
	}
	
	public function getFeatureName(){
		return $this->featureName;
	}

	public function setRoleId( $string ){
		$this->roleId = $string;
	}

	public function getRoleId(){
		return $this->roleId;
	}

	public function setUserId( $string ){
		$this->userId = $string;
	}

	public function getUserId(){
		return $this->userId;
	}
}
