<?php

namespace OSC\Permission;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('permission', 'p');
		$this->idField = 'p.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

    public function filterById( $arg ){
        $this->addWhere("p.id = " . (int)$arg. " ");
    }

    public function filterByRoleId( $arg ){
        $this->addWhere("p.role_id = " . (int)$arg. "");
	}
	
    public function filterByRoleFeature( $arg ){
        $this->addWhere("p.feature_name in ('" . $arg . "') ");
	}
	
}
