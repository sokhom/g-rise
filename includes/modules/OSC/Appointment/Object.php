<?php

namespace OSC\Appointment;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\DoctorListOnly\Collection as DoctorCol,
	OSC\CustomerListOnly\Collection as CustomerCol
;

class Object extends DbObj {
		
	protected
		$description,
		$customerId,
		$doctorId,
		$customerDetail,
		$doctorDetail,
		$appointmentDate,
		$appointmentPeriod
	;

	public function __construct( $params = array() )
	{
		parent::__construct($params);
		$this->doctorDetail = new DoctorCol();
		$this->customerDetail = new CustomerCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'customer_id',
				'customer_detail',
				'doctor_id',
				'doctor_detail',
				'appointment_date',
				'appointment_period',
				'description',
				'create_by',
				'update_by',
				'status'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				appointment_date,
				description,
				customer_id,
				doctor_id,
				appointment_period,
				create_by,
				create_date,
				update_by,
				status
			FROM
				appointment
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Appointment not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->customerDetail->setFilter('id', $this->getCustomerId());
		$this->customerDetail->populate();

		$this->doctorDetail->setFilter('id', $this->getDoctorId());
		$this->doctorDetail->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				appointment
			SET
				update_by = '" . $this->getUpdateBy() . "',
				appointment_date = '" . $this->getAppointmentDate() . "',
				appointment_period = '" . $this->getAppointmentPeriod() . "',
				customer_id = '" . $this->getCustomerId() . "',
				doctor_id = '" . $this->getDoctorId() . "',
				description = '" . $this->getDescription() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				appointment
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				appointment
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				appointment
			(
				appointment_date,
				appointment_period,
				customer_id,
				doctor_id,
				description,
				create_by,
				status,
				create_date
			)
				VALUES
			(
				'" . $this->getAppointmentDate() . "',
				'" . $this->getAppointmentPeriod() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->getDoctorId() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
				1,
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setAppointmentPeriod( $string ){
		$this->appointmentPeriod = $string;
	}
	public function getAppointmentPeriod(){
		return $this->appointmentPeriod;
	}

	public function setAppointmentDate( $string ){
		$this->appointmentDate = $string;
	}
	public function getAppointmentDate(){
		return $this->appointmentDate;
	}

	public function setDoctorDetail( $string ){
		$this->doctorDetail = $string;
	}
	public function getDoctorDetail(){
		return $this->doctorDetail;
	}

	public function setDoctorId( $string ){
		$this->doctorId = $string;
	}
	public function getDoctorId(){
		return $this->doctorId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setCustomerId( $string ){
		$this->customerId = $string;
	}
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerDetail( $string ){
		$this->customerDetail = $string;
	}
	public function getCustomerDetail(){
		return $this->customerDetail;
	}
}
