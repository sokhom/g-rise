<?php

namespace OSC\Appointment;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		// $this->addTable('appointment_detail', 'ap');
		$this->addTable('appointment', 'a');
		$this->idField = 'a.id';
		$this->setDistinct(true);
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByCustomerName( $arg ){
		$this->addTable('customers', 'cus');
		$this->addWhere("cus.customers_name LIKE '%" . $arg. "%' or cus.customers_code LIKE '%" . $arg. "%' ");
		$this->addWhere("cus.id = a.customer_id");
	}

	public function filterById( $arg ){
		$this->addWhere("a.id = '" . (int)$arg. "' ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("a.customer_id = '" . (int)$arg. "' ");
	}

	public function filterByDoctorId( $arg ){
		$this->addWhere("a.doctor_id = '" . (int)$arg. "' ");
	}
	
	public function filterBySortASC($arg){
		$this->addOrderBy('a.appointment_period', $arg);
		$this->addOrderBy('a.appointment_date', $arg);
	}

	public function filterByAlertAppointment(){
		$this->addWhere("DATEDIFF(a.appointment_date, NOW()) <= 7");
	}

	public function filterByStatus( $arg ){
		// $this->addWhere("a.status = '" . (int)$arg. "' ");
	}

	public function filterByDate($from, $to){
		$this->addWhere("a.appointment_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function filterByInvoiceNo( $arg ){
		$this->addWhere("a.invoice_no LIKE '%" . $arg. "%' ");
	}

	public function sortById($arg){
		$this->addOrderBy('a.id', $arg);
	}
}
