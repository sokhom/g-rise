<?php

namespace OSC\CustomerDocument;

use Aedea\Core\Database\StdObject as DbObj;

class Object extends DbObj {
		
	protected
		$image,
		$checkByStaffId,
		$checkByStaffName,
		$date,
		$remark,
		$description,
		$customersId
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'image',
				'date',
				'check_by_staff_id',
				'check_by_staff_name',
				'description',
				'customers_id',
				'status',
				'remark',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				image,
				date,
				check_by_staff_id,
				remark,
				check_by_staff_name,
				description,
				customers_id
			FROM
				customers_document
			WHERE
				id = '" . $this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Customer Document not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				customers_document
			SET
				image = '" . $this->getImage() . "',
				customers_id = '" . $this->getCustomersId() . "',
				description = '" . $this->getDescription() . "',
				remark = '" . $this->getRemark() . "',
				check_by_staff_id = '" . $this->getCheckByStaffId() . "',
				check_by_staff_name = '" . $this->getCheckByStaffName() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				customers_document
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				customers_document
			WHERE
				customers_id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				customers_document
			(
				image,
				date,
				check_by_staff_id,
				check_by_staff_name,
				description,
				remark,
				customers_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getImage() ). "',
				'" . $this->getDate() . "',
				'" . $this->getCheckByStaffId() . "',
				'" . $this->getCheckByStaffName() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->dbEscape(  $this->getRemark() ). "',
				'" . $this->getCustomersId() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setRemark( $string ){
		$this->remark = $string;
	}

	public function getRemark(){
		return $this->remark;
	}


	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setCustomersId( $string ){
		$this->customersId = (int)$string;
	}

	public function getCustomersId(){
		return $this->customersId;
	}

	public function setImage( $string ){
		$this->image = $string;
	}
	
	public function getImage(){
		return $this->image;
	}

	public function setDate( $string ){
		$this->date = $string;
	}
	
	public function getDate(){
		return $this->date;
	}

	public function setCheckByStaffId( $string ){
		$this->checkByStaffId = $string;
	}
	
	public function getCheckByStaffId(){
		return $this->checkByStaffId;
	}

	public function setCheckByStaffName( $string ){
		$this->checkByStaffName = $string;
	}
	
	public function getCheckByStaffName(){
		return $this->checkByStaffName;
	}

}
