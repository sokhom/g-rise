<?php

namespace OSC\CustomerDocument;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('customers_document', 'ct');
		$this->idField = 'ct.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		$this->addWhere("ct.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("ct.name LIKE '%" . $arg. "%' ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("ct.customers_id = '" . (int)$arg. "' ");
	}

}
