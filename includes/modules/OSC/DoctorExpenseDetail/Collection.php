<?php

namespace OSC\DoctorExpenseDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('doctor_expense_detail', 'ded');
		$this->idField = 'ded.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByExpenseId( $arg ){
		$this->addWhere("ded.doctor_expense_id = '" . (int)$arg. "' ");
	}

	public function filterByStockOutId( $arg ){
		$this->addWhere("ded.stock_out_id = '" . (int)$arg. "' ");
	}

	public function sortByProductName($arg){
		$this->addOrderBy('ded.product_name', $arg);
	}

}
