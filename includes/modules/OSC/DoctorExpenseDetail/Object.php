<?php

namespace OSC\DoctorExpenseDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$stockOutId
		, $stockOutNo
		, $productId
		, $discountCash
		, $discountPercent
		, $productName
		, $qty
		, $price
		, $total
		, $description
		, $doctorExpenseId
		, $selected
		, $productKindOf
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'product_name',
				'qty',
				'stock_out_no',
				'discount_cash',
				'discount_percent',
				'product_id',
				'total',
				'stock_out_id',
				'product_kind_of',
				'description',
				'price',
				'doctor_expense_id',
				'selected'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				stock_out_id,
				product_kind_of,
				product_id,
				product_name,
				discount_cash,
				discount_percent,
				qty,
				total,
				description,
				price,
				doctor_expense_id,
				selected
			FROM
				doctor_expense_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock Out Detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_expense_detail
			(
				stock_out_id,
				stock_out_no,
				discount_percent,
				discount_cash,
				product_id,
				product_name,
				qty,
				total,
				description,
				price,
				selected,
				doctor_expense_id,
				product_kind_of
			)
				VALUES
			(
				'" . $this->getStockOutId() . "',
				'" . $this->getStockOutNo() . "',
				'" . $this->getDiscountPercent() . "',
				'" . $this->getDiscountCash() . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductName() . "',
				'" . $this->getQty() . "',
				'" . $this->getTotal() . "',
				'" . $this->getDescription() . "',
				'" . $this->getPrice() . "',
				'" . $this->getSelected() . "',
				'" . $this->getDoctorExpenseId() . "',
				'" . $this->getProductKindOf() . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setProductKindOf( $string ){
		$this->productKindOf = $string;
	}

	public function getProductKindOf(){
		return $this->productKindOf;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}

	public function getProductName(){
		return $this->productName;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}

	public function getTotal(){
		return $this->total;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}

	public function getPrice(){
		return $this->price;
	}

	public function setQty( $string ){
		$this->qty = (int)$string;
	}

	public function getQty(){
		return $this->qty;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}

	public function getProductId(){
		return $this->productId;
	}

	public function setStockOutId( $string ){
		$this->stockOutId = (int)$string;
	}
	
	public function getStockOutId(){
		return $this->stockOutId;
	}

	public function setStockOutNo( $string ){
		$this->stockOutNo = (string)$string;
	}

	public function getStockOutNo(){
		return $this->stockOutNo;
	}

	public function setDiscountCash( $string ){
		$this->discountCash = doubleval($string);
	}

	public function getDiscountCash(){
		return $this->discountCash;
	}

	public function setDiscountPercent( $string ){
		$this->discountPercent = doubleval($string);
	}

	public function getDiscountPercent(){
		return $this->discountPercent;
	}

	public function setSelected( $string ){
		$this->selected = (int)$string;
	}

	public function getSelected(){
		return $this->selected;
	}

	public function setDoctorExpenseId( $string ){
		$this->doctorExpenseId = (int)$string;
	}

	public function getDoctorExpenseId(){
		return $this->doctorExpenseId;
	}
}
