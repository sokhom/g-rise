<?php

namespace OSC\CaseFlow;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$invoiceNo
		, $invoiceId
		, $invoiceDate
		, $customerId
		, $customerCode
		, $customerName
		, $paymentMethod
		, $payment
	;

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'invoice_no',
				'invoice_id',
				'invoice_date',
				'customer_id',
				'customer_code',
				'customer_name',
				'payment_method',
				'payment',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				invoice_no,
				invoice_date,
				customer_id,
				customer_name,
				customer_code,
				payment_method,
				invoice_id,
				payment,
				status
			FROM
				cash_flow
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Cash Flow not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				cash_flow
			(
				invoice_id,
				invoice_no,
				invoice_date,
				customer_id,
				customer_name,
				customer_code,
				payment_method,
				payment,
				status,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getInvoiceId() . "',
				'" . $this->getInvoiceNo() . "',
				'" . $this->getInvoiceDate() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->getCustomerName() . "',
				'" . $this->getCustomerCode() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getPayment() . "',
				1,
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}
	public function getCustomerCode(){
		return $this->customerCode;
	}

	public function setCustomerName( $string ){
		$this->customerName = $string;
	}
	public function getCustomerName(){
		return $this->customerName;
	}

	public function setInvoiceDate( $string ){
		$this->invoiceDate = date('Y-m-d', strtotime( $string ));
	}
	public function getInvoiceDate(){
		return $this->invoiceDate;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}
	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

	public function setPaymentMethod( $string ){
		$this->paymentMethod = $string;
	}
	public function getPaymentMethod(){
		return $this->paymentMethod;
	}

	public function setPayment( $string ){
		$this->payment = doubleval($string);
	}
	public function getPayment(){
		return $this->payment;
	}

	public function setInvoiceId( $string ){
		$this->invoiceId = (int)$string;
	}
	public function getInvoiceId(){
		return $this->invoiceId;
	}
}
