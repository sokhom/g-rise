<?php

namespace OSC\LocationType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('location_type', 'lt');
		$this->idField = 'lt.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("lt.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("lt.name LIKE '%" . $arg. "%' or lt.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("lt.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('lt.name', $arg);
	}


}
