<?php

namespace OSC\StockOut;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\StockOutDetail\Collection as StockOutDetailCol
	, OSC\StockOutDetailDoctor\Collection as StockOutDetailDoctorCol
;

class Object extends DbObj {
		
	protected
		$stockOutDate
		, $stockOutNo
		, $customerId
		, $customerName
		, $customerTelephone
		, $customerCode
		, $requestById
		, $requestByName
		, $approveById
		, $approveByName
		, $bankCharge
		, $grandTotal
		, $subTotal
		, $payment
		, $paymentMethod
		, $discountType
		, $discountAmount
		, $discountTotalAmount
		, $exchangeId
		, $remain
		, $note
		, $detail
		, $remark
		, $addMorePrice
		, $overdue
		, $creator
		, $doctorDetail
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new StockOutDetailCol();
		$this->doctorDetail = new StockOutDetailDoctorCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'stock_out_date',
				'stock_out_no',
				'customer_id',
				'customer_name',
				'customer_telephone',
				'customer_code',
				'payment_method',
				'creator',
				'bank_charge',
				'sub_total',
				'grand_total',
				'payment',
				'remain',
				'status',
				'discount_amount',
				'discount_total_amount',
				'discount_type',
				'detail',
				'doctor_detail',
				'create_date',
				'create_by',
				'overdue',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				stock_out_date,
				customer_id,
				customer_name,
				customer_telephone,
				customer_code,
				stock_out_no,
				discount_amount,
				discount_type,
				discount_total_amount,
				payment_method,
				bank_charge,
				grand_total,
				overdue,
				sub_total,
				payment,
				remain,
				status,
				create_by as creator,
				create_date
			FROM
				stock_out s
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock Out not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('stock_out_id', $this->getId());
		$this->detail->populate();

		$this->doctorDetail->setFilter('stock_out_id', $this->getId());
		$this->doctorDetail->populate();
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				stock_out
			(
				stock_out_date,
				customer_id,
				customer_name,
				customer_code,
				customer_telephone,
				exchange_id,
				stock_out_no,
				discount_amount,
				discount_type,
				discount_total_amount,
				payment_method,
				bank_charge,
				grand_total,
				sub_total,
				payment,
				remain,
				status,
				create_by,
				create_date,
				overdue
			)
				VALUES
			(
				'" . $this->getStockOutDate() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->dbEscape( $this->getCustomerName() ) . "',
				'" . $this->getCustomerCode() . "',
				'" . $this->getCustomerTelephone() . "',
				'" . $this->getExchangeId() . "',
				'" . $this->getStockOutNo() . "',
				'" . $this->getDiscountAmount() . "',
				'" . $this->getDiscountType() . "',
				'" . $this->getDiscountTotalAmount() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getBankCharge() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getSubTotal() . "',
				'" . $this->getPayment() . "',
				'" . $this->getRemain() . "',
				1,
				'" . $this->getCreateBy() . "',
				NOW(),
				'" . $this->getOverdue() . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}


	public function updateBalaceAferVoidRecivePayment(){
		tep_db_query("
			UPDATE
				stock_out
			SET
				remain = remain + '" . $this->getRemain() . "',
				payment = payment - '" . $this->getPayment() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = " . $this->getCustomerId() . "
		");
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
		");
		// update case flow
		$this->dbQuery("
			UPDATE
				case_flow
			SET
                status = '" . $this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				invoice_no = '" . $this->getStockOutNo() . "'
		");
	}

	public function updateBlanace(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
                remain = remain - '" . $this->getPayment() . "',
				payment = payment + '" . $this->getPayment() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}

	public function updateOverdue(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
                overdue = '" . $this->getOverdue() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}

	public function updateBalanceAndTotalOfItem(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
				discount_total_amount = '" . $this->getDiscountTotalAmount() . "',
				sub_total = '" . $this->getSubTotal() . "',
				grand_total = '" . $this->getGrandTotal() . "',
				remain = '" . $this->getRemain() . "',
				status = 2,
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}
	
	public function updateBalanceAndTotalOfItemAfterHasVoid(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
				sub_total = '" . $this->getSubTotal() . "',
				discount_total_amount = '" . $this->getDiscountTotalAmount() . "',
				grand_total = '" . $this->getGrandTotal() . "',
				remain = '" . $this->getRemain() . "',
				status = 1,
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}
	
	public function setCreator( $string ){
		$this->creator = $string;
	}

	public function getCreator(){
		return $this->creator;
	}

	public function setOverdue( $string ){
		$this->overdue = $string;
	}

	public function getOverdue(){
		return $this->overdue;
	}

	public function setRemark( $string ){
		$this->remark = $string;
	}

	public function getRemark(){
		return $this->remark;
	}

	public function setAddMorePrice( $string ){
		$this->addMorePrice = doubleval($string);
	}

	public function getAddMorePrice(){
		return $this->addMorePrice;
	}

	public function setDiscountTotalAmount( $string ){
		$this->discountTotalAmount = doubleval($string);
	}

	public function getDiscountTotalAmount(){
		return $this->discountTotalAmount;
	}

	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}

	public function getCustomerCode(){
		return $this->customerCode;
	}

	public function setCustomerTelephone( $string ){
		$this->customerTelephone = $string;
	}

	public function getCustomerTelephone(){
		return $this->customerTelephone;
	}

	public function setDiscountAmount( $string ){
		$this->discountAmount = $string;
	}

	public function getDiscountAmount(){
		return $this->discountAmount;
	}

	public function setDiscountType( $string ){
		$this->discountType = $string;
	}

	public function getDiscountType(){
		return $this->discountType;
	}

	public function setRemain( $string ){
//		if( $string < 0){
//			$string= 0;
//		}
		$this->remain = doubleval($string);
	}

	public function getRemain(){
		return $this->remain;
	}

	public function setDoctorDetail( $string ){
		$this->doctorDetail = $string;
	}

	public function getDoctorDetail(){
		return $this->doctorDetail;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}

	public function setPayment( $string ){
		$this->payment =  doubleval($string);
	}

	public function getPayment(){
		return $this->payment;
	}

	public function setGrandTotal( $string ){
		$this->grandTotal = doubleval($string);
	}

	public function getGrandTotal(){
		return $this->grandTotal;
	}

	public function setBankCharge( $string ){
		$this->bankCharge = doubleval($string);
	}

	public function getBankCharge(){
		return $this->bankCharge;
	}

	public function setSubTotal( $string ){
		$this->subTotal = doubleval($string);
	}

	public function getSubTotal(){
		return $this->subTotal;
	}

	public function setStockOutDate( $date ){
		$this->stockOutDate = date('Y-m-d', strtotime( $date ));
	}
	
	public function getStockOutDate(){
		return $this->stockOutDate;
	}

	public function setStockOutNo( $string ){
		$this->stockOutNo = $string;
	}

	public function getStockOutNo(){
		return $this->stockOutNo;
	}

	public function setPaymentMethod( $string ){
		$this->paymentMethod = $string;
	}

	public function getPaymentMethod(){
		return $this->paymentMethod;
	}

	public function setCustomerName( $string ){
		$this->customerName = (string)$string;
	}

	public function getCustomerName(){
		return $this->customerName;
	}

	public function setExchangeId( $string ){
		$this->exchangeId = (int)$string;
	}

	public function getExchangeId(){
		return $this->exchangeId;
	}

	public function setCustomerId( $string ){
		$this->customerId = $string;
	}

	public function getCustomerId(){
		return $this->customerId;
	}

}
