<?php

namespace OSC\StockOut;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable("stock_out", "so");
		$this->idField = "so.id";
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByOverdue($arg){
		$this->addWhere("DATEDIFF( now(), so.overdue ) >= ".$arg." ");
	}

	public function filterByDate($from, $to){
		$this->addWhere("so.stock_out_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function filterById( $arg ){
		$this->addWhere("so.id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("so.status >= '" . (int)$arg. "' ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("so.customer_id = '" . (int)$arg. "' ");
	}

	public function filterByRemain(){
		$this->addWhere("so.remain > 0 ");
	}

	public function filterByInvoice( $arg ){
		// $this->addTable("customers", "c");
		$this->addWhere("so.stock_out_no LIKE '%" . $arg. "%' OR so.customer_name LIKE '%" . $arg. "%' OR so.customer_telephone LIKE '%" . $arg. "%' OR so.customer_code LIKE '%" . $arg. "%'");
		// $this->addWhere("so.customer_id = c.id");
		// $this->addWhere("c.customers_telephone LIKE '%" . $arg. "%'");
	}

	public function sortById($arg){
		$this->addOrderBy("so.stock_out_date", $arg);
		$this->addOrderBy("so.id", $arg);
	}
	
}
