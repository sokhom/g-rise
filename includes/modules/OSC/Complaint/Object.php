<?php

namespace OSC\Complaint;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\DoctorList\Collection as  StaffCol
;

class Object extends DbObj {
		
	protected
		$title
		, $description
		, $resolveText
		, $resolveBy
		, $resolveByName
		, $resolveByDetail
		, $complaintDate
		, $resolveDate
		, $customerId
		, $customerName
		, $invoiceNo
	;

	// public function __construct( $params = array() ){
    //     parent::__construct($params);
    //     $this->resolveByDetail = new StaffCol();
	// }
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'title',
				'description',
				'resolve_by',
				'resolve_by_name',
				'resolve_text',
				'creator',
				'status',
				'update_name',
				'complaint_date',
				'resolve_date',
				// 'resolve_by_detail',
				'customer_id',
				'customer_name',
				'invoice_no'
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				title,
				description,
				customer_id,
				customer_name,
				invoice_no,
				resolve_by,
				resolve_by_name,
				resolve_text,
				status,
				complaint_date,
				resolve_date,
				create_by as creator,
				update_by as update_name
			FROM
				complaint
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Feed Back not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
		
        // $this->resolveByDetail->setFilter('id', $this->getResolveBy());
		// $this->resolveByDetail->populate();
		
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				complaint
			SET
				title = '" . $this->dbEscape(  $this->getTitle() ). "',
				customer_id = '" . $this->dbEscape(  $this->getCustomerId() ). "',
				customer_name = '" . $this->dbEscape(  $this->getCustomerName() ). "',
				invoice_no = '" . $this->dbEscape(  $this->getInvoiceNo() ). "',
				complaint_date = '" . $this->getComplaintDate() . "',
				resolve_date = '" . $this->getResolveDate() . "',
				resolve_by_name = '" . $this->getResolveByName() . "',
				description = '" .$this->dbEscape(   $this->getDescription() ). "',
				resolve_by = '" . $this->getResolveBy() . "',
				resolve_text = '" .$this->dbEscape(   $this->getResolveText() ). "',
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				complaint
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				complaint
			(
				title,
				customer_id,
				customer_name,
				invoice_no,
				description,
				resolve_by,
				resolve_by_name,
				resolve_date,
				complaint_date,
				resolve_text,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getTitle() ). "',
				'" . $this->dbEscape(  $this->getCustomerId() ). "',
				'" . $this->dbEscape(  $this->getCustomerName() ). "',
				'" . $this->dbEscape(  $this->getInvoiceNo() ). "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->dbEscape(  $this->getResolveBy() ). "',
				'" . $this->dbEscape(  $this->getResolveByName() ). "',
				'" . $this->getResolveDate() . "',
				'" . $this->getComplaintDate() . "',
				'" . $this->dbEscape(  $this->getResolveText() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}
	public function getTitle(){
		return $this->title;
	}
	
	public function setResolveBy( $string ){
		$this->resolveBy = (int)$string;
	}
	public function getResolveBy(){
		return $this->resolveBy;
	}
	
	public function setResolveByName( $string ){
		$this->resolveByName = $string;
	}
	public function getResolveByName(){
		return $this->resolveByName;
	}

	public function setResolveText( $string ){
		$this->resolveText = $string;
	}
	public function getResolveText(){
		return $this->resolveText;
	}
	
	public function setResolveByDetail( $string ){
		$this->resolveByDetail = $string;
	}
	public function getResolveByDetail(){
		return $this->resolveByDetail;
	}

	public function setResolveDate( $string ){
		$this->resolveDate = $string;
	}
	public function getResolveDate(){
		return $this->resolveDate;
	}

	public function setComplaintDate( $string ){
		$this->complaintDate = $string;
	}
	public function getComplaintDate(){
		return $this->complaintDate;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerName( $string ){
		$this->customerName = $string;
	}
	public function getCustomerName(){
		return $this->customerName;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}
	public function getInvoiceNo(){
		return $this->invoiceNo;
	}
}
