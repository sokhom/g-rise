<?php

namespace OSC\Complaint;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('complaint', 'cm');
		$this->idField = 'cm.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		$this->addWhere("cm.title LIKE '%" . $arg. "%' OR cm.customer_name LIKE '%" . $arg. "%' OR cm.invoice_no LIKE '%" . $arg. "%'");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("cm.status = '" . (int)$arg. "' ");
	}
	
	public function filterById( $arg ){
		$this->addWhere("cm.id = '" . (int)$arg. "' ");
	}
	public function sortByName($arg){
		$this->addOrderBy('cm.title', $arg);
	}

	public function sortById($arg){
		$this->addOrderBy('cm.id', $arg);
	}
}
