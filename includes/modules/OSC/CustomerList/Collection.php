<?php

namespace OSC\CustomerList;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('customers', 'c');
		$this->idField = 'c.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByFilterFromDropdown( $arg ){
		$this->addWhere("c.customers_first_name LIKE '%" . $arg. "%' 
			OR c.customers_last_name LIKE '%" . $arg. "%' 
			OR c.customers_telephone LIKE '%" . $arg. "%' 
            OR c.customers_address LIKE '%" . $arg. "%' 
            OR c.customers_email_address LIKE '%" . $arg. "%' 
			OR c.customers_code LIKE '%" . $arg. "%' and status = 1 ");
	}

	public function filterByName( $arg ){
		$this->addWhere("c.customers_first_name LIKE '%" . $arg. "%' 
			OR c.customers_last_name LIKE '%" . $arg. "%' 
			OR c.customers_telephone LIKE '%" . $arg. "%' 
            OR c.customers_address LIKE '%" . $arg. "%' 
            OR c.customers_email_address LIKE '%" . $arg. "%' 
			OR c.customers_code LIKE '%" . $arg. "%' ");
	}

	public function filterByTelephone( $arg ){
		$arg = str_replace(" ", "", $arg); // replace if has space to without space
        $this->addWhere("REPLACE(c.customers_telephone, ' ', '') LIKE '%" . $arg. "%'");
	}

	public function filterById( $arg ){
        $this->addWhere("c.id = '" . (int)$arg. "' ");
	}

	public function filterByCustomerTypeId( $arg ){
		$this->addWhere("c.customers_type_id = '" . (int)$arg. "' ");
	}

	public function filterByCustomerCodeNull(){
		$this->addWhere("c.customers_code = '' OR c.customers_code IS NULL");
	}

	public function filterByCustomerCodeNotNull(){
		$this->addWhere("c.customers_code != ''");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("c.status = '" . $arg. "' ");
	}

	public function sortByCode($arg){
		$this->addOrderBy('c.customers_code', $arg);
	}

	public function sortByName($arg){
		$this->addOrderBy('c.customers_first_name', $arg);
	}

	public function filterByBranchId($arg){
		$this->addWhere("c.branch_id = '" . $arg. "'");
	}

	public function filterByBirthday(){
		$this->addWhere("DAY(c.customers_dob) = DAY(NOW()) AND MONTH(c.customers_dob) = MONTH(NOW())");
	}

}
