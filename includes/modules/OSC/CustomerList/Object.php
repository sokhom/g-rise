<?php

namespace OSC\CustomerList;

use
    Aedea\Core\Database\StdObject as DbObj
    , OSC\CustomerType\Collection as  CustomerTypeCol
    , OSC\Branch\Collection as  BranchCol
    , OSC\CustomerDocument\Collection as  DocumentCol
    , OSC\DoctorList\Collection as  StaffCol
    , OSC\CustomerFollowUp\Collection as  CustomerFollowUpCol
;

class Object extends DbObj {

    protected
        $customersTypeId
        , $branchId
        , $branchDetail
        , $customersPhoto
        , $customersFirstName
        , $customersLastName
        , $eventDate
        , $shootDate
        , $urgent
        , $staffResponsible
        , $staffResponsibleDetail
        , $customersEmailAddress
        , $customersGender
        , $customersDob
        , $customersDob1
        , $title
        , $title1
        , $customersTelephone
        , $customersAddress
        , $customersRelativeContact
        , $customersCode
        , $customersCountry
        , $customersTypeDetail
        , $customersDocuments
        , $customersMaterialStatus
        , $customersDescription
        , $customersOccupation
        , $facebookAccount
        , $updateDate
        , $creator
        , $locationShoot
        , $updateName
        , $checkFrom
        , $interestPackageTypeId
        , $interestPackageType
        , $locationTypeId
        , $locationTypeName
        , $locationId
        , $locationName
        , $customerFollowUp
        , $memberCard
        , $memberShipId
    ;

    public function toArray( $params = array() ){
        $args = array(
            'include' => array(
                'id',
                'customers_first_name',
                'customers_last_name',
                'customers_type_id',
                'member_card',
                'location_type_id',
                'customers_dob1',
                'member_ship_id',
                'title1',
                'title',
                'location_type_name',
                'location_id',
                'location_name',
                'check_from',
                'interest_package_type_id',
                'interest_package_type',
                'customers_code',
                'customers_gender',
                'customers_dob',
                'status',
                'create_date',
                'update_date',
                'update_name',
                'customers_telephone',
                'customers_occupation',
                'customers_material_status',
                'customers_description',
                'customers_email_address',
                'customers_photo',
                'customers_address',
                'customers_relative_contact',
                'customers_type_detail',
                'customers_country',
                'creator',
                'branch_id',
                'facebook_account',
                'branch_detail',
                'location_shoot',
                'event_date',
                'shoot_date',
                'staff_responsible',
                'staff_responsible_detail',
                'customer_follow_up'
            )
        );

        return parent::toArray($args);
    }

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->customersTypeDetail = new CustomerTypeCol();
        $this->customerFollowUp = new CustomerFollowUpCol();
        
        // $this->branchDetail = new BranchCol();
        $this->staffResponsibleDetail = new StaffCol();
    }

    public function load( $params = array() ){
        $q = $this->dbQuery("
            SELECT
                m.name as member_card,
                customers_first_name,
                customers_last_name,
                interest_package_type_id,
                p.name as interest_package_type,
                check_from,
                title,
                member_ship_id,
                title1,
                location_type_id,
                location_type_name,
                location_id,
                location_name,
                event_date,
                shoot_date,
                staff_responsible,
				customers_type_id,
                facebook_account,
                customers_code,
                location_shoot,
                customers_gender,
                c.update_by as update_name,
                c.update_date,
                c.create_by as creator,
                c.create_date,
                customers_dob,
                customers_dob1,
                customers_description,
                customers_material_status,
                customers_occupation, 
                customers_telephone,
                customers_email_address,
                customers_photo,
                customers_relative_contact,
                customers_country,
                customers_address,
				c.status,
                branch_id
			FROM
                customers c left join package_type p on interest_package_type_id = p.id
                    left join member_ship m on m.id = c.member_ship_id
			WHERE
				c.id = '" . (int)$this->getId() . "'	
		");

        if( ! $this->dbNumRows($q) ){
            throw new \Exception(
                "404: Customers List not found",
                404
            );
        }
        $this->setProperties($this->dbFetchArray($q));

        $this->customersTypeDetail->setFilter('id', $this->getCustomersTypeId());
        $this->customersTypeDetail->populate();

        $this->staffResponsibleDetail->setFilter('id', $this->getStaffResponsible());
        $this->staffResponsibleDetail->populate();

        // $this->branchDetail->setFilter('id', $this->getBranchId());
        // $this->branchDetail->populate();

        $this->customerFollowUp->setFilter('customer_id', $this->getId());
        $this->customerFollowUp->populate();
    }

    public function update($id){
        if( !$id ) {
            throw new Exception("save method requires id to be set");
        }
        $this->dbQuery("
			UPDATE
				customers
            SET
                interest_package_type_id = '" . $this->getInterestPackageTypeId() . "',
                member_ship_id = '" . $this->getMemberShipId() . "',
                member_card = '" . $this->dbEscape( $this->getMemberCard() ) . "',
                check_from = '" . $this->dbEscape( $this->getCheckFrom() ) . "',
                location_id = '" . $this->dbEscape( $this->getLocationId() ) . "',
                location_name = '" . $this->dbEscape( $this->getLocationName() ) . "',
                location_type_id = '" . $this->dbEscape( $this->getLocationTypeId() ) . "',
                location_type_name = '" . $this->dbEscape( $this->getLocationTypeName() ) . "',
                customers_first_name = '" . $this->dbEscape( $this->getCustomersFirstName() ) . "',
                customers_last_name = '" . $this->dbEscape( $this->getCustomersLastName() ) . "',
                location_shoot = '" . $this->dbEscape( $this->getLocationShoot() ) . "',
                event_date = '" . $this->dbEscape( $this->getEventDate() ) . "',
                shoot_date = '" . $this->dbEscape( $this->getShootDate() ) . "',
                staff_responsible = '" . $this->getStaffResponsible()  . "',
                facebook_account = '" . $this->dbEscape( $this->getFacebookAccount() ) . "',
                customers_occupation = '" . $this->dbEscape( $this->getCustomersOccupation() ). "',
                customers_description = '" . $this->dbEscape( $this->getCustomersDescription() ). "',
                customers_material_status = '" . $this->getCustomersMaterialStatus() . "',
				customers_type_id = '" . $this->getCustomersTypeId() . "',
                customers_dob = '" . $this->getCustomersDob() . "',
                customers_dob1 = '" . $this->getCustomersDob1() . "',
                title = '" . $this->getTitle() . "',
                title1 = '" . $this->getTitle1() . "',
                customers_photo = '" . $this->getCustomersPhoto() . "',
				customers_telephone = '" . $this->getCustomersTelephone() . "',
				customers_address = '" . $this->dbEscape( $this->getCustomersAddress() ). "',
				customers_email_address = '" . $this->getCustomersEmailAddress() . "',
				customers_relative_contact = '" . $this->getCustomersRelativeContact() . "',
				customers_gender = '" . $this->getCustomersGender() . "',
				customers_code = '" . $this->getCustomersCode() . "',
				customers_country = '" . $this->getCustomersCountry() . "',
                branch_id = '" . $this->getBranchId() . "',
				update_by = '" . $this->getUpdateBy() . "',
                update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$id . "'
		");
    }

    public function updateStatus(){
        if( !$this->getId() ){
            throw new Exception("save method requires id to be set");
        }
        $this->dbQuery("
			UPDATE
				customers
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function delete($id){
        if( !$id ) {
            throw new Exception("delete method requires id to be set");
        }
        $this->dbQuery("
			DELETE FROM
				customers
			WHERE
				id = '" . (int)$id . "'
		");
    }

    public function insert(){
        $this->dbQuery("
			INSERT INTO
				customers
			(
                member_ship_id,
                customers_first_name,
                customers_last_name,
                member_card,
                event_date,
                shoot_date,
                location_id,
                location_name,
                location_type_id,
                location_type_name,
                staff_responsible,
                facebook_account,
                branch_id,
                location_shoot,
				customers_address,
                customers_photo,
				customers_telephone,
                customers_dob,
                customers_dob1,
                title,
                title1,
                customers_occupation,
                customers_material_status,
                customers_description,
				customers_relative_contact,
				customers_email_address,
				customers_type_id,
				customers_gender,
				customers_country,
				customers_code,
				create_date,
				create_by,
                create_by_id,
                interest_package_type_id,
                check_from
			)
				VALUES
			(
                '" . $this->getMemberShipId() . "',
                '" . $this->dbEscape( $this->getCustomersFirstName() ). "',
                '" . $this->dbEscape( $this->getCustomersLastName() ). "',
                '" . $this->dbEscape( $this->getMemberCard() ). "',
                '" . $this->dbEscape( $this->getEventDate() ). "',
                '" . $this->dbEscape( $this->getShootDate() ). "',
                '" . $this->dbEscape( $this->getLocationId() ). "',
                '" . $this->dbEscape( $this->getLocationName() ). "',
                '" . $this->dbEscape( $this->getLocationTypeId() ). "',
                '" . $this->dbEscape( $this->getLocationTypeName() ). "',
                '" . $this->dbEscape( $this->getStaffResponsible() ). "',
                '" . $this->dbEscape( $this->getFacebookAccount() ). "',
                '" . $this->getBranchId() . "',
                '" . $this->dbEscape(  $this->getLocationShoot() ) . "',
				'" . $this->dbEscape(  $this->getCustomersAddress() ) . "',
                '" . $this->getCustomersPhoto() . "',
				'" . $this->getCustomersTelephone() . "',
                '" . $this->getCustomersDob() . "',
                '" . $this->getCustomersDob1() . "',
                '" . $this->getTitle() . "',
                '" . $this->getTitle1() . "',
                '" . $this->dbEscape( $this->getCustomersOccupation() ). "',
                '" . $this->getCustomersMaterialStatus() . "',
                '" . $this->dbEscape( $this->getCustomersDescription() ) . "',
				'" . $this->getCustomersRelativeContact() . "',
				'" . $this->getCustomersEmailAddress() . "',
				'" . $this->getCustomersTypeId() . "',
				'" . $this->getCustomersGender() . "',
				'" . $this->getCustomersCountry() . "',
				'" . $this->getCustomersCode() . "',
 				NOW(),
 				'" . $this->getCreateBy() . "',
                 '" . $this->getCreateById() . "',
                 '" . $this->getInterestPackageTypeId() . "',
                 '" . $this->getCheckFrom() . "'
			)
		");
        $this->setId( $this->dbInsertId() );
    }
    public function setMemberShipId( $string ){
        $this->memberShipId = (int)$string;
    }
    public function getMemberShipId(){
        return $this->memberShipId;
    }
    
    public function setMemberCard( $string ){
        $this->memberCard = $string;
    }
    public function getMemberCard(){
        return $this->memberCard;
    }
    
    public function setTitle( $string ){
        $this->title = $string;
    }
    public function getTitle(){
        return $this->title;
    }

    public function setTitle1( $string ){
        $this->title1 = $string;
    }
    public function getTitle1(){
        return $this->title1;
    }

    public function setCustomersDob1( $string ){
        $this->customersDob1 = $string;
    }
    public function getCustomersDob1(){
        return $this->customersDob1;
    }

    public function setCustomerFollowUp( $string ){
        $this->customerFollowUp = $string;
    }
    public function getCustomerFollowUp(){
        return $this->customerFollowUp;
    }

    public function setLocationId( $string ){
        $this->locationId = (int)$string;
    }
    public function getLocationId(){
        return $this->locationId;
    }

    public function setLocationName( $string ){
        $this->locationName = $string;
    }
    public function getLocationName(){
        return $this->locationName;
    }
    
    public function setLocationTypeId( $string ){
        $this->locationTypeId = (int)$string;
    }
    public function getLocationTypeId(){
        return $this->locationTypeId;
    }

    public function setLocationTypeName( $string ){
        $this->locationTypeName = $string;
    }
    public function getLocationTypeName(){
        return $this->locationTypeName;
    }
    
    public function setCustomersTypeDetail( $string ){
        $this->customersTypeDetail = $string;
    }
    public function getCustomersTypeDetail(){
        return $this->customersTypeDetail;
    }

    public function setLocationShoot( $string ){
        $this->locationShoot = $string;
    }
    public function getLocationShoot(){
        return $this->locationShoot;
    }

    public function setFacebookAccount( $string ){
        $this->facebookAccount = $string;
    }

    public function getFacebookAccount(){
        return $this->facebookAccount;
    }


    public function setCustomersEmailAddress( $string ){
        $this->customersEmailAddress = $string;
    }

    public function getCustomersEmailAddress(){
        return $this->customersEmailAddress;
    }

    public function setCustomersCode( $string ){
        $this->customersCode = $string;
    }
    public function getCustomersCode(){
        return $this->customersCode;
    }

    public function setCustomersCountry( $string ){
        $this->customersCountry = $string;
    }
    public function getCustomersCountry(){
        return $this->customersCountry;
    }

    public function setCustomersTypeId( $string ){
        $this->customersTypeId = (int)$string;
    }
    public function getCustomersTypeId(){
        return $this->customersTypeId;
    }

    public function setCustomersDob( $string ){
        $this->customersDob = $string;//date("Y-m-d", strtotime($string));
    }
    public function getCustomersDob(){
        return $this->customersDob;
    }

    public function setCustomersAddress( $string ){
        $this->customersAddress = $string;
    }
    public function getCustomersAddress(){
        return $this->customersAddress;
    }

    public function setCustomersPhoto( $string ){
        $this->customersPhoto = $string;
    }
    public function getCustomersPhoto(){
        return $this->customersPhoto;
    }

    public function setCustomersTelephone( $string ){
        $this->customersTelephone = $string;
    }
    public function getCustomersTelephone(){
        return $this->customersTelephone;
    }

    public function setCustomersRelativeContact( $string ){
        $this->customersRelativeContact = $string;
    }

    public function getCustomersRelativeContact(){
        return $this->customersRelativeContact;
    }

    public function setCustomersFirstName( $string ){
        $this->customersFirstName = $string;
    }

    public function getCustomersFirstName(){
        return $this->customersFirstName;
    }

    
    public function setCustomersLastName( $string ){
        $this->customersLastName = $string;
    }
    public function getCustomersLastName(){
        return $this->customersLastName;
    }


    public function setCustomersGender( $string ){
        $this->customersGender = $string;
    }

    public function getCustomersGender(){
        return $this->customersGender;
    }

    public function setBranchId( $string ){
        $this->branchId = (int)$string;
    }

    public function getBranchId(){
        return $this->branchId;
    }

    public function setBranchDetail( $string ){
        $this->branchDetail = $string;
    }

    public function getBranchDetail(){
        return $this->branchDetail;
    }

    public function setCustomersDocuments( $string ){
        $this->customersdocuments = $string;
    }

    public function getCustomersDocuments(){
        return $this->customersdocuments;
    }

    public function setCustomersDescription( $string ){
        $this->customersDescription = $string;
    }

    public function getCustomersDescription(){
        return $this->customersDescription;
    }

    public function setCustomersMaterialStatus( $string ){
        $this->customersMaterialStatus = $string;
    }

    public function getCustomersMaterialStatus(){
        return $this->customersMaterialStatus;
    }

    public function setCustomersOccupation( $string ){
        $this->customersOccupation = $string;
    }

    public function getCustomersOccupation(){
        return $this->customersOccupation;
    }


    public function setUpdateDate( $string ){
        $this->updateDate = $string;
    }

    public function getUpdateDate(){
        return $this->updateDate;
    }


    public function setUpdateName( $string ){
        $this->updateName = $string;
    }

    public function getUpdateName(){
        return $this->updateName;
    }

    public function setCreator( $string ){
        $this->creator = $string;
    }

    public function getCreator(){
        return $this->creator;
    }
    
    public function setEventDate( $string ){
        $this->eventDate = $string;
    }

    public function getEventDate(){
        return $this->eventDate;
    }

    
    public function setShootDate( $string ){
        $this->shootDate = $string;
    }
    public function getShootDate(){
        return $this->shootDate;
    }
    
    public function setUrgent( $string ){
        $this->urgent = (int)$string;
    }
    public function getUrgent(){
        return $this->urgent;
    }
    
    public function setStaffResponsible( $string ){
        $this->staffResponsible = (int)$string;
    }
    public function getStaffResponsible(){
        return $this->staffResponsible;
    }
    
    public function setStaffResponsibleDetail( $string ){
        $this->staffResponsibleDetail = $string;
    }
    public function getStaffResponsibleDetail(){
        return $this->staffResponsibleDetail;
    }
    
    public function setCheckFrom( $string ){
        $this->checkFrom = $string;
    }
    public function getCheckFrom(){
        return $this->checkFrom;
    }
    
    public function setInterestPackageTypeId( $string ){
        $this->interestPackageTypeId = (int)$string;
    }
    public function getInterestPackageTypeId(){
        return $this->interestPackageTypeId;
    }
    
    public function setInterestPackageType( $string ){
        $this->interestPackageType = $string;
    }
    public function getInterestPackageType(){
        return $this->interestPackageType;
    }


}
