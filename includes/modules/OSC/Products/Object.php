<?php

namespace OSC\Products;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\ProductsType\Collection
		as ProductTypeCol
	, OSC\UMType\Collection
		as UMTypeCol
;

class Object extends DbObj {
		
	protected
		$price
		, $cost
		, $productsName
		, $productsQuantity
		, $productsTypeFields
		, $productsTypeId
		, $productsTypeName
		, $productsDescription
		, $barcode
		, $productsKindOf
		, $orderAlert
		, $productsImage
		, $umTypeId
		, $umDetail
		, $isSale
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'um_type_id',
				'price',
				'products_kind_of',
				'products_type_id',
				'cost',
				'products_image',
				'status',
				'products_quantity',
				'products_type_fields',
				'is_sale',
				'barcode',
				'products_description',
				'products_name',
				'um_detail',
				'order_alert'
			)
		);
		return parent::toArray($args);
	}

	public function __construct( $params = array() ){
 		parent::__construct($params);
		
 		$this->productsTypeFields = new ProductTypeCol();
		$this->umDetail = new UMTypeCol();
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				price,
				cost,
				um_type_id,
				is_sale,
				status,
				products_quantity,
				products_image,
				products_description,
				products_name,
				products_type_id,
				barcode,
				products_kind_of,
				order_alert
			FROM
				products
			WHERE
				id = '" . (int)$this->getId() . "'
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Products not found",
				404
			);
		}
		
		$this->setProperties($this->dbFetchArray($q));

 		$this->productsTypeFields->setFilter('id', $this->getProductsTypeId());
 		$this->productsTypeFields->populate();

 		$this->umDetail->setFilter('id', $this->getUmTypeId());
 		$this->umDetail->populate();

	}

	
	// New functionality for calculate dynamic UM type when sale
	public function updateStockOutWithUM($productId, $qty, $umAmount){
		if( !$productId ) {
			throw new Exception("save method requires id");
		}
		// start filter product cost and qty to calculate
		$query = $this->dbQuery("
			SELECT
				p.products_quantity,
				p.products_price_in,
				u.amount
			FROM
				products p INNER JOIN um_type u on p.um_type_id = u.id
			WHERE
				p.id = '" . (int)$productId . "'
		");
		$product = $this->dbFetchArray($query);	
		// calculate UM
		$totalOldQty = doubleval($product['products_quantity']) * doubleval($product['amount']);
		$totalAddQty =  $qty * $umAmount;
		$newQty = ($totalOldQty - $totalAddQty) / doubleval($product['amount']);
		
		// start update stock
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = '" . $newQty . "'
			WHERE
				id = '" . (int)$productId . "'
		");
	}

	// old functionality deduct stock when sale
	public function updateStock($id) {
		if( !$id ) {
			throw new Exception("save method requires id");
		}
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = products_quantity - '" . $this->getProductsQuantity() . "'
			WHERE
				id = '" . (int)$id . "'
		");


	}

	// new function
	// functionality for calculate dynamic UM type when purchase
	public function updateStockInWithUM($productId, $qty, $umAmount){
		if( !$productId ) {
			throw new Exception("save method requires id");
		}
		// start filter product cost and qty to calculate
		$query = $this->dbQuery("
			SELECT
				p.products_quantity,
				p.products_price_in,
				u.amount
			FROM
				products p INNER JOIN um_type u on p.um_type_id = u.id
			WHERE
				p.id = '" . (int)$productId . "'
		");
		$product = $this->dbFetchArray($query);
		$totalOldPrice = doubleval($product['products_price_in']) * doubleval($product['products_quantity']);
		$totalNewPrice = $this->getProductsQuantity() * $this->getProductsPriceIn();

		// calculate UM
		$totalOldQty = doubleval($product['products_quantity']) * doubleval($product['amount']);
		$totalAddQty =  $qty * $umAmount;
		$newQty = ($totalOldQty + $totalAddQty) / doubleval($product['amount']);
		
		// sum qty and calculate average cost
		$totalQty = doubleval($qty) + doubleval($product['products_quantity']);
		$averageCost =  ($totalOldPrice + $totalNewPrice) / $totalQty;

		// start update average cost and stock
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = '" . $newQty . "',
				products_price_in = '" . $averageCost . "'
			WHERE
				id = '" . (int)$productId . "'
		");
	}

	// old function
	public function updateStockIn($id) {
		if( !$id ) {
			throw new Exception("save method requires id");
		}
		// start filter product cost and qty to calculate
		$query = $this->dbQuery("
			SELECT
				products_quantity,
				products_price_in
			FROM
				products
			WHERE
				id = '" . (int)$id . "'
		");
		$product = $this->dbFetchArray($query);
		$totalOldPrice = $product['products_price_in'] * $product['products_quantity'];
		$totalNewPrice = $this->getProductsQuantity() * $this->getProductsPriceIn();

		$totalQty = $this->getProductsQuantity() + doubleval($product['products_quantity']);
		$averageCost =  ($totalOldPrice + $totalNewPrice) / $totalQty;
		// assign cost to data
		$this->setProductsPriceIn($averageCost);
		// start update average cost and stock
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = '" . $totalQty . "',
				products_price_in = '" . $this->getProductsPriceIn() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	/*** Functionality for deduct stock after transfer stock  ****/
	public function updateStockFromTransferWhenVoid($id, $qty){
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = products_quantity - '" . doubleval($qty) . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	/*** Functionality for add stock after transfer stock change status to void ****/
	public function updateStockFromTransferWhenSave($id, $qty){
		$this->dbQuery("
			UPDATE
				products
			SET
				products_quantity = products_quantity + '" . doubleval($qty) . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus($id) {
		if( !$id ) {
			throw new Exception("save method requires id");
		}
		$this->dbQuery("
			UPDATE
				products
			SET 
				status = '" . (int)$this->getStatus() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				products
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function update($id){
		$this->dbQuery("
			UPDATE
				products
			SET
				products_name = '" . $this->dbEscape(  $this->getProductsName() ) . "',
				um_type_id = '" . $this->getUmTypeId() . "',
				products_quantity = '" . $this->getProductsQuantity() . "',
				products_image = '" . $this->getProductsImage() . "',
				products_kind_of = '" . $this->getProductsKindOf() . "',
				products_description = '" . $this->dbEscape( $this->getProductsDescription() ) . "',
 				price = '" . $this->getPrice() . "',
				cost = '" . $this->getCost() . "',
 				products_price_in = '" . $this->getProductsPriceIn() . "',
				barcode = '" . $this->getBarcode() . "',
				is_sale = '" . $this->getIsSale() . "',
 				products_type_id = '" . $this->getProductsTypeId() . "',
 				update_by = '" . $this->getUpdateBy() . "',
 				order_alert = '" . $this->getOrderAlert(). "'
			WHERE
				id = '" . (int)$id . "'
		");
		
	}
	
	public function insert(){	
		$this->dbQuery("
			INSERT INTO
				products
			(
				products_quantity,
				products_image,
				is_sale,
				um_type_id,
				products_name,
				price,
				cost,
				products_description,
				products_type_id,
				products_kind_of,
				barcode,
				status,
				order_alert,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getProductsQuantity() . "',
				'" . $this->getProductsImage() . "',
				'" . $this->getIsSale() . "',
				'" . $this->getUmTypeId() . "',
				'" . $this->dbEscape( $this->getProductsName() ). "',
 				'" . $this->getPrice() . "',
 				'" . $this->getCost() . "',
				'" . $this->dbEscape( $this->getProductsDescription() ). "',
				'" . $this->getProductsTypeId() . "',
				'" . $this->getProductsKindOf() . "',
				'" . $this->getBarcode() . "',
 				1,
 				'" . $this->getOrderAlert() ."',
 				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");	
		$this->setId( $this->dbInsertId() );
	}

	public function setIsSale( $string ){
		$this->isSale = (int)$string;
	}

	public function getIsSale(){
		return $this->isSale;
	}

	public function setUmTypeId( $string ){
		$this->umTypeId = (int)$string;
	}

	public function getUmTypeId(){
		return $this->umTypeId;
	}

	public function setUmDetail( $string ){
		$this->umDetail = $string;
	}

	public function getUmDetail(){
		return $this->umDetail;
	}

	public function setOrderAlert( $string ){
		$this->orderAlert = (int)$string;
	}

	public function getOrderAlert(){
		return $this->orderAlert;
	}

	public function setProductsImage( $string ){
		$this->productsImage = $string;
	}
	
	public function getProductsImage(){
		return $this->productsImage;
	}

	public function setProductsDescription( $string ){
		$this->productsDescription = $string;
	}
	
	public function getProductsDescription(){
		return $this->productsDescription;
	}
	
	public function setProductsName( $string ){
		$this->productsName = $string;
	}
	
	public function getProductsName(){
		return $this->productsName;
	}
	
	public function setProductsQuantity( $int ){
		$this->productsQuantity = doubleval($int);
	}
	
	public function getProductsQuantity(){
		return $this->productsQuantity;
	}
	
	public function setProductsTypeFields( $type ){
		$this->productsTypeFields = $type;
	}
	
	public function getProductsTypeFields(){
		return $this->productsTypeFields;
	}

	public function setProductsTypeId( $type ){
		$this->productsTypeId = (int)$type;
	}

	public function getProductsTypeId(){
		return $this->productsTypeId;
	}

	public function setProductsTypeName( $type ){
		$this->productsTypeName = $type;
	}

	public function getProductsTypeName(){
		return $this->productsTypeName;
	}

	public function getPrice(){
		return $this->price;
	}
	public function setPrice( $int ){
		$this->price = doubleval($int);
	}

	public function getCost(){
		return $this->cost;
	}
	public function setCost( $int ){
		$this->cost = doubleval($int);
	}

	public function getBarcode(){
		return $this->barcode;
	}
	public function setBarcode( $int ){
		$this->barcode = $int;
	}

	public function setProductsKindOf( $string ){
		$this->productsKindOf = $string;
	}

	public function getProductsKindOf(){
		return $this->productsKindOf;
	}

}
