<?php

namespace OSC\RoleSample;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$roleName
        , $description
	;

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'role_name',
                'description',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				role_name,
				description
			FROM
				role
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Setting not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				role
			SET
				role_name = '" . $this->getRoleName() . "',
				description = '" . $this->getDescription() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");

	}

	public function setRoleName( $string ){
		$this->roleName = $string;
	}

	public function getRoleName(){
		return $this->roleName;
	}

    public function setDescription( $string ){
        $this->description = $string;
    }

    public function getDescription(){
        return $this->description;
    }

}
