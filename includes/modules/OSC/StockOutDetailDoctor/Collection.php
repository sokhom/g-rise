<?php

namespace OSC\StockOutDetailDoctor;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('stock_out_detail_doctor', 'sod');
		$this->idField = 'sod.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		if($arg){
			$this->addWhere("sod.id = '" . (int)$arg. "' ");
		}
	}

	public function filterByStockOutId( $arg ){
		$this->addWhere("sod.stock_out_id = '" . (int)$arg. "' ");
	}

	public function sortByDoctorName($arg){
		$this->addOrderBy('sod.doctor_name', $arg);
	}

}
