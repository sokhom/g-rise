<?php

namespace OSC\StockOutDetailDoctor;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$stockOutId
		, $stockOutNo
		, $doctorId
		, $doctorName
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'doctor_name',
				'doctor_id',
				'stock_out_no',
				'stock_out_id',
				'id'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				stock_out_id,
				stock_out_no,
				doctor_id,
				doctor_name
			FROM
				stock_out_detail_doctor
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock Out Detail Doctor not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				stock_out_detail_doctor
			(
				stock_out_id,
				stock_out_no,
				doctor_id,
				doctor_name,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getStockOutId() . "',
				'" . $this->getStockOutNo() . "',
				'" . $this->getDoctorId() . "',
				'" . $this->getDoctorName()  . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}
	
	public function update(){
		tep_db_query("
			UPDATE
				stock_out_detail_doctor
			SET
				doctor_id = '" . $this->getDoctorId() . "',
				doctor_name = '" . $this->getDoctorName() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function setDoctorId( $string ){
		$this->doctorId = (int)$string;
	}

	public function getDoctorId(){
		return $this->doctorId;
	}

	public function setDoctorName( $string ){
		$this->doctorName = $string;
	}

	public function getDoctorName(){
		return $this->doctorName;
	}

	public function setStockOutId( $string ){
		$this->stockOutId = (int)$string;
	}
	
	public function getStockOutId(){
		return $this->stockOutId;
	}

	public function setStockOutNo( $string ){
		$this->stockOutNo = (string)$string;
	}

	public function getStockOutNo(){
		return $this->stockOutNo;
	}
	
}
