<?php

namespace OSC\CustomerListOnly;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$customersName
		, $customersEmailAddress
        , $customersGender
        , $customersDob
        , $customersTelephone
        , $customersAddress
		, $customersCountry
		, $customersCode
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'customers_name',
				'customers_telephone',
                'customers_email_address',
                'customers_address',
				'customers_code',
                'customers_gender',
                'customers_dob',
                'customers_country',
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				customers_name,
				customers_address,
				customers_telephone,
				customers_dob,
				customers_email_address,
				customers_gender,
				customers_country,
				customers_code
			FROM
				customers
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Customer List not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function setCustomersName( $string ){
		$this->customersName = $string;
	}
	
	public function getCustomersName(){
		return $this->customersName;
	}

    public function setCustomersGender( $string ){
        $this->customersGender = $string;
    }

    public function getCustomersGender(){
        return $this->customersGender;
    }

    public function setCustomersTelephone( $string ){
        $this->customersTelephone = $string;
    }
    public function getCustomersTelephone(){
        return $this->customersTelephone;
    }
	
    public function setCustomersDob( $string ){
        $this->customersDob = $string;//date("Y-m-d", strtotime($string));
    }
    public function getCustomersDob(){
        return $this->customersDob;
    }

    public function setCustomersAddress( $string ){
        $this->customersAddress = $string;
    }
    public function getCustomersAddress(){
        return $this->customersAddress;
    }


    public function setCustomersEmailAddress( $string ){
        $this->customersEmailAddress = $string;
    }

    public function getCustomersEmailAddress(){
        return $this->customersEmailAddress;
    }

    public function setCustomersCode( $string ){
        $this->customersCode = $string;
    }
    public function getCustomersCode(){
        return $this->customersCode;
    }

    public function setCustomersCountry( $string ){
        $this->customersCountry = $string;
    }
    public function getCustomersCountry(){
        return $this->customersCountry;
    }
}
