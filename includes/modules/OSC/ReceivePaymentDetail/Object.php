<?php
namespace OSC\ReceivePaymentDetail;
use
    Aedea\Core\Database\StdObject as DbObj,
    OSC\ReceivePaymentDetail\Collection as ReceivePaymentDetailCol
;

class Object extends DbObj{
     protected
        $receivePaymentId,
        $invoiceNo,
        $invoiceDate,
        $customerId,
        $customerCode,
        $customerName,
        $balance,
        $subTotal,
        $discountAmount,
        $grandTotal,
        $deposit,
        $payment
     ;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new ReceivePaymentDetailCol();
    }

    public function toArray($params=array()){
        $args= array(
            'include'=>array(
                'id',
                'receive_payment_id',
                'invoice_no',
                'invoice_date',
                'customer_id',
                'customer_name',
                'customer_code',
                'sub_total',
                'discount_amount',
                'grand_total',
                'deposit',
                'payment',
                'balance',
            )
        );
        return parent::toArray($args);
    }
    public function load( $params = array() ){
        $q = $this->dbQuery("
			SELECT
                receive_payment_id,
                invoice_no,
                invoice_date,
                customer_id,
                customer_code,
                customer_name,
                balance,
                sub_total,
                discount_amount,
                grand_total,
                deposit,
                payment
			FROM
				receive_payment_detail
			WHERE
				id = '" . (int)$this->getId() . "'
		");

        if( ! $this->dbNumRows($q) ){
            throw new \Exception(
                "404: Receive Payment Detail not found",
                404
            );
        }
        $this->setProperties($this->dbFetchArray($q));
    }

    public function insert(){
        $this->dbQuery("
			INSERT INTO
				receive_payment_detail
			(
				receive_payment_id,
                invoice_no,
                invoice_date,
                customer_id,
                customer_code,
                customer_name,
                balance,
                sub_total,
                discount_amount,
                grand_total,
                deposit,
                payment,
                create_date
			)
				VALUES
			(
			    '" . $this->getReceivePaymentId() . "',
				'" . $this->getInvoiceNo() . "',
				'" . $this->getInvoiceDate() . "',
                '" . $this->getCustomerId() . "',
                '" . $this->getCustomerCode() . "',
				'" . $this->getCustomerName() . "',
                '" . $this->getBalance() . "',
                '" . $this->getSubTotal() . "',
                '" . $this->getDiscountAmount() . "',
                '" . $this->getGrandTotal() . "',
                '" . $this->getDeposit() . "',
                '" . $this->getPayment() . "',
				NOW()
			)
		");
        $this->setId( $this->dbInsertId() );
    }

    public function update(){
        $this->dbQuery("
			UPDATE
				receive_payment_detail
			SET
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function delete(){
        if (!$this->getId()) {
            throw new Exception("delete method requires id to be set");
        }
        $this->dbQuery("
			DELETE FROM
				receive_payment_detail
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function setInvoiceNo($string){
        $this->invoiceNo = $string;
    }
    public function getInvoiceNo(){
        return $this->invoiceNo;
    }

    public function setInvoiceDate($string){
        $this->invoiceDate = $string;
    }
    public function getInvoiceDate(){
        return $this->invoiceDate;
    }

    public function setReceivePaymentId($string){
        $this->receivePaymentId = $string;
    }
    public function getReceivePaymentId(){
        return $this->receivePaymentId;
    }

    public function setBalance($string){
        $this->balance = doubleval($string);
    }
    public function getBalance(){
        return $this->balance;
    }

    public function setCustomerId($string){
        $this->customerId = $string;
    }
    public function getCustomerId(){
        return $this->customerId;
    }

    public function setCustomerCode($string){
        $this->customerCode = $string;
    }
    public function getCustomerCode(){
        return $this->customerCode;
    }

    public function setCustomerName($string){
        $this->customerName = $string;
    }
    public function getCustomerName(){
        return $this->customerName;
    }
    
    public function setSubTotal($string){
        $this->subTotal = doubleval($string);
    }
    public function getSubTotal(){
        return $this->subTotal;
    }

    public function setGrandTotal($string){
        $this->grandTotal = doubleval($string);
    }
    public function getGrandTotal(){
        return $this->grandTotal;
    }
    
    public function setDiscountAmount($string){
        $this->discountAmount = doubleval($string);
    }
    public function getDiscountAmount(){
        return $this->discountAmount;
    }
    
    public function setDeposit($string){
        $this->deposit = doubleval($string);
    }
    public function getDeposit(){
        return $this->deposit;
    }
    
    public function setPayment($string){
        $this->payment = doubleval($string);
    }
    public function getPayment(){
        return $this->payment;
    }
}
