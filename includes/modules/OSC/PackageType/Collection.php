<?php

namespace OSC\PackageType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('package_type', 'pkt');
		$this->idField = 'pkt.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("pkt.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("pkt.name LIKE '%" . $arg. "%' or pkt.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pkt.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('pkt.name', $arg);
	}


}
