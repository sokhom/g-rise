<?php

namespace OSC\Location;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\LocationType\Collection as LocationType
;

class Object extends DbObj {
		
	protected
		$name,
		$description,
		$typeId,
		$locationType
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->locationType = new LocationType();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'status',
				'type_id',
				'location_type',
				'description'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				description,
				type_id,
				status
			FROM
				location
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Location not found",
				404
			);
		}
		
		$this->setProperties($this->dbFetchArray($q));
		
		$this->locationType->setFilter('id', $this->getTypeId());
        $this->locationType->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				location
			SET
				name = '" . $this->dbEscape(  $this->getName() ). "',
				type_id = '" . $this->getTypeId() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				location
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				location
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				location
			(
				name,
				type_id,
				description,
				status,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getName() ). "',
				'" . $this->getTypeId() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				1,
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}
	
	public function setName( $string ){
		$this->name = (string)$string;
	}
	public function getName(){
		return $this->name;
	}
	
	public function setDescription( $string ){
		$this->description = (string)$string;
	}
	public function getDescription(){
		return $this->description;
	}
	
	public function setTypeId( $string ){
		$this->typeId = (int)$string;
	}
	public function getTypeId(){
		return $this->typeId;
	}
	
	public function setLocationType( $string ){
		$this->locationType = $string;
	}
	public function getLocationType(){
		return $this->locationType;
	}
}
