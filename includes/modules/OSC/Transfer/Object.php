<?php

namespace OSC\Transfer;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\TransferDetail\Collection as TransferDetailCol
;

class Object extends DbObj {
		
	protected
		$transferBranchName
		, $transferBranchId
		, $transferDate
		, $transferNo
		, $description
		, $transferDetail
		, $total
		, $transferTypeId
		, $transferTypeName
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->transferDetail = new TransferDetailCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'transfer_branch_name',
				'transfer_no',
				'transfer_branch_id',
				'transfer_date',
				'transfer_detail',
				'transfer_type_id',
				'transfer_type_name',
				'description',
				'status',
				'total',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				transfer_branch_name,
				transfer_branch_id,
				transfer_no,
				transfer_date,
				transfer_type_id,
				transfer_type_name,
				description,
				status,
				total
			FROM
				transfer_master
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: transfer not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
		
		$this->transferDetail->setFilter('transfer_id', $this->getId());
		$this->transferDetail->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				transfer_master
			SET
				transfer_date = '" . $this->getTransferDate() . "',
				transfer_branch_name = '" . $this->getTransferBranchName() . "',
				transfer_branch_id = '" . $this->getTransferBranchId() . "',
				description = '" . $this->getDescription() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				transfer_master
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				transfer_master
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				transfer_master
			(
				transfer_date,
				transfer_no,
				transfer_type_id,
				transfer_type_name,
				transfer_branch_id,
				transfer_branch_name,
				total,
				description,
				status,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getTransferDate() . "',
				'" . $this->getTransferNo() . "',
				'" . $this->getTransferTypeId() . "',
				'" . $this->getTransferTypeName() . "',
				'" . $this->getTransferBranchId() . "',
				'" . $this->getTransferBranchName() . "',
				'" . $this->getTotal() . "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setTransferNo( $string ){
		$this->transferNo = $string;
	}

	public function getTransferNo(){
		return $this->transferNo;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}

	public function getTotal(){
		return $this->total;
	}

	public function setTransferDate( $string ){
		$this->transferDate = $string;
	}
	
	public function getTransferDate(){
		return $this->transferDate;
	}

	public function setTransferBranchId( $string ){
		$this->transferBranchId = $string;
	}
	
	public function getTransferBranchId(){
		return $this->transferBranchId;
	}

	public function setTransferBranchName( $string ){
		$this->transferBranchName = $string;
	}
	
	public function getTransferBranchName(){
		return $this->transferBranchName;
	}

	public function setTransferDetail( $string ){
		$this->transferDetail = $string;
	}
	
	public function getTransferDetail(){
		return $this->transferDetail;
	}

	public function setTransferTypeId( $string ){
		$this->transferTypeId = (int)$string;
	}
	
	public function getTransferTypeId(){
		return $this->transferTypeId;
	}
	
	public function setTransferTypeName( $string ){
		$this->transferTypeName = $string;
	}
	
	public function getTransferTypeName(){
		return $this->transferTypeName;
	}
}
