<?php

namespace OSC\Transfer;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('transfer_master', 'tm');
		$this->idField = 'tm.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		$this->addWhere("tm.id = '" . (int)$arg. "' ");
	}

	public function filterByBranchId( $arg ){
		$this->addWhere("tm.transfer_branch_id = '" . (int)$arg. "' ");
	}

	public function filterByTransferTypeId( $arg ){
		$this->addWhere("tm.transfer_type_id = '" . (int)$arg. "' ");
	}

	public function filterByBranchName( $arg ){
		$this->addWhere("tm.transfer_branch_id = '" . (int)$arg . "' ");
	}

	public function filterByTransferNo($arg){
		$this->addWhere("tm.transfer_no LIKE '%" . $arg . "%' OR tm.transfer_branch_name LIKE '%" . $arg . "%'  OR tm.description LIKE '%" . $arg . "%'");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("tm.status = '" . (int)$arg. "' ");
	}

    public function filterByDate($from, $to){
        $this->addWhere("tm.transfer_date BETWEEN '" . $from . "' AND '" . $to . "' ");
    }

	public function sortById( $arg ){
		$this->addOrderBy('tm.id', $arg);
	}
}
