<?php

namespace OSC\DoctorPayslipDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('doctor_payslip_detail', 'dpd');
		$this->idField = 'dpd.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByDoctorName( $arg ){
		$this->addWhere("dpd.doctor_name LIKE '%" . $arg. "%' ");
	}

	public function filterByDoctorId( $arg ){
		$this->addWhere("dpd.doctor_id = " . (int)$arg. "");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("dpd.status = '" . (int)$arg. "' ");
	}

	public function filterByDate($from, $to){
		// WHERE From_date >= '2013-01-03' AND To_date   <= '2013-01-09'
		$this->addWhere("NOT (dpd.from_date > '". $from ."' OR dpd.to_date < '". $to ."') ");
	}

	public function filterById( $arg ){
		$this->addWhere("dpd.id = '" . (int)$arg. "' ");
	}

	public function filterByDoctorPayslipId( $arg ){
		$this->addWhere("dpd.doctor_payslip_id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('dpd.from_date', $arg);
		$this->addOrderBy('dpd.id', $arg);
	}
}
