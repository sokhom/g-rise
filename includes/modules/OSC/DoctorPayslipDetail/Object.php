<?php

namespace OSC\DoctorPayslipDetail;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\DoctorPayslipAllowanceDetail\Collection as doctorPayslipAllowanceDetailCol
;

class Object extends DbObj {
		
	protected
		$doctorId,
		$doctorPayslipId,
		$doctorName,
		$netPay,
		$basicSalary,
		$totalPayment,
		$taxPercentage,
		$fromDate,
		$toDate,
		$other,
		$deductBasicSalary,
		$deductTotalGet,
		$detail
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new doctorPayslipAllowanceDetailCol();
	}
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'doctor_payslip_id',
				'doctor_id',
				'doctor_name',
				'basic_salary',
				'total_payment',
				'tax_percentage',
				'deduct_basic_salary',
				'deduct_total_get',
				'net_pay',
				'status',
				'from_date',
				'to_date',
				'create_by',
				'other',
				'detail'
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				doctor_payslip_id,
				doctor_id,
				doctor_name,
				from_date,
				deduct_basic_salary,
				deduct_total_get,
				to_date,
				basic_salary,
				total_payment,
				tax_percentage,
				net_pay,
				create_by,
				status,
				other
			FROM
				doctor_payslip_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: doctor payslip detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('doctor_payslip_detail_id', $this->getId());
		$this->detail->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}var_dump((int)$this->getId());éxit;
		$this->dbQuery("
			UPDATE
				doctor_payslip_detail
			SET
				deduct_basic_salary = '" . $this->dbEscape( $this->getDeductBasicSalary() ) . "',
				deduct_total_get = '" . $this->dbEscape( $this->getDeductTotalGet() ) . "',
				from_date = '" . $this->dbEscape( $this->getFromDate() ) . "',
				to_date = '" . $this->dbEscape( $this->getToDate() ) . "',
				basic_salary = '" . $this->dbEscape( $this->getBasicSalary() ) . "',
				total_payment = '" . $this->dbEscape( $this->getTotalPayment() ) . "',
				tax_percentage = '" . $this->dbEscape( $this->getTaxPercentage() ) . "',
				net_pay = '" . $this->getNetPay() . "',
				other = '" . $this->getOther() . "',
				update_by_id = '" . $this->getUpdateById() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_payslip_detail
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				doctor_payslip_id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				doctor_payslip_detail
			WHERE
				doctor_payslip_id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_payslip_detail
			(
				doctor_payslip_id,
				doctor_id,
				doctor_name,
				deduct_basic_salary,
				deduct_total_get,
				other,
				from_date,
				to_date,
				basic_salary,
				total_payment,
				tax_percentage,
				net_pay,
				status,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getDoctorPayslipId() . "',
				'" . $this->getDoctorId() . "',
				'" . $this->dbEscape( $this->getDoctorName() ). "',
				'" . $this->getDeductBasicSalary() . "',
				'" . $this->getDeductTotalGet() . "',
				'" . $this->getOther() . "',
				'" . $this->getFromDate() . "',
				'" . $this->getToDate() . "',
				'" . $this->getBasicSalary() . "',
				'" . $this->getTotalPayment() . "',
				'" . $this->getTaxPercentage() . "',
				'" . $this->getNetPay() . "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDeductTotalGet( $string ){
		$this->deductTotalGet = doubleval($string);
	}

	public function getDeductTotalGet(){
		return $this->deductTotalGet;
	}

	public function setDeductBasicSalary( $string ){
		$this->deductBasicSalary = doubleval($string);
	}

	public function getDeductBasicSalary(){
		return $this->deductBasicSalary;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}

	public function setDoctorPayslipId( $string ){
		$this->doctorPayslipId = (int)$string;
	}

	public function getDoctorPayslipId(){
		return $this->doctorPayslipId;
	}

	public function setNetPay( $string ){
		$this->netPay = doubleval($string);
	}

	public function getNetPay(){
		return $this->netPay;
	}

	public function setOther( $string ){
		$this->other = doubleval($string);
	}

	public function getOther(){
		return $this->other;
	}

	public function setBasicSalary( $string ){
		$this->basicSalary = doubleval($string);
	}

	public function getBasicSalary(){
		return $this->basicSalary;
	}

	public function setTotalPayment( $string ){
		$this->totalPayment = doubleval($string);
	}

	public function getTotalPayment(){
		return $this->totalPayment;
	}

	public function setTaxPercentage( $string ){
		$this->taxPercentage = doubleval($string);
	}

	public function getTaxPercentage(){
		return $this->taxPercentage;
	}


	public function setDoctorId( $string ){
		$this->doctorId = (int)$string;
	}
	
	public function getDoctorId(){
		return $this->doctorId;
	}

	public function setDoctorName( $string ){
		$this->doctorName = $string;
	}
	
	public function getDoctorName(){
		return $this->doctorName;
	}

	public function setFromDate( $string ){
		$this->fromDate = $string;
	}

	public function getFromDate(){
		return $this->fromDate;
	}

	public function setToDate( $string ){
		$this->toDate = $string;
	}

	public function getToDate(){
		return $this->toDate;
	}

}
