<?php

namespace OSC\UserInfo;

use
	Aedea\Core\Database\StdObject as DbObj
    , OSC\Role\Collection as RoleCol
;

class Object extends DbObj {
		
	protected
		$userName
		, $userPassword
		, $roleId
		, $email
		, $fullName
        , $roleDetail
	;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->roleDetail = new RoleCol();
    }

    public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'user_name',
				'full_name',
				'role_id',
                'role_detail',
                'email',
				'status'
			)
		);
		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				user_name,
				full_name,
				role_id,
				email,
				status
			FROM
				administrators
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Administrator not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

        $this->roleDetail->setFilter('id', $this->getRoleId());
        $this->roleDetail->populate();
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				administrators
			(
				user_name,
				user_password,
				email,
				role_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getUserName() ). "',
				'" . $this->getUserPassword() . "',
				'" . $this->getEmail() . "',
				'" . $this->getRoleId() . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function update(){
		$this->dbQuery("
			UPDATE
				administrators
			SET
				user_name = '" . $this->dbEscape( $this->getUserName() ). "',
				user_password = '" . $this->getUserPassword() . "',
				email = '" . $this->getEmail() . "',
				role_id = '" . $this->getRoleId() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				administrators
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				administrators
			WHERE
				id = '" . (int)$id . "'
		");
	}

    public function setEmail( $string ){
        $this->email = (string)$string;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setUserName( $string ){
		$this->userName = (string)$string;
	}

	public function getUserName(){
		return $this->userName;
	}

	public function setRoleId( $string ){
		$this->roleId = (int)$string;
	}

	public function getRoleId(){
		return $this->roleId;
	}

    public function setRoleDetail( $string ){
        $this->roleDetail = $string;
    }

    public function getRoleDetail(){
        return $this->roleDetail;
    }

	public function setUserPassword( $string ){
		$this->userPassword = (string)$string;
	}
	public function getUserPassword(){
		return $this->userPassword;
	}
	
    public function setFullName( $string ){
		$this->fullName = (string)$string;
	}
	public function getFullName(){
		return $this->fullName;
	}
}
