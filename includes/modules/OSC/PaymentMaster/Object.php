<?php

namespace OSC\PaymentMaster;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\PaymentMasterDetail\Collection as PaymentDetailCol
	;

class Object extends DbObj {

	protected
		$paymentNo
		, $vendorId
		, $vendorName
		, $paymentDate
		, $description
		, $discountType
		, $discountAmount
		, $discountTotalAmount
		, $grandTotal
		, $paymentMethod
		, $bankCharge
		, $subTotal
        , $payment
        , $remain
		, $detail
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new PaymentDetailCol();
	}


	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'payment_no',
				'vendor_id',
				'vendor_name',
				'payment_date',
				'description',
				'discount_type',
                'discount_amount',
                'discount_total_amount',
                'payment_method',
                'detail',
                'grand_total',
                'sub_total',
                'payment',
                'remain',
				'bank_charge',
				'detail',
				'status'
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				payment_no,
				vendor_id,
				vendor_name,
				payment_date,
				description,
				discount_type,
                discount_amount,
                discount_total_amount,
                payment_method,
                bank_charge,
                grand_total,
                sub_total,
                payment,
                status,
                remain
			FROM
				payment_master
			WHERE
				id = '" . (int)$this->getId() . "'	
		");

		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Payment Master not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('payment_id', $this->getId());
		$this->detail->populate();
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				payment_master
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				payment_master
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				payment_master
			(
				payment_no,
				vendor_id,
				vendor_name,
				payment_date,
				description,
				discount_type,
                discount_amount,
                discount_total_amount,
                payment_method,
                bank_charge,
                grand_total,
                sub_total,
                payment,
                status,
                remain,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getPaymentNo() . "',
				'" . $this->getVendorId() . "',
				'" . $this->getVendorName() . "',
				'" . $this->getPaymentDate() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getDiscountType() . "',
				'" . $this->getDiscountAmount() . "',
				'" . $this->getDiscountTotalAmount() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getBankCharge() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getSubTotal() . "',
				'" . $this->getPayment() . "',
				1,
				'" . $this->getRemain() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setBankCharge( $string ){
		$this->bankCharge = doubleval($string);
	}
	public function getBankCharge(){
		return $this->bankCharge;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}
	public function getDetail(){
		return $this->detail;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setVendorId( $string ){
		$this->vendorId = $string;
	}

	public function getVendorId(){
		return $this->vendorId;
	}

	public function setVendorName( $string ){
		$this->vendorName = $string;
	}

	public function getVendorName(){
		return $this->vendorName;
	}

	public function setPaymentNo( $string ){
		$this->paymentNo = $string;
	}

	public function getPaymentNo(){
		return $this->paymentNo;
	}

	public function setReferenceNo( $string ){
		$this->referenceNo = $string;
	}

	public function getReferenceNo(){
		return $this->referenceNo;
	}

	public function setPaymentDate( $string ){
		$this->paymentDate = $string;
	}

	public function getPaymentDate(){
		return $this->paymentDate;
	}

    public function setPaymentMethod($string){
        $this->paymentMethod = $string;
    }
    public function getPaymentMethod(){
        return $this->paymentMethod;
    }

    public function setDiscountType($string){
        $this->discountType = $string;
    }
    public function getDiscountType(){
        return $this->discountType;
    }

    public function setDiscountAmount($string){
        $this->discountAmount = doubleval( $string );
    }
    public function getDiscountAmount(){
        return $this->discountAmount;
    }

    public function setDiscountTotalAmount($string){
        $this->discountTotalAmount = doubleval( $string );
    }
    public function getDiscountTotalAmount(){
        return $this->discountTotalAmount;
    }

    public function setGrandTotal($string){
        $this->grandTotal = doubleval( $string );
    }
    public function getGrandTotal(){
        return $this->grandTotal;
    }

    public function setExchangeId($string){
        $this->exchangeId =(int)$string;
    }
    public function getExchangeId(){
        return $this->exchangeId;
    }

    public function setSubTotal($string){
        $this->subTotal = doubleval( $string );
    }
    public function getSubTotal(){
        return $this->subTotal;
    }

    public function setPayment($string){
        $this->payment = doubleval( $string );
    }
    public function getPayment(){
        return $this->payment;
    }

    public function setRemain($string){
        $this->remain = doubleval( $string );
    }
    public function getRemain(){
        return $this->remain;;
    }


}
