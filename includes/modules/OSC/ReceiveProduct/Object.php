<?php

namespace OSC\ReceiveProduct;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\ReceiveProductDetail\Collection as receiveProductDetailCol
;

class Object extends DbObj {
		
	protected
		$receiveProductBranchName
		, $receiveProductBranchId
		, $receiveProductDate
		, $receiveProductNo
		, $description
		, $receiveProductDetail
		, $total
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->receiveProductDetail = new receiveProductDetailCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'receive_product_branch_name',
				'receive_product_no',
				'receive_product_branch_id',
				'receive_product_date',
				'receive_product_detail',
				'description',
				'status',
				'total',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				receive_product_branch_name,
				receive_product_branch_id,
				receive_product_no,
				receive_product_date,
				description,
				status,
				total
			FROM
				receive_product_master
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: receive product not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
		
		$this->receiveProductDetail->setFilter('receive_product_id', $this->getId());
		$this->receiveProductDetail->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				receive_product_master
			SET
				receive_product_date = '" . $this->getReceiveProductDate() . "',
				receive_product_branch_name = '" . $this->getReceiveProductBranchName() . "',
				receive_product_branch_id = '" . $this->getReceiveProductBranchId() . "',
				description = '" . $this->getDescription() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				receive_product_master
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				receive_product_master
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				receive_product_master
			(
				receive_product_date,
				receive_product_no,
				receive_product_branch_id,
				receive_product_branch_name,
				total,
				description,
				status,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getReceiveProductDate() . "',
				'" . $this->getReceiveProductNo() . "',
				'" . $this->getReceiveProductBranchId() . "',
				'" . $this->getReceiveProductBranchName() . "',
				'" . $this->getTotal() . "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setReceiveProductNo( $string ){
		$this->receiveProductNo = $string;
	}

	public function getReceiveProductNo(){
		return $this->receiveProductNo;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}

	public function getTotal(){
		return $this->total;
	}

	public function setReceiveProductDate( $string ){
		$this->receiveProductDate = $string;
	}
	
	public function getReceiveProductDate(){
		return $this->receiveProductDate;
	}

	public function setReceiveProductBranchId( $string ){
		$this->receiveProductBranchId = $string;
	}
	
	public function getReceiveProductBranchId(){
		return $this->receiveProductBranchId;
	}

	public function setReceiveProductBranchName( $string ){
		$this->receiveProductBranchName = $string;
	}
	
	public function getReceiveProductBranchName(){
		return $this->receiveProductBranchName;
	}

	public function setReceiveProductDetail( $string ){
		$this->receiveProductDetail = $string;
	}
	
	public function getReceiveProductDetail(){
		return $this->receiveProductDetail;
	}

	public function setReceiveProductTypeId( $string ){
		$this->receiveProductTypeId = (int)$string;
	}
	
	public function getReceiveProductTypeId(){
		return $this->receiveProductTypeId;
	}
	
	public function setReceiveProductTypeName( $string ){
		$this->receiveProductTypeName = $string;
	}
	
	public function getReceiveProductTypeName(){
		return $this->receiveProductTypeName;
	}
}
