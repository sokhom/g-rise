<?php

namespace OSC\ReceiveProduct;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('receive_product_master', 'rp');
		$this->idField = 'rp.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		$this->addWhere("rp.id = '" . (int)$arg. "' ");
	}

	// public function filterByTransferTypeId( $arg ){
	// 	$this->addWhere("rp.transfer_type_id = '" . (int)$arg. "' ");
	// }

	public function filterByBranchName( $arg ){
		$this->addWhere("rp.receive_product_branch_id = '" . (int)$arg . "' ");
	}

	public function filterByTransferNo($arg){
		$this->addWhere("rp.receive_product_no LIKE '%" . $arg . "%' OR rp.receive_product_branch_name LIKE '%" . $arg . "%' OR rp.description LIKE '%" . $arg . "%'");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("rp.status = '" . (int)$arg. "' ");
	}

    public function filterByDate($from, $to){
        $this->addWhere("rp.receive_product_date BETWEEN '" . $from . "' AND '" . $to . "' ");
    }

	public function sortById( $arg ){
		$this->addOrderBy('rp.id', $arg);
	}
}
