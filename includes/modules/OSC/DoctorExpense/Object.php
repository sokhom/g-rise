<?php

namespace OSC\DoctorExpense;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\DoctorExpenseDetail\Collection as doctorDetailCol
	// OSC\CustomerListOnly\Collection as customerCol
;

class Object extends DbObj {
		
	protected
		$doctorId
		, $doctorName
		, $doctorBasicSalary
		, $customerId
		, $customerName
		, $expenseDate
		, $invoiceNo
		, $invoiceDate
		, $note
		, $payment
		, $paymentType
		, $total
		, $grandTotal
		, $doctorDetail
		, $customerRemain
		, $customerPayment
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->doctorDetail = new doctorDetailCol();
		// $this->customerDetail = new customerCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'doctor_id',
				'doctor_name',
				'doctor_basic_salary',
				'customer_id',
				'customer_name',
				'status',
				'note',
				'expense_date',
				'invoice_no',
				'invoice_date',
				'payment',
				'payment_type',
				'total',
				'grand_total',
				'customer_remain',
				'customer_payment',
				'doctor_detail',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				doctor_id,	
				doctor_name,	
				doctor_basic_salary,
				customer_id,
				customer_name,	
				status,
				note,
				expense_date,
				invoice_no,
				invoice_date,
				payment,
				payment_type,
				total,
				grand_total,
				customer_remain,
				customer_payment
			FROM
				doctor_expense
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Doctor Expense not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		// $this->customerDetail->setFilter('id', $this->getCustomerId());
		// $this->customerDetail->populate();

		$this->doctorDetail->setFilter('expense_id', $this->getId());
		$this->doctorDetail->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_expense
			SET
				expense_date = '" . $this->getExpenseDate() . "',
				invoice_no = '" . $this->getInvoiceNo() . "',
				invoice_date = '" . $this->getInvoiceDate() . "',
				doctor_id = '" . $this->getDoctorId() . "',
				doctor_name = '" . $this->getDoctorName() . "',
				doctor_basic_salary = '" . $this->getDoctorBasicSalary() . "',
				customer_id = '" . $this->getCustomerId() . "',
				customer_name = '" . $this->getCustomerName() . "',
				note = '" . $this->getNote() . "',
				payment = '" . $this->getPayment() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_expense
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				doctor_expense
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_expense
			(
				doctor_id,
				doctor_name,
				doctor_basic_salary,
				customer_id,
				customer_name,
				status,
				note,
				payment_type,
				total,
				grand_total,
				expense_date,
				invoice_no,
				invoice_date,
				payment,
				customer_remain,
				customer_payment,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getDoctorId() . "',
				'" . $this->dbEscape( $this->getDoctorName() ). "',
				'" . $this->getDoctorBasicSalary() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->dbEscape( $this->getCustomerName() ). "',
				1,
				'" . $this->dbEscape(  $this->getNote() ). "',
				'" . $this->getPaymentType() . "',
				'" . $this->getTotal() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getExpenseDate() . "',
				'" . $this->getInvoiceNo() . "',
				'" . $this->getInvoiceDate() . "',
				'" . $this->getPayment() . "',
				'" . $this->getCustomerPayment() . "',
				'" . $this->getCustomerRemain() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDoctorId( $string ){
		$this->doctorId = (int)$string;
	}

	public function getDoctorId(){
		return $this->doctorId;
	}

	public function setDoctorName( $string ){
		$this->doctorName = $string;
	}

	public function getDoctorName(){
		return $this->doctorName;
	}

	public function setDoctorBasicSalary( $string ){
		$this->doctorBasicSalary = $string;
	}

	public function getDoctorBasicSalary(){
		return $this->doctorBasicSalary;
	}

	public function setNote( $string ){
		$this->note = $string;
	}

	public function getNote(){
		return $this->note;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}

	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

	public function setInvoiceDate( $string ){
		$this->invoiceDate = $string;
	}

	public function getInvoiceDate(){
		return $this->invoiceDate;
	}

	public function setPayment( $string ){
		$this->payment = doubleval($string);
	}

	public function getPayment(){
		return $this->payment;
	}

	public function setExpenseDate( $string ){
		$this->expenseDate = date('Y-m-d', strtotime( $string ));
	}

	public function getExpenseDate(){
		return $this->expenseDate;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}

	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerName( $string ){
		$this->customerName = $string;
	}
	
	public function getCustomerName(){
		return $this->customerName;
	}

	public function setTotal( $string ){
		$this->total = doubleval($string);
	}
	
	public function getTotal(){
		return $this->total;
	}

	public function setGrandTotal( $string ){
		$this->grandTotal = doubleval($string);
	}
	
	public function getGrandTotal(){
		return $this->grandTotal;
	}
	public function setPaymentType( $string ){
		$this->paymentType = $string;
	}
	
	public function getPaymentType(){
		return $this->paymentType;
	}

	public function setDoctorDetail( $string ){
		$this->doctorDetail = $string;
	}
	
	public function getDoctorDetail(){
		return $this->doctorDetail;
	}

	public function setCustomerRemain( $string ){
		$this->customerRemain = $string;
	}
	
	public function getCustomerRemain(){
		return $this->customerRemain;
	}

	public function setCustomerPayment( $string ){
		$this->customerPayment = $string;
	}
	
	public function getCustomerPayment(){
		return $this->customerPayment;
	}
}
