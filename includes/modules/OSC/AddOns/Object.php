<?php

namespace OSC\AddOns;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\AddOnType\Collection as  addOnTypeCol
;

class Object extends DbObj {
		
	protected
		$name
		, $addOnTypeId
		, $price
		, $description
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new addOnTypeCol();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'description',
				'add_on_type_id',
				'price',
				'status',
				'detail'
			)
		);
		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				description,
				price,
				add_on_type_id,
				status
			FROM
				add_ons
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: add one not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('id', $this->getAddOnTypeId());
        $this->detail->populate();

	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				add_ons
			SET
				name = '" . $this->dbEscape(  $this->getName() ). "',
				add_on_type_id = '" . $this->getAddOnTypeId() . "',
				price = '" . $this->getPrice() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				add_ons
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				add_ons
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				add_ons
			(
				name,
				add_on_type_id,
				price,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getName() ). "',
				'" . $this->getAddOnTypeId() . "',
				'" . $this->getPrice() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setName( $string ){
		$this->name = $string;
	}	
	public function getName(){
		return $this->name;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}	
	public function getPrice(){
		return $this->price;
	}

	public function setAddOnTypeId( $string ){
		$this->addOnTypeId = (int)$string;
	}	
	public function getAddOnTypeId(){
		return $this->addOnTypeId;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}	
	public function getDetail(){
		return $this->detail;
	}

}
