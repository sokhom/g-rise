<?php

namespace OSC\AddOns;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('add_ons', 'ao');
		$this->idField = 'ao.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("ao.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("ao.name LIKE '%" . $arg. "%' or ao.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("ao.status = '" . (int)$arg. "' ");
	}

	public function filterByType( $arg ){
		$this->addWhere("ao.add_on_type_id = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('ao.name', $arg);
	}


}
