<?php

namespace OSC\RealProcessDelevery;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$realProcessId
		, $deleveryById
		, $deleveryBy
		, $deleveryDate
		, $deleveryTime
		, $description
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'delevery_by_id',
				'delevery_by',
				'delevery_date',
				'delevery_time',
				'description'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				delevery_by_id,
				delevery_by,
				delevery_date,
				delevery_time,
				description,
				real_process_id
			FROM
				real_process_delevery
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process Delevery not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete(){
		if( !$this->getRealProcessId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process_delevery
			WHERE
				real_process_id = " . $this->getRealProcessId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process_delevery
			(
				delevery_by_id,
				delevery_by,
				delevery_date,
				delevery_time,
				description,
				real_process_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getDeleveryById() . "',
				'" . $this->getDeleveryBy() . "',
				'" . $this->getDeleveryDate() . "',
				'" . $this->getDeleveryTime() . "',
				'" . $this->dbEscape( $this->getDescription() ) . "',
				'" . $this->getRealProcessId() . "',
				'" . $this->getCreateBy() . "',
 				NOW()

			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDeleveryDate($string ){
		$this->deleveryDate = $string;
	}
	public function getDeleveryDate(){
		return $this->deleveryDate;
	}

	public function setDeleveryTime( $string ){
		$this->deleveryTime = $string;
	}	
	public function getDeleveryTime(){
		return $this->deleveryTime;
	}

	public function setDeleveryBy( $string ){
		$this->deleveryBy = $string;
	}	
	public function getDeleveryBy(){
		return $this->deleveryBy;
	}
	
	public function setDeleveryById( $string ){
		$this->deleveryById = $string;
	}	
	public function getDeleveryById(){
		return $this->deleveryById;
	}

	public function setRealProcessId( $string ){
		$this->realProcessId = (int)$string;
	}	
	public function getRealProcessId(){
		return $this->realProcessId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}	
	public function getDescription(){
		return $this->description;
	}

}
