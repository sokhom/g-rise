<?php

namespace OSC\RealProcessDelevery;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('real_process_delevery', 'rpd');
		$this->idField = 'rpd.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("rpd.id = '" . (int)$arg. "' ");
	}

	public function filterByRealProcessId( $arg ){
			$this->addWhere("rpd.real_process_id = '" . (int)$arg. "' ");
	}

	public function filterByDeleveryId( $arg ){
		$this->addWhere("rpd.delevery_by_id = '" . (int)$arg. "' ");
	}

}
