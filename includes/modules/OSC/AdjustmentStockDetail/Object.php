<?php

namespace OSC\AdjustmentStockDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$productName,
		$productId,
		$adjustmentId,
		$productKindOf,
		$productQuantity,
		$barcode,
		$productImage,
		$productPriceIn,
		$productPriceOut,
		$note,
		$adjustQty,
		$productTypeId,
		$productTypeName,
		$balanceQty,
		$totalAmount,
		$productUmTypeId, 
		$productUmTypeAmount, 
		$productUmTypeName, 
		$umTypeId, 
		$umTypeName, 
		$umTypeAmount,
		$umTypeCollection
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'product_name',
				'product_id',
				'adjustment_id',
				'product_kind_of',
				'product_quantity',
				'barcode',
				'product_image',
				'product_price_in',
				'product_price_out',
				'note',
				'adjust_qty',
				'total_amount',
				'balance_qty',
				'um_type_id',
				'um_type_name',
				'um_type_amount',
				'product_um_type_name',
				'product_um_type_amount',
				'product_um_type_id',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				product_name,
				product_id,
				adjustment_id,
				product_kind_of,
				product_quantity,
				barcode,
				product_image,
				product_price_in,
				product_price_out,
				note,
				adjust_qty,
				total_amount,
				balance_qty,
				um_type_id,
				um_type_name,
				um_type_amount,
				product_um_type_name,
				product_um_type_amount,
				product_um_type_id
			FROM
				adjustment_stock_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Adjustment stock detail not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				adjustment_stock_detail
			SET
				name = '" . $this->getName() . "',
				detail = '" . $this->getDetail() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_type
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				adjustment_stock_detail
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				adjustment_stock_detail
			(
				um_type_id,
				um_type_name,
				um_type_amount,
				product_um_type_id,
				product_um_type_amount,
				product_um_type_name,
				product_name,
				product_id,
				product_type_id,
				product_type_name,
				adjustment_id,
				product_kind_of,
				product_quantity,
				barcode,
				product_image,
				product_price_in,
				product_price_out,
				total_amount,
				balance_qty,
				note,
				adjust_qty,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getUmTypeId() . "',
				'" . $this->getUmTypeName() . "',
				'" . $this->getUmTypeAmount() . "',
				'" . $this->getProductUmTypeId() . "',
				'" . $this->getProductUmTypeAmount() . "',
				'" . $this->getProductUmTypeName() . "',
				'" . $this->getProductName() . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductTypeId() . "',
				'" . $this->getProductTypeName() . "',
				'" . $this->getAdjustmentId() . "',
				'" . $this->getProductKindOf() . "',
				'" . $this->getProductQuantity() . "',
				'" . $this->getBarcode() . "',
				'" . $this->getProductImage() . "',
				'" . $this->getProductPriceIn() . "',
				'" . $this->getProductPriceOut() . "',
				'" . $this->getTotalAmount() . "',
				'" . $this->getBalanceQty() . "',
				'" . $this->getNote() . "',
				'" . $this->getAdjustQty() . "',
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setProductTypeName( $string ){
		$this->productTypeName = $string;
	}
	
	public function getProductTypeName(){
		return $this->productTypeName;
	}

	public function setProductTypeId( $string ){
		$this->productTypeId = (int)$string;
	}
	
	public function getProductTypeId(){
		return $this->productTypeId;
	}

	public function setAdjustmentId( $string ){
		$this->adjustmentId = (int)$string;
	}

	public function getAdjustmentId(){
		return $this->adjustmentId;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}
	
	public function getProductId(){
		return $this->productId;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}
	
	public function getProductName(){
		return $this->productName;
	}

	public function setProductKindOf( $string ){
		$this->productKindOf = $string;
	}
	
	public function getProductKindOf(){
		return $this->productKindOf;
	}
	
	public function setProductQuantity( $string ){
		$this->productQuantity = doubleval($string);
	}
	
	public function getProductQuantity(){
		return $this->productQuantity;
	}

	public function setProductImage( $string ){
		$this->productImage = $string;
	}
	
	public function getProductImage(){
		return $this->productImage;
	}

	public function setBarcode( $string ){
		$this->barcode = $string;
	}
	
	public function getBarcode(){
		return $this->barcode;
	}
	
	public function setProductPriceOut( $string ){
		$this->productPriceOut = doubleval($string);
	}
	
	public function getProductPriceOut(){
		return $this->productPriceOut;
	}
	
	public function setProductPriceIn( $string ){
		$this->productPriceIn = doubleval($string);
	}
	
	public function getProductPriceIn(){
		return $this->productPriceIn;
	}
	
	public function setNote( $string ){
		$this->note = $string;
	}
	
	public function getNote(){
		return $this->note;
	}
	
	public function setAdjustQty( $string ){
		$this->adjustQty = doubleval($string);
	}
	
	public function getAdjustQty(){
		return $this->adjustQty;
	}
	
	public function setBalanceQty( $string ){
		$this->balanceQty = doubleval($string);
	}
	
	public function getBalanceQty(){
		return $this->balanceQty;
	}

	public function setTotalAmount( $string ){
		$this->totalAmount = doubleval($string);
	}
	
	public function getTotalAmount(){
		return $this->totalAmount;
	}

	/* UM Type */
	public function setProductUmTypeId( $string ){
		$this->productUmTypeId = (int)$string;
	}
	
	public function getProductUmTypeId(){
		return $this->productUmTypeId;
	}

	public function setProductUmTypeAmount( $string ){
		$this->productUmTypeAmount = doubleval($string);
	}
	
	public function getProductUmTypeAmount(){
		return $this->productUmTypeAmount;
	}

	public function setProductUmTypeName( $string ){
		$this->productUmTypeName = $string;
	}
	
	public function getProductUmTypeName(){
		return $this->productUmTypeName;
	}
	
	public function setUmTypeId( $string ){
		$this->umTypeId = doubleval($string);
	}
	
	public function getUmTypeId(){
		return $this->umTypeId;
	}

	public function setUmTypeAmount( $string ){
		$this->umTypeAmount = doubleval($string);
	}
	
	public function getUmTypeAmount(){
		return $this->umTypeAmount;
	}

	public function setUmTypeName( $string ){
		$this->umTypeName = $string;
	}
	
	public function getUmTypeName(){
		return $this->umTypeName;
	}

	public function setUmTypeCollection( $string ){
		$this->umTypeCollection = $string;
	}
	
	public function getUmTypeCollection(){
		return $this->umTypeCollection;
	}
	

}
