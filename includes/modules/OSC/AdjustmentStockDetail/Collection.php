<?php

namespace OSC\AdjustmentStockDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('adjustment_stock_detail', 'asd');
		$this->idField = 'asd.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		if($arg){
			$this->addWhere("asd.id = '" . (int)$arg. "' ");
		}
	}

	public function filterByAdjustmentId( $arg ){
		$this->addWhere("asd.adjustment_id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('dt.id', $arg);
	}
}
