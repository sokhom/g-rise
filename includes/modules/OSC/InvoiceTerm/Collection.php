<?php

namespace OSC\InvoiceTerm;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('invoice_term', 'it');
		$this->idField = 'it.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("it.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("it.title LIKE '%" . $arg. "%' ");
	}

	public function filterByType( $arg ){
		$this->addWhere("it.type = '" . $arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('it.title', $arg);
	}


}
