<?php

namespace OSC\InvoiceTerm;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$title
		, $type
		, $description
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'type',
				'title',
				'description',
				'status',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				title,
				type,
				description
			FROM
				invoice_term
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: invoice term not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				invoice_term
			SET
				title = '" . $this->dbEscape(  $this->getTitle() ). "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				invoice_term
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				invoice_term
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				invoice_term
			(
				title,
				type,
				status,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getTitle() ). "',
				'" . $this->dbEscape(  $this->getType() ). "',
				1,
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}	
	public function getTitle(){
		return $this->title;
	}

	public function setType( $string ){
		$this->type = $string;
	}	
	public function getType(){
		return $this->type;
	}

}
