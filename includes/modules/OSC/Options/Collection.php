<?php

namespace OSC\Options;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('options', 'o');
		$this->idField = 'o.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("o.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("o.code LIKE '%" . $arg. "%' or o.name LIKE '%" . $arg. "%' or o.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("o.status = '" . (int)$arg. "' ");
	}

	public function filterByOptionType( $arg ){
		$this->addWhere("o.option_type_id = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('o.name', $arg);
	}


}
