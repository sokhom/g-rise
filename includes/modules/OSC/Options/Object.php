<?php

namespace OSC\Options;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\OptionType\Collection as optionType
;

class Object extends DbObj {
		
	protected
		$name
		, $qty
		, $code
		, $optionTypeId
		, $optionType
		, $description
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->optionType = new optionType();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'qty',
				'option_type',
				'option_type_id',
				'code',
				'description',
				'status',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				qty,
				description,
				option_type_id,
				code,
				status
			FROM
				options
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: options not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->optionType->setFilter('id', $this->getOptionTypeId());
        $this->optionType->populate();
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				options
			SET
				name = '" . $this->dbEscape(  $this->getName() ). "',
				qty = '" . $this->getQty() . "',
				option_type_id = '" . $this->getOptionTypeId() . "',
				code = '" . $this->getCode() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				options
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				options
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				options
			(
				name,
				qty,
				code,
				option_type_id,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getName() ). "',
				'" . $this->getQty(). "',
				'" . $this->getCode(). "',
				'" . $this->getOptionTypeId() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setName( $string ){
		$this->name = $string;
	}	
	public function getName(){
		return $this->name;
	}

	public function setQty( $string ){
		$this->qty = doubleval($string);
	}	
	public function getQty(){
		return $this->qty;
	}

	public function setCode( $string ){
		$this->code = $string;
	}	
	public function getCode(){
		return $this->code;
	}

	public function setOptionTypeId( $string ){
		$this->optionTypeId = (int)$string;
	}	
	public function getOptionTypeId(){
		return $this->optionTypeId;
	}

	public function setOptionType( $string ){
		$this->optionType = $string;
	}	
	public function getOptionType(){
		return $this->optionType;
	}

}
