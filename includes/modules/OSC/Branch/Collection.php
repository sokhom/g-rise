<?php

namespace OSC\Branch;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('branch', 'b');
		$this->idField = 'b.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("b.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("b.name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("b.status = '" . (int)$arg. "' ");
	}


	public function sortByName( $arg ){
		$this->addOrderBy('b.name', $arg);
	}


}
