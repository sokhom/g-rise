<?php

namespace OSC\TransferDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('transfer_detail', 'td');
		$this->idField = 'td.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("td.id = '" . (int)$arg. "' ");
	}

	public function filterByTransferId( $arg ){
		$this->addWhere("td.transfer_id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("td.status = '" . (int)$arg. "' ");
	}

}
