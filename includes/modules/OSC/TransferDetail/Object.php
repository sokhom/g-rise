<?php

namespace OSC\TransferDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$transferId
		, $productId
		, $productName
		, $productBarcode
		, $qty
		, $qtyOnHand
		, $balanceQty
		, $cost
		, $price
		, $productUmTypeId
		, $productUmTypeAmount
		, $productUmTypeName
		, $umTypeId
		, $umTypeName
		, $umTypeAmount
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'transfer_id',
				'um_type_id',
				'um_type_name',
				'um_type_amount',
				'product_um_type_name',
				'product_um_type_amount',
				'product_um_type_id',
				'product_id',
				'product_name',
				'product_barcode',
				'qty',
				'qty_on_hand',
				'balance_qty',
				'price',
				'cost',
				'status',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				transfer_id,
				product_id,
				um_type_id,
				um_type_name,
				um_type_amount,
				product_um_type_name,
				product_um_type_amount,
				product_um_type_id,
				product_name,
				qty_on_hand,
				balance_qty,
				product_barcode,
				qty,
				cost,
				price
			FROM
				transfer_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: transfer not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				transfer_detail
			SET
				product_id = '" . $this->getProductId() . "',
				product_barcode = '" . $this->getProductBarcode() . "',
				transfer_id = '" . $this->getTransferId() . "',
				product_name = '" . $this->getProductName() . "',
				um_type_id = '" . $this->getUmTypeId() . "',
				um_type_name = '" . $this->getUmTypeName() . "',
				um_type_amount = '" . $this->getUmTypeAmount() . "',
				product_um_type = '" . $this->getProductUmType() . "',
				product_um_amount = '" . $this->getProductUmAmount() . "',
				cost = '" . $this->getCost() . "',
				price = '" . $this->getPrice() . "',
				qty = '" . $this->getQty() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				transfer_detail
			SET
				status = '" . $this->getStatus() . "',
				update_by_id = '" . $this->getUpdateById() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				transfer_detail
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				transfer_detail
			(
				transfer_id,
				product_id,
				product_type_id,
				product_type_name,
				product_name,
				product_barcode,
				um_type_id,
				um_type_name,
				um_type_amount,
				product_um_type_id,
				product_um_type_amount,
				product_um_type_name,
				qty,
				qty_on_hand,
				balance_qty,
				price,
				cost,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->getTransferId() . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductTypeId() . "',
				'" . $this->getProductTypeName() . "',
				'" . $this->getProductName() . "',
				'" . $this->getProductBarcode() . "',
				'" . $this->getUmTypeId() . "',
				'" . $this->getUmTypeName() . "',
				'" . $this->getUmTypeAmount() . "',
				'" . $this->getProductUmTypeId() . "',
				'" . $this->getProductUmTypeAmount() . "',
				'" . $this->getProductUmTypeName() . "',
				'" . $this->getQty() . "',
				'" . $this->getQtyOnHand() . "',
				'" . $this->getBalanceQty() . "',
				'" . $this->getPrice() . "',
				'" . $this->getCost() . "',
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}

	public function getProductName(){
		return $this->productName;
	}

	public function setTransferId( $string ){
		$this->transferId = (int)$string;
	}
	
	public function getTransferId(){
		return $this->transferId;
	}

	public function setProductId( $string ){
		$this->productId = (int)$string;
	}
	
	public function getProductId(){
		return $this->productId;
	}

	public function setProductBarcode( $string ){
		$this->productBarcode = $string;
	}
	
	public function getProductBarcode(){
		return $this->productBarcode;
	}

	public function setQty( $string ){
		$this->qty = doubleval($string);
	}
	
	public function getQty(){
		return $this->qty;
	}

	public function setCost( $string ){
		$this->cost = doubleval($string);
	}
	
	public function getCost(){
		return $this->cost;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}
	
	public function getPrice(){
		return $this->price;
	}

	public function setBalanceQty( $string ){
		$this->balanceQty = doubleval($string);
	}
	
	public function getBalanceQty(){
		return $this->balanceQty;
	}

	public function setQtyOnHand( $string ){
		$this->qtyOnHand = doubleval($string);
	}
	
	public function getQtyOnHand(){
		return $this->qtyOnHand;
	}

	public function setProductTypeId( $string ){
		$this->productTypeId = (int)$string;
	}
	
	public function getProductTypeId(){
		return $this->productTypeId;
	}

	public function setProductTypeName( $string ){
		$this->productTypeName = $string;
	}
	
	public function getProductTypeName(){
		return $this->productTypeName;
	}

	public function setProductUmTypeId( $string ){
		$this->productUmTypeId = (int)$string;
	}
	
	public function getProductUmTypeId(){
		return $this->productUmTypeId;
	}

	public function setProductUmTypeAmount( $string ){
		$this->productUmTypeAmount = doubleval($string);
	}
	
	public function getProductUmTypeAmount(){
		return $this->productUmTypeAmount;
	}

	public function setProductUmTypeName( $string ){
		$this->productUmTypeName = $string;
	}
	
	public function getProductUmTypeName(){
		return $this->productUmTypeName;
	}
	
	public function setUmTypeId( $string ){
		$this->umTypeId = doubleval($string);
	}
	
	public function getUmTypeId(){
		return $this->umTypeId;
	}

	public function setUmTypeAmount( $string ){
		$this->umTypeAmount = doubleval($string);
	}
	
	public function getUmTypeAmount(){
		return $this->umTypeAmount;
	}

	public function setUmTypeName( $string ){
		$this->umTypeName = $string;
	}
	
	public function getUmTypeName(){
		return $this->umTypeName;
	}
}
