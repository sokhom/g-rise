<?php

namespace OSC\RealProcessDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('real_process_detail', 'prd');
		$this->idField = 'prd.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("prd.id = '" . (int)$arg. "' ");
	}

	public function filterByRealProcessId( $arg ){
			$this->addWhere("prd.real_process_id = '" . (int)$arg. "' ");
	}

	public function filterByInvoice( $arg ){
		$this->addWhere("prd.invoice_no LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("prd.status = '" . (int)$arg. "' ");
	}

	public function sortByInvoiceNo( $arg ){
		$this->addOrderBy('prd.invoice_no', $arg);
	}


}
