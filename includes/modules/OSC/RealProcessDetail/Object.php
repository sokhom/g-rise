<?php

namespace OSC\RealProcessDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$realProcessId
		, $optionId
		, $optionName
		, $optionCode
		, $qty
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'option_id',
				'option_name',
				'option_code',
				'qty'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				option_id,
				option_name,
				option_code,
				qty,
				real_process_id
			FROM
				real_process_detail
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	// public function update(){
	// 	if( !$this->getId() ) {
	// 		throw new Exception("save method requires id to be set");
	// 	}
	// 	$this->dbQuery("
	// 		UPDATE
	// 			real_process_detail
	// 		SET
	// 			invoice_no = '" . $this->dbEscape(  $this->getInvoiceNo() ). "',
	// 			confirm_date = '" . $this->getConfirmDate() . "',
	// 			location = '" . $this->getLocation() . "',
	// 			shoot_date = '" . $this->getShootDate() . "',
	// 			dress_select_date = '" . $this->getDressSelectDate() . "',
	// 			description = '" . $this->dbEscape(  $this->getDescription() ). "',
	// 			update_by = '" . $this->getUpdateBy() . "'
	// 		WHERE
	// 			id = '" . (int)$this->getId() . "'
	// 	");
	// }

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process_detail
			WHERE
				real_process_id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process_detail
			(
				option_id,
				option_name,
				option_code,
				qty,
				real_process_id
			)
				VALUES
			(
				'" . $this->getOptionId() . "',
				'" . $this->getOptionName(). "',
				'" . $this->getOptionCode(). "',
				'" . $this->getQty(). "',
				'" . $this->getRealProcessId(). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setOptionCode($string ){
		$this->optionCode = $string;
	}
	public function getOptionCode(){
		return $this->optionCode;
	}

	public function setOptionId( $string ){
		$this->optionId = $string;
	}	
	public function getOptionId(){
		return $this->optionId;
	}

	public function setOptionName( $string ){
		$this->optionName = $string;
	}	
	public function getOptionName(){
		return $this->optionName;
	}

	public function setRealProcessId( $string ){
		$this->realProcessId = $string;
	}	
	public function getRealProcessId(){
		return $this->realProcessId;
	}

	public function setQty( $string ){
		$this->qty = $string;
	}	
	public function getQty(){
		return $this->qty;
	}

}
