<?php

namespace OSC\ExpenseType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('expense_type', 'ep');
		$this->idField = 'ep.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		$this->addWhere("ep.name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("ep.status = '" . (int)$arg. "' ");
	}
	
	public function filterById( $arg ){
		$this->addWhere("ep.id = '" . (int)$arg. "' ");
	}
	public function sortByName($arg){
		$this->addOrderBy('ep.name', $arg);
	}

	public function sortById($arg){
		$this->addOrderBy('ep.id', $arg);
	}
}
