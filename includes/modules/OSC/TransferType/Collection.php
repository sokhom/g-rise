<?php

namespace OSC\TransferType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable("transfer_type", "pt");
		$this->idField = "pt.id";
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function sortById($arg){
		$this->addOrderBy("pt.id", $arg);
	}
	
	public function sortByName($arg){
		$this->addOrderBy("pt.name", $arg);
	}
	

	public function filterById( $arg ){
		$this->addWhere("pt.id = '" . (int)$arg . "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pt.status = '" . (int)$arg . "' ");
	}

	public function filterByName( $arg ){
		if($arg) {
			$this->addWhere("pt.name LIKE '%" . $arg . "%' ");
		}
	}
}
