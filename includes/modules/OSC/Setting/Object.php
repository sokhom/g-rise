<?php

namespace OSC\Setting;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$companyName
		, $description
		, $logo
        , $address
		, $telephone
		, $minPayment
		, $inv
		, $receiptNo
		, $quotationNo
		, $customerCode
	;

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'company_name',
				'address',
                'telephone',
				'description',
				'quotation_no',
				'receipt_no',
				'customer_code',
				'min_payment',
				'logo',
				'inv',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				company_name,
				description,
				customer_code,
				logo,
				inv,
				quotation_no,
				receipt_no,
				address,
				min_payment,
				telephone
			FROM
				setting
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Setting not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				setting
			SET
				company_name = '" . $this->dbEscape( $this->getCompanyName() ). "',
				customer_code = '" . $this->dbEscape( $this->getCustomerCode() ). "',
				inv = '" . $this->dbEscape($this->getInv() ). "',
				receipt_no = '" . $this->dbEscape($this->getReceiptNo() ). "',
				quotation_no = '" . $this->dbEscape($this->getQuotationNo() ). "',
				min_payment = '" . $this->getMinPayment() . "',
				address = '" . $this->dbEscape($this->getAddress() ). "',
				telephone = '" . $this->dbEscape($this->getTelephone() ). "',
				description = '" . $this->dbEscape($this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "',
				logo = '" . $this->dbEscape($this->getLogo()) . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");

	}

//	public function insert(){
//		$this->dbQuery("
//			INSERT INTO
//				setting
//			(
//				company_name,
//				description,
//				logo,
//				create_by,
//				create_date
//			)
//				VALUES
//			(
//				'" . $this->getCompanyName() . "',
//				'" . $this->getDescription() . "',
//				'" . $this->getLogo() . "',
//				'" . $this->getCreateBy() . "',
// 				NOW()
//			)
//		");
//		$this->setId( $this->dbInsertId() );
//	}

	public function setMinPayment( $string ){
		$this->minPayment = (int)$string;
	}
	public function getMinPayment(){
		return $this->minPayment;
	}

	public function setInv( $string ){
		$this->inv = $string;
	}
	public function getInv(){
		return $this->inv;
	}

	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}
	public function getCustomerCode(){
		return $this->customerCode;
	}


	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setLogo( $string ){
		$this->logo = $string;
	}

	public function getLogo(){
		return $this->logo;
	}

	public function setCompanyName( $string ){
		$this->companyName = $string;
	}
	
	public function getCompanyName(){
		return $this->companyName;
	}

    public function setTelephone( $string ){
        $this->telephone = $string;
    }

    public function getTelephone(){
        return $this->telephone;
    }

    public function setAddress( $string ){
        $this->address = $string;
    }

    public function getAddress(){
        return $this->address;
    }

	public function setQuotationNo( $string ){
		$this->quotationNo = $string;
	}
	public function getQuotationNo(){
		return $this->quotationNo;
	}

	public function setReceiptNo( $string ){
		$this->receiptNo = $string;
	}
	public function getReceiptNo(){
		return $this->receiptNo;
	}
}
