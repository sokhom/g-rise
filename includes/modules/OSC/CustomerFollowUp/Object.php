<?php

namespace OSC\CustomerFollowUp;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$customerId
		, $description
		, $creator
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'description',
				'creator',
				'create_date'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				customer_id,
				description,
				create_date,
				create_by as creator
			FROM
				customer_follow_up
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Customer follow up not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete($id){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				customer_follow_up
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				customer_follow_up
			(
				customer_id,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getCustomerId() . "',
				'" . $this->dbEscape( $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setCreator( $string ){
		$this->creator = $string;
	}
	public function getCreator(){
		return $this->creator;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}
	
	public function getCustomerId(){
		return $this->customerId;
	}

}
