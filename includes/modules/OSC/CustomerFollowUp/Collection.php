<?php

namespace OSC\CustomerFollowUp;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('customer_follow_up', 'cfu');
		$this->idField = 'cfu.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("cfu.id = '" . (int)$arg. "' ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("cfu.customer_id = '" . (int)$arg. "' ");
	}

	// public function filterByStatus( $arg ){
	// 	$this->addWhere("cfu.status = '" . (int)$arg. "' ");
	// }

	// public function sortByName($arg){
	// 	$this->addOrderBy('cfu.name', $arg);
	// }
	
}
