<?php

namespace OSC\DoctorPayslip;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('doctor_payslip', 'dp');
		$this->idField = 'dp.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		$this->addWhere("dp.title LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("dp.status = '" . (int)$arg. "' ");
	}

	public function filterByDate($from, $to){
		// WHERE From_date >= '2013-01-03' AND To_date   <= '2013-01-09'
		$this->addWhere("NOT (dp.from_date > '". $from ."' OR dp.to_date < '". $to ."') ");
	}

	public function filterById( $arg ){
		$this->addWhere("dp.id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('dp.from_date', $arg);
		$this->addOrderBy('dp.id', $arg);
	}
}
