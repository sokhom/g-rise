<?php

namespace OSC\DoctorPayslip;

use
	Aedea\Core\Database\StdObject as DbObj,
	OSC\DoctorPayslipDetail\Collection as doctorPayslipDetailCol
;

class Object extends DbObj {
		
	protected
		$title
		, $note
		, $fromDate
		, $toDate
		, $totalNetPay
		, $detail
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new doctorPayslipDetailCol();
		// $this->customerDetail = new customerCol();
	}

	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'title',
				'from_date',
				'to_date',
				'total_net_pay',
				'note',
				'status',
				'detail',
				'create_by'
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				title,
				from_date,
				to_date,
				total_net_pay,
				note,
				create_by,
				status
			FROM
				doctor_payslip
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: doctor payslip not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('doctor_payslip_id', $this->getId());
		$this->detail->populate();
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_payslip
			SET
				title = '" . $this->dbEscape( $this->getTitle() ). "',
				note = '" . $this->dbEscape(  $this->getNote() ). "',
				total_net_pay = '" . $this->getTotalNetPay() . "',
				from_date = '" . $this->getFromDate() . "',
				to_date = '" . $this->getToDate() . "',
				update_by_id = '" . $this->getUpdateById() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_payslip
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				doctor_payslip
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_payslip
			(
				title,
				note,
				total_net_pay,
				from_date,
				to_date,
				status,
				create_by,
				create_by_id,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getTitle() ). "',
				'" .  $this->dbEscape( $this->getNote() ) . "',
				'" . $this->getTotalNetPay() . "',
				'" . $this->getFromDate() . "',
				'" . $this->getToDate() . "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setNote( $string ){
		$this->note = $string;
	}

	public function getNote(){
		return $this->note;
	}

	public function setTotalNetPay( $string ){
		$this->totalNetPay = doubleval($string);
	}

	public function getTotalNetPay(){
		return $this->totalNetPay;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}
	
	public function getTitle(){
		return $this->title;
	}

	public function setFromDate( $string ){
		$this->fromDate = $string;
	}

	public function getFromDate(){
		return $this->fromDate;
	}

	public function setToDate( $string ){
		$this->toDate = $string;
	}

	public function getToDate(){
		return $this->toDate;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}
}
