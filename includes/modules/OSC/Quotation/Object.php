<?php

namespace OSC\Quotation;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\QuotationDetail\Collection
		as  InvoiceDetailCol
;

class Object extends DbObj {
		
	protected
		$invoiceNo
		, $invoiceDate
		, $customerTel
		, $customerId
		, $customerName
		, $customerGender
		, $customerTypeId
		, $customerTypeName
		, $customerCode
		, $customerStaffResponsible
		, $customerStaffResponsibleId
		, $customerEventDate
		, $customerShootDate
		, $total
		, $noted
		, $paymentMethod
		, $exchangeId
		, $description
		, $subTotal
		, $discount
		, $discountType
		, $totalDiscount
		, $grandTotal
		, $deposit
		, $balance
		, $detail
		, $creator
		, $overdue
		, $termCondition
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new InvoiceDetailCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'description',
				'invoice_no',
				'exchange_id',
				'term_condition',
				'invoice_date',
				'customer_type_id',
				'customer_gender',
				'customer_type_name',
				'customer_code',
				'customer_staff_responsible',
				'customer_id',
				'customer_name',
				'customer_tel',
				'customer_event_date',
				'customer_shoot_date',
				'payment_method',
				'sub_total',
				'discount',
				'discount_type',
				'total_discount',
				'grand_total',
				'deposit',
				'balance',
				'detail',
				'status',
				'creator',
				'create_date',
				'overdue'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				invoice_no,
				create_date,
				create_by as creator,
				customer_gender,
				customer_tel,
				customer_type_id,
				term_condition,
				customer_type_name,
				customer_code,
				customer_shoot_date,				
				customer_id,
				customer_name,
				customer_event_date,
				customer_staff_responsible,
				description,
				payment_method,
				sub_total,
				discount,
				discount_type,
				total_discount,
				overdue,
				grand_total,
				deposit,
				invoice_date,
				balance,
				exchange_id,
				status
			FROM
				quotation
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: quotation not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
		$this->detail->setFilter('id', $this->getId());
		$this->detail->populate();
	}

	public function update(){
		$this->dbQuery("
			UPDATE
				invoice
			SET
                balance = '" . $this->getBalance() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	// update balance from receive payment when save
	public function updateBalance(){
		$this->dbQuery("
			UPDATE
				invoice
			SET
				balance = balance - '" . $this->getDeposit() . "',
				overdue ='" . $this->getOverdue() . "',
               	deposit = deposit + '" . $this->getDeposit() . "',
               	update_by = '" . $this->getUpdateBy() . "'
			WHERE
				invoice_no = '" . $this->getInvoiceNo() . "'
		");
	}

	
	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				quotation
			SET
                status = '" . $this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				invoice_no = '" . $this->getInvoiceNo() . "'
		");
	}


	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				quotation
			WHERE
				id = '" . $this->getId() . "'
		");

		$this->dbQuery("
			DELETE FROM
				quotation_detail
			WHERE
				id = '" . $this->getId() . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				quotation
			(
				invoice_no,
				invoice_date,
				term_condition,
				customer_id,
				customer_name,
				customer_gender,
				customer_type_id,
				customer_type_name,
				customer_event_date,
				customer_shoot_date,
				customer_code,
				customer_staff_responsible,
				customer_staff_responsible_id,
				customer_tel,
				payment_method,
				sub_total,
				discount,
				discount_type,
				total_discount,
				grand_total,
				deposit,
				balance,
				create_by,
				create_date,
				overdue,
				status,
				exchange_id,
				description
			)
				VALUES
			(
				'" . $this->getInvoiceNo() . "',
				'" . $this->getInvoiceDate() . "',
				'" . $this->dbEscape( $this->getTermCondition() ) . "',
				'" . $this->getCustomerId() . "',
				'" . $this->getCustomerName() . "',
				'" . $this->getCustomerGender() . "',
				'" . $this->getCustomerTypeId() . "',
				'" . $this->getCustomerTypeName() . "',
				'" . $this->getCustomerEventDate() . "',
				'" . $this->getCustomerShootDate() . "',
				'" . $this->getCustomerCode() . "',
				'" . $this->getCustomerStaffResponsible() . "',
				'" . $this->getCustomerStaffResponsibleId() . "',
				'" . $this->getCustomerTel() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getSubTotal() . "',
				'" . $this->getDiscount() . "',
				'" . $this->getDiscountType() . "',
				'" . $this->getTotalDiscount() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getDeposit() . "',
				'" . $this->getBalance() . "',
				'" . $this->getCreateBy() . "',
				NOW(),
				NOW(),
				1,
				'" . $this->getExchangeId() . "',
				'" . $this->dbEscape( $this->getDescription() ). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setTermCondition( $string ){
		$this->termCondition = $string;
	}
	public function getTermCondition(){
		return $this->termCondition;
	}

	public function setOverdue( $string ){
		$this->overdue = $string;
	}
	public function getOverdue(){
		return $this->overdue;
	}

	public function setCustomerShootDate( $string ){
		$this->customerShootDate = $string;
	}
	public function getCustomerShootDate(){
		return $this->customerShootDate;
	}

	public function setCustomerTel( $string ){
		$this->customerTel = $string;
	}
	public function getCustomerTel(){
		return $this->customerTel;
	}

	public function setCustomerEventDate( $string ){
		$this->customerEventDate = $string;
	}
	public function getCustomerEventDate(){
		return $this->customerEventDate;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerName( $string ){
		$this->customerName = $string;
	}
	public function getCustomerName(){
		return $this->customerName;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}
	public function getDetail(){
		return $this->detail;
	}

	public function setInvoiceDate( $string ){
		$this->invoiceDate = date('Y-m-d', strtotime( $string ));
	}
	public function getInvoiceDate(){
		return $this->invoiceDate;
	}

	public function setCreator( $string ){
		$this->creator = $string;
	}
	public function getCreator(){
		return $this->creator;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}
	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

	public function setCustomerStaffResponsible( $string ){
		$this->customerStaffResponsible = $string;
	}
	public function getCustomerStaffResponsible(){
		return $this->customerStaffResponsible;
	}

	public function setCustomerStaffResponsibleId( $string ){
		$this->customerStaffResponsibleId = (int)$string;
	}
	public function getCustomerStaffResponsibleId(){
		return $this->customerStaffResponsibleId;
	}

	public function setNoted( $string ){
		$this->noted = $string;
	}
	public function getNoted(){
		return $this->noted;
	}

	public function setTotal( $string ){
		$this->total = $string;
	}
	public function getTotal(){
		return $this->total;
	}

	public function setGrandTotal( $string ){
		$this->grandTotal = doubleval($string);
	}
	public function getGrandTotal(){
		return $this->grandTotal;
	}

	public function setSubTotal( $string ){
		$this->subTotal = doubleval($string);
	}
	public function getSubTotal(){
		return $this->subTotal;
	}

	public function setPaymentMethod( $string ){
		$this->paymentMethod = $string;
	}
	public function getPaymentMethod(){
		return $this->paymentMethod;
	}

	public function setDiscount( $string ){
		$this->discount = doubleval($string);
	}
	public function getDiscount(){
		return $this->discount;
	}

	public function setDiscountType( $string ){
		$this->discountType = $string;
	}
	public function getDiscountType(){
		return $this->discountType;
	}

	public function setTotalDiscount( $string ){
		$this->totalDiscount = doubleval($string);
	}
	public function getTotalDiscount(){
		return $this->totalDiscount;
	}

	public function setDeposit( $string ){
		$this->deposit = doubleval($string);
	}
	public function getDeposit(){
		return $this->deposit;
	}

	public function setBalance( $decimal ){
		$this->balance = doubleval($decimal);
	}
	public function getBalance(){
		return $this->balance;
	}

	public function setDoctorId( $string ){
		$this->doctorId = (int)$string;
	}
	public function getDoctorId(){
		return $this->doctorId;
	}

	public function setCustomerTypeId( $string ){
		$this->customerTypeId = (int)$string;
	}
	public function getCustomerTypeId(){
		return $this->customerTypeId;
	}

	public function setCustomerTypeName( $string ){
		$this->customerTypeName = $string;
	}
	public function getCustomerTypeName(){
		return $this->customerTypeName;
	}

	public function setCustomerGender( $string ){
		$this->customerGender = $string;
	}
	public function getCustomerGender(){
		return $this->customerGender;
	}

	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}
	public function getCustomerCode(){
		return $this->customerCode;
	}

	public function setExchangeId( $string ){
		$this->exchangeId = (int)$string;
	}
	public function getExchangeId(){
		return $this->exchangeId;
	}
	
	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}


}
