<?php

namespace OSC\Quotation;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('quotation', 'q');
		$this->idField = 'q.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){		
		$this->addWhere("q.id = '" . (int)$arg. "' ");
	}

	public function sortById( $arg ){
		$this->addOrderBy("q.id", $arg);
	}

	public function filterByStatus( $arg ){
		$this->addWhere("q.status = '" . (int)$arg. "' ");
	}

	public function filterByInvoiceDate($arg){
		$this->addWhere("q.invoice_date = '" . $arg . "' ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("q.customer_id = '" . (int)$arg. "' ");
	}

	public function filterByCustomerTypeId( $arg ){
		$this->addWhere("q.customer_type_id = '" . (int)$arg. "' ");
	}

	public function filterByInvoiceNo( $arg ){
		$this->addWhere("q.invoice_no LIKE '%" . $arg. "%' OR q.customer_name LIKE '%" . $arg. "%' OR q.customer_code LIKE '%" . $arg. "%'");
	}

	public function filterByDate($from, $to){
		$this->addWhere("q.invoice_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}
	
}
