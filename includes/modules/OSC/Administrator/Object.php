<?php

namespace OSC\Administrator;

use
	Aedea\Core\Database\StdObject as DbObj
    , OSC\RoleSample\Collection as RoleCol
;

class Object extends DbObj {
		
	protected
		$userName
		, $userPassword
		, $roleId
        , $email
        , $roleDetail
		, $linkTo
		, $fullName
		, $blockIp
	;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->roleDetail = new RoleCol();
    }

    public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'user_name',
				'role_id',
                'role_detail',
                'email',
				'status',
				'full_name',
				'link_to',
				'block_ip'
			)
		);
		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				user_name,
				role_id,
				link_to,
				email,
				status,
				full_name,
				block_ip
			FROM
				administrators
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Administrator not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

        $this->roleDetail->setFilter('id', $this->getRoleId());
        $this->roleDetail->populate();
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				administrators
			(
				user_name,
				full_name,
				user_password,
				email,
				link_to,
				block_ip,
				role_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getUserName() ) . "',
				'" . $this->dbEscape( $this->getFullName() ) . "',
				'" . $this->getUserPassword() . "',
				'" . $this->getEmail() . "',
				'" . $this->getLinkTo() . "',
				'" . $this->getBlockIp() . "',
				'" . $this->getRoleId() . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function update(){
		$this->dbQuery("
			UPDATE
				administrators
			SET
				user_name = '" . $this->dbEscape( $this->getUserName() ) . "',
				full_name = '" . $this->dbEscape( $this->getFullName() ) . "',
				link_to = '" . $this->getLinkTo() . "',
				block_ip = '" . $this->getBlockIp() . "',
				email = '" . $this->getEmail() . "',
				role_id = '" . $this->getRoleId() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updatePassword(){
		$this->dbQuery("
			UPDATE
				administrators
			SET
				user_password = '" . $this->getUserPassword() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				administrators
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				administrators
			WHERE
				id = '" . (int)$id . "'
		");
	}

    public function setBlockIp( $string ){
        $this->blockIp = (string)$string;
    }

    public function getBlockIp(){
        return $this->blockIp;
    }

    public function setEmail( $string ){
        $this->email = (string)$string;
    }

    public function getEmail(){
        return $this->email;
    }

    public function setUserName( $string ){
		$this->userName = (string)$string;
	}

	public function getUserName(){
		return $this->userName;
	}

	public function setRoleId( $string ){
		$this->roleId = (int)$string;
	}

	public function getRoleId(){
		return $this->roleId;
	}

	public function setLinkTo( $string ){
		$this->linkTo = (int)$string;
	}

	public function getLinkTo(){
		return $this->linkTo;
	}

    public function setRoleDetail( $string ){
        $this->roleDetail = $string;
    }

    public function getRoleDetail(){
        return $this->roleDetail;
    }

	public function setUserPassword( $string ){
		$this->userPassword = (string)$string;
	}
	
	public function getUserPassword(){
		return $this->userPassword;
	}
	
    public function setFullName( $string ){
		$this->fullName = (string)$string;
	}
	public function getFullName(){
		return $this->fullName;
	}
}
