<?php

namespace OSC\StockTransaction;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('stock_transaction', 'st');
		$this->idField = 'st.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
		$this->addWhere("st.id = '" . $arg. "%' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("st.product_name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus($arg){
		$this->addWhere("st.status = '" . (int)$arg ."' ");
	}

	public function filterByDate($from, $to){
		$this->addWhere("st.create_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function sortByProductName($arg){
		$this->addOrderBy('st.product_name', $arg);
	}

}
