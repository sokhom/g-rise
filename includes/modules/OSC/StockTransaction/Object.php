<?php

namespace OSC\StockTransaction;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$referenceId,
		$productId,
		$productTypeName,
		$productTypeId,
		$productName,
		$productDescription,
		$barcode,
		$qtyOnHand,
		$productKindOf,
		$cost,
		$stockIn,
		$stockOut,
		$totalAmount,
		$transactionDate,
		$wholeSalePrice,
		$retailPrice,
		$umType,
		$umTypeAmount,
		$umTypeRetail,
		$umTypeRetailAmount
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'product_type',
				'product_name',
				'barcode',
				'transaction_date',
				'product_description',
				'product_kind_of',
				'qty_on_hand',
				'cost',
				'price',
				'stock_in',
				'stock_out',
				'total_amount',
				'create_by',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				product_id,
				produc_type,
				product_name,
				barcode,
				product_kind_of,
				product_description,
				qty_on_hand,
				cost,
				price,
				transaction_date,
				stock_in,
				stock_out,
				create_by
			FROM
				stock_transaction
			WHERE
				id = '" . (int)$this->getId() . "'
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock transaction not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}
	public function insert(){
		$this->dbQuery("
			INSERT INTO
				stock_transaction
			(
				reference_id,
				product_id,
				product_type_name,
				product_type_id,
				product_name,
				barcode,
				product_description,
				product_kind_of,
				qty_on_hand,
				cost,
				whole_sale_price,
				retail_price,
				um_type,
				um_type_amount,
				um_type_retail,
				um_type_retail_amount,
				stock_in,
				stock_out,
				transaction_date,
				create_by,
				create_by_id,
				status,
				create_date
			)
				VALUES
			(
				'" . $this->getReferenceId() . "',
				'" . $this->getProductId() . "',
				'" . $this->getProductTypeName() . "',
				'" . $this->getProductTypeId() . "',
				'" . $this->getProductName() . "',
				'" . $this->getBarcode() . "',
				'" . $this->getProductDescription() . "',
				'" . $this->getProductKindOf() . "',
				'" . $this->getQtyOnHand() . "',
				'" . $this->getCost() . "',
				'" . $this->getWholeSalePrice() . "',
				'" . $this->getRetailPrice() . "',
				'" . $this->getUmType() . "',
				'" . $this->getUmTypeAmount() . "',
				'" . $this->getUmTypeRetail() . "',
				'" . $this->getUmTypeRetailAmount() . "',
				'" . $this->getStockIn() . "',
				'" . $this->getStockOut() . "',
				'" . $this->getTransactionDate() . "',
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
				1,
			  	NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function updateStatus(){
		if( !$this->getReferenceId() ) {
			throw new Exception("save method requires reference id to be set");
		}
		$this->dbQuery("
			UPDATE
				stock_transaction
			SET
				status = '" . $this->getStatus() . "',
				update_by_id = '" . $this->getUpdateById() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				reference_id = '" . $this->getReferenceId() . "'
		");
	}

	public function setTransactionDate( $string ){
		$this->transactionDate = $string;
	}

	public function getTransactionDate(){
		return $this->transactionDate;
	}

	public function setProductKindOf( $string ){
		$this->productKindOf = $string;
	}

	public function getProductKindOf(){
		return $this->productKindOf;
	}

	public function setTotalAmount( $string ){
		$this->totalAmount = $string;
	}

	public function getTotalAmount(){
		return $this->totalAmount;
	}

	public function setStockOut( $string ){
		$this->stockOut = (int)$string;
	}

	public function getStockOut(){
		return $this->stockOut;
	}

	public function setStockIn( $string ){
		$this->stockIn = (int)$string;
	}

	public function getStockIn(){
		return $this->stockIn;
	}

	public function setQtyOnHand( $string ){
		$this->qtyOnHand = (int)$string;
	}

	public function getQtyOnHand(){
		return $this->qtyOnHand;
	}

	public function setProductDescription( $string ){
		$this->productDescription = $string;
	}

	public function getProductDescription(){
		return $this->productDescription;
	}

	public function setBarcode( $string ){
		$this->barcode = $string;
	}

	public function getBarcode(){
		return $this->barcode;
	}

	public function setRetailPrice( $string ){
		$this->retailPrice = $string;
	}

	public function getRetailPrice(){
		return $this->retailPrice;
	}

	public function setWholeSalePrice( $string ){
		$this->wholeSalePrice = $string;
	}

	public function getWholeSalePrice(){
		return $this->wholeSalePrice;
	}

	public function setProductName( $string ){
		$this->productName = $string;
	}

	public function getProductName(){
		return $this->productName;
	}

	public function setProductTypeName( $string ){
		$this->productTypeName = $string;
	}

	public function getProductTypeName(){
		return $this->productTypeName;
	}

	public function setProductTypeId( $string ){
		$this->productTypeId = $string;
	}

	public function getProductTypeId(){
		return $this->productTypeId;
	}

	public function setProductId( $string ){
		$this->productId = $string;
	}

	public function getProductId(){
		return $this->productId;
	}

	public function setReferenceId( $string ){
		$this->referenceId = $string;
	}
	
	public function getReferenceId(){
		return $this->referenceId;
	}

	public function setCost( $string ){
		$this->cost = $string;
	}

	public function getCost(){
		return $this->cost;
	}

	public function setUmType( $string ){
		$this->umType = $string;
	}

	public function getUmType(){
		return $this->umType;
	}
	public function setUmTypeAmount( $string ){
		$this->umTypeAmount = doubleval($string);
	}

	public function getUmTypeAmount(){
		return $this->umTypeAmount;
	}

	public function setUmTypeRetail( $string ){
		$this->umTypeRetail = $string;
	}

	public function getUmTypeRetail(){
		return $this->umTypeRetail;
	}
	public function setUmTypeRetailAmount( $string ){
		$this->umTypeRetailAmount = doubleval($string);
	}

	public function getUmTypeRetailAmount(){
		return $this->umTypeRetailAmount;
	}

}
