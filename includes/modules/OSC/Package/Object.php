<?php

namespace OSC\Package;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\PackageOption\Collection as packageOption
	, OSC\PackageAddOn\Collection as  packageAddOn
	, OSC\PackageType\Collection as  packageType
;

class Object extends DbObj {
		
	protected
		$name
		, $price
		, $description
		, $addOnDetail
		, $packageTypeId
		, $packageType
		, $optionDetail
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->addOnDetail = new packageAddOn();
        $this->optionDetail = new packageOption();
        $this->packageType = new packageType();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'description',
				'price',
				'status',
				'package_type_id',
				'package_type',
				'add_on_detail',
				'option_detail'
			)
		);
		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				description,
				package_type_id,
				price,
				status
			FROM
				packages
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: packages not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->optionDetail->setFilter('package_id', $this->getId());
        $this->optionDetail->populate();

		$this->addOnDetail->setFilter('package_id', $this->getId());
		$this->addOnDetail->populate();
		
		$this->packageType->setFilter('id', $this->getPackageTypeId());
		$this->packageType->populate();
		
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages
			SET
				name = '" . $this->dbEscape(  $this->getName() ). "',
				package_type_id = '" . $this->getPackageTypeId() . "',
				price = '" . $this->getPrice() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				packages
			WHERE
				id = " . $this->getId() . "
		");
		$this->dbQuery("
			DELETE FROM
				packages_option
			WHERE
				package_id = " . $this->getId() . "
		");
		$this->dbQuery("
			DELETE FROM
				packages_add_on
			WHERE
				package_id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				packages
			(
				name,
				package_type_id,
				price,
				description,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getName() ). "',
				'" . $this->getPackageTypeId() . "',
				'" . $this->getPrice() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}
	public function setPackageType( $string ){
		$this->packageType = $string;
	}
	public function getPackageType(){
		return $this->packageType;
	}

	public function setPackageTypeId( $string ){
		$this->packageTypeId = (int)$string;
	}
	public function getPackageTypeId(){
		return $this->packageTypeId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setName( $string ){
		$this->name = $string;
	}	
	public function getName(){
		return $this->name;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}	
	public function getPrice(){
		return $this->price;
	}

	public function setAddOnDetail( $string ){
		$this->addOnDetail = $string;
	}	
	public function getAddOnDetail(){
		return $this->addOnDetail;
	}

	public function setOptionDetail( $string ){
		$this->optionDetail = $string;
	}	
	public function getOptionDetail(){
		return $this->optionDetail;
	}

}
