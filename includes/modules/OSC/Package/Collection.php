<?php

namespace OSC\Package;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('packages', 'pk');
		$this->idField = 'pk.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("pk.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("pk.name LIKE '%" . $arg. "%' or pk.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pk.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('pk.name', $arg);
	}


}
