<?php

namespace OSC\Service;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('services', 's');
		$this->idField = 's.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		if($arg){
			$this->addWhere("s.name LIKE '%" . $arg. "%' OR s.code LIKE '%" . $arg. "%' OR s.detail LIKE '%" . $arg. "%' ");
		}
	}

	public function filterByCode( $arg ){
		$this->addWhere("s.code = '" . $arg . "' ");
	}

	public function filterById( $arg ){
		if($arg){
			$this->addWhere("s.id = '" . (int)$arg. "' ");
		}
	}

	public function filterByServiceTypeId( $arg ){
		$this->addWhere("s.type_id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('s.id', $arg);
	}
}
