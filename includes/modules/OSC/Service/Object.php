<?php

namespace OSC\Service;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\ServiceType\Collection
		as  ServicesType
;

class Object extends DbObj {
		
	protected
		$name
		, $typeDetail
		, $typeId
		, $code
		, $image
		, $imageThumbnail
		, $isSale
		, $price
		, $detail
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->typeDetail = new ServicesType();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'type_id',
				'type_detail',
				'code',
				'is_sale',
				'price',
				'detail',
				'status',
				'image',
				'image_thumbnail'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				detail,
				type_id,
				code,
				is_sale,
				price,
				image,
				image_thumbnail,
				status
			FROM
				services
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Service not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->typeDetail->setFilter('id', $this->getTypeId());
		$this->typeDetail->populate();

	}

	public function updateStatus($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				services
			SET
				status = '" . $this->getStatus() . "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				services
			SET
				name = '" . $this->dbEscape( $this->getName() ). "',
				type_id = '" . $this->getTypeId() . "',
				code = '" . $this->getCode() . "',
				image = '" . $this->getImage() . "',
				image_thumbnail = '" . $this->getImageThumbnail() . "',
				price = '" . $this->getPrice() . "',
				is_sale = '" . $this->getIsSale() . "',
				detail = '" . $this->dbEscape( $this->getDetail() ). "'
			WHERE
				id = '" . (int)$id . "'
		");

	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				services
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				services
			(
				name,
				detail,
				type_id,
				code,
				is_sale,
				price,
				image,
				image_thumbnail,
				status,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getName() ). "',
				'" . $this->dbEscape( $this->getDetail() ). "',
				'" . $this->getTypeId() . "',
				'" . $this->getCode() . "',
				'" . $this->getIsSale() . "',
				'" . $this->getPrice() . "',
				'" . $this->getImage() . "',
				'" . $this->getImageThumbnail() . "',
				1,
				'" . $this->getCreateBy() ."',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}

	public function setName( $string ){
		$this->name = $string;
	}
	
	public function getName(){
		return $this->name;
	}

	public function setIsSale( $string ){
		$this->isSale = (int)$string;
	}
	
	public function getIsSale(){
		return $this->isSale;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}
	public function getPrice(){
		return $this->price;
	}

	public function setCode( $string ){
		$this->code = $string;
	}
	public function getCode(){
		return $this->code;
	}

	public function setImage( $string ){
		$this->image = $string;
	}
	public function getImage(){
		return $this->image;
	}

	public function setTypeId( $string ){
		$this->typeId = (int)$string;
	}

	public function getTypeId(){
		return $this->typeId;
	}

	public function setTypeDetail( $string ){
		$this->typeDetail = $string;
	}
	public function getTypeDetail(){
		return $this->typeDetail;
	}

	public function setImageThumbnail( $string ){
		$this->imageThumbnail = $string;
	}

	public function getImageThumbnail(){
		return $this->imageThumbnail;
	}

}
