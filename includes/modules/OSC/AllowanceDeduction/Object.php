<?php

namespace OSC\AllowanceDeduction;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$name
		, $description
		, $specificValue
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'name',
				'description',
				'specific_value',
				'id',
				'status'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				description,
				specific_value,
				status
			FROM
				allowance_deduction
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: allowance_deduction not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				allowance_deduction
			SET
				name = '" . $this->dbEscape( $this->getName() ). "',
				specific_value = '" . $this->getSpecificValue() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				allowance_deduction
			SET
				status = '" . $this->getStatus() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}
	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				allowance_deduction
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				allowance_deduction
			(
				name,
				description,
				specific_value,
				create_date,
				create_by,
				create_by_id,
				status
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getName() ) . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getSpecificValue() . "',
 				NOW(),
				 '" . $this->getCreateBy() . "',
				 '" . $this->getCreateById() . "',
				 1
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setSpecificValue( $string ){
		$this->specificValue = doubleval($string);
	}

	public function getSpecificValue(){
		return $this->specificValue;
	}

	public function setName( $string ){
		$this->name = (string)$string;
	}

	public function getName(){
		return $this->name;
	}

	public function setDescription( $string ){
		$this->description = (string)$string;
	}
	
	public function getDescription(){
		return $this->description;
	}
	
}
