<?php

namespace OSC\ExchangeRate;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$exchangeRateIn
		, $exchangeRateOut
		, $exchangeType
		, $creator
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'exchange_rate_in',
				'exchange_rate_out',
				'exchange_type',
				'create_date',
				'creator'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				exchange_rate_in,
				exchange_rate_out,
				exchange_type,
				DATE_FORMAT(create_date, '%Y-%m-%d') as create_date,
				create_by as creator
			FROM
				exchange_rate
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Exchange Rate not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				exchange_rate
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				exchange_rate
			(
				exchange_rate_in,
				exchange_rate_out,
				exchange_type,
				create_date,
				create_by
			)
				VALUES
			(
				'" . $this->getExchangeRateIn() . "',
				'" . $this->getExchangeRateOut() . "',
				'" . $this->getExchangeType() . "',
 				NOW(),
				'" . $this->getCreateBy() . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setCreator( $string ){
		$this->creator = $string;
	}

	public function getCreator(){
		return $this->creator;
	}

	public function setExchangeRateIn( $string ){
		$this->exchangeRateIn = doubleval($string);
	}

	public function getExchangeRateIn(){
		return $this->exchangeRateIn;
	}

	public function setExchangeRateOut( $string ){
		$this->exchangeRateOut = doubleval($string);
	}

	public function getExchangeRateOut(){
		return $this->exchangeRateOut;
	}

	public function setExchangeType( $string ){
		$this->exchangeType = $string;
	}
	
	public function getExchangeType(){
		return $this->exchangeType;
	}

}
