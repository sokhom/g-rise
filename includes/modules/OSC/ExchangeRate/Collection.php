<?php

namespace OSC\ExchangeRate;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable("exchange_rate", "er");
		$this->idField = "er.id";
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function sortById($arg){
		$this->addOrderBy("er.id", $arg);
	}

	public function filterById($arg){
		$this->addWhere("er.id = '" . $arg . "' ");
	}

}
