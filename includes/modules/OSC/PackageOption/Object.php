<?php

namespace OSC\PackageOption;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\Options\Collection as  optionCol
;

class Object extends DbObj {
		
	protected
		$packageId
		, $optionId
		, $description
		, $detail
	;
	
    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new optionCol();
	}
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'option_id',
				'description',
				'detail',
			)
		);
		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				option_id,
				description
			FROM
				packages_option
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: packages option not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('id', $this->getOptionId());
        $this->detail->populate();

	}

	public function update(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages_option
			SET
				option_id = '" . $this->getOptionId() . "',
				description = '" . $this->dbEscape(  $this->getDescription() ). "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				packages_option
			SET
				status = '" . $this->getStatus() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function deleteByPackage(){
		if( !$this->getPackageId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				packages_option
			WHERE
				package_id = " . $this->getPackageId() . "
		");
	}

	public function delete(){
		if( !$this->getId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				packages_option
			WHERE
				id = " . $this->getId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				packages_option
			(
				option_id,
				package_id,
				description
			)
				VALUES
			(
				'" . $this->getOptionId() . "',
				'" . $this->getPackageId() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}
	public function getDescription(){
		return $this->description;
	}

	public function setPackageId( $string ){
		$this->packageId = (int)$string;
	}	
	public function getPackageId(){
		return $this->packageId;
	}

	public function setOptionId( $string ){
		$this->optionId = doubleval($string);
	}	
	public function getOptionId(){
		return $this->optionId;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}	
	public function getDetail(){
		return $this->detail;
	}

}
