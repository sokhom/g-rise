<?php

namespace OSC\PackageOption;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('packages_option', 'pko');
		$this->idField = 'pko.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("pko.id = '" . (int)$arg. "' ");
	}

	public function filterByPackageId( $arg ){
		$this->addWhere("pko.package_id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("pko.name LIKE '%" . $arg. "%' or pko.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("pko.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('pko.name', $arg);
	}


}
