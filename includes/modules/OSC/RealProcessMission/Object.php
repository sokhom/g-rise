<?php

namespace OSC\RealProcessMission;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$realProcessId
		, $staffId
		, $staffName
		, $date
		, $description
		, $role
		, $note
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'staff_id',
				'staff_name',
				'role',
				'date',
				'note',
				'description'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				staff_id,
				staff_name,
				date,
				note,
				description,
				role,
				real_process_id
			FROM
				real_process_staff_mission
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Real process mission not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function delete(){
		if( !$this->getRealProcessId() ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				real_process_staff_mission
			WHERE
				real_process_id = " . $this->getRealProcessId() . "
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				real_process_staff_mission
			(
				staff_id,
				staff_name,
				date,
				role,
				description,
				note,
				real_process_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getStaffId() . "',
				'" . $this->getStaffName() . "',
				'" . $this->dbEscape( $this->getDate() ) . "',
				'" . $this->dbEscape( $this->getRole() ) . "',
				'" . $this->dbEscape( $this->getDescription() ) . "',
				'" . $this->dbEscape( $this->getNote() ) . "',
				'" . $this->getRealProcessId() . "',
				'" . $this->getCreateBy() . "',
 				NOW()

			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDate($string ){
		$this->date = $string;
	}
	public function getDate(){
		return $this->date;
	}

	public function setStaffId( $string ){
		$this->staffId = (int)$string;
	}	
	public function getStaffId(){
		return $this->staffId;
	}

	public function setStaffName( $string ){
		$this->staffName = $string;
	}	
	public function getStaffName(){
		return $this->staffName;
	}

	public function setNote( $string ){
		$this->note = $string;
	}	
	public function getNote(){
		return $this->note;
	}

	public function setRealProcessId( $string ){
		$this->realProcessId = $string;
	}	
	public function getRealProcessId(){
		return $this->realProcessId;
	}

	public function setDescription( $string ){
		$this->description = $string;
	}	
	public function getDescription(){
		return $this->description;
	}

	public function setRole( $string ){
		$this->role = $string;
	}	
	public function getRole(){
		return $this->role;
	}

}
