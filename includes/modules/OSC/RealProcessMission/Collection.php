<?php

namespace OSC\RealProcessMission;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('real_process_staff_mission', 'prs');
		$this->idField = 'prs.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("prs.id = '" . (int)$arg. "' ");
	}

	public function filterByRealProcessId( $arg ){
			$this->addWhere("prs.real_process_id = '" . (int)$arg. "' ");
	}

	public function filterByInvoice( $arg ){
		$this->addWhere("prs.invoice_no LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("prs.status = '" . (int)$arg. "' ");
	}


}
