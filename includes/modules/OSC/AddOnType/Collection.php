<?php

namespace OSC\AddOnType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('add_on_type', 'aot');
		$this->idField = 'aot.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("aot.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("aot.name LIKE '%" . $arg. "%' or aot.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("aot.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('aot.name', $arg);
	}


}
