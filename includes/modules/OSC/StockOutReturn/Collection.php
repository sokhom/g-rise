<?php

namespace OSC\StockOutReturn;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable("stock_out_return", "so");
		$this->idField = "so.id";
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByOverdue($arg){
		$this->addWhere("DATEDIFF( now(), so.overdue ) >= ".$arg." ");
	}

	public function filterByDate($from, $to){
		$this->addWhere("date(so.create_date) BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function filterById( $arg ){
		$this->addWhere("so.id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("so.status = " . (int)$arg. " ");
	}

	public function filterByCustomerId( $arg ){
		$this->addWhere("so.customer_id = '" . (int)$arg. "' ");
	}

	public function filterByRemain(){
		$this->addWhere("so.remain > 0 ");
	}

	public function filterByInvoice( $arg ){
		// $this->addTable("customers", "c");
		// $this->addWhere("c.id = so.customer_id");
		// $this->addWhere("c.customers_telephone LIKE '%" . $arg. "%'");
		$this->addWhere("so.stock_out_no LIKE '%" . $arg. "%' OR so.customer_name LIKE '%" . $arg. "%' ");
	}

	public function sortById($arg){
		// $this->addOrderBy("so.stock_out_return_date", $arg);
		$this->addOrderBy("so.id", $arg);
	}
	
}
