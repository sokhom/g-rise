<?php

namespace OSC\StockOutReturn;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\StockOutReturnDetail\Collection as StockOutReturnDetail
;

class Object extends DbObj {
		
	protected
		$stockOutReturnDate
		, $stockOutDate
		, $stockOutReturnNo
		, $stockOutNo
		, $customerId
		, $customerName
		, $customerTelephone
		, $customerCode
		, $bankCharge
		, $grandTotal
		, $subTotal
		, $payment
		, $paymentMethod
		, $discountType
		, $discountAmount
		, $discountTotalAmount
		, $exchangeId
		, $remain
		, $note
		, $detail
		, $doctorDetail
		, $remark
		, $addMorePrice
		, $overdue
		, $title
		, $creator
	;

	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new StockOutReturnDetail();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'stock_out_return_date',
				'stock_out_no',
				'stock_out_date',
				'stock_out_return_no',
				'customer_id',
				'customer_name',
				'customer_code',
				'payment_method',
				'bank_charge',
				'sub_total',
				'grand_total',
				'payment',
				'remain',
				'status',
				'discount_amount',
				'discount_total_amount',
				'discount_type',
				'detail',
				'customer_telephone',
				'create_date',
				'creator',
				'title',
				'note',
				'overdue',
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				stock_out_return_date,
				customer_id,
				customer_name,
				customer_code,
				customer_telephone,
				stock_out_return_no,
				discount_amount,
				discount_type,
				stock_out_date,
				stock_out_no,
				discount_total_amount,
				payment_method,
				bank_charge,
				grand_total,
				overdue,
				sub_total,
				payment,
				remain,
				status,
				create_by as creator,
				title,
				note,
				create_date
			FROM
				stock_out_return s
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Stock Out Return not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('stock_out_return_id', $this->getId());
		$this->detail->populate();
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				stock_out_return
			(
				stock_out_return_date,
				stock_out_date,
				customer_id,
				customer_name,				
				customer_code,
				customer_telephone,
				exchange_id,
				stock_out_return_no,
				stock_out_no,
				discount_amount,
				discount_type,
				discount_total_amount,
				payment_method,
				bank_charge,
				grand_total,
				sub_total,
				payment,
				remain,
				status,
				create_by,
				create_date,
				overdue,
				note,
				title
			)
				VALUES
			(
				'" . $this->getStockOutReturnDate() . "',
				'" . $this->getStockOutDate() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->getCustomerName() . "',
				'" . $this->getCustomerCode() . "',
				'" . $this->getCustomerTelephone() . "',
				'" . $this->getExchangeId() . "',
				'" . $this->getStockOutReturnNo() . "',
				'" . $this->getStockOutNo() . "',
				'" . $this->getDiscountAmount() . "',
				'" . $this->getDiscountType() . "',
				'" . $this->getDiscountTotalAmount() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getBankCharge() . "',
				'" . $this->getGrandTotal() . "',
				'" . $this->getSubTotal() . "',
				'" . $this->getPayment() . "',
				'" . $this->getRemain() . "',
				1,
				'" . $this->getCreateBy() . "',
				NOW(),
				'" . $this->getOverdue() . "',
				'" . $this->dbEscape( $this->getNote() ) . "',
				'" . $this->dbEscape( $this->getTitle() ) . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}


	public function updateBalaceAferVoidRecivePayment(){
		tep_db_query("
			UPDATE
				stock_out_return
			SET
				remain = remain + '" . $this->getRemain() . "',
				payment = payment - '" . $this->getPayment() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_return_no = '" . $this->getStockOutReturnNo() . "'
					and
				customer_id = " . $this->getCustomerId() . "
		");
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				stock_out_return
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				id = '" . $this->getId() . "'
		");
		
	}

	public function updateBlanace(){
		$this->dbQuery("
			UPDATE
				stock_out_return
			SET
                remain = remain - '" . $this->getPayment() . "',
				payment = payment + '" . $this->getPayment() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_return_no = '" . $this->getStockOutReturnNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}

	public function updateOverdue(){
		$this->dbQuery("
			UPDATE
				stock_out
			SET
                overdue = '" . $this->getOverdue() . "',
                update_by = '" . $this->getUpdateBy() . "'
			WHERE
				stock_out_no = '" . $this->getStockOutNo() . "'
					and
				customer_id = '" . $this->getCustomerId() . "'
		");
	}
	
	public function setCreator( $string ){
		$this->creator = $string;
	}

	public function getCreator(){
		return $this->creator;
	}
	
	public function setCustomerCode( $string ){
		$this->customerCode = $string;
	}

	public function getCustomerCode(){
		return $this->customerCode;
	}

	public function setCustomerTelephone( $string ){
		$this->customerTelephone = $string;
	}

	public function getCustomerTelephone(){
		return $this->customerTelephone;
	}

	public function setOverdue( $string ){
		$this->overdue = $string;
	}

	public function getOverdue(){
		return $this->overdue;
	}

	public function setRemark( $string ){
		$this->remark = $string;
	}

	public function getRemark(){
		return $this->remark;
	}

	public function setAddMorePrice( $string ){
		$this->addMorePrice = doubleval($string);
	}

	public function getAddMorePrice(){
		return $this->addMorePrice;
	}

	public function setDiscountTotalAmount( $string ){
		$this->discountTotalAmount = doubleval($string);
	}

	public function getDiscountTotalAmount(){
		return $this->discountTotalAmount;
	}

	public function setDiscountAmount( $string ){
		$this->discountAmount = $string;
	}

	public function getDiscountAmount(){
		return $this->discountAmount;
	}

	public function setDiscountType( $string ){
		$this->discountType = $string;
	}

	public function getDiscountType(){
		return $this->discountType;
	}

	public function setRemain( $string ){
//		if( $string < 0){
//			$string= 0;
//		}
		$this->remain = doubleval($string);
	}

	public function getRemain(){
		return $this->remain;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}

	public function getDetail(){
		return $this->detail;
	}

	public function setPayment( $string ){
		$this->payment =  doubleval($string);
	}

	public function getPayment(){
		return $this->payment;
	}

	public function setGrandTotal( $string ){
		$this->grandTotal = doubleval($string);
	}

	public function getGrandTotal(){
		return $this->grandTotal;
	}

	public function setBankCharge( $string ){
		$this->bankCharge = doubleval($string);
	}

	public function getBankCharge(){
		return $this->bankCharge;
	}

	public function setSubTotal( $string ){
		$this->subTotal = doubleval($string);
	}

	public function getSubTotal(){
		return $this->subTotal;
	}

	public function setStockOutDate( $date ){
		$this->stockOutDate = date('Y-m-d', strtotime( $date ));
	}
	
	public function getStockOutDate(){
		return $this->stockOutDate;
	}

	public function setStockOutReturnDate( $date ){
		$this->stockOutReturnDate = date('Y-m-d', strtotime( $date ));
	}
	
	public function getStockOutReturnDate(){
		return $this->stockOutReturnDate;
	}

	public function setStockOutReturnNo( $string ){
		$this->stockOutReturnNo = $string;
	}

	public function getStockOutReturnNo(){
		return $this->stockOutReturnNo;
	}

	public function setStockOutNo( $string ){
		$this->stockOutNo = $string;
	}

	public function getStockOutNo(){
		return $this->stockOutNo;
	}

	public function setPaymentMethod( $string ){
		$this->paymentMethod = $string;
	}

	public function getPaymentMethod(){
		return $this->paymentMethod;
	}

	public function setCustomerName( $string ){
		$this->customerName = (string)$string;
	}

	public function getCustomerName(){
		return $this->customerName;
	}

	public function setExchangeId( $string ){
		$this->exchangeId = (int)$string;
	}

	public function getExchangeId(){
		return $this->exchangeId;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}

	public function getCustomerId(){
		return $this->customerId;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}

	public function getTitle(){
		return $this->title;
	}
	
	public function setNote( $string ){
		$this->note = $string;
	}

	public function getNote(){
		return $this->note;
	}

	public function setDoctorDetail( $string ){
		$this->doctorDetail = $string;
	}

	public function getDoctorDetail(){
		return $this->doctorDetail;
	}

	
}
