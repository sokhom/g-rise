<?php

namespace OSC\Expense;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('expense', 'e');
		$this->idField = 'e.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		$this->addWhere("e.name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("e.status = '" . (int)$arg. "' ");
	}

	public function filterByDate($from, $to){
		$this->addWhere("e.expense_date BETWEEN '" . $from . "' AND '" . $to . "' ");
	}

	public function filterById( $arg ){
		$this->addWhere("e.id = '" . (int)$arg. "' ");
	}

	public function filterByExpenseTypeId( $arg ){
		$this->addWhere("e.expense_type_id = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('e.expense_date', $arg);
		$this->addOrderBy('e.id', $arg);
	}

	public function sortByName($arg){
		$this->addOrderBy('e.name', $arg);
	}
}
