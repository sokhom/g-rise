<?php

namespace OSC\Expense;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$name
		, $description
		, $expenseTypeId
		, $expenseTypeName
		, $expenseDate
		, $amount
		, $detail
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'expense_date',
				'amount',
				'description',
				'status',
				'create_by',
				'expense_type_id',
				'expense_type_name',
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				e.name,
				e.description,
				e.expense_date,
				e.amount,
				e.create_by,
				e.status,
				e.expense_type_id,
				et.name as expense_type_name
			FROM
				expense e INNER JOIN expense_type et on e.expense_type_id = et.id
			WHERE
				e.id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Expense not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				expense
			SET
				name = '" . $this->dbEscape(  $this->getName() ). "',
				description = '" .  $this->dbEscape( $this->getDescription() ). "',
				amount = '" . $this->getAmount() . "',
				expense_date = '" . $this->getExpenseDate() . "',
				update_by_id = '" . $this->getUpdateById() . "',
				expense_type_id = '" . $this->getExpenseTypeId() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				expense
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				expense
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				expense
			(
				name,
				description,
				amount,
				expense_date,
				status,
				create_by,
				create_by_id,
				create_date,
				expense_type_id
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getName() ). "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getAmount() . "',
				'" . $this->getExpenseDate() . "',
				1,
				'" . $this->getCreateBy() . "',
				'" . $this->getCreateById() . "',
 				NOW(),
				'" . $this->getExpenseTypeId() . "'
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setAmount( $string ){
		$this->amount = doubleval($string);
	}

	public function getAmount(){
		return $this->amount;
	}

	public function setName( $string ){
		$this->name = $string;
	}
	
	public function getName(){
		return $this->name;
	}

	public function setExpenseDate( $string ){
		$this->expenseDate = $string;
	}

	public function getExpenseDate(){
		return $this->expenseDate;
	}

	public function setExpenseTypeId( $string ){
		$this->expenseTypeId = (int)$string;
	}

	public function getExpenseTypeId(){
		return $this->expenseTypeId;
	}

	public function setExpenseTypeName( $string ){
		$this->expenseTypeName = $string;
	}

	public function getExpenseTypeName(){
		return $this->expenseTypeName;
	}
}
