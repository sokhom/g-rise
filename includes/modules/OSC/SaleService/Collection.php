<?php

namespace OSC\SaleService;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('sale_service', 'ss');
		$this->idField = 'ss.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}
	
	public function filterByStatus( $arg ){
		$this->addWhere("ss.status = '" . (int)$arg. "' ");
	}

	public function filterByProductId( $arg ){
		$this->addWhere("ss.product_id = '" . (int)$arg. "' ");
	}

	public function filterByMulti( $arg ){
		$this->addWhere("ss.description LIKE '%" . $arg. "%' OR ss.name LIKE '%" . $arg. "%' ");
	}

	public function sortById($arg){
		$this->addOrderBy("ss.id", $arg);
	}

	public function filterById($arg){
		$this->addWhere("ss.id = '" . (int)$arg. "' ");
	}
}
