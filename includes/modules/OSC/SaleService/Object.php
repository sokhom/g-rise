<?php

namespace OSC\SaleService;

use
	Aedea\Core\Database\StdObject as DbObj
	, OSC\SaleServiceDetail\Collection as  SaleServiceDetailCol
;

class Object extends DbObj {
		
	protected
		$name,
		$price,
		$cost,
		$description,
		$image,
		$detail,
		$barcode
	;
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		$this->detail = new SaleServiceDetailCol();
	}

	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'name',
				'price',
				'barcode',
				'detail',
				'cost',
				'status',
				'description',
				'image'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				name,
				price,
				barcode,
				cost,
				description,
				image,
				status
			FROM
				sale_service
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: sale_service not found",
				404
			);
		}
		
		$this->setProperties($this->dbFetchArray($q));

		$this->detail->setFilter('sale_service_id', $this->getId());
		$this->detail->populate();
	}
	
	public function insert(){
		$this->dbQuery("
			INSERT INTO
				sale_service
			(
				name,
				cost,
				barcode,
				price,
				description,
				image,
				status,
				create_by_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape( $this->getName() ) . "',
				'" . $this->getCost() . "',
				'" . $this->getBarcode() . "',
				'" . $this->getPrice() . "',
				'" . $this->dbEscape( $this->getDescription() ) . "',
				'" . $this->getImage() . "',
				1,
				'" . $this->getCreateById() . "',
				'" . $this->getCreateBy() . "',
				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function updateStatus(){
		$this->dbQuery("
			UPDATE
				sale_service
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . $this->getId() . "'
		");
	}

	public function update(){
		$this->dbQuery("
			UPDATE
				sale_service
			SET
                name = '" . $this->getName() . "',
				barcode = '" . $this->getBarcode() . "',
				cost = '" . $this->getCost() . "',
				price = '" . $this->getPrice() . "',
				description = '" . $this->getDescription() . "',
				image = '" . $this->getImage() . "',
                update_by = '" . $this->getUpdateBy() . "',
				update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . $this->getId() . "'
		");
	}

	public function setBarcode( $string ){
		$this->barcode = (string)$string;
	}
	
	public function getBarcode(){
		return $this->barcode;
	}

	public function setName( $string ){
		$this->name = (string)$string;
	}
	
	public function getName(){
		return $this->name;
	}

	public function setDescription( $string ){
		$this->description = (string)$string;
	}
	
	public function getDescription(){
		return $this->description;
	}

	public function setPrice( $string ){
		$this->price = doubleval($string);
	}
	
	public function getPrice(){
		return $this->price;
	}

	public function setCost( $string ){
		$this->cost = doubleval($string);
	}
	
	public function getCost(){
		return $this->cost;
	}

	public function setImage( $string ){
		$this->image = $string;
	}
	
	public function getImage(){
		return $this->image;
	}

	public function setDetail( $string ){
		$this->detail = $string;
	}
	
	public function getDetail(){
		return $this->detail;
	}
}
