<?php

namespace OSC\FeedBack;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$title
		, $description
		, $feedBackDate
		, $customerId
		, $customerName
		, $invoiceNo
		, $rate
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'title',
				'customer_id',
				'customer_name',
				'invoice_no',
				'feed_back_date',
				'description',
				'creator',
				'update_name',
				'create_date',
				'rate',
			)
		);

		return parent::toArray($args);
	}

	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				title,
				description,
				feed_back_date,
				customer_id,
				customer_name,
				invoice_no,
				create_by as creator,
				rate,
				update_by as update_name,
				create_date
			FROM
				feed_back
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Feed Back not found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update($id){
		if( !$id ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				feed_back
			SET
				title = '" . $this->getTitle() . "',
				rate = '" . $this->getRate() . "',
				customer_id = '" . $this->getCustomerId() . "',
				customer_name = '" . $this->getCustomerName() . "',
				invoice_no = '" . $this->getInvoiceNo() . "',
				feed_back_date = '" . $this->getFeedBackDate() . "',
				description = '" . $this->getDescription() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				feed_back
			WHERE
				id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				feed_back
			(
				title,
				description,
				feed_back_date,
				customer_id,
				rate,
				customer_name,
				invoice_no,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->dbEscape(  $this->getTitle() ). "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				'" . $this->getFeedBackDate() . "',
				'" . $this->getCustomerId() . "',
				'" . $this->getRate() . "',
				'" . $this->dbEscape(  $this->getCustomerName() ). "',
				'" . $this->dbEscape(  $this->getInvoiceNo() ). "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setTitle( $string ){
		$this->title = $string;
	}
	public function getTitle(){
		return $this->title;
	}

	public function setFeedBackDate( $string ){
		$this->feedBackDate = $string;
	}
	public function getFeedBackDate(){
		return $this->feedBackDate;
	}

	public function setRate( $string ){
		$this->rate = (int)$string;
	}
	public function getRate(){
		return $this->rate;
	}

	public function setCustomerId( $string ){
		$this->customerId = (int)$string;
	}
	public function getCustomerId(){
		return $this->customerId;
	}

	public function setCustomerName( $string ){
		$this->customerName = $string;
	}
	public function getCustomerName(){
		return $this->customerName;
	}

	public function setInvoiceNo( $string ){
		$this->invoiceNo = $string;
	}
	public function getInvoiceNo(){
		return $this->invoiceNo;
	}

}
