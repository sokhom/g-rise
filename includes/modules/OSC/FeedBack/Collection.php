<?php

namespace OSC\FeedBack;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('feed_back', 'fb');
		$this->idField = 'fb.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		$this->addWhere("fb.title LIKE '%" . $arg. "%' OR fb.customer_name LIKE '%" . $arg. "%' OR fb.invoice_no LIKE '%" . $arg. "%'");
	}

	
	public function filterById( $arg ){
		$this->addWhere("fb.id = '" . (int)$arg. "' ");
	}

	public function sortByName($arg){
		$this->addOrderBy('fb.title', $arg);
	}

	public function sortById($arg){
		$this->addOrderBy('fb.id', $arg);
	}

}
