<?php

namespace OSC\MemberShip;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('member_ship', 'ct');
		$this->idField = 'ct.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("ct.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("ct.name LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("ct.status = '" . (int)$arg. "' ");
	}

	public function sortByName($arg){
		$this->addOrderBy('ct.name', $arg);
	}
	
}
