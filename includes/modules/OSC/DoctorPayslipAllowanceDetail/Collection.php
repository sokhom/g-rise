<?php

namespace OSC\DoctorPayslipAllowanceDetail;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('doctor_payslip_allowance_detail', 'dp');
		$this->idField = 'dp.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterByName( $arg ){
		if($arg){
			$this->addWhere("dp.name LIKE '%" . $arg. "%' ");
		}
	}

	public function filterById( $arg ){
		if($arg){
			$this->addWhere("dp.id = '" . (int)$arg. "' ");
		}
	}

	public function filterByAllowanceId( $arg ){
		$this->addWhere("dp.allowance_id = '" . (int)$arg. "' ");
	}

	public function filterByDoctorPayslipDetailId( $arg ){
		$this->addWhere("dp.doctor_payslip_detail_id = '" . (int)$arg. "' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("dp.status = '" . (int)$arg. "' ");
	}

	public function sortById($arg){
		$this->addOrderBy('dp.id', $arg);
	}
}
