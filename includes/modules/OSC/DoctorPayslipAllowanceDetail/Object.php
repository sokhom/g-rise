<?php

namespace OSC\DoctorPayslipAllowanceDetail;

use
	Aedea\Core\Database\StdObject as DbObj
;

class Object extends DbObj {
		
	protected
		$doctorPayslipDetailId
		, $description
		, $allowanceId
		, $allowanceName
		, $amount
	;
	
	public function toArray( $params = array() ){
		$args = array(
			'include' => array(
				'id',
				'doctor_payslip_detail_id',
				'allowance_id',
				'allowance_name',
				'amount',
				'description',
				'status'
			)
		);

		return parent::toArray($args);
	}
	
	public function load( $params = array() ){
		$q = $this->dbQuery("
			SELECT
				doctor_payslip_detail_id,
				allowance_id,
				amount,
				description,
				(SELECT name FROM allowance_deduction WHERE id = d.allowance_id) as allowance_name,
				status
			FROM
				doctor_payslip_allowance_detail d
			WHERE
				id = '" . (int)$this->getId() . "'	
		");
		
		if( ! $this->dbNumRows($q) ){
			throw new \Exception(
				"404: Doctor Payslip Allowane Detail Not Found",
				404
			);
		}
		$this->setProperties($this->dbFetchArray($q));
	}

	public function update(){
		if( $this->getId() ) {
			$this->dbQuery("
				UPDATE
					doctor_payslip_allowance_detail
				SET
					allowance_id = '" . $this->getAllowanceId() . "',
					amount = '". $this->getAmount() ."',
					description = '" . $this->getDescription() . "',
					update_by = '" . $this->getUpdateBy() . "'
				WHERE
					id = '" . (int)$this->getId() . "'
			");
		}else{
			$this->dbQuery("
				INSERT INTO
					doctor_payslip_allowance_detail
				(
					doctor_payslip_detail_id,
					allowance_id,
					amount,
					description,
					status,
					create_by_id,
					create_by,
					create_date
				)
					VALUES
				(
					'" . $this->getDoctorPayslipDetailId() . "',
					'" . $this->getAllowanceId() . "',
					'" . $this->getAmount() . "',
					'" . $this->dbEscape(  $this->getDescription() ). "',
					1,
					'" . $this->getCreateById() . "',
					'" . $this->getCreateBy() . "',
					NOW()
				)
			");
			$this->setId( $this->dbInsertId() );
		}

	}

	public function updateStatus(){
		if( !$this->getId() ) {
			throw new Exception("save method requires id to be set");
		}
		$this->dbQuery("
			UPDATE
				doctor_payslip_allowance_detail
			SET
				status = '" . $this->getStatus() . "',
				update_by = '" . $this->getUpdateBy() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
	}

	public function delete($id){
		if( !$id ) {
			throw new Exception("delete method requires id to be set");
		}
		$this->dbQuery("
			DELETE FROM
				doctor_payslip_allowance_detail
			WHERE
				doctor_payslip_detail_id = '" . (int)$id . "'
		");
	}

	public function insert(){
		$this->dbQuery("
			INSERT INTO
				doctor_payslip_allowance_detail
			(
				doctor_payslip_detail_id,
				allowance_id,
				amount,
				description,
				status,
				create_by_id,
				create_by,
				create_date
			)
				VALUES
			(
				'" . $this->getDoctorPayslipDetailId() . "',
				'" . $this->getAllowanceId() . "',
				'" . $this->getAmount() . "',
				'" . $this->dbEscape(  $this->getDescription() ). "',
				1,
				'" . $this->getCreateById() . "',
				'" . $this->getCreateBy() . "',
 				NOW()
			)
		");
		$this->setId( $this->dbInsertId() );
	}

	public function setAllowanceName( $string ){
		$this->allowanceName = $string;
	}

	public function getAllowanceName(){
		return $this->allowanceName;
	}
	public function setDescription( $string ){
		$this->description = $string;
	}

	public function getDescription(){
		return $this->description;
	}

	public function setDoctorPayslipDetailId( $string ){
		$this->doctorPayslipDetailId = (int)$string;
	}
	
	public function getDoctorPayslipDetailId(){
		return $this->doctorPayslipDetailId;
	}

	public function setAmount( $string ){
		$this->amount = doubleval($string);
	}

	public function getAmount(){
		return $this->amount;
	}
	
	public function setAllowanceId( $string ){
		$this->allowanceId = (int)$string;
	}

	public function getAllowanceId(){
		return $this->allowanceId;
	}
}
