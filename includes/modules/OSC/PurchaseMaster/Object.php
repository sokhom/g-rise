<?php
namespace
    OSC\PurchaseMaster;
use
    Aedea\Core\Database\StdObject as DbObj,
    OSC\PurchaseDetail\Collection as PurchaseDetailCol
;

class Object extends DbObj{
     protected
        $supplierName,
        $supplierId,
        $purchaseDate,
        $purchaseNo,
        $reffNo,
        $paymentNext,
        $note,
        $total,
        $payment,
        $remain,
        $detail,
        $paymentMethod
     ;

    public function __construct( $params = array() ){
        parent::__construct($params);
        $this->detail = new PurchaseDetailCol();
    }

    public function toArray($params=array()){
        $args= array(
            'include'=>array(
                'id',
                'supplier_id',
                'supplier_name',
                'status',
                'payment_method',
                'purchase_date',
                'purchase_no',
                'reff_no',
                'note',
                'total',
                'payment',
                'payment_next',
                'detail',
                'remain'
            )
        );
        return parent::toArray($args);
    }
    public function load( $params = array() ){
        $q = $this->dbQuery("
			SELECT
                supplier_id,
                supplier_name,
                purchase_date,
                purchase_no,
                payment_method,
                reff_no,
                note,
                total,
                payment,
                payment_next,
                status,
                remain
			FROM
				purchase_master
			WHERE
				id = '" . (int)$this->getId() . "'
		");

        if( ! $this->dbNumRows($q) ){
            throw new \Exception(
                "404: Purchase Master not found",
                404
            );
        }
        $this->setProperties($this->dbFetchArray($q));
        $this->detail->setFilter('purchase_id', $this->getId());
        $this->detail->populate();
    }

    public function insert(){
        $this->dbQuery("
			INSERT INTO
				purchase_master
			(
				supplier_id,
                supplier_name,
                note,
                create_by,
                create_by_id,
                reff_no,
                total,
                payment,
                payment_method,
                purchase_date,
                purchase_no,
                remain,
                create_date,
                status
			)
				VALUES
			(
			    '" . $this->getSupplierId() . "',
				'" . $this->getSupplierName() . "',
				'" . $this->dbEscape($this->getNote()) . "',
				'" . $this->getCreateBy() . "',
                '" . $this->getCreateById() . "',
				'" . $this->dbEscape($this->getReffNo()) . "',
				'" . $this->getTotal() . "',
				'" . $this->getPayment() . "',
				'" . $this->getPaymentMethod() . "',
				'" . $this->getPurchaseDate() . "',
                '" . $this->getPurchaseNo() . "',
				'" . $this->getRemain() . "',
				NOW(),
				1
			)
		");
        $this->setId( $this->dbInsertId() );
    }

    // Functionality for update balance of vendor
    public function update(){
        // update payment if need
        //payment = payment + '" . $this->getPayment() . "',
        $this->dbQuery("
			UPDATE
				purchase_master
			SET
                update_by = '" . $this->getUpdateBy() . "',
                update_by_id = '" . $this->getUpdateById() . "',
                remain = remain - '" . $this->getPayment() . "'
			WHERE
				reff_no = '" . $this->getReffNo() . "'
		");
    }

    public function updateStatus(){
        $this->dbQuery("
			UPDATE
				purchase_master
			SET
                status = '" . (int)$this->getStatus() . "',
                update_by = '" . $this->getUpdateBy() . "',
                update_by_id = '" . $this->getUpdateById() . "'
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function delete(){
        if (!$this->getId()) {
            throw new Exception("delete method requires id to be set");
        }
        $this->dbQuery("
			DELETE FROM
				purchase_master
			WHERE
				id = '" . (int)$this->getId() . "'
		");
    }

    public function setSupplierId($string){
        $this->supplierId =(int)$string;
    }
    public function getSupplierId(){
        return $this->supplierId;
    }

    public function setDetail($string){
        $this->detail =(int)$string;
    }
    public function getDetail(){
        return $this->detail;
    }

    public function setPurchaseDate($date){
        $this->purchaseDate = date('Y-m-d', strtotime( $date ));
    }
    public function getPurchaseDate(){
        return $this->purchaseDate;
    }

    public function setPurchaseNo($string){
        $this->purchaseNo = $string;
    }
    public function getPurchaseNo(){
        return $this->purchaseNo;
    }

    public function setSupplierName($string){
        $this->supplierName = $string;
    }
    public function getSupplierName(){
        return $this->supplierName;
    }

    public function setReffNo($string){
        $this->reffNo = $string;
    }
    public function getReffNo(){
        return $this->reffNo;
    }

    public function setNote($string){
        $this->note = $string;
    }
    public function getNote(){
        return $this->note;
    }

    public function setTotal($string){
        $this->total = doubleval($string);
    }
    public function getTotal(){
        return $this->total;
    }

    public function setPayment($string){
        $this->payment = doubleval($string);
    }
    public function getPayment(){
        return $this->payment;
    }

    public function setPaymentNext($string){
        $this->paymentNext = doubleval($string);
    }
    public function getPaymentNext(){
        return $this->paymentNext;
    }

    public function setRemain($string){
//        if( $string < 0){
//            $string= 0;
//        }
        $this->remain = doubleval( $string );
    }
    public function getRemain(){
        return $this->remain;
    }

    public function setPaymentMethod($string){
        $this->paymentMethod = $string;
    }
    public function getPaymentMethod(){
        return $this->paymentMethod;
    }
}
