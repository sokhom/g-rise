<?php

namespace OSC\OptionType;

use Aedea\Core\Database\StdCollection;

class Collection extends StdCollection {
	
	public function __construct( $params = array() ){
		parent::__construct($params);
		
		$this->addTable('option_type', 'op');
		$this->idField = 'op.id';
		$this->setDistinct(true);
		
		$this->objectType = __NAMESPACE__ . '\Object';		
	}

	public function filterById( $arg ){
			$this->addWhere("op.id = '" . (int)$arg. "' ");
	}

	public function filterByName( $arg ){
		$this->addWhere("op.name LIKE '%" . $arg. "%' or op.description LIKE '%" . $arg. "%' ");
	}

	public function filterByStatus( $arg ){
		$this->addWhere("op.status = '" . (int)$arg. "' ");
	}

	public function sortByName( $arg ){
		$this->addOrderBy('op.name', $arg);
	}


}
