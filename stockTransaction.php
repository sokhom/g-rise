<?php
    require('includes/application_top.php');
    $where = '';
    // $whereFilter = '';
    if($_GET['from_date'] && $_GET['to_date']){
        $fromDate = $_GET['from_date'];//2016-09-15 00:00:00
        $endDate = $_GET['to_date'];//2016-09-17 23:59:59
        $where = " and st.transaction_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";

        // $whereInSubQuery = " AND st.stock_out_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";
    }
    // $filter = $_GET['filter'];
    // if($filter){
    //     $whereFilter = " AND st.product_name LIKE '%". $filter ."%'";
    // }
    // $filterKindOf = $_GET['kind_of'];
    // if($filterKindOf){
    //     $whereFilterKindOf = " AND st.product_kind_of = '". $filterKindOf ."'";

    //     $whereFilterKindOfInSubQuery = " AND product_kind_of = '". $filterKindOf ."'";
    // }
    // $query = tep_db_query("
    //     SELECT
    //       st.product_id,
    //       st.product_name,
    //       pt.name as product_type_name,
    //       st.product_kind_of,
    //       st.product_description,
    //       st.barcode,
    //       st.um_type,
    //       st.qty_on_hand,
    //       (SELECT cost FROM stock_transaction WHERE product_id = st.product_id AND status = 1  ORDER BY id DESC LIMIT 1) as cost,
    //       (SELECT whole_sale_price FROM stock_transaction WHERE product_id = st.product_id AND status = 1  ORDER BY id DESC LIMIT 1) as price,
    //       SUM(st.stock_in) as stock_in,
    //       SUM(st.stock_out) as stock_out
    //     FROM
    //       stock_transaction st 
    //         inner join 
    //             products_type pt on st.product_type_id = pt.id
    //     WHERE
    //       st.status = 1 and st.product_kind_of = 'item'
    //       " . $where . $whereFilter . $whereFilterKindOf ."
    //         GROUP BY
    //       st.product_id
    //     ORDER BY st.product_name ASC
    // ");

    $query = tep_db_query("
        SELECT
            st.product_id,
            p.products_name as product_name,
            pt.name as product_type_name,
            st.product_kind_of,
            st.product_description,
            st.barcode,
            st.um_type,
            st.qty_on_hand,
            p.products_price_in as cost,
            p.products_price_out_whole_sale as price,
            SUM(st.stock_in) as stock_in,
            SUM(st.stock_out) as stock_out
        FROM
            stock_transaction st 
                inner join 
                    products_type pt on st.product_type_id = pt.id
                inner join
                    products p on p.id = st.product_id
        WHERE
            st.status = 1 and st.product_kind_of = 'item'
            ".$where."
                GROUP BY
            st.product_id
        ORDER BY st.product_name ASC
    ");
    $array = [];
    while($stockTransaction = tep_db_fetch_array($query)){
        $array[] = array(
            "id" => (int)$stockTransaction['product_id'],
            "product_name" => $stockTransaction['product_name'],
            "product_description" => $stockTransaction['product_description'],
            "product_kind_of" => $stockTransaction['product_kind_of'],
            "product_type" => $stockTransaction['product_type_name'],
            'um_type' => $stockTransaction['um_type'],
            "barcode" => $stockTransaction['barcode'],
            "qty_on_hand" => doubleval($stockTransaction['qty_on_hand']),
            "cost" => doubleval($stockTransaction['cost']),
            "price" => doubleval($stockTransaction['price']),
            "stock_in" => doubleval($stockTransaction['stock_in']),
            "stock_out" => doubleval($stockTransaction['stock_out'])
        );
    }
    echo json_encode(
        array(
            'elements' => $array)
    );
