<?php
    require('includes/application_top.php');
    $where = '';
    $whereFilter = '';
    if($_GET['from_date'] && $_GET['to_date']){
        $fromDate = $_GET['from_date'] . " 00:00:00";//2016-09-15 00:00:00
        $endDate = $_GET['to_date']. ' 23:59:59';//2016-09-17 23:59:59
        $where = " AND st.create_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";

        $whereInSubQuery = " AND create_date BETWEEN '". $fromDate ."' AND '". $endDate ."'";
    }
    $filter = $_GET['filter'];
    if($filter){
        $whereFilter = " AND st.product_name LIKE '%". $filter ."%'";
    }
    // $filterKindOf = $_GET['kind_of'];
    // if($filterKindOf){
    //     $whereFilterKindOf = " AND st.product_kind_of = '". $filterKindOf ."'";

    //     $whereFilterKindOfInSubQuery = " AND product_kind_of = '". $filterKindOf ."'";
    // }
    $query = tep_db_query("
        SELECT
          st.product_id,
          st.product_name,
          pt.name as product_type_name,
          st.product_description,
          st.barcode,
          (SELECT cost FROM stock_transaction WHERE product_id = st.product_id AND status = 1  ORDER BY id DESC LIMIT 1) as price,
          SUM(st.stock_out) as stock_out
        FROM
          stock_transaction st inner join services_type pt on st.product_type_id = pt.id
        WHERE
          st.status = 1 and st.product_kind_of = 'service'
          " . $where . $whereFilter . $whereFilterKindOf ."
            GROUP BY
          st.product_id
        ORDER BY stock_out DESC, st.product_name DESC
    ");

    $array = [];
    while($stockTransaction = tep_db_fetch_array($query)){
        $array[] = array(
            "id" => (int)$stockTransaction['product_id'],
            "product_name" => $stockTransaction['product_name'],
            "product_description" => $stockTransaction['product_description'],
            
            "product_type" => $stockTransaction['product_type_name'],
            "barcode" => $stockTransaction['barcode'],
            
            "price" => doubleval($stockTransaction['price']),
            
            "stock_out" => doubleval($stockTransaction['stock_out'])
        );
    }
    echo json_encode(
        array(
            'elements' => $array)
    );
